<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpBuyerSellerChat
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpBuyerSellerChat\Controller\Adminhtml\Server;

class Start extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    protected $directoryList;

    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->directoryList = $directoryList;
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $response = new \Magento\Framework\DataObject();
        $response->setError(false);
        $data = $this->getRequest()->getParams();
        $response->setRoot($this->directoryList->getRoot());
        $rootPath = $this->directoryList->getRoot();
        // @codingStandardsIgnoreStart
        $node = exec('whereis node');
        // @codingStandardsIgnoreEnd
        
        $nodePath = explode(' ', $node);
        if (!isset($nodePath[1]) || $nodePath[1] == '') {
            // @codingStandardsIgnoreStart
            $node = exec('whereis nodejs');
            // @codingStandardsIgnoreEnd
            $nodePath = explode(' ', $node);
        }
        
        if (count($nodePath)) {
            // @codingStandardsIgnoreStart
            if (substr(php_uname(), 0, 7) == "Windows") {
                pclose(popen("start /B ". $nodePath[1].' '.$rootPath.'/server.js', "r"));
            } else {
                exec($nodePath[1].' '.$rootPath.'/server.js' . " > /dev/null &");
            }
            // @codingStandardsIgnoreEnd
            $response->setMessage(
                __('Server Running.')
            );
            $this->messageManager->addSuccess(__('Server has been started.'));
        } else {
            $response->setError(true);
            $response->addMessage(__('Nodejs Path not found.'));
            $this->messageManager->setError(__('Nodejs Path not found.'));
        }

        return $this->resultJsonFactory->create()->setJsonData($response->toJson());
    }
}
