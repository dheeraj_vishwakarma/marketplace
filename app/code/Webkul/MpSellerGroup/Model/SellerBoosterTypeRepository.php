<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType\CollectionFactory;

class SellerBoosterTypeRepository implements \Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface
{
    /**
     * @var SellerBoosterTypeFactory
     */
    protected $_sellerBoosterTypeFactory;

    /**
     * @var SellerBooster[]
     */
    protected $_instances = [];

    /**
     * @var SellerBooster[]
     */
    protected $_instancesByBoosterId = [];

    /**
     * @var SellerBooster[]
     */
    protected $_instancesBySellerId = [];

    /**
     * @var CollectionFactory
     */
    protected $_collectionTypeFactory;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType
     */
    protected $_resourceModel;

    /**
     * @param SellerBoosterTypeFactory $sellerBoosterTypeFactory
     * @param CollectionFactory $collectionTypeFactory
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType $resourceModel
     */
    public function __construct(
        SellerBoosterTypeFactory $sellerBoosterTypeFactory,
        CollectionFactory $collectionTypeFactory,
        \Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType $resourceModel
    ) {
        $this->_resourceModel = $resourceModel;
        $this->_sellerBoosterTypeFactory = $sellerBoosterTypeFactory;
        $this->_collectionTypeFactory = $collectionTypeFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface $sellerBooster)
    {
        $boosterTypeId = $sellerBooster->getId();
        try {
            $this->_resourceModel->save($sellerBooster);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save seller group'));
        }
        unset($this->_instances[$sellerBooster->getId()]);
        unset($this->_instancesByBoosterId[$sellerBooster->getBoosterId()]);
        unset($this->_instancesBySellerId[$sellerBooster->getSellerId()]);

        return $this->getById($sellerBooster->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function getBoosterType($boosterId = null)
    {
        $sellerBoosterData = $this->_sellerBoosterTypeFactory->create();
        $boosterTypeId = '';
        $sellerBoosterCollection = $this->_collectionTypeFactory->create()
                                    ->addFieldToFilter('booster_id', $boosterId);
        foreach ($sellerBoosterCollection as $value) {
            $boosterTypeId = $value->getId();
        }
        $sellerBoosterData->load($boosterTypeId);
        $this->_instances[$boosterTypeId] = $sellerBoosterData;
        $this->_instancesByBoosterId[$sellerBoosterData->getBoosterId()] = $sellerBoosterData;
        $this->_instancesBySellerId[$sellerBoosterData->getSellerId()] = $sellerBoosterData;

        return $this->_instances[$boosterTypeId];
    }

    /**
     * {@inheritdoc}
     */
    public function getById($boosterTypeId)
    {
        $sellerBoosterData = $this->_sellerBoosterTypeFactory->create();
        $sellerBoosterData->load($boosterTypeId);
        if (!$sellerBoosterData->getId()) {
            // seller booster does not exist
            return [];
        }
        $this->_instances[$boosterTypeId] = $sellerBoosterData;
        $this->_instancesByBoosterId[$sellerBoosterData->getBoosterId()] = $sellerBoosterData;
        $this->_instancesBySellerId[$sellerBoosterData->getSellerId()] = $sellerBoosterData;

        return $this->_instances[$boosterTypeId];
    }

    /**
     * {@inheritdoc}
     */
    public function getBySellerId($sellerId)
    {
        $sellerBoosterData = $this->_sellerBoosterTypeFactory->create();
        $boosterTypeId = '';
        $sellerBoosterCollection = $this->_collectionTypeFactory->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('expired_status', 0)
            ->setOrder('created_at', 'desc')
            ->setPageSize(1)
            ->setCurPage(1);
        foreach ($sellerBoosterCollection as $value) {
            $boosterTypeId = $value->getId();
        }
        $sellerBoosterData->load($boosterTypeId);
        $this->_instances[$boosterTypeId] = $sellerBoosterData;
        $this->_instancesByBoosterId[$sellerBoosterData->getBoosterId()] = $sellerBoosterData;
        $this->_instancesBySellerId[$sellerBoosterData->getSellerId()] = $sellerBoosterData;

        return $this->_instances[$boosterTypeId];
    }

    public function getCollectionBySellerId($sellerId, $onlyActive = true, $paymentState = null)
    {
        $sellerBoosterCollection = $this->_collectionTypeFactory->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->setOrder('created_at', 'desc');

        if ($onlyActive) {
            $sellerBoosterCollection->addFieldToFilter('expired_status', 0);
        }
        
        if ($paymentState) {
            $sellerBoosterCollection->addFieldToFilter('transaction_status', $paymentState);
        }

        return $sellerBoosterCollection;
    }

    /**
     * {@inheritdoc}
     */
    public function getBySellerCount($sellerId)
    {
        $sellerBoosterData = $this->_sellerBoosterTypeFactory->create();
        $boosterTypeId = '';
        $sellerBoosterCollection = $this->_collectionTypeFactory->create()
            ->addFieldToFilter('seller_id', $sellerId);

        return count($sellerBoosterCollection);
    }

    /**
     * {@inheritdoc}
     */
    public function getByOrderId($orderId)
    {
        $sellerBoosterData = $this->_sellerBoosterTypeFactory->create();
        $boosterTypeId = '';
        $sellerBoosterCollection = $this->_collectionTypeFactory->create()
                                    ->addFieldToFilter('order_id', $orderId);
        foreach ($sellerBoosterCollection as $value) {
            $boosterTypeId = $value->getId();
        }
        $sellerBoosterData->load($boosterTypeId);
        $this->_instances[$boosterTypeId] = $sellerBoosterData;
        $this->_instancesByBoosterId[$sellerBoosterData->getBoosterId()] = $sellerBoosterData;
        $this->_instancesBySellerId[$sellerBoosterData->getSellerId()] = $sellerBoosterData;

        return $this->_instances[$boosterTypeId];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface $sellerBooster)
    {
        $boosterId = $sellerBooster->getBoosterId();
        $sellerId = $sellerBooster->getSellerId();
        $boosterTypeId = $sellerBooster->getId();
        try {
            $this->_resourceModel->delete($sellerBooster);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove seller group record for id %1', $boosterTypeId)
            );
        }
        unset($this->_instances[$boosterTypeId]);
        unset($this->_instancesByBoosterId[$boosterId]);
        unset($this->_instancesBySellerId[$sellerId]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($boosterTypeId)
    {
        $sellerBooster = $this->getById($boosterTypeId);

        return $this->delete($sellerBooster);
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        /** @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType\Collection $collection */
        $collection = $this->_collectionTypeFactory->create();
        $collection->load();

        return $collection;
    }

    /**
     * Merge data from DB and updates from request.
     *
     * @param array $sellerBoosterDataArray
     * @param bool  $createNew
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface|SellerBoosterType
     *
     * @throws NoSuchEntityException
     */
    protected function initializeGroupData(array $sellerBoosterDataArray, $createNew)
    {
        if ($createNew) {
            $sellerBooster = $this->_sellerBoosterTypeFactory->create();
        } else {
            unset($this->_instances[$sellerBoosterDataArray['entity_id']]);
            $sellerBooster = $this->getBoosterType($sellerBoosterDataArray['entity_id']);
        }
        foreach ($sellerBoosterDataArray as $key => $value) {
            $sellerBooster->setData($key, $value);
        }

        return $sellerBooster;
    }
}
