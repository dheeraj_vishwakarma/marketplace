<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory;

class SellerGroupTypeRepository implements \Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface
{
    /**
     * @var SellerGroupTypeFactory
     */
    protected $_sellerGroupTypeFactory;

    /**
     * @var SellerGroup[]
     */
    protected $_instances = [];

    /**
     * @var SellerGroup[]
     */
    protected $_instancesByGroupId = [];

    /**
     * @var SellerGroup[]
     */
    protected $_instancesBySellerId = [];

    /**
     * @var CollectionFactory
     */
    protected $_collectionTypeFactory;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType
     */
    protected $_resourceModel;

    /**
     * @param SellerGroupTypeFactory                                    $sellerGroupTypeFactory
     * @param CollectionFactory                                         $collectionTypeFactory
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType $resourceModel
     */
    public function __construct(
        SellerGroupTypeFactory $sellerGroupTypeFactory,
        CollectionFactory $collectionTypeFactory,
        \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType $resourceModel
    ) {
        $this->_resourceModel = $resourceModel;
        $this->_sellerGroupTypeFactory = $sellerGroupTypeFactory;
        $this->_collectionTypeFactory = $collectionTypeFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface $sellerGroup)
    {
        $groupTypeId = $sellerGroup->getId();
        try {
            $this->_resourceModel->save($sellerGroup);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save seller group'));
        }
        unset($this->_instances[$sellerGroup->getId()]);
        unset($this->_instancesByGroupId[$sellerGroup->getGroupId()]);
        unset($this->_instancesBySellerId[$sellerGroup->getSellerId()]);

        return $this->getById($sellerGroup->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupType($groupId = null)
    {
        $sellerGroupData = $this->_sellerGroupTypeFactory->create();
        $groupTypeId = '';
        $sellerGroupCollection = $this->_collectionTypeFactory->create()
                                    ->addFieldToFilter('group_id', $groupId);
        foreach ($sellerGroupCollection as $value) {
            $groupTypeId = $value->getId();
        }
        $sellerGroupData->load($groupTypeId);
        $this->_instances[$groupTypeId] = $sellerGroupData;
        $this->_instancesByGroupId[$sellerGroupData->getGroupId()] = $sellerGroupData;
        $this->_instancesBySellerId[$sellerGroupData->getSellerId()] = $sellerGroupData;

        return $this->_instances[$groupTypeId];
    }

    /**
     * {@inheritdoc}
     */
    public function getById($groupTypeId)
    {
        $sellerGroupData = $this->_sellerGroupTypeFactory->create();
        $sellerGroupData->load($groupTypeId);
        if (!$sellerGroupData->getId()) {
            // seller group does not exist
            return [];
        }
        $this->_instances[$groupTypeId] = $sellerGroupData;
        $this->_instancesByGroupId[$sellerGroupData->getGroupId()] = $sellerGroupData;
        $this->_instancesBySellerId[$sellerGroupData->getSellerId()] = $sellerGroupData;

        return $this->_instances[$groupTypeId];
    }

    /**
     * {@inheritdoc}
     */
    public function getBySellerId($sellerId)
    {
        $sellerGroupData = $this->_sellerGroupTypeFactory->create();
        $groupTypeId = '';
        $sellerGroupCollection = $this->_collectionTypeFactory->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('expired_status', 0)
            ->setOrder('created_at', 'desc')
            ->setPageSize(1)
            ->setCurPage(1);
        foreach ($sellerGroupCollection as $value) {
            $groupTypeId = $value->getId();
        }
        $sellerGroupData->load($groupTypeId);
        $this->_instances[$groupTypeId] = $sellerGroupData;
        $this->_instancesByGroupId[$sellerGroupData->getGroupId()] = $sellerGroupData;
        $this->_instancesBySellerId[$sellerGroupData->getSellerId()] = $sellerGroupData;

        return $this->_instances[$groupTypeId];
    }

    /**
     * {@inheritdoc}
     */
    public function getBySellerCount($sellerId)
    {
        $sellerGroupData = $this->_sellerGroupTypeFactory->create();
        $groupTypeId = '';
        $sellerGroupCollection = $this->_collectionTypeFactory->create()
            ->addFieldToFilter('seller_id', $sellerId);

        return count($sellerGroupCollection);
    }

    /**
     * {@inheritdoc}
     */
    public function getByOrderId($orderId)
    {
        $sellerGroupData = $this->_sellerGroupTypeFactory->create();
        $groupTypeId = '';
        $sellerGroupCollection = $this->_collectionTypeFactory->create()
                                    ->addFieldToFilter('order_id', $orderId);
        foreach ($sellerGroupCollection as $value) {
            $groupTypeId = $value->getId();
        }
        $sellerGroupData->load($groupTypeId);
        $this->_instances[$groupTypeId] = $sellerGroupData;
        $this->_instancesByGroupId[$sellerGroupData->getGroupId()] = $sellerGroupData;
        $this->_instancesBySellerId[$sellerGroupData->getSellerId()] = $sellerGroupData;

        return $this->_instances[$groupTypeId];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface $sellerGroup)
    {
        $groupId = $sellerGroup->getGroupId();
        $sellerId = $sellerGroup->getSellerId();
        $groupTypeId = $sellerGroup->getId();
        try {
            $this->_resourceModel->delete($sellerGroup);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove seller group record for id %1', $groupTypeId)
            );
        }
        unset($this->_instances[$groupTypeId]);
        unset($this->_instancesByGroupId[$groupId]);
        unset($this->_instancesBySellerId[$sellerId]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($groupTypeId)
    {
        $sellerGroup = $this->getById($groupTypeId);

        return $this->delete($sellerGroup);
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        /** @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\Collection $collection */
        $collection = $this->_collectionTypeFactory->create();
        $collection->load();

        return $collection;
    }

    /**
     * Merge data from DB and updates from request.
     *
     * @param array $sellerGroupDataArray
     * @param bool  $createNew
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface|SellerGroupType
     *
     * @throws NoSuchEntityException
     */
    protected function initializeGroupData(array $sellerGroupDataArray, $createNew)
    {
        if ($createNew) {
            $sellerGroup = $this->_sellerGroupTypeFactory->create();
        } else {
            unset($this->_instances[$sellerGroupDataArray['entity_id']]);
            $sellerGroup = $this->getGroupType($sellerGroupDataArray['entity_id']);
        }
        foreach ($sellerGroupDataArray as $key => $value) {
            $sellerGroup->setData($key, $value);
        }

        return $sellerGroup;
    }
}
