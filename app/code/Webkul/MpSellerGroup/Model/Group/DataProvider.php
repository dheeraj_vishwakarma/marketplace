<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Group;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Session\SessionManagerInterface;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\Collection;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\CollectionFactory as SellerGroupCollectionFactory;

/**
 * Class DataProvider data provider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var SessionManagerInterface
     */
    protected $session;

    /**
     * Constructor.
     *
     * @param string                        $name
     * @param string                        $primaryFieldName
     * @param string                        $requestFieldName
     * @param SellerGroupCollectionFactory  $sellerGroupCollectionFactory
     * @param array                         $meta
     * @param array                         $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        SellerGroupCollectionFactory $sellerGroupCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $sellerGroupCollectionFactory->create();
        $this->collection->addFieldToSelect('*');
    }

    /**
     * Get session object.
     *
     * @return SessionManagerInterface
     */
    protected function getSession()
    {
        if ($this->session === null) {
            $this->session = ObjectManager::getInstance()->get(
                \Magento\Framework\Session\SessionManagerInterface::class
            );
        }

        return $this->session;
    }

    /**
     * Get data.
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var Seller Group $group */
        foreach ($items as $group) {
            $result['group'] = $group->getData();
            $this->loadedData[$group->getId()] = $result;
        }

        $data = $this->getSession()->getSellerGroupFormData();
        if (!empty($data)) {
            $groupId = isset($data['mpsellergroup_group']['entity_id'])
            ? $data['mpsellergroup_group']['entity_id'] : null;
            $this->loadedData[$groupId] = $data;
            $this->getSession()->unsSellerGroupFormData();
        }

        return $this->loadedData;
    }
}
