<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Group\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class AllowedModulesFunctions Allowed Modules Functions
 */
class AllowedModulesFunctions implements OptionSourceInterface
{
    /**
     * @var \Magento\Framework\Module\ModuleListInterface
     */
    protected $_moduleList;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_request;

    /**
     * @var \Webkul\Marketplace\Model\ControllersRepository
     */
    protected $_controllersRepository;

    /**
     * Constructor
     *
     * @param \Magento\Framework\Module\ModuleListInterface   $moduleList
     * @param \Magento\Framework\App\RequestInterface         $request
     * @param \Webkul\Marketplace\Model\ControllersRepository $controllersRepository
     */
    public function __construct(
        \Magento\Framework\Module\ModuleListInterface $moduleList,
        \Magento\Framework\App\RequestInterface $request,
        \Webkul\Marketplace\Model\ControllersRepository $controllersRepository,
        \Magento\Framework\DataObject $dataobject
    ) {
        $this->_moduleList = $moduleList;
        $this->_request = $request;
        $this->_controllersRepository = $controllersRepository;
        $this->dataobject = $dataobject;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $modules = $this->_moduleList->getNames();
        $dispatchResult = new \Magento\Framework\DataObject($modules);
        $modules = $dispatchResult->toArray();
        sort($modules);
        foreach ($modules as $moduleName) {
            if (strpos($moduleName, 'Webkul') !== false) {
                $controllersList = $this->_controllersRepository->getByModuleName($moduleName);
                foreach ($controllersList as $key => $value) {
                    $options[] = [
                        'label' => $value['label'],
                        'value' => $value['controller_path'],
                    ];
                }
            }
        }
        return $options;
    }
}
