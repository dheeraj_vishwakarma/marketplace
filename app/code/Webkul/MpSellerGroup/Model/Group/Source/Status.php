<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Group\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class Status group status
 */
class Status implements OptionSourceInterface
{
    /**
     * @var \Webkul\MpSellerGroup\Model\SellerGroup
     */
    protected $_marketplaceSellerGroup;

    /**
     * Constructor
     *
     * @param \Magento\Cms\Model\Page $cmsPage
     */
    public function __construct(\Webkul\MpSellerGroup\Model\SellerGroup $marketplaceSellerGroup)
    {
        $this->_marketplaceSellerGroup = $marketplaceSellerGroup;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->_marketplaceSellerGroup->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
