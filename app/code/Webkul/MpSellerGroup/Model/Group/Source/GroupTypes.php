<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

/**
 * Used in seller group type.
 */
namespace Webkul\MpSellerGroup\Model\Group\Source;

use Magento\Framework\Data\OptionSourceInterface;

class GroupTypes implements OptionSourceInterface
{
    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::MEMBERSHIP_TYPE_ONE_TIME,
                'label' => __('One Time Membership'),
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::MEMBERSHIP_TYPE_SUBSCRIPTION,
                'label' => __('Subscription Type Membership'),
            ]
        ];
    }
}
