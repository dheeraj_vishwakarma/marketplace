<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Group\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class CheckTypes check group types
 */
class CheckTypes implements OptionSourceInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::ONLY_NUMBER_OF_PRODUCTS,
                'label'=>__('Only Number Of Products')
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::ONLY_TIME,
                'label'=>__('Only Time')
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::TIME_AND_NUMBER_OF_PRODUCTS,
                'label'=>__('Time and Number Of Products')
            ],
        ];
    }
}
