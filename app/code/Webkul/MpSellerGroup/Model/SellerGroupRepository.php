<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\MpSellerGroup\Api\Data\SellerGroupInterface;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\CollectionFactory;

class SellerGroupRepository implements \Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface
{
    /**
     * @var SellerGroupFactory
     */
    protected $_sellerGroupFactory;

    /**
     * @var SellerGroup[]
     */
    protected $_instances = [];

    /**
     * @var SellerGroup[]
     */
    protected $_instancesById = [];

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup
     */
    protected $_resourceModel;

    /**
     * @param SellerGroupFactory                                    $sellerGroupFactory
     * @param CollectionFactory                                     $collectionFactory
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup $resourceModel
     */
    public function __construct(
        SellerGroupFactory $sellerGroupFactory,
        CollectionFactory $collectionFactory,
        \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup $resourceModel
    ) {
        $this->_resourceModel = $resourceModel;
        $this->_sellerGroupFactory = $sellerGroupFactory;
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(SellerGroupInterface $sellerGroup)
    {
        $sellerGroupId = $sellerGroup->getId();
        try {
            $this->_resourceModel->save($sellerGroup);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                // $e->getMessage()
                __(" ")
            );
        }
        unset($this->_instances[$sellerGroup->getGroupCode()]);
        unset($this->_instancesById[$sellerGroup->getId()]);

        return $this->getById($sellerGroup->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function getGroup($groupCode = null)
    {
        $sellerGroupData = $this->_sellerGroupFactory->create();
        $groupId = '';
        $sellerGroupCollection = $this->_collectionFactory->create()
                                    ->addFieldToFilter('group_code', $groupCode);
        foreach ($sellerGroupCollection as $value) {
            $groupId = $value->getId();
        }
        $sellerGroupData->load($groupId);
        $this->_instances[$groupCode] = $sellerGroupData;
        $this->_instancesById[$sellerGroupData->getId()] = $sellerGroupData;

        return $this->_instances[$groupCode];
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupByProductId($productID = null)
    {
        $sellerGroupData = $this->_sellerGroupFactory->create();
        $groupCode = '';
        $groupId = '';
        $sellerGroupCollection = $this->_collectionFactory->create()
            ->addFieldToFilter('product_id', $productID);
        foreach ($sellerGroupCollection as $value) {
            $groupId = $value->getId();
            $groupCode = $value->getGroupCode();
        }
        $sellerGroupData->load($groupId);
        $this->_instances[$groupCode] = $sellerGroupData;
        $this->_instancesById[$sellerGroupData->getId()] = $sellerGroupData;

        return $this->_instances[$groupCode];
    }

    /**
     * {@inheritdoc}
     */
    public function getById($groupId)
    {
        $sellerGroupData = $this->_sellerGroupFactory->create();
        $sellerGroupData->load($groupId);
        if (!$sellerGroupData->getId()) {
            // seller group does not exist
            return [];
        }
        $this->_instancesById[$groupId] = $sellerGroupData;
        $this->_instances[$sellerGroupData->getGroupCode()] = $sellerGroupData;

        return $this->_instancesById[$groupId];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(SellerGroupInterface $sellerGroup)
    {
        $groupCode = $sellerGroup->getGroupCode();
        $groupId = $sellerGroup->getId();
        try {
            $this->_resourceModel->delete($sellerGroup);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove seller group %1', $groupCode)
            );
        }
        unset($this->_instances[$groupCode]);
        unset($this->_instancesById[$groupId]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($groupId)
    {
        $sellerGroup = $this->getById($groupId);

        return $this->delete($sellerGroup);
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        /** @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\Collection $collection */
        $collection = $this->_collectionFactory->create();
        $collection->load();

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function getActiveList()
    {
        /** @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\Collection $collection */
        $collection = $this->_collectionFactory->create();
        $collection->addFieldToFilter('status', 1);
        $collection->setOrder('sort_order', 'ASC');
        $collection->load();

        return $collection;
    }

    /**
     * Merge data from DB and updates from request.
     *
     * @param array $sellerGroupDataArray
     * @param bool  $createNew
     *
     * @return SellerGroupInterface|SellerGroup
     *
     * @throws NoSuchEntityException
     */
    protected function initializeGroupData(array $sellerGroupDataArray, $createNew)
    {
        if ($createNew) {
            $sellerGroup = $this->_sellerGroupFactory->create();
        } else {
            unset($this->_instances[$sellerGroupDataArray['group_code']]);
            $sellerGroup = $this->getGroup($sellerGroupDataArray['group_code']);
        }
        foreach ($sellerGroupDataArray as $key => $value) {
            $sellerGroup->setData($key, $value);
        }

        return $sellerGroup;
    }
}
