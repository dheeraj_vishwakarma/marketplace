<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Model\AbstractModel;
use Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * SellerBoosterType Model.
 *
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType _getResource()
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType getResource()
 */
class SellerBoosterType extends AbstractModel implements SellerBoosterTypeInterface, IdentityInterface
{
    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**
     * MpSellerGroup SellerBoosterType cache tag.
     */
    const CACHE_TAG = 'marketplace_sellergroup_boostertype';

    /**
     * @var string
     */
    protected $_cacheTag = 'marketplace_sellergroup_boostertype';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'marketplace_sellergroup_boostertype';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType::class);
    }

    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteSellerGroupType();
        }

        return parent::load($id, $field);
    }

    /**
     * Load No-Route SellerBoosterType.
     *
     * @return \Webkul\MpSellerGroup\Model\SellerBoosterType
     */
    public function noRouteSellerGroupType()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Seller ID.
     *
     * @return int|null
     */
    public function getSellerId()
    {
        return parent::getData(self::SELLER_ID);
    }

    /**
     * Set Seller ID.
     *
     * @param int $sellerId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Get Booster Id.
     *
     * @return int|null
     */
    public function getBoosterId()
    {
        return parent::getData(self::BOOSTER_ID);
    }

    /**
     * Set Booster Id.
     *
     * @param int $boosterId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setBoosterId($boosterId)
    {
        return $this->setData(self::BOOSTER_ID, $boosterId);
    }

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount()
    {
        return parent::getData(self::FEE_AMOUNT);
    }

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setFeeAmount($feeAmount)
    {
        return $this->setData(self::FEE_AMOUNT, $feeAmount);
    }

    /**
     * Get Transaction Date.
     *
     * @return string|null
     */
    public function getTransactionDate()
    {
        return parent::getData(self::TRANSACTION_DATE);
    }
    
    /**
     * Set Transaction Date.
     *
     * @param string $transactionDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionDate($transactionDate)
    {
        return $this->setData(self::TRANSACTION_DATE, $transactionDate);
    }

    /**
     * Get Expired On Date.
     *
     * @return string|null
     */
    public function getExpiredOn()
    {
        return parent::getData(self::EXPIRED_ON);
    }

    /**
     * Set Expire Date.
     *
     * @param string $expireDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setExpiredOn($expireDate)
    {
        return $this->setData(self::EXPIRED_ON, $expireDate);
    }

    /**
     * Get Transaction Id.
     *
     * @return string|null
     */
    public function getTransactionId()
    {
        return parent::getData(self::TRANSACTION_ID);
    }

    /**
     * Set Transaction Id.
     *
     * @param string $transactionId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionId($transactionId)
    {
        return $this->setData(self::TRANSACTION_ID, $transactionId);
    }

    /**
     * Get Transaction Email.
     *
     * @return string|null
     */
    public function getTransactionEmail()
    {
        return parent::getData(self::TRANSACTION_EMAIL);
    }

    /**
     * Set Transaction Email.
     *
     * @param string $transactionEmail
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionEmail($transactionEmail)
    {
        return $this->setData(self::TRANSACTION_EMAIL, $transactionEmail);
    }

    /**
     * Get Transaction Status.
     *
     * @return int|null
     */
    public function getTransactionStatus()
    {
        return parent::getData(self::TRANSACTION_STATUS);
    }

    /**
     * Set Transaction Status.
     *
     * @param int $transactionStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionStatus($transactionStatus)
    {
        return $this->setData(self::TRANSACTION_STATUS, $transactionStatus);
    }

    /**
     * Get allowed_modules_functionalities.
     *
     * @return int|null
     */
    public function getAllowedModulesFunctionalities()
    {
        return parent::getData(self::ALLOWED_MODULES_FUNCTIONALITIES);
    }

    /**
     * Set allowed_modules_functionalities.
     *
     * @param int $allowedModulesFunctionalities
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setAllowedModulesFunctionalities($allowedModulesFunctionalities)
    {
        return $this->setData(self::ALLOWED_MODULES_FUNCTIONALITIES, $allowedModulesFunctionalities);
    }

    /**
     * Get Order Id.
     *
     * @return int|null
     */
    public function getOrderId()
    {
        return parent::getData(self::ORDER_ID);
    }

    /**
     * Set Order Id.
     *
     * @param int $orderId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Get Expired Status.
     *
     * @return int|null
     */
    public function getExpiredStatus()
    {
        return parent::getData(self::EXPIRED_STATUS);
    }

    /**
     * Set Expired Status.
     *
     * @param int $expiredStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setExpiredStatus($expiredStatus)
    {
        return $this->setData(self::EXPIRED_STATUS, $expiredStatus);
    }

    /**
     * Product created date.
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set product created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Product updated date.
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::UPDATED_AT);
    }

    /**
     * Set product updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
