<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Model\AbstractModel;
use Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * SellerGroupType Model.
 *
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType _getResource()
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType getResource()
 */
class SellerGroupType extends AbstractModel implements SellerGroupTypeInterface, IdentityInterface
{
    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**
     * MpSellerGroup SellerGroupType cache tag.
     */
    const CACHE_TAG = 'marketplace_sellergroup_type';

    /**
     * @var string
     */
    protected $_cacheTag = 'marketplace_sellergroup_type';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'marketplace_sellergroup_type';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType::class);
    }

    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteSellerGroupType();
        }

        return parent::load($id, $field);
    }

    /**
     * Load No-Route SellerGroupType.
     *
     * @return \Webkul\MpSellerGroup\Model\SellerGroupType
     */
    public function noRouteSellerGroupType()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Seller ID.
     *
     * @return int|null
     */
    public function getSellerId()
    {
        return parent::getData(self::SELLER_ID);
    }

    /**
     * Set Seller ID.
     *
     * @param int $sellerId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Get Group Id.
     *
     * @return int|null
     */
    public function getGroupId()
    {
        return parent::getData(self::GROUP_ID);
    }

    /**
     * Set Group Id.
     *
     * @param int $groupId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setGroupId($groupId)
    {
        return $this->setData(self::GROUP_ID, $groupId);
    }

    /**
     * Get Number of products.
     *
     * @return int|null
     */
    public function getNoOfProducts()
    {
        return parent::getData(self::NO_OF_PRODUCTS);
    }

    /**
     * Set Number of products.
     *
     * @param int $noOfProducts
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setNoOfProducts($noOfProducts)
    {
        return $this->setData(self::NO_OF_PRODUCTS, $noOfProducts);
    }

    /**
     * Get Time Periods.
     *
     * @return int|null
     */
    public function getTimePeriods()
    {
        return parent::getData(self::TIME_PERIODS);
    }

    /**
     * Set Time Periods.
     *
     * @param int $timePeriods
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTimePeriods($timePeriods)
    {
        return $this->setData(self::TIME_PERIODS, $timePeriods);
    }

    /**
     * Get Check Type.
     *
     * @return int|null
     */
    public function getCheckType()
    {
        return parent::getData(self::CHECK_TYPE);
    }

    /**
     * Set Check Type.
     *
     * @param int $checkType
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setCheckType($checkType)
    {
        return $this->setData(self::CHECK_TYPE, $checkType);
    }

    /**
     * Get Group Type.
     *
     * @return int|null
     */
    public function getGroupType()
    {
        return parent::getData(self::GROUP_TYPE);
    }

    /**
     * Set Group Type.
     *
     * @param int $groupType
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setGroupType($groupType)
    {
        return $this->setData(self::GROUP_TYPE, $groupType);
    }

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount()
    {
        return parent::getData(self::FEE_AMOUNT);
    }

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setFeeAmount($feeAmount)
    {
        return $this->setData(self::FEE_AMOUNT, $feeAmount);
    }

    /**
     * Get Failed Transactions.
     *
     * @return int|null
     */
    public function getFailedTransactions()
    {
        return parent::getData(self::FAILED_TRANSACTION);
    }

    /**
     * Set Failed Transactions.
     *
     * @param string $failedTransactions
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setFailedTransactions($failedTransactions)
    {
        return $this->setData(self::FAILED_TRANSACTION, $failedTransactions);
    }

    /**
     * Get Transaction Date.
     *
     * @return string|null
     */
    public function getTransactionDate()
    {
        return parent::getData(self::TRANSACTION_DATE);
    }
    
    /**
     * Set Transaction Date.
     *
     * @param string $transactionDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionDate($transactionDate)
    {
        return $this->setData(self::TRANSACTION_DATE, $transactionDate);
    }

    /**
     * Get Expired On Date.
     *
     * @return string|null
     */
    public function getExpiredOn()
    {
        return parent::getData(self::EXPIRED_ON);
    }

    /**
     * Set Expire Date.
     *
     * @param string $expireDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setExpiredOn($expireDate)
    {
        return $this->setData(self::EXPIRED_ON, $expireDate);
    }

    /**
     * Get Transaction Id.
     *
     * @return string|null
     */
    public function getTransactionId()
    {
        return parent::getData(self::TRANSACTION_ID);
    }

    /**
     * Set Transaction Id.
     *
     * @param string $transactionId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionId($transactionId)
    {
        return $this->setData(self::TRANSACTION_ID, $transactionId);
    }

    /**
     * Get ipn_track_id.
     *
     * @return string|null
     */
    public function getIpnTrackId()
    {
        return parent::getData(self::IPN_TRACK_ID);
    }

    /**
     * Set ipn_track_id.
     *
     * @param string $ipnTrackId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setIpnTrackId($ipnTrackId)
    {
        return $this->setData(self::IPN_TRACK_ID, $ipnTrackId);
    }

    /**
     * Get Transaction Email.
     *
     * @return string|null
     */
    public function getTransactionEmail()
    {
        return parent::getData(self::TRANSACTION_EMAIL);
    }

    /**
     * Set Transaction Email.
     *
     * @param string $transactionEmail
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionEmail($transactionEmail)
    {
        return $this->setData(self::TRANSACTION_EMAIL, $transactionEmail);
    }

    /**
     * Get Transaction Status.
     *
     * @return int|null
     */
    public function getTransactionStatus()
    {
        return parent::getData(self::TRANSACTION_STATUS);
    }

    /**
     * Set Transaction Status.
     *
     * @param int $transactionStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionStatus($transactionStatus)
    {
        return $this->setData(self::TRANSACTION_STATUS, $transactionStatus);
    }

    /**
     * Get allowed_modules_functionalities.
     *
     * @return int|null
     */
    public function getAllowedModulesFunctionalities()
    {
        return parent::getData(self::ALLOWED_MODULES_FUNCTIONALITIES);
    }

    /**
     * Set allowed_modules_functionalities.
     *
     * @param int $allowedModulesFunctionalities
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setAllowedModulesFunctionalities($allowedModulesFunctionalities)
    {
        return $this->setData(self::ALLOWED_MODULES_FUNCTIONALITIES, $allowedModulesFunctionalities);
    }

    /**
     * Get Order Id.
     *
     * @return int|null
     */
    public function getOrderId()
    {
        return parent::getData(self::ORDER_ID);
    }

    /**
     * Set Order Id.
     *
     * @param int $orderId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setOrderId($orderId)
    {
        return $this->setData(self::ORDER_ID, $orderId);
    }

    /**
     * Get Expired Status.
     *
     * @return int|null
     */
    public function getExpiredStatus()
    {
        return parent::getData(self::EXPIRED_STATUS);
    }

    /**
     * Set Expired Status.
     *
     * @param int $expiredStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setExpiredStatus($expiredStatus)
    {
        return $this->setData(self::EXPIRED_STATUS, $expiredStatus);
    }

    /**
     * Product created date.
     *
     * @return string|null
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set product created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Product updated date.
     *
     * @return string|null
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::UPDATED_AT);
    }

    /**
     * Set product updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }
}
