<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile\CollectionFactory;
use Webkul\MpSellerGroup\Model\RecurringProfile;

class RecurringProfileRepository implements \Webkul\MpSellerGroup\Api\RecurringProfileRepositoryInterface
{
    /**
     * @var RecurringProfileFactory
     */
    protected $_recurringProfileFactory;

    /**
     * @var SellerGroup[]
     */
    protected $_instances = [];

    /**
     * @var SellerGroup[]
     */
    protected $_instancesByGroupId = [];

    /**
     * @var SellerGroup[]
     */
    protected $_instancesBySellerId = [];

    /**
     * @var CollectionFactory
     */
    protected $_collectionTypeFactory;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile
     */
    protected $_resourceModel;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Subscription
     */
    protected $subscriptionHelper;

    /**
     * @param RecurringProfileFactory                                    $recurringProfileFactory
     * @param CollectionFactory                                         $collectionTypeFactory
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile $resourceModel
     * @param \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper
     */
    public function __construct(
        RecurringProfileFactory $recurringProfileFactory,
        CollectionFactory $collectionTypeFactory,
        \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile $resourceModel,
        \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper
    ) {
        $this->_resourceModel = $resourceModel;
        $this->_recurringProfileFactory = $recurringProfileFactory;
        $this->_collectionTypeFactory = $collectionTypeFactory;
        $this->subscriptionHelper = $subscriptionHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface $recurringProfile)
    {
        $profileId = $recurringProfile->getId();
        try {
            $this->_resourceModel->save($recurringProfile);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__('Unable to save recurring profile'));
        }
        unset($this->_instances[$recurringProfile->getId()]);
        unset($this->_instancesByGroupId[$recurringProfile->getGroupId()]);
        unset($this->_instancesBySellerId[$recurringProfile->getSellerId()]);

        return $this->getById($recurringProfile->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function getById($profileId)
    {
        $recurringProfileData = $this->_recurringProfileFactory->create();
        /* @var \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile\Collection $recurringProfileData */
        $recurringProfileData->load($profileId);
        if (!$recurringProfileData->getId()) {
            // seller group does not exist
            return [];
        }
        $this->_instances[$profileId] = $recurringProfileData;
        $this->_instancesByGroupId[$recurringProfileData->getGroupId()] = $recurringProfileData;
        $this->_instancesBySellerId[$recurringProfileData->getSellerId()] = $recurringProfileData;

        return $this->_instances[$profileId];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface $recurringProfile)
    {
        $groupId = $recurringProfile->getGroupId();
        $sellerId = $recurringProfile->getSellerId();
        $profileId = $recurringProfile->getId();
        try {
            $this->_resourceModel->delete($recurringProfile);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove recurring profile record for id %1', $profileId)
            );
        }
        unset($this->_instances[$profileId]);
        unset($this->_instancesByGroupId[$groupId]);
        unset($this->_instancesBySellerId[$sellerId]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($profileId)
    {
        $recurringProfile = $this->getById($profileId);

        return $this->delete($recurringProfile);
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        /** @var \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile\Collection $collection */
        $collection = $this->_collectionTypeFactory->create();
        $collection->load();

        return $collection;
    }

    /**
     * Cancel active or suspended profile
     */
    public function cancel($profileId)
    {
        $recurringProfile = $this->getById($profileId);
        $recurringProfile->checkWorkflow(RecurringProfile::STATE_CANCELLED, false);
        $result = $this->subscriptionHelper->cancelBillingAgreement($recurringProfile->getReferenceId());
        if ($result) {
            $recurringProfile->cancel();
        }
    }

    /**
     * Suspend active profile
     */
    public function suspend($profileId)
    {
        $recurringProfile = $this->getById($profileId);
        $recurringProfile->checkWorkflow(RecurringProfile::STATE_SUSPENDED, false);
        $result = $this->subscriptionHelper->suspendBillingAgreement($recurringProfile->getReferenceId());
        if ($result) {
            $recurringProfile->suspend();
        }
    }

    /**
     * Activate suspended profile
     */
    public function activate($profileId)
    {
        $recurringProfile = $this->getById($profileId);
        $recurringProfile->checkWorkflow(RecurringProfile::STATE_ACTIVE, false);
        $result = $this->subscriptionHelper->activateBillingAgreement($recurringProfile->getReferenceId());
        if ($result) {
            $recurringProfile->activate();
        }
    }

    public function fetchUpdate($profileId)
    {
        $recurringProfile = $this->getById($profileId);
        $result = $this->subscriptionHelper->getBillingAgreement($recurringProfile->getReferenceId());
        $isChanged = false;

        if ($result && !empty($result['state'])) {
            $state = strtolower($result['state']);
            $isChanged = $recurringProfile->fetchUpdate($state);
        }
        return $isChanged;
    }
}
