<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\StateException;

/**
 * RecurringProfile Model.
 *
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile _getResource()
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile getResource()
 */
class RecurringProfile extends \Magento\Catalog\Model\AbstractModel implements
    IdentityInterface,
    RecurringProfileInterface
{
    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**
     * Available states
     *
     * @var string
     */
    const STATE_UNKNOWN   = 'unknown';
    const STATE_PENDING   = 'pending';
    const STATE_ACTIVE    = 'active';
    const STATE_SUSPENDED = 'suspended';
    const STATE_CANCELLED  = 'cancelled';
    const STATE_EXPIRED   = 'expired';

    /**
     * MpSellerGroup RecurringProfile cache tag
     */
    const CACHE_TAG = 'marketplace_sellergroup_recurring_profile';

    /**
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * @var string
     */
    protected $_eventPrefix = 'marketplace_sellergroup_recurring_profile';

    /**
     * Allowed actions matrix
     *
     * @var array
     */
    protected $_workflow = null;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Subscription
     */
    protected $subscriptionHelper;

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile::class);
    }

    /**
     * Load object data
     *
     * @param  int|null $id
     * @param  string   $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteRecurringProfile();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route RecurringProfile
     *
     * @return \Webkul\MpSellerGroup\Model\RecurringProfile
     */
    public function noRouteRecurringProfile()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    public function canCancel()
    {
        return $this->checkWorkflow(self::STATE_CANCELLED);
    }

    /**
     * Check whether profile can be changed to specified state
     *
     * @param string $againstState
     * @param bool $soft
     * @return bool
     * @throws \Exception
     */
    public function checkWorkflow($againstState, $soft = true)
    {
        $this->_initWorkflow();
        $state = $this->getState();
        $result = (!empty($this->_workflow[$state])) && in_array($againstState, $this->_workflow[$state]);
        if (!$soft && !$result) {
            throw new CouldNotSaveException(__('This profile state cannot be changed to "%1".', $againstState));
        }
        return $result;
    }

    /**
     * Initialize the workflow reference
     */
    protected function _initWorkflow()
    {
        if (null === $this->_workflow) {
            $this->_workflow = [
                'unknown'   => ['pending', 'active', 'suspended', 'cancelled'],
                'pending'   => ['active', 'cancelled'],
                'active'    => ['suspended', 'cancelled'],
                'suspended' => ['active', 'cancelled'],
                'cancelled'  => [],
                'expired'   => [],
            ];
        }
    }

    /**
     * Check whether the workflow allows to suspend the profile
     *
     * @return bool
     */
    public function canSuspend()
    {
        return $this->checkWorkflow(self::STATE_SUSPENDED);
    }

    /**
     * Check whether the workflow allows to activate the profile
     *
     * @return bool
     */
    public function canActivate()
    {
        return $this->checkWorkflow(self::STATE_ACTIVE);
    }

    public function canFetchUpdate()
    {
        return true;
    }

    /**
     * Cancel active or suspended profile
     */
    public function cancel()
    {
        $this->setState(self::STATE_CANCELLED)->save();
    }

    /**
     * Suspend active profile
     */
    public function suspend()
    {
        $this->setState(self::STATE_SUSPENDED)->save();
    }

    /**
     * Activate the suspended profile
     */
    public function activate()
    {
        $this->setState(self::STATE_ACTIVE)->save();
    }

    /**
     * update state of a profile
     */
    public function fetchUpdate($state)
    {
        $isChanged = true;
        if ($state == self::STATE_ACTIVE) {
            $this->setState(self::STATE_ACTIVE)->save();
        } elseif ($state == self::STATE_PENDING) {
            $this->setState(self::STATE_PENDING)->save();
        } elseif ($state == self::STATE_CANCELLED) {
            $this->setState(self::STATE_CANCELLED)->save();
        } elseif ($state == self::STATE_SUSPENDED) {
            $this->setState(self::STATE_SUSPENDED)->save();
        } elseif ($state == self::STATE_EXPIRED) {
            $this->setState(self::STATE_EXPIRED)->save();
        } else {
            $isChanged = false;
        }
        return $isChanged;
    }
}
