<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Booster\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class ProfileStates Profile States
 */
class TransactionStatus implements OptionSourceInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => 'pending',
                'label'=>__('Pending')
            ],
            [
                'value' => 'paid',
                'label'=>__('Paid')
            ]
        ];
    }
}
