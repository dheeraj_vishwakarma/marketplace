<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Plugin;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session as CustomerSession;

class ValidateAllowedAction
{
    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $data;

    /**
     * @var \Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface
     */
    protected $sellerGroupRepo;

    /**
     * @var \Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface
     */
    protected $boosterTypeRepo;

    /**
     * @var \Webkul\Marketplace\Model\Product
     */
    protected $product;

    /**
     * @var \Webkul\Marketplace\Model\ControllersRepository
     */
    protected $controllerRepo;

    /**
     * @param Context $context
     * @param \Webkul\MpSellerGroup\Helper\Data $data
     * @param \Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface $sellerGroupRepo
     * @param \Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface $boosterTypeRepo
     * @param \Webkul\Marketplace\Model\Product $product
     * @param \Webkul\Marketplace\Model\ControllersRepository $controllerRepo
     * @param CustomerSession $customerSession
     */
    public function __construct(
        Context $context,
        \Webkul\MpSellerGroup\Helper\Data $data,
        \Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface $sellerGroupRepo,
        \Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface $boosterTypeRepo,
        \Webkul\Marketplace\Model\Product $product,
        \Webkul\Marketplace\Model\ControllersRepository $controllerRepo,
        CustomerSession $customerSession
    ) {
        $this->_customerSession = $customerSession;
        $this->sellerGroupRepo = $sellerGroupRepo;
        $this->boosterTypeRepo = $boosterTypeRepo;
        $this->product = $product;
        $this->data  = $data;
    }

    public function getCustomerId()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $marketplaceHelper = $objectManager->create(\Webkul\Marketplace\Helper\Data::class);
        $sellerId = $marketplaceHelper->getCustomerId();
        return $sellerId;
    }

    /**
     * @param  Webkul\Marketplace\Helper\Data $subject
     * @param  callable                       $proceed
     * @param  string                         $actionName
     * @return boolean
     */
    public function aroundIsAllowedAction(\Webkul\Marketplace\Helper\Data $subject, callable $proceed, $actionName = '')
    {
        $returnValue = $proceed($actionName);
        $sellerGroupHelper = $this->data;
        if (!$sellerGroupHelper->getStatus()) {
            return true;
        }
        $sellerId = $this->getCustomerId();
        $sellerGroupTypeRepository = $this->sellerGroupRepo;
        if (!$sellerGroupTypeRepository->getBySellerCount($sellerId)) {
            $products = $this->product->getCollection()
            ->addFieldToFilter('seller_id',$sellerId);
            $getDefaultGroupStatus = $sellerGroupHelper->getDefaultGroupStatus();
            if ($getDefaultGroupStatus) {
                $allowqty = $sellerGroupHelper->getDefaultProductAllowed();
                $allowFunctionalities = explode(',', $sellerGroupHelper->getDefaultAllowedFeatures());
                if ($allowqty >= $products->getSize()) {
                    if (in_array($actionName, $allowFunctionalities, true)) {
                        return true;
                    }
                }
            }
        }
        $getSellerGroup = $sellerGroupTypeRepository->getBySellerId($sellerId);
        $transactionStatus = $getSellerGroup['transaction_status'];
        if (!empty($getSellerGroup->getData()) && $transactionStatus != 'pending' && $transactionStatus != '') {
            $getSellerTypeGroup = $getSellerGroup;
            
            $allowedModuleFunctonalities = $getSellerTypeGroup['allowed_modules_functionalities'];
            $sellerBoosterTypes = $this->boosterTypeRepo->getCollectionBySellerId($sellerId, true, 'paid');
            if (!empty($sellerBoosterTypes->getData())) {
                $allowedModuleFunctonalities = $this->getAllFunctionalitiesData(
                    $allowedModuleFunctonalities,
                    $sellerBoosterTypes
                );
            }

            $allowedModuleArr = $this->getAllowedControllersBySetData(
                $allowedModuleFunctonalities
            );
            if (in_array($actionName, $allowedModuleArr, true)) {
                return true;
            } elseif ($actionName == 'marketplace/product/' && in_array(
                'marketplace/product/add',
                $allowedModuleArr,
                true
            )
                ) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get All Functionalities Data
     *
     * @param string $allowedModuleFunctonalities
     * @param object $sellerBoosterTypes
     * @return string
     */
    public function getAllFunctionalitiesData($allowedModuleFunctonalities, $sellerBoosterTypes)
    {
        $boosterFunctionalities = '';
        foreach ($sellerBoosterTypes as $boosterType) {
            $boosterFunctionalities = $boosterFunctionalities.",".$boosterType['allowed_modules_functionalities'];
        }
        $allowedModuleFunctonalities = $allowedModuleFunctonalities .$boosterFunctionalities;
        return $allowedModuleFunctonalities;
    }

    /**
     * @param  $allowedModule
     * @return array
     */
    public function getAllowedControllersBySetData($allowedModule)
    {
        $allowedModuleArr=[];
        if ($allowedModule && $allowedModule!='all') {

            $allowedModuleControllers = explode(',', $allowedModule);
            foreach ($allowedModuleControllers as $key => $value) {
                array_push($allowedModuleArr, $value);
            }
        } else {
            $controllersRepository = $this->controllerRepo;
            $controllersList = $controllersRepository->getList();
            foreach ($controllersList as $key => $value) {
                array_push($allowedModuleArr, $value['controller_path']);
            }
        }
        return array_unique($allowedModuleArr);
    }
}
