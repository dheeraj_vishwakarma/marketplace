<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster\CollectionFactory;

class SellerGroupBoosterRepository implements \Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface
{
    /**
     * @var SellerGroupBoosterFactory
     */
    protected $_sellerGroupBoosterFactory;

    /**
     * @var SellerGroupBooster[]
     */
    protected $_instances = [];

    /**
     * @var SellerGroupBooster[]
     */
    protected $_instancesById = [];

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster
     */
    protected $_resourceModel;

    /**
     * @param SellerGroupBoosterFactory $sellerGroupBoosterFactory
     * @param CollectionFactory $collectionFactory
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster $resourceModel
     */
    public function __construct(
        SellerGroupBoosterFactory $sellerGroupBoosterFactory,
        CollectionFactory $collectionFactory,
        \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster $resourceModel
    ) {
        $this->_resourceModel = $resourceModel;
        $this->_sellerGroupBoosterFactory = $sellerGroupBoosterFactory;
        $this->_collectionFactory = $collectionFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function save(SellerGroupBoosterInterface $booster)
    {
        $boosterId = $booster->getId();
        try {
            $this->_resourceModel->save($booster);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(
                // $e->getMessage()
                __(" ")
            );
        }
        unset($this->_instances[$booster->getBoosterCode()]);
        unset($this->_instancesById[$booster->getId()]);

        return $this->getById($booster->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function getBooster($boosterCode = null)
    {
        $boosterData = $this->_sellerGroupBoosterFactory->create();
        $boosterId = '';
        $boosterCollection = $this->_collectionFactory->create()
                                    ->addFieldToFilter('booster_code', $boosterCode);
        foreach ($boosterCollection as $value) {
            $boosterId = $value->getId();
        }
        $boosterData->load($boosterId);
        $this->_instances[$boosterCode] = $boosterData;
        $this->_instancesById[$boosterData->getId()] = $boosterData;

        return $this->_instances[$boosterCode];
    }

    /**
     * {@inheritdoc}
     */
    public function getBoosterByProductId($productId = null)
    {
        $boosterData = $this->_sellerGroupBoosterFactory->create();
        $boosterCode = '';
        $boosterId = '';
        $boosterCollection = $this->_collectionFactory->create()
            ->addFieldToFilter('product_id', $productId);
        foreach ($boosterCollection as $value) {
            $boosterId = $value->getId();
            $boosterCode = $value->getBoosterCode();
        }
        $boosterData->load($boosterId);
        $this->_instances[$boosterCode] = $boosterData;
        $this->_instancesById[$boosterData->getId()] = $boosterData;

        return $this->_instances[$boosterCode];
    }

    /**
     * {@inheritdoc}
     */
    public function getById($boosterId)
    {
        $boosterData = $this->_sellerGroupBoosterFactory->create();
        $boosterData->load($boosterId);
        if (!$boosterData->getId()) {
            // seller group booster does not exist
            return [];
        }
        $this->_instancesById[$boosterId] = $boosterData;
        $this->_instances[$boosterData->getBoosterCode()] = $boosterData;

        return $this->_instancesById[$boosterId];
    }

    /**
     * {@inheritdoc}
     */
    public function delete(SellerGroupBoosterInterface $booster)
    {
        $boosterCode = $booster->getBoosterCode();
        $boosterId = $booster->getId();
        try {
            $this->_resourceModel->delete($booster);
        } catch (\Exception $e) {
            throw new \Magento\Framework\Exception\StateException(
                __('Unable to remove seller group booster %1', $boosterCode)
            );
        }
        unset($this->_instances[$boosterCode]);
        unset($this->_instancesById[$boosterId]);

        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($boosterId)
    {
        $booster = $this->getById($boosterId);

        return $this->delete($booster);
    }

    /**
     * {@inheritdoc}
     */
    public function getList()
    {
        /** @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster\Collection $collection */
        $collection = $this->_collectionFactory->create();
        $collection->load();

        return $collection;
    }

    /**
     * {@inheritdoc}
     */
    public function getActiveList()
    {
        /** @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster\Collection $collection */
        $collection = $this->_collectionFactory->create();
        $collection->addFieldToFilter('status', 1);
        $collection->setOrder('sort_order', 'ASC');
        $collection->load();

        return $collection;
    }

    /**
     * Merge data from DB and updates from request.
     *
     * @param array $sellerBoosterDataArray
     * @param bool  $createNew
     *
     * @return SellerGroupBoosterInterface|SellerGroupBooster
     *
     * @throws NoSuchEntityException
     */
    protected function initializeBoosterData(array $sellerBoosterDataArray, $createNew)
    {
        if ($createNew) {
            $sellerBooster = $this->_sellerGroupBoosterFactory->create();
        } else {
            unset($this->_instances[$sellerBoosterDataArray['booster_code']]);
            $sellerBooster = $this->getBooster($sellerBoosterDataArray['booster_code']);
        }
        foreach ($sellerBoosterDataArray as $key => $value) {
            $sellerBooster->setData($key, $value);
        }

        return $sellerBooster;
    }
}
