<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Webkul\MpSellerGroup\Api\SellerGroupManagementInterface;
use Webkul\MpSellerGroup\Model\SellerGroup as Status;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\CollectionFactory;

class SellerGroupManagement implements SellerGroupManagementInterface
{
    /**
     * @var CollectionFactory
     */
    protected $_sellerGroupFactory;

    /**
     * @param CollectionFactory $sellerGroupFactory
     */
    public function __construct(CollectionFactory $sellerGroupFactory)
    {
        $this->_sellerGroupFactory = $sellerGroupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function getGroupCount($status = null)
    {
        $sellerGroupData = $this->_sellerGroupFactory->create();
        switch ($status) {
            case Status::STATUS_ENABLED:
                $sellerGroupData->addFieldToFilter('status', Status::STATUS_ENABLED);
                break;
            case Status::STATUS_DISABLED:
                $sellerGroupData->addFieldToFilter('status', Status::STATUS_DISABLED);
                break;
        }

        return $sellerGroupData->getSize();
    }
}
