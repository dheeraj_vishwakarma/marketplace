<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Group;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * MpSellerGroup Group Subscribe Controller Class
 */
class Subscribe extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Subscription
     */
    protected $subscriptionHelper;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\CollectionFactory
     */
    protected $sellerGroupCollection;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $helper;

    /**
     * @var ResultFactory
     */
    protected $result;

    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $loginUrl;

    /**
     * @param Context $context
     * @param Magento\Customer\Model\Session $customerSession
     * @param \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\CollectionFactory $sellerGroupCollection
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param ResultFactory $result
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper,
        \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup\CollectionFactory $sellerGroupCollection,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Magento\Customer\Model\Url $loginUrl,
        ResultFactory $result
    ) {
        $this->_customerSession = $customerSession;
        $this->subscriptionHelper = $subscriptionHelper;
        $this->sellerGroupCollection = $sellerGroupCollection;
        $this->_storeManager = $storeManager;
        $this->helper = $helper;
        $this->resultRedirect = $result;
        $this->loginUrl = $loginUrl;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->loginUrl->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        try {
            $params = $this->getRequest()->getParams();
            $requestParams = [];
            if (!empty($params['product'])) {
                $productId = $params['product'];
                $collection = $this->sellerGroupCollection->create()
                  ->addFieldToFilter('product_id', $productId);
                if ($collection->getSize()) {
                    foreach ($collection as $data) {
                        $requestParams = $this->preparePlanData($data);
                    }
                }
            }
            $accessToken = $this->subscriptionHelper->getAccessToken();
            $planId = $this->subscriptionHelper->createSubscriptionPlan($requestParams, $accessToken);
            if ($planId) {
                $state = $this->subscriptionHelper->activateSubscriptionPlan($planId, $accessToken);
                $planData = $this->subscriptionHelper->getSubscriptionPlan($planId, $accessToken);
                if ($state && $planData && !empty($planData['state']) && $planData['state']=="ACTIVE") {
                    $requestParams = $this->prepareAgreementRequestData($data, $planId);
                    $agreementData = $this->subscriptionHelper->createAnAgreement($requestParams, $accessToken);
                    return $this->setUrl($agreementData);
                }
            }
        } catch (\Exception $e) {
            $this->helper->logInfo("Controller_Group_Subscribe execute : ".$e->getMessage());
        }
        $this->messageManager->addWarning(__("Sorry! Admin has not added the payment details yet. Please try later!"));
        return $this->resultRedirectFactory->create()->setPath(
            'mpsellergroup/group/manage',
            ['_secure' => $this->getRequest()->isSecure()]
        );
    }

    public function prepareAgreementRequestData($data, $planId)
    {
        $params = [
            "name" => $data['schedule_description'],
            "description" => "Agreement with a regular "
                .$data['bill_frequency']
                . " "
                . $this->subscriptionHelper->getBillingPeriodUnit($data['bill_period_unit'])
                ." payment definition",
            "start_date" => $this->helper->getCurrentDate()."T".$this->helper->getCurrentTime()."Z",
            "plan" => [
                "id" => $planId,
            ],
            "payer" => [
                "payment_method" => "paypal",
            ]
        ];

        return $params;
    }

    private function preparePlanData($data)
    {
        $currency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        $params = [
            "name" => $data['schedule_description'],
            "description" => $data['schedule_description'],
            "type" => "fixed",
            "payment_definitions" => [
                [
                    "name" => "Regular payment definition",
                    "type" => "REGULAR",
                    "frequency" => $this->subscriptionHelper->getBillingPeriodUnit($data['bill_period_unit']),
                    "frequency_interval" => $data['bill_frequency'],
                    "amount" => [
                        "value" => $data['fee_amount'],
                        "currency" => $currency
                    ],
                    "cycles" => $data['max_bill_cycles'],
                ]
            ],
            "merchant_preferences" => [
                "setup_fee" => [
                    "value" => $data['initial_fee'],
                    "currency" => $this->_storeManager->getStore()->getCurrentCurrency()->getCode()
                ],
                "return_url" => $this->_url->getUrl(
                    'mpsellergroup/group/return',
                    ["group_id" => $data['entity_id'], 'seller_id' => $this->helper->getSellerId()]
                ),
                "cancel_url" => $this->_url->getUrl('mpsellergroup/group/cancel'),
                "auto_bill_amount" => $data['auto_bill'] == 1 ? "YES" : "NO",
                "initial_fail_amount_action" => $data['allow_initial_fee_failure'] == 1 ? "CONTINUE" : "CANCEL",
                "max_fail_attempts" => $data['max_payment_failure']
            ]
        ];

        return $params;
    }

    /**
     * @param $setUrl
     */

    protected function setUrl($agreementData)
    {
        if ($agreementData
          && !empty($agreementData)
          && !empty($agreementData['plan'])
          && !empty($agreementData['plan']['state'])
          && $agreementData['plan']['state']=="ACTIVE"
        ) {
            $links = $agreementData['links'];
            foreach ($links as $link) {
                if ($link['rel'] == "approval_url") {
                    $resultRedirect = $this->resultRedirect->create(ResultFactory::TYPE_REDIRECT);
                    $resultRedirect->setUrl($link['href']);
                    return $resultRedirect;
                }
            }
        }
    }
}
