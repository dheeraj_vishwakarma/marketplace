<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;

use Magento\Framework\View\Result\PageFactory;
use Magento\Backend\Model\View\Result\ForwardFactory;

use Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterfaceFactory;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;

use Magento\Catalog\Api\Data\ProductInterfaceFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\DataObject;

use Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory as AttributeSetCollection;
use Magento\Catalog\Model\Product\Attribute\ManagementFactory as UserDefineAttribute;

/**
 * Webkul MpSellerGroup Admin Booster Controller
 */
abstract class Booster extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var \Magento\Eav\Model\ResourceModel\Entity\Attribute\Set\CollectionFactory
     */
    protected $_attributeSetCollection;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $_resultPage;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @var SellerGroupBoosterInterfaceFactory
     */
    protected $sellerBoosterDataFactory;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $_sellerBoosterRepository;

    /**
     * @var ProductInterfaceFactory
     */
    protected $_productDataFactory;

    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $_mediaDirectory;

    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $_fileUploaderFactory;

    /**
     * Store manager
     *
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * Core registry
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    protected $_filesystemFile;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Webkul\Marketplace\Model\ControllersRepository
     */
    protected $_controllersRepository;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $_marketplaceHelper;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\ManagementFactory
     */
    protected $userDefineAttribute;

    /**
     * @var \Magento\Indexer\Console\Command\IndexerReindexCommand
     */
    protected $reindexCommand;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Eav\Model\Entity\Attribute\Set
     */
    protected $set;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ForwardFactory $resultForwardFactory
     * @param SellerGroupBoosterInterfaceFactory $sellerBoosterDataFactory
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param ProductInterfaceFactory $productDataFactory
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param Filesystem $filesystem
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\Filesystem\Io\File $filesystemFile
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Webkul\Marketplace\Model\ControllersRepository $controllersRepository
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelper
     * @param UserDefineAttribute $userDefineAttribute
     * @param AttributeSetCollection $attributeSetCollection
     * @param \Magento\Indexer\Console\Command\IndexerReindexCommand $reindexCommand
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param \Magento\Eav\Model\Entity\Attribute\Set $set
     * @param DataObject $dataObject
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ForwardFactory $resultForwardFactory,
        SellerGroupBoosterInterfaceFactory $sellerBoosterDataFactory,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        ProductInterfaceFactory $productDataFactory,
        ProductRepositoryInterface $productRepositoryInterface,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        Filesystem $filesystem,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Filesystem\Io\File $filesystemFile,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\Marketplace\Model\ControllersRepository $controllersRepository,
        \Webkul\Marketplace\Helper\Data $marketplaceHelper,
        UserDefineAttribute $userDefineAttribute,
        AttributeSetCollection $attributeSetCollection,
        \Magento\Indexer\Console\Command\IndexerReindexCommand $reindexCommand,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Magento\Eav\Model\Entity\Attribute\Set $set,
        DataObject $dataObject
    ) {
        parent::__construct($context);
        $this->userDefineAttribute = $userDefineAttribute;
        $this->_attributeSetCollection = $attributeSetCollection;
        $this->_resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->sellerBoosterDataFactory = $sellerBoosterDataFactory;
        $this->_sellerBoosterRepository = $sellerBoosterRepository;
        $this->_productDataFactory = $productDataFactory;
        $this->_productRepository = $productRepositoryInterface;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->_mediaDirectory = $filesystem->getDirectoryWrite(DirectoryList::MEDIA);
        $this->_fileUploaderFactory = $fileUploaderFactory;
        $this->storeManager = $storeManager;
        $this->_coreRegistry = $coreRegistry;
        $this->_filesystemFile = $filesystemFile;
        $this->_date = $date;
        $this->_controllersRepository = $controllersRepository;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->reindexCommand = $reindexCommand;
        $this->helper = $helper;
        $this->dataObject =$dataObject;
        $this->set = $set;
    }

    /**
     * Check for is allowed.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpSellerGroup::booster');
    }

    /**
     * get Attribute set id.
     *
     * @return integer
     */
    public function getAttrSetId($attrSetName)
    {
        $attributeSet = $this->_attributeSetCollection->create()
            ->addFieldToSelect('*')
            ->addFieldToFilter('attribute_set_name', $attrSetName);
        $attributeSetId = 0;
        foreach ($attributeSet as $attr):
            $attributeSetId = $attr->getAttributeSetId();
        endforeach;
        return $attributeSetId;
    }
    /**
     * get Attribute set id.
     *
     * @return integer
     */
    public function getUserDefineAttribute()
    {
        $customAttr = [];
        $id = $this->getAttrSetId("Mp Seller Group");
        $data = $this->userDefineAttribute->create()->getAttributes($id);
        foreach ($data as $key => $value) {
            if ($value['is_user_defined'] == 1) {
                $customAttr[] = $value->getData();
            }
        }
        return $customAttr;
    }
}
