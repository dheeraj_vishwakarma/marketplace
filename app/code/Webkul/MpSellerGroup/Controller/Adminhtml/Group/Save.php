<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Group;

use Webkul\MpSellerGroup\Model\SellerGroup;

class Save extends \Webkul\MpSellerGroup\Controller\Adminhtml\Group
{
    /**
     * Save seller group action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Validator\Exception|\Exception
     */
    public function execute()
    {
        $returnToEdit = false;
        $originalRequestData = $this->getRequest()->getPostValue();
        $sellerGroupId = isset($originalRequestData['mpsellergroup_group']['entity_id'])
            ? $originalRequestData['mpsellergroup_group']['entity_id']
            : null;
        if ($originalRequestData) {
            try {
                $sellerGroupData = $originalRequestData['mpsellergroup_group'];
                $sellerGroupData['group_image'] = $this->getSellerGroupImageName($sellerGroupData);
                $sellerGroupData['group_tmp_image'] = false;
                if (isset($originalRequestData['mpsellergroup_group']['group_image'][0]['tmp_name'])) {
                    $sellerGroupData['group_tmp_image'] = true;
                }
                $request = $this->getRequest();
                $isExistingSellerGroup = (bool) $sellerGroupId;
                $sellerGroup = $this->sellerGroupDataFactory->create();
                if ($isExistingSellerGroup) {
                    $sellerGroupData['entity_id'] = $sellerGroupId;
                }
                if (!$isExistingSellerGroup) {
                    $sellerGroupData['created_at'] = $this->_date->gmtDate();
                }
                $sellerGroupData['updated_at'] = $this->_date->gmtDate();
                $sellerGroupData = $this->allowedfunctionalites($sellerGroupData);

                $productId = $this->saveGroupProduct($sellerGroupData);

                $sellerGroupData['product_id'] = $productId;

                if ($sellerGroupData['subscription'] == SellerGroup::SUBSCRIPTION_DISABLE) {
                    $sellerGroupData['group_type'] = SellerGroup::MEMBERSHIP_TYPE_ONE_TIME;
                } elseif ($sellerGroupData['subscription'] == SellerGroup::SUBSCRIPTION_ENABLE) {
                    $sellerGroupData['group_type'] = SellerGroup::MEMBERSHIP_TYPE_SUBSCRIPTION;
                }

                $sellerGroup->setData($sellerGroupData);

                // Save group
                $sellerGroupId = $this->saveGroup($sellerGroup, $isExistingSellerGroup);

                $this->_getSession()->unsSellerGroupFormData();
                // Done Saving sellerGroup, finish save action
                $this->messageManager->addSuccess(__('You saved the seller group.'));
                $returnToEdit = (bool) $this->getRequest()->getParam('back', false);
            } catch (\Magento\Framework\Validator\Exception $exception) {
                $messages = $exception->getMessages();
                if (empty($messages)) {
                    $messages = $exception->getMessage();
                }
                $this->_addSessionErrorMessages($messages);
                $this->_getSession()->setSellerGroupFormData($originalRequestData);
                $returnToEdit = true;
            } catch (\Exception $exception) {
                $this->messageManager->addException(
                    $exception,
                    __('Something went wrong while saving the group. %1', $exception->getMessage())
                );
                $this->_getSession()->setSellerGroupFormData($originalRequestData);
                $returnToEdit = true;
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect = $this->setPath($returnToEdit, $sellerGroupId, $resultRedirect);

        return $resultRedirect;
    }

    /**
     * Check allowed modules functionalities
     */

    private function allowedfunctionalites($sellerGroupData)
    {
        if (isset($sellerGroupData['allowed_modules_functionalities'])) {
            $allowedModule = $sellerGroupData['allowed_modules_functionalities'];
            if ($allowedModule && $allowedModule!='all') {
                $sellerGroupData['allowed_modules_functionalities'] = implode(',', $allowedModule);
            } else {
                $sellerGroupData['allowed_modules_functionalities'] = 'all';
            }
        } else {
            $sellerGroupData['allowed_modules_functionalities'] = 'all';
        }
        return $sellerGroupData;
    }
    /**
     * Save seller group
     */
    protected function saveGroup($sellerGroup, $isExistingSellerGroup = null)
    {
        $sellerGroupId = "";
        if ($isExistingSellerGroup) {
            $sellerGroupId = $this->_sellerGroupRepository->save($sellerGroup);
            $sellerGroupId = $sellerGroup->getId();
        } else {
            $sellerGroup = $this->_sellerGroupRepository->save($sellerGroup);
            $sellerGroupId = $sellerGroup->getId();
        }
        return $sellerGroupId;
    }

    /**
     * Set path
     */

    protected function setPath($returnToEdit, $sellerGroupId, $resultRedirect)
    {
        if ($returnToEdit) {
            if (!empty($sellerGroupId)) {
                $resultRedirect->setPath(
                    'mpsellergroup/group/edit',
                    ['id' => $sellerGroupId, '_current' => true]
                );
            } else {
                $resultRedirect->setPath(
                    'mpsellergroup/group/new',
                    ['_current' => true]
                );
            }
        } else {
            $resultRedirect->setPath('mpsellergroup/group/index');
        }
        return $resultRedirect;
    }

    /**
     * Get seller group image name.
     *
     * @return string
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getSellerGroupImageName($sellerGroupData)
    {
        if (isset($sellerGroupData['group_image'][0]['name'])) {
            if (isset($sellerGroupData['group_image'][0]['name'])) {
                return $sellerGroupData['group_image'] = $sellerGroupData['group_image'][0]['name'];
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Please upload group image.')
                );
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Please upload group image.')
            );
        }
    }

    /**
     * @param Array $data
     * @return int
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function saveGroupProduct($data)
    {
        $customDefineAttribute = $this->getUserDefineAttribute();
        $content = [];
        foreach ($customDefineAttribute as $key => $value) {
            $content['not_attributes'][] = $value['entity_attribute_id'];
        }
        if (!$customDefineAttribute) {
            $content['not_attributes'] = 0;
        }

        $content['attribute_set_name'] = 'Mp Seller Group';
        $content['attributes'] = 0;
        $content['groups'] = 0;
        $content['removeGroups'] = 0;

        $model = $this->set
            ->setEntityTypeId($this->getAttrSetId("Mp Seller Group"));
        $model->load($this->getAttrSetId("Mp Seller Group"));
        $model->organizeData($content);
        $model->save();
        $productId = '';
        try {
            $catalogProduct = $this->_productDataFactory->create();
            if (empty($data['product_id'])) {
                $productData['name'] = $data['group_name'];
                $productData['visibility'] = 1;
                if ($this->getAttrSetId("Mp Seller Group")) {
                    $productData['attribute_set_id'] = $this->getAttrSetId("Mp Seller Group");
                }
                $productData['type_id'] = "virtual";
                $productData['price'] = $data['fee_amount'];
                $productData['tax_class_id'] = 0;
                $productData['sku'] = $this->randString(10);
                $catalogProduct->setData($productData);
                $websites = [];
                foreach ($this->_marketplaceHelper->getAllWebsites() as $website) {
                    $websites[] = $website->getId();
                }
                $catalogProduct->setWebsiteIds($websites);
                $catalogProduct->setStockData(
                    [
                        'use_config_manage_stock' => 0,
                        'is_in_stock' => 1,
                        'qty' => 9999,
                        'manage_stock' => 0,
                        'use_config_notify_stock_qty' => 0
                    ]
                );

                $product = $this->_productRepository->save($catalogProduct);

                $productId = $product->getId();

                $catalogProduct = $this->_productRepository->getById($productId);

                $baseTmpPath = 'mpsellergroup/groupimage/';
                $target = $this->_mediaDirectory->getAbsolutePath($baseTmpPath);
                $imgPath = $target.$data['group_image'];
                $catalogProduct->addImageToMediaGallery(
                    $imgPath,
                    ['image','small_image','thumbnail'],
                    false,
                    false
                );
                $catalogProduct->save();
                $this->reindexCommand->run(
                    new \Symfony\Component\Console\Input\StringInput('catalog_product_attribute'),
                    new \Symfony\Component\Console\Output\ConsoleOutput()
                );
            } else {
                $productData['entity_id'] = $data['product_id'];
                $productId = $data['product_id'];
                $catalogProduct = $this->_productRepository->getById(
                    $data['product_id'],
                    true,
                    0
                );
                $catalogProduct->setName($data['group_name']);
                $catalogProduct->setPrice($data['fee_amount']);
                if ($data['group_tmp_image']) {
                    $baseTmpPath = 'mpsellergroup/groupimage/';
                    $target = $this->_mediaDirectory->getAbsolutePath($baseTmpPath);
                    $imgPath = $target.$data['group_image'];
                    $catalogProduct->addImageToMediaGallery(
                        $imgPath,
                        ['image','small_image','thumbnail'],
                        false,
                        false
                    );
                }
                $catalogProduct->save();
                $this->reindexCommand->run(
                    new \Symfony\Component\Console\Input\StringInput('catalog_product_attribute'),
                    new \Symfony\Component\Console\Output\ConsoleOutput()
                );
            }
        } catch (\Exception $exception) {
            $this->messageManager->addException(
                $exception,
                $exception->getMessage()
            );
        }

        return $productId;
    }
    /**
     * @param int $length
     * @param string $charset = 'abcdefghijklmnopqrstuvwxyz0123456789'
     * @return string
     */
    public function randString(
        $length,
        $charset = 'abcdefghijklmnopqrstuvwxyz0123456789'
    ) {
        $str = 'group-';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[random_int(0, $count - 1)];
        }

        return $str;
    }
}
