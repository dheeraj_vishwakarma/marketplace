<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Group;

use Webkul\MpSellerGroup\Api\Data\SellerGroupInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Edit extends \Webkul\MpSellerGroup\Controller\Adminhtml\Group
{
    /**
     * seller group edit action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $sellerGroupId = $this->initCurrentSellerGroup();
        $isExistingSellerGroup = (bool)$sellerGroupId;
        if ($isExistingSellerGroup) {
            try {
                $sellerGroupDirPath = $this->_mediaDirectory->getAbsolutePath(
                    'mpsellergroup'
                );
                $sellerGroupImageDirPath = $this->_mediaDirectory->getAbsolutePath(
                    'mpsellergroup/groupimage'
                );
                if (!$this->_filesystemFile->fileExists($sellerGroupDirPath)) {
                    $this->_filesystemFile->mkdir($sellerGroupDirPath, 0777, true);
                }
                if (!$this->_filesystemFile->fileExists($sellerGroupImageDirPath)) {
                    $this->_filesystemFile->mkdir($sellerGroupImageDirPath, 0777, true);
                }
                $group_imageBaseTmpPath = 'mpsellergroup/groupimage/';
                $group_imageTarget = $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ).$group_imageBaseTmpPath;

                $sellerGroupData = [];
                $sellerGroupData['mpsellergroup_group'] = [];
                $sellerGroup = null;
                $sellerGroup = $this->_sellerGroupRepository->getById(
                    $sellerGroupId
                );
                $result = $sellerGroup->getData();
                if (!empty($result)) {
                    $sellerGroupData['mpsellergroup_group'] = $result;

                    /* *
                     * for group_image image
                     */
                    $sellerGroupData['mpsellergroup_group']['group_image'] = [];
                    $sellerGroupData['mpsellergroup_group']['group_image'][0] = [];
                    $sellerGroupData['mpsellergroup_group']['group_image'][0]['name'] =
                    $result['group_image'];
                    $sellerGroupData['mpsellergroup_group']['group_image'][0]['id'] =
                    $result['group_image'];
                    $sellerGroupData['mpsellergroup_group']['group_image'][0]['url'] =
                    $group_imageTarget.$result['group_image'];
                    $group_imageFilePath = $this->_mediaDirectory->getAbsolutePath(
                        $group_imageBaseTmpPath
                    ).$result['group_image'];

                    if ($this->_filesystemFile->fileExists($group_imageFilePath)) {
                        $sellerGroupData['mpsellergroup_group']['group_image'][0]['size'] =
                        $this->getFileSize($group_imageFilePath);
                    } else {
                        $sellerGroupData['mpsellergroup_group']['group_image'][0]['size'] = 0;
                    }

                    $sellerGroupData['mpsellergroup_group'][SellerGroupInterface::ENTITY_ID]=
                    $sellerGroupId;

                    $allowedModule =
                    $sellerGroupData['mpsellergroup_group']['allowed_modules_functionalities'];
                    if ($allowedModule && $allowedModule!='all') {
                        $sellerGroupData['mpsellergroup_group']['allowed_modules_functionalities']=
                        explode(',', $allowedModule);
                    } else {
                        $allowedModuleArr=[];
                        $controllersList = $this->_controllersRepository->getList();
                        foreach ($controllersList as $key => $value) {
                            array_push($allowedModuleArr, $value['controller_path']);
                        }
                        $sellerGroupData['mpsellergroup_group']['allowed_modules_functionalities']=
                        $allowedModuleArr;
                    }
                    $this->_getSession()->setSellerGroupFormData($sellerGroupData);
                    $this->_coreRegistry->register(
                        'sellerGroupData',
                        $sellerGroupData
                    );
                } else {
                    $this->messageManager->addError(
                        __('Requested group doesn\'t exist')
                    );
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath('mpsellergroup/group/index');
                    return $resultRedirect;
                }
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while editing the seller group.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('mpsellergroup/group/index');
                return $resultRedirect;
            }
        }

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Webkul_MpSellerGroup::group');
        $this->prepareDefaultGroupTitle($resultPage);
        $resultPage->setActiveMenu('Webkul_MpSellerGroup::group');
        if ($isExistingSellerGroup) {
            $resultPage->getConfig()->getTitle()->prepend(
                __('Edit Group with id %1', $sellerGroupId)
            );
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Seller Group'));
        }
        return $resultPage;
    }

    /**
     * seller group initialization
     *
     * @return string seller group id
     */
    protected function initCurrentSellerGroup()
    {
        $sellerGroupId = (int)$this->getRequest()->getParam('id');

        if ($sellerGroupId) {
            $this->_coreRegistry->register(
                'mpsellergroup_group',
                $sellerGroupId
            );
        }

        return $sellerGroupId;
    }

    /**
     * Prepare seller group default title
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return void
     */
    protected function prepareDefaultGroupTitle(
        \Magento\Backend\Model\View\Result\Page $resultPage
    ) {
        $resultPage->getConfig()->getTitle()->prepend(__('Seller Group'));
    }

    public function getFileSize($file)
    {
        $fileSize = $this->_mediaDirectory->stat($file)['size'];
        return $fileSize;
    }
}
