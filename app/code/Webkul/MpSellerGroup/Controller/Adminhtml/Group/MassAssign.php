<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Group;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;

/**
 * Class Group MassAssign.
 */
class MassAssign extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    protected $_filter;

    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * @var sellerGroupRepository
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeFactory
     */
    protected $_sellerGroupTypeFactory;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @param Context                                     $context
     * @param Filter                                      $filter
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        Context $context,
        Filter $filter,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        CollectionFactory $collectionFactory,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        \Webkul\MpSellerGroup\Model\SellerGroupTypeFactory $sellerGroupTypeFactory,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
    ) {
        $this->_filter = $filter;
        $this->_collectionFactory = $collectionFactory;
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeFactory = $sellerGroupTypeFactory;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_date = $date;
        parent::__construct($context);
    }

    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->_filter->getCollection($this->_collectionFactory->create());
        foreach ($collection as $item) {
            $sellerId = $item->getSellerId();
            $getGroupByCode = $this->_sellerGroupRepository->getGroup(
                $this->getRequest()->getParam('group_code')
            );
            $data = [];
            $existingGroupId = '';
            if ($getGroupByCode->getId()) {
                $existingGroupId = $getGroupByCode->getId();
                $data['seller_id'] = $sellerId;
                $data['group_id'] = $existingGroupId;
                $data['no_of_products'] = $getGroupByCode->getNoOfProducts();
                $data['time_periods'] = $getGroupByCode->getTimePeriods();
                $data['fee_amount'] = $getGroupByCode->getFeeAmount();
                $data['check_type'] = $getGroupByCode->getCheckType();
                $today = $this->_date->gmtDate();
                $expiredOn = strtotime('+'.$getGroupByCode->getTimePeriods().' days', strtotime($today));
                $data['expired_on'] = date('Y-m-d H:i:s', $expiredOn);
                $data['transaction_date'] = $today;
                $data['transaction_status'] = 'Paid';
                $data['allowed_modules_functionalities'] = $getGroupByCode->getAllowedModulesFunctionalities();
                $data['created_at'] = $today;
                $data['updated_at'] = $today;
            }
            if ($existingGroupId) {
                $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($sellerId);
                $existingGroupTypeId = '';
                if ($getSellerGroup->getId()) {
                    $existingGroupTypeId = $getSellerGroup->getId();
                }
                $sellerGroup = $this->_sellerGroupTypeFactory->create();
                if ($getSellerGroup->getGroupId() == $existingGroupId) {
                    unset($data['transaction_date']);
                    unset($data['created_at']);
                    unset($data['expired_on']);
                    $this->loadSellerGroup($sellerGroup, $existingGroupTypeId);
                    $sellerGroup->setUpdatedAt($this->_date->gmtDate());
                }
                $sellerGroup = $this->saveSellerData($sellerGroup, $data);
                $existingGroupTypeId = $sellerGroup->getId();
            } else {
                $this->messageManager
                ->addError(
                    __(
                        'Group with %1 group code does not exist.',
                        $this->getRequest()->getParam('group_code')
                    )
                );
            }
        }
        $this->messageManager->addSuccess(
            __(
                'A total of %1 seller(s) have been updated.',
                $collection->getSize()
            )
        );

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('marketplace/seller/index');
    }

    /**
     * Load seller Group
     */

    public function loadSellerGroup($sellerGroup, $existingGroupTypeId)
    {
        return $sellerGroup->load($existingGroupTypeId);
    }

    /**
     * Save Seller Data
     */

    public function saveSellerData($sellerGroup, $data)
    {
        return $sellerGroup->addData($data)->save();
    }
    /**
     * Check for is allowed.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpSellerGroup::group');
    }
}
