<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Booster;

use Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class Edit extends \Webkul\MpSellerGroup\Controller\Adminhtml\Booster
{
    /**
     * seller group edit action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $sellerBoosterId = $this->initCurrentSellerBooster();
        $isExistingSellerBooster = (bool)$sellerBoosterId;
        if ($isExistingSellerBooster) {
            try {
                $sellerGroupDirPath = $this->_mediaDirectory->getAbsolutePath(
                    'mpsellergroup'
                );
                $sellerBoosterImageDirPath = $this->_mediaDirectory->getAbsolutePath(
                    'mpsellergroup/boosterimage'
                );
                if (!$this->_filesystemFile->fileExists($sellerGroupDirPath)) {
                    $this->_filesystemFile->mkdir($sellerGroupDirPath, 0777, true);
                }
                if (!$this->_filesystemFile->fileExists($sellerBoosterImageDirPath)) {
                    $this->_filesystemFile->mkdir($sellerBoosterImageDirPath, 0777, true);
                }
                $booster_imageBaseTmpPath = 'mpsellergroup/boosterimage/';
                $booster_imageTarget = $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ).$booster_imageBaseTmpPath;

                $sellerBoosterData = [];
                $sellerBoosterData['mpsellergroup_booster'] = [];
                $sellerBooster = null;
                $sellerBooster = $this->_sellerBoosterRepository->getById(
                    $sellerBoosterId
                );
                $result = $sellerBooster->getData();
                if (!empty($result)) {
                    $sellerBoosterData['mpsellergroup_booster'] = $result;

                    /* *
                     * for booster_image image
                     */
                    $sellerBoosterData['mpsellergroup_booster']['booster_image'] = [];
                    $sellerBoosterData['mpsellergroup_booster']['booster_image'][0] = [];
                    $sellerBoosterData['mpsellergroup_booster']['booster_image'][0]['name'] =
                    $result['booster_image'];
                    $sellerBoosterData['mpsellergroup_booster']['booster_image'][0]['id'] =
                    $result['booster_image'];
                    $sellerBoosterData['mpsellergroup_booster']['booster_image'][0]['url'] =
                    $booster_imageTarget.$result['booster_image'];
                    $group_imageFilePath = $this->_mediaDirectory->getAbsolutePath(
                        $booster_imageBaseTmpPath
                    ).$result['booster_image'];

                    if ($this->_filesystemFile->fileExists($group_imageFilePath)) {
                        $sellerBoosterData['mpsellergroup_booster']['booster_image'][0]['size'] =
                        $this->getFileSize($group_imageFilePath);
                    } else {
                        $sellerBoosterData['mpsellergroup_booster']['booster_image'][0]['size'] = 0;
                    }

                    $sellerBoosterData['mpsellergroup_booster'][SellerGroupBoosterInterface::ENTITY_ID]=
                    $sellerBoosterId;

                    $allowedModule =
                    $sellerBoosterData['mpsellergroup_booster']['allowed_modules_functionalities'];
                    if ($allowedModule && $allowedModule != 'all') {
                        $sellerBoosterData['mpsellergroup_booster']['allowed_modules_functionalities'] =
                        explode(',', $allowedModule);
                    } else {
                        $allowedModuleArr = [];
                        $controllersList = $this->_controllersRepository->getList();
                        foreach ($controllersList as $key => $value) {
                            array_push($allowedModuleArr, $value['controller_path']);
                        }
                        $sellerBoosterData['mpsellergroup_booster']['allowed_modules_functionalities'] =
                        $allowedModuleArr;
                    }
                    $this->_getSession()->setSellerBoosterFormData($sellerBoosterData);
                    $this->_coreRegistry->register(
                        'sellerBoosterData',
                        $sellerBoosterData
                    );
                } else {
                    $this->messageManager->addError(
                        __('Requested booster doesn\'t exist')
                    );
                    $resultRedirect = $this->resultRedirectFactory->create();
                    $resultRedirect->setPath('mpsellergroup/booster/index');
                    return $resultRedirect;
                }
            } catch (NoSuchEntityException $e) {
                $this->messageManager->addException(
                    $e,
                    __('Something went wrong while editing the seller booster.')
                );
                $resultRedirect = $this->resultRedirectFactory->create();
                $resultRedirect->setPath('mpsellergroup/booster/index');
                return $resultRedirect;
            }
        }

        $resultPage = $this->_resultPageFactory->create();
        $resultPage->setActiveMenu('Webkul_MpSellerGroup::booster');
        $this->prepareDefaultGroupTitle($resultPage);
        $resultPage->setActiveMenu('Webkul_MpSellerGroup::booster');
        if ($isExistingSellerBooster) {
            $resultPage->getConfig()->getTitle()->prepend(
                __('Edit Booster with id %1', $sellerBoosterId)
            );
        } else {
            $resultPage->getConfig()->getTitle()->prepend(__('New Seller Booster'));
        }
        return $resultPage;
    }

    /**
     * seller booster initialization
     *
     * @return string seller booster id
     */
    protected function initCurrentSellerBooster()
    {
        $sellerBoosterId = (int)$this->getRequest()->getParam('id');

        if ($sellerBoosterId) {
            $this->_coreRegistry->register(
                'mpsellergroup_booster',
                $sellerBoosterId
            );
        }

        return $sellerBoosterId;
    }

    /**
     * Prepare seller booster default title
     *
     * @param \Magento\Backend\Model\View\Result\Page $resultPage
     * @return void
     */
    protected function prepareDefaultGroupTitle(
        \Magento\Backend\Model\View\Result\Page $resultPage
    ) {
        $resultPage->getConfig()->getTitle()->prepend(__('Seller Booster'));
    }

    public function getFileSize($file)
    {
        $fileSize = $this->_mediaDirectory->stat($file)['size'];
        return $fileSize;
    }
}
