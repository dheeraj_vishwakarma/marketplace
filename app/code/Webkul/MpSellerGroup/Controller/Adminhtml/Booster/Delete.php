<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Booster;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Controller\ResultFactory;

class Delete extends \Webkul\MpSellerGroup\Controller\Adminhtml\Booster
{
    /**
     * seller booster edit action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $sellerBoosterId = (int)$this->getRequest()->getParam('id');
        if ($sellerBoosterId) {
            $sellerBooster = $this->_sellerBoosterRepository->getById(
                $sellerBoosterId
            );
            if ($sellerBooster->getEntityId()) {
                $boosterName = $sellerBooster->getBoosterName();
                $this->helper->deleteProductById($sellerBooster->getProductId());
                $sellerBooster->delete();
                $this->messageManager->addSuccess(
                    __(
                        'Booster %1 has been deleted successfully.',
                        $boosterName
                    )
                );
            }
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('mpsellergroup/booster/index/');
    }
}
