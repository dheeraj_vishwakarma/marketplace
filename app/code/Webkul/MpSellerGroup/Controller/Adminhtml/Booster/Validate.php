<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Booster;

class Validate extends \Webkul\MpSellerGroup\Controller\Adminhtml\Booster
{
    /**
     * Seller Booster validation
     *
     * @param \Magento\Framework\DataObject $response
     * @return SellerGroupBoosterInterface|null
     *
     * @throws \Magento\Framework\Validator\Exception|\Exception
     */
    protected function _validateBooster($response)
    {
        $sellerBooster = null;
        $errors = [];

        try {
            /** @var SellerGroupInterface $sellerBooster */
            $sellerBooster = $this->sellerBoosterDataFactory->create();

            $data = $this->getRequest()->getParams();

            $dataResult = $data['mpsellergroup_booster'];
            $errors = [];
            $getBoosterByCode = $this->_sellerBoosterRepository->getBooster($dataResult['booster_code']);
            $existingBoosterId = '';
            if ($getBoosterByCode->getId()) {
                $existingBoosterId = $getBoosterByCode->getId();
            }
            $entityId = '';
            if (isset($dataResult['entity_id'])) {
                $entityId = $dataResult['entity_id'];
            }
            if ($existingBoosterId) {
                if ($existingBoosterId != $entityId) {
                    $errors[] =  __('Booster with %1 booster code already exist.', $dataResult['booster_code']);
                }
            }
            $this->chkvalidation($dataResult);
        } catch (\Magento\Framework\Validator\Exception $exception) {
            $exceptionMsg = $exception->getMessages(
                \Magento\Framework\Message\MessageInterface::TYPE_ERROR
            );
            /**
             * @var $error Error
             */
            foreach ($exceptionMsg as $error) {
                $errors[] = $error->getText();
            }
        }

        if ($errors) {
            $messages = $response->hasMessages() ? $response->getMessages() : [];
            foreach ($errors as $error) {
                $messages[] = $error;
            }
            $response->setMessages($messages);
            $response->setError(1);
        }

        return $sellerBooster;
    }
    
    /**
     * Check validation
     */

    private function chkvalidation($dataResult)
    {
        if (!isset($dataResult['booster_image'][0]['name'])) {
            $errors[] =  __('Please upload booster image.');
        }
        if (!isset($dataResult['booster_code'])) {
            $errors[] =  __('Booster code field can not be blank.');
        }
    }

    /**
     * AJAX customer validation action
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = $this->dataObject;

        $response->setError(0);

        $sellerBooster = $this->_validateBooster($response);

        $resultJson = $this->resultJsonFactory->create();
        if ($response->getError()) {
            $response->setError(true);
            $response->setMessages($response->getMessages());
        }

        $resultJson->setData($response);
        return $resultJson;
    }
}
