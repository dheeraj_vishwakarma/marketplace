Legend

+ Features
- Bugs

-------------------------------- Version 3.0.2 --------------------------------------

	+ Added the feature of seller membership boosters

-------------------------------- Version 3.0.1 --------------------------------------

	+ Implemented db_schema.xml
	- Compatibility issues with marketplace addons and other minor bugs fixed

-------------------------------- Version 3.0.0 --------------------------------------

	- Recurring profile issues fixed
	- Fixed issue with membership expiration mail
	+ Compatible with marketplace 3.0.X

-------------------------------- Version 2.1.0 --------------------------------------

	+ Compatible with magento ver 2.3.x and marketplace ver 2.2.x

-------------------------------- Version 2.0.6 --------------------------------------

	+ Recurring Profile feature added through paypal

-------------------------------- Version 2.0.5 --------------------------------------

	+ Updated composer.json as per magento 2.2.x version.
	+ Updated according to magento 2.2.x and marketplace 2.1.x version

-------------------------------- Version 2.0.4 --------------------------------------

	+ Seller Group Sorting feature Added
	- Fixed other minor bugs.

-------------------------------- Version 2.0.3 --------------------------------------

	- Assign group from admin issue fixed.
	- Fixed other minor bugs.
	
-------------------------------- Version 2.0.2 --------------------------------------

	+ Updated composer.json as per magento 2.1.x version.

-------------------------------- Version 2.0.1 --------------------------------------

	- Removed feature to make payment with only paypal for group payment.
	+ Allow seller to make payment with all allowed payment methods by admin for purchasing seller group from checkout page.
	+ Added mail notification on membership group purchase, payment success, expire and notify for group expiration to seller.
	+ Added cron job for module disable/delete on group expire.
	+ Added acl system(allowed marketplace module's functionalities) per group.

-------------------------------- Version 2.0.0 --------------------------------------

	+ Admin can define seller to a group.
	+ By group limitation seller will be restricted to product upload.
	+ Admin define Seller groups like Platinum, Gold and Silver.
