#Installation

Magento2 Marketplace Seller Groups module installation is very easy, please follow the steps for installation-

1. Unzip the respective extension zip and create Webkul(vendor) and MpSellerGroup(module) name folder inside your magento/app/code/ directory and then move all module's files into magento root directory Magento2/app/code/Webkul/MpSellerGroup/ folder.

Run Following Command via terminal 
----------------------------------- 
php bin/magento setup:upgrade
php bin/magento setup:di:compile
php bin/magento setup:static-content:deploy

2. Flush the cache and reindex all.

now module is properly installed

#User Guide

For Magento2 Marketplace Seller Groups module's working process follow user guide - http://webkul.com/blog/marketplace-seller-groups-magento2/

#Support

Find us our support policy - https://store.webkul.com/support.html/

#Refund

Find us our refund policy - https://store.webkul.com/refund-policy.html/