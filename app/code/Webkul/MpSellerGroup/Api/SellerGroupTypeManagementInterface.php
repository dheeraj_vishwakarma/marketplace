<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api;

/**
 * @api
 */
interface SellerGroupTypeManagementInterface
{
    /**
     * Assign group to seller.
     *
     * @param int    $groupId
     * @param int    $sellerId
     * @param string $groupCode
     *
     * @return int
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function assign($groupId, $sellerId, $groupCode);

    /**
     * Remove group from seller.
     *
     * @param int    $groupId
     * @param int    $sellerId
     * @param string $groupCode
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     *
     * @return bool
     */
    public function unassign($groupId, $sellerId, $groupCode);

    /**
     * Retrieve related seller group based on given group status.
     *
     * @param string $groupCode
     *
     * @return Webkul\MpSellerGroup\Api\Data\SellerGroupInterface[]
     */
    public function getGroupByStatus($status = null);
}
