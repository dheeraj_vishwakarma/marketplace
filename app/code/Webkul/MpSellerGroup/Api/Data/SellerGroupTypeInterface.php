<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api\Data;

/**
 * MpSellerGroup SellerGroupType interface.
 *
 * @api
 */
interface SellerGroupTypeInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID = 'entity_id';

    const SELLER_ID = 'seller_id';

    const GROUP_ID = 'group_id';

    const NO_OF_PRODUCTS = 'no_of_products';

    const TIME_PERIODS = 'time_periods';

    const CHECK_TYPE = 'check_type';

    const GROUP_TYPE = 'group_type';

    const FEE_AMOUNT = 'fee_amount';

    const FAILED_TRANSACTION = 'failed_transactions';
    
    const TRANSACTION_DATE = 'transaction_date';
    
    const EXPIRED_ON = 'expired_on';

    const TRANSACTION_ID = 'transaction_id';

    const IPN_TRACK_ID = 'ipn_track_id';

    const TRANSACTION_EMAIL = 'transaction_email';

    const TRANSACTION_STATUS = 'transaction_status';

    const ALLOWED_MODULES_FUNCTIONALITIES = 'allowed_modules_functionalities';

    const EXPIRED_STATUS = 'expired_status';

    const ORDER_ID = 'order_id';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**#@-*/

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setId($id);

    /**
     * Get Seller ID.
     *
     * @return int|null
     */
    public function getSellerId();

    /**
     * Set Seller ID.
     *
     * @param int $sellerId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setSellerId($sellerId);

    /**
     * Get Group Id.
     *
     * @return int|null
     */
    public function getGroupId();

    /**
     * Set Group Id.
     *
     * @param int $groupId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setGroupId($groupId);

    /**
     * Get Number of products.
     *
     * @return int|null
     */
    public function getNoOfProducts();

    /**
     * Set Number of products.
     *
     * @param int $noOfProducts
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setNoOfProducts($noOfProducts);

    /**
     * Get Time Periods.
     *
     * @return int|null
     */
    public function getTimePeriods();

    /**
     * Set Time Periods.
     *
     * @param int $timePeriods
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTimePeriods($timePeriods);

    /**
     * Get Check Type.
     *
     * @return int|null
     */
    public function getCheckType();

    /**
     * Set Check Type.
     *
     * @param int $checkType
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setCheckType($checkType);

    /**
     * Get Group Type.
     *
     * @return int|null
     */
    public function getGroupType();

    /**
     * Set Group Type.
     *
     * @param int $groupType
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setGroupType($groupType);

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount();

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setFeeAmount($feeAmount);

    /**
     * Get Failed Transactions.
     *
     * @return int|null
     */
    public function getFailedTransactions();

    /**
     * Set Failed Transactions.
     *
     * @param string $failedTransactions
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setFailedTransactions($failedTransactions);

    /**
     * Get Transaction Date.
     *
     * @return string|null
     */
    public function getTransactionDate();

    /**
     * Set Transaction Date.
     *
     * @param string $transactionDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionDate($transactionDate);

    /**
     * Get Expired On Date.
     *
     * @return string|null
     */
    public function getExpiredOn();

    /**
     * Set Expire Date.
     *
     * @param string $expireDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setExpiredOn($expireDate);

    /**
     * Get Transaction Id.
     *
     * @return string|null
     */
    public function getTransactionId();

    /**
     * Set Transaction Id.
     *
     * @param string $transactionId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionId($transactionId);

    /**
     * Get ipn_track_id.
     *
     * @return string|null
     */
    public function getIpnTrackId();

    /**
     * Set ipn_track_id.
     *
     * @param string $ipnTrackId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setIpnTrackId($ipnTrackId);

    /**
     * Get Transaction Email.
     *
     * @return int|null
     */
    public function getTransactionEmail();

    /**
     * Set Transaction Email.
     *
     * @param int $transactionEmail
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionEmail($transactionEmail);

    /**
     * Get Transaction Status.
     *
     * @return int|null
     */
    public function getTransactionStatus();

    /**
     * Set Transaction Status.
     *
     * @param int $transactionStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setTransactionStatus($transactionStatus);

    /**
     * Get allowed_modules_functionalities.
     *
     * @return int|null
     */
    public function getAllowedModulesFunctionalities();

    /**
     * Set allowed_modules_functionalities.
     *
     * @param int $allowedModulesFunctionalities
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setAllowedModulesFunctionalities($allowedModulesFunctionalities);

    /**
     * Get Order Id.
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * Set Order Id.
     *
     * @param int $orderId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setOrderId($orderId);

    /**
     * Get Expired Status.
     *
     * @return int|null
     */
    public function getExpiredStatus();

    /**
     * Set Expired Status.
     *
     * @param int $expiredStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     */
    public function setExpiredStatus($expiredStatus);

    /**
     * Product created date.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set product created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Product updated date.
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set product updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
