<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api\Data;

/**
 * MpSellerGroup SellerGroup interface.
 *
 * @api
 */
interface SellerGroupInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID = 'entity_id';

    const GROUP_NAME = 'group_name';

    const GROUP_CODE = 'group_code';

    const NO_OF_PRODUCTS = 'no_of_products';

    const TIME_PERIODS = 'time_periods';

    const FEE_AMOUNT = 'fee_amount';

    const GROUP_IMAGE = 'group_image';
    
    const GROUP_COLOR = 'group_color';

    const CHECK_TYPE = 'check_type';

    const GROUP_TYPE = 'group_type';

    const STATUS = 'status';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    const SORT_ORDER = 'sort_order';

    /**#@-*/

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setId($id);

    /**
     * Get Group Name.
     *
     * @return int|null
     */
    public function getGroupName();

    /**
     * Set Group Name.
     *
     * @param int $groupName
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setGroupName($groupName);

    /**
     * Get Group Code.
     *
     * @return int|null
     */
    public function getGroupCode();

    /**
     * Set Group Code.
     *
     * @param int $groupCode
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setGroupCode($groupCode);

    /**
     * Get Number of products.
     *
     * @return int|null
     */
    public function getNoOfProducts();

    /**
     * Set Number of products.
     *
     * @param int $noOfProducts
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setNoOfProducts($noOfProducts);

    /**
     * Get Time Periods.
     *
     * @return int|null
     */
    public function getTimePeriods();

    /**
     * Set Time Periods.
     *
     * @param int $timePeriods
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setTimePeriods($timePeriods);

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount();

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setFeeAmount($feeAmount);

    /**
     * Get Status.
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set Status.
     *
     * @param int $status
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setStatus($status);

    /**
     * Product created date.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Group created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Product updated date.
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set Group updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Group Image.
     *
     * @return string|null
     */
    public function getGroupImage();

    /**
     * Set group image.
     *
     * @param string $groupImage
     *
     * @return $this
     */
    public function setGroupImage($groupImage);

    /**
     * Group Color.
     *
     * @return string|null
     */
    public function getGroupColor();

    /**
     * Set group color.
     *
     * @param string $groupColor
     *
     * @return $this
     */
    public function setGroupColor($groupColor);

    /**
     * Group type.
     *
     * @return string|null
     */
    public function getGroupType();

    /**
     * Set group type.
     *
     * @param string $groupType
     *
     * @return $this
     */
    public function setGroupType($groupType);

    /**
     * Group Check type.
     *
     * @return string|null
     */
    public function getCheckType();

    /**
     * Set group check type.
     *
     * @param string $checkType
     *
     * @return $this
     */
    public function setCheckType($checkType);

    /**
     * Group Sort Order.
     *
     * @return string|null
     */
    public function getSortOrder();

    /**
     * Set group sort order.
     *
     * @param string $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder);
}
