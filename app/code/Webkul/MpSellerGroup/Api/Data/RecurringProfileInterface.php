<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api\Data;

/**
 * MpSellerGroup RecurringProfileInterface interface.
 *
 * @api
 */
interface RecurringProfileInterface
{
    /**
     * Constants for keys of data array.
     * Identical to the name of the getter in snake case
     */
    const ENTITY_ID = 'profile_id';

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID
     *
     * @param int $id contains id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface
     */
    public function setId($id);
}
