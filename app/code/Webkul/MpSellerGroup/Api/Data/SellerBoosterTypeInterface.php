<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Api\Data;

/**
 * MpSellerGroup SellerBoosterType interface.
 *
 * @api
 */
interface SellerBoosterTypeInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID = 'entity_id';

    const SELLER_ID = 'seller_id';

    const BOOSTER_ID = 'booster_id';

    const FEE_AMOUNT = 'fee_amount';
    
    const TRANSACTION_DATE = 'transaction_date';
    
    const EXPIRED_ON = 'expired_on';

    const TRANSACTION_ID = 'transaction_id';

    const TRANSACTION_EMAIL = 'transaction_email';

    const TRANSACTION_STATUS = 'transaction_status';

    const ALLOWED_MODULES_FUNCTIONALITIES = 'allowed_modules_functionalities';

    const EXPIRED_STATUS = 'expired_status';

    const ORDER_ID = 'order_id';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**#@-*/

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setId($id);

    /**
     * Get Seller ID.
     *
     * @return int|null
     */
    public function getSellerId();

    /**
     * Set Seller ID.
     *
     * @param int $sellerId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setSellerId($sellerId);

    /**
     * Get Booster Id.
     *
     * @return int|null
     */
    public function getBoosterId();

    /**
     * Set Booster Id.
     *
     * @param int $boosterId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setBoosterId($boosterId);

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount();

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setFeeAmount($feeAmount);

    /**
     * Get Transaction Date.
     *
     * @return string|null
     */
    public function getTransactionDate();

    /**
     * Set Transaction Date.
     *
     * @param string $transactionDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setTransactionDate($transactionDate);

    /**
     * Get Expired On Date.
     *
     * @return string|null
     */
    public function getExpiredOn();

    /**
     * Set Expire Date.
     *
     * @param string $expireDate
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setExpiredOn($expireDate);

    /**
     * Get Transaction Id.
     *
     * @return string|null
     */
    public function getTransactionId();

    /**
     * Set Transaction Id.
     *
     * @param string $transactionId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setTransactionId($transactionId);

    /**
     * Get Transaction Email.
     *
     * @return int|null
     */
    public function getTransactionEmail();

    /**
     * Set Transaction Email.
     *
     * @param int $transactionEmail
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setTransactionEmail($transactionEmail);

    /**
     * Get Transaction Status.
     *
     * @return int|null
     */
    public function getTransactionStatus();

    /**
     * Set Transaction Status.
     *
     * @param int $transactionStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setTransactionStatus($transactionStatus);

    /**
     * Get allowed_modules_functionalities.
     *
     * @return int|null
     */
    public function getAllowedModulesFunctionalities();

    /**
     * Set allowed_modules_functionalities.
     *
     * @param int $allowedModulesFunctionalities
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setAllowedModulesFunctionalities($allowedModulesFunctionalities);

    /**
     * Get Order Id.
     *
     * @return int|null
     */
    public function getOrderId();

    /**
     * Set Order Id.
     *
     * @param int $orderId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setOrderId($orderId);

    /**
     * Get Expired Status.
     *
     * @return int|null
     */
    public function getExpiredStatus();

    /**
     * Set Expired Status.
     *
     * @param int $expiredStatus
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     */
    public function setExpiredStatus($expiredStatus);

    /**
     * Product created date.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set product created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Product updated date.
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set product updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);
}
