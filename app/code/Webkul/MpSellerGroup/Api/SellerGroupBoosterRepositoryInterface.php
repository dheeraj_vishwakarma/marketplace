<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api;

/**
 * @api
 */
interface SellerGroupBoosterRepositoryInterface
{
    /**
     * Create SellerGroup Booster
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface $booster
     * @param bool                                                $saveOptions
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface $booster);

    /**
     * Get info about SellerGroup Booster by boosterCode.
     *
     * @param string $sku
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBooster($boosterCode);

    /**
     * Get info about seller group booster by booster product id.
     *
     * @param int $productId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBoosterByProductId($productId);

    /**
     * Get info about seller group booster by booster id.
     *
     * @param int $boosterId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($boosterId);

    /**
     * Delete group booster
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface $booster
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface $booster);

    /**
     * @param int $boosterId
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($boosterId);

    /**
     * Get Seller Group Booster list.
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterSearchResultsInterface
     */
    public function getList();

    /**
     * Get Active Seller Group Booster list.
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterSearchResultsInterface
     */
    public function getActiveList();
}
