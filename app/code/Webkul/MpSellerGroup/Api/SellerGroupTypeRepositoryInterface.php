<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api;

/**
 * @api
 */
interface SellerGroupTypeRepositoryInterface
{
    /**
     * Create Seller Group.
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface $sellerGroup
     * @param bool                                                $saveOptions
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface $sellerGroup);

    /**
     * Get info about seller group by group id.
     *
     * @param int $groupId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getGroupType($groupId);

    /**
     * Get info about seller group by group type id.
     *
     * @param int $groupTypeId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($groupTypeId);

    /**
     * Get info about seller group by seller id.
     *
     * @param int $sellerId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBySellerId($sellerId);

    /**
     * Get count of seller group by seller id.
     *
     * @param int $sellerId
     *
     * @return string
     */
    public function getBySellerCount($sellerId);

    /**
     * Get info about seller group by order id.
     *
     * @param int $orderId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByOrderId($orderId);

    /**
     * Delete seller group record
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface $group
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface $group);

    /**
     * @param int $groupId
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($groupTypeId);
}
