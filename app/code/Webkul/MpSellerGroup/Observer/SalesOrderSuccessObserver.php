<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelperData;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface;
use Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpSellerGroup\Helper\Email;
use Webkul\MpSellerGroup\Model\SellerGroupTypeFactory;
use Webkul\MpSellerGroup\Model\SellerBoosterTypeFactory;

/**
 * Webkul MpSellerGroup SalesOrderSuccessObserver Observer.
 */
class SalesOrderSuccessObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var SellerGroupHelperData
     */
    protected $_sellerGroupHelperData;

    /**
     * @var SellerGroupTypeRepositoryInterface
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerGroupTypeInterface
     */
    protected $_sellerGroupTypeInterface;

    /**
     * @var SellerBoosterTypeRepositoryInterface
     */
    protected $sellerBoosterTypeRepository;

    /**
     * @var SellerBoosterTypeInterface
     */
    protected $sellerBoosterTypeInterface;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var Email
     */
    protected $_email;

    /**
     * @var SellerGroupType
     */
    protected $_sellerGroupType;

    /**
     * @var SellerBoosterType
     */
    protected $sellerBoosterType;

    /**
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param ManagerInterface $messageManager
     * @param OrderRepositoryInterface $orderRepository
     * @param SellerGroupHelperData $sellerGroupHelperData
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param SellerGroupTypeInterface $sellerGroupTypeInterface
     * @param SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository
     * @param SellerBoosterTypeInterface $sellerBoosterTypeInterface
     * @param DateTime $date
     * @param MarketplaceHelper $marketplaceHelper
     * @param Email $email
     * @param SellerGroupTypeFactory $sellerGroupType
     * @param SellerBoosterTypeFactory $sellerBoosterType
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        ManagerInterface $messageManager,
        OrderRepositoryInterface $orderRepository,
        SellerGroupHelperData $sellerGroupHelperData,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerGroupTypeInterface $sellerGroupTypeInterface,
        SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository,
        SellerBoosterTypeInterface $sellerBoosterTypeInterface,
        DateTime $date,
        MarketplaceHelper $marketplaceHelper,
        Email $email,
        SellerGroupTypeFactory $sellerGroupType,
        SellerBoosterTypeFactory $sellerBoosterType
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_messageManager = $messageManager;
        $this->_orderRepository = $orderRepository;
        $this->_sellerGroupHelperData = $sellerGroupHelperData;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_sellerGroupTypeInterface = $sellerGroupTypeInterface;
        $this->sellerBoosterTypeRepository = $sellerBoosterTypeRepository;
        $this->sellerBoosterTypeInterface = $sellerBoosterTypeInterface;
        $this->_date = $date;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_email = $email;
        $this->_sellerGroupType = $sellerGroupType;
        $this->sellerBoosterType = $sellerBoosterType;
    }

    /**
     * Sale order place after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_sellerGroupHelperData->getStatus()) {
                $orderIds = $observer->getOrderIds();
                foreach ($orderIds as $orderId) {
                    $order = $this->_orderRepository->get($orderId);
                    $adminStoremail = $this->_marketplaceHelper->getAdminEmailId();
                    $defaultTransEmailId = $this->_marketplaceHelper->getDefaultTransEmailId();
                    $adminEmail = $adminStoremail ? $adminStoremail : $defaultTransEmailId;
                    $adminUsername = 'Admin';

                    $senderInfo = [];
                    $receiverInfo = [];

                    $receiverInfo = [
                        'name' => $order->getFirstName(),
                        'email' => $order->getCustomerEmail(),
                    ];
                    $senderInfo = [
                        'name' => $adminUsername,
                        'email' => $adminEmail,
                    ];

                    $collection = $this->_sellerGroupType->create()
                        ->getCollection()
                        ->addFieldToFilter('order_id', $orderId)
                        ->addFieldToFilter('seller_id', $order->getCustomerId());
                    if (!$collection->getSize()) {
                        foreach ($order->getAllItems() as $item) {
                            $productId = $item->getProductId();
                            $collection = $this->_sellerGroupRepository->getGroupByProductId($productId);
                            $this->saveGroupData($collection, $orderId, $order, $senderInfo, $receiverInfo);
                        }
                    }

                    $collection = $this->sellerBoosterType->create()
                        ->getCollection()
                        ->addFieldToFilter('order_id', $orderId)
                        ->addFieldToFilter('seller_id', $order->getCustomerId());
                    if (!$collection->getSize()) {
                        foreach ($order->getAllItems() as $item) {
                            $productId = $item->getProductId();
                            $collection = $this->sellerBoosterRepository->getBoosterByProductId($productId);
                            $this->saveBoosterData($collection, $orderId, $order, $senderInfo, $receiverInfo);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }

    /**
     * Save Group
     */
    public function saveGroup($sellerGroup)
    {
        $this->_sellerGroupTypeRepository->save($sellerGroup);
    }

    /**
     * Save Group Data
     */
    public function saveGroupData($collection, $orderId, $order, $senderInfo, $receiverInfo)
    {
        if ($collection['product_id']) {
            $data = [
                'order_id'          => $orderId,
                'seller_id'         => $order->getCustomerId(),
                'group_id'          => $collection['entity_id'],
                'group_type'        => $collection['group_type'],
                'no_of_products'    => $collection['no_of_products'],
                'time_periods'      => $collection['time_periods'],
                'fee_amount'        => $collection['fee_amount'],
                'allowed_modules_functionalities' => $collection['allowed_modules_functionalities'],
                'check_type'        => $collection['check_type'],
                'transaction_id' => 'N\A',
                'transaction_status' => 'pending',
                'expired_status'    => 0,
                'expired_on'        => $this->_date->gmtDate()
            ];
            $sellerGroup = $this->_sellerGroupTypeInterface;
            $sellerGroup->setData($data);
            $sellerGroup->setTransactionDate($this->_date->gmtDate());
            $sellerGroup->setCreatedAt($this->_date->gmtDate());
            $sellerGroup->setUpdatedAt($this->_date->gmtDate());
            $this->saveGroup($sellerGroup);
            $emailTempVariables = [];
            $emailTempVariables['seller_name'] = $order->getCustomerName();
            $emailTempVariables['group_name'] = $collection['group_name'];
            $emailTempVariables['fee_amount'] = $collection['fee_amount'];
            $emailTempVariables['no_of_products'] = $collection['no_of_products'];
            $emailTempVariables['time_periods'] = $collection['time_periods'].__(' Days');
            if ($collection['check_type'] == 0) {
                $emailTempVariables['time_periods'] = __('N/A');
            } elseif ($collection['check_type'] == 1) {
                $emailTempVariables['no_of_products'] = __('Unlimited');
            }
            $this->_email->sendProductGroupBuyEmail(
                $emailTempVariables,
                $senderInfo,
                $receiverInfo
            );
        }
    }

    /**
     * Save Booster
     */
    public function saveBooster($sellerBooster)
    {
        $this->sellerBoosterTypeRepository->save($sellerBooster);
    }

    /**
     * Save Booster Data
     */
    public function saveBoosterData($collection, $orderId, $order, $senderInfo, $receiverInfo)
    {
        if ($collection['product_id']) {
            $data = [
                'order_id'          => $orderId,
                'seller_id'         => $order->getCustomerId(),
                'booster_id'        => $collection['entity_id'],
                'fee_amount'        => $collection['fee_amount'],
                'allowed_modules_functionalities' => $collection['allowed_modules_functionalities'],
                'check_type'        => $collection['check_type'],
                'transaction_id' => 'N\A',
                'transaction_status' => 'pending',
                'expired_status'    => 0,
                'expired_on'        => $this->_date->gmtDate()
            ];
            $sellerBooster = $this->sellerBoosterTypeInterface;
            $sellerBooster->setData($data);
            $sellerBooster->setTransactionDate($this->_date->gmtDate());
            $sellerBooster->setCreatedAt($this->_date->gmtDate());
            $sellerBooster->setUpdatedAt($this->_date->gmtDate());
            $this->saveBooster($sellerBooster);
        }
    }
}
