<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelperData;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface;
use Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpSellerGroup\Helper\Email;
use Magento\Quote\Model\QuoteRepository;
use Magento\Checkout\Model\Session;
use Magento\Framework\Session\SessionManager;

/**
 * Webkul MpSellerGroup SalesOrderPlaceAfterObserver Observer.
 */
class SalesOrderPlaceAfterObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var SellerGroupHelperData
     */
    protected $_sellerGroupHelperData;

    /**
     * @var SellerGroupTypeRepositoryInterface
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerGroupTypeInterface
     */
    protected $_sellerGroupTypeInterface;

    /**
     * @var SellerBoosterTypeRepositoryInterface
     */
    protected $sellerBoosterTypeRepository;

    /**
     * @var SellerBoosterTypeInterface
     */
    protected $sellerBoosterTypeInterface;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var Email
     */
    protected $_email;

    /**
     * @var Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var Magento\Framework\Session\SessionManager
     */
    protected $coreSession;

    /**
     * @var Magento\Quote\Model\QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @param Session $checkoutSession
     * @param QuoteRepository $quoteRepository
     * @param SessionManager $coreSession
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param ManagerInterface $messageManager
     * @param OrderRepositoryInterface $orderRepository
     * @param SellerGroupHelperData $sellerGroupHelperData
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param SellerGroupTypeInterface $sellerGroupTypeInterface
     * @param SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository
     * @param SellerBoosterTypeInterface $sellerBoosterTypeInterface
     * @param DateTime $date
     * @param MarketplaceHelper $marketplaceHelper
     * @param Email $email
     */
    public function __construct(
        Session $checkoutSession,
        QuoteRepository $quoteRepository,
        SessionManager $coreSession,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        ManagerInterface $messageManager,
        OrderRepositoryInterface $orderRepository,
        SellerGroupHelperData $sellerGroupHelperData,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerGroupTypeInterface $sellerGroupTypeInterface,
        SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository,
        SellerBoosterTypeInterface $sellerBoosterTypeInterface,
        DateTime $date,
        MarketplaceHelper $marketplaceHelper,
        Email $email
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_messageManager = $messageManager;
        $this->_orderRepository = $orderRepository;
        $this->_sellerGroupHelperData = $sellerGroupHelperData;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_sellerGroupTypeInterface = $sellerGroupTypeInterface;
        $this->sellerBoosterTypeRepository = $sellerBoosterTypeRepository;
        $this->sellerBoosterTypeInterface = $sellerBoosterTypeInterface;
        $this->_date = $date;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_email = $email;
        $this->checkoutSession = $checkoutSession;
        $this->coreSession = $coreSession;
        $this->quoteRepository =$quoteRepository;
    }

    /**
     * Sale order place after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_sellerGroupHelperData->getStatus()) {
                $isMultiShipping = $this->checkoutSession->getQuote()->getIsMultiShipping();
                if (!$isMultiShipping) {
                    /** @var $orderInstance Order */
                    $orderId = $observer->getOrder()->getId();
                    $order = $this->_orderRepository->get($orderId);
                    $this->afterOrderPlace($order, $orderId);
                } else {
                    $quoteId = $this->checkoutSession->getLastQuoteId();
                    $quote = $this->quoteRepository->get($quoteId);
                    if ($quote->getIsMultiShipping() == 1 || $isMultiShipping == 1) {
                        $orderIds = $this->coreSession->getOrderIds();
                        foreach ($orderIds as $ids => $orderIncId) {
                            $lastOrderId = $ids;
                            /** @var $orderInstance Order */
                            $order = $this->_orderRepository->get($lastOrderId);
                            $this->afterOrderPlace($order, $lastOrderId);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }

    protected function afterOrderPlace($order, $orderId)
    {
        $adminStoremail = $this->_marketplaceHelper->getAdminEmailId();
        $defaultTransEmailId = $this->_marketplaceHelper->getDefaultTransEmailId();
        $adminEmail = $adminStoremail ? $adminStoremail : $defaultTransEmailId;
        $adminUsername = 'Admin';

        $senderInfo = [];
        $receiverInfo = [];

        $receiverInfo = [
            'name' => $order->getFirstName(),
            'email' => $order->getCustomerEmail(),
        ];
        $senderInfo = [
            'name' => $adminUsername,
            'email' => $adminEmail,
        ];
        foreach ($order->getAllItems() as $item) {
            $productId = $item->getProductId();
            $collection = $this->_sellerGroupRepository->getGroupByProductId($productId);
            if ($collection['product_id']) {
                $data = [
                            'order_id'          => $orderId,
                            'seller_id'         => $order->getCustomerId(),
                            'group_id'          => $collection['entity_id'],
                            'group_type'        => $collection['group_type'],
                            'no_of_products'    => $collection['no_of_products'],
                            'time_periods'      => $collection['time_periods'],
                            'fee_amount'        => $collection['fee_amount'],
                            'allowed_modules_functionalities' => $collection['allowed_modules_functionalities'],
                            'check_type'        => $collection['check_type'],
                            'transaction_id' => 'N\A',
                            'transaction_status' => 'pending',
                            'expired_status'    => 0,
                            'expired_on'        => $this->_date->gmtDate()
                        ];
                $sellerGroup = $this->_sellerGroupTypeInterface;
                $sellerGroup->setData($data);
                $sellerGroup->setTransactionDate($this->_date->gmtDate());
                $sellerGroup->setCreatedAt($this->_date->gmtDate());
                $sellerGroup->setUpdatedAt($this->_date->gmtDate());
                $this->saveSellerGroup($sellerGroup);
                $emailTempVariables = [];
                $emailTempVariables['seller_name'] = $order->getCustomerName();
                $emailTempVariables['group_name'] = $collection['group_name'];
                $emailTempVariables['fee_amount'] = $collection['fee_amount'];
                $emailTempVariables['no_of_products'] = $collection['no_of_products'];
                $emailTempVariables['time_periods'] = $collection['time_periods'].__(' Days');
                if ($collection['check_type'] == 0) {
                    $emailTempVariables['time_periods'] = __('N/A');
                } elseif ($collection['check_type'] == 1) {
                    $emailTempVariables['no_of_products'] = __('Unlimited');
                }
                $this->_email->sendProductGroupBuyEmail(
                    $emailTempVariables,
                    $senderInfo,
                    $receiverInfo
                );
            }

            $collection = $this->sellerBoosterRepository->getBoosterByProductId($productId);
            if ($collection['product_id']) {
                $data = [
                            'order_id'          => $orderId,
                            'seller_id'         => $order->getCustomerId(),
                            'booster_id'        => $collection['entity_id'],
                            'fee_amount'        => $collection['fee_amount'],
                            'allowed_modules_functionalities' => $collection['allowed_modules_functionalities'],
                            'transaction_id' => 'N\A',
                            'transaction_status' => 'pending',
                            'expired_status'    => 0,
                            'expired_on'        => $this->_date->gmtDate()
                        ];
                $sellerBooster = $this->sellerBoosterTypeInterface;
                $sellerBooster->setData($data);
                $sellerBooster->setTransactionDate($this->_date->gmtDate());
                $sellerBooster->setCreatedAt($this->_date->gmtDate());
                $sellerBooster->setUpdatedAt($this->_date->gmtDate());
                $this->saveSellerBooster($sellerBooster);
            }
        }
    }

    /**
     * Save Seller GRoup
     */
    public function saveSellerGroup($sellerGroup)
    {
        $this->_sellerGroupTypeRepository->save($sellerGroup);
    }

    /**
     * Save Seller Booster
     */
    public function saveSellerBooster($sellerBooster)
    {
        $this->sellerBoosterTypeRepository->save($sellerBooster);
    }
}
