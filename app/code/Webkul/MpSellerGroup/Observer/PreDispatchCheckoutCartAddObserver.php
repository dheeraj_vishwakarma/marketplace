<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\HTTP\PhpEnvironment\Response;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Webkul\MpSellerGroup\Helper\Data as HelperData;

/**
 * Webkul MpSellerGroup PreDispatchCheckoutCartAddObserver Observer.
 */
class PreDispatchCheckoutCartAddObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var RequestInterface
     */
    protected $_requestInterface;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var Response
     */
    protected $_response;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var HelperData
     */
    protected $_helper;

    /**
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param RequestInterface $requestInterface
     * @param CheckoutSession $checkoutSession
     * @param Response $response
     * @param ManagerInterface $messageManager
     * @param UrlInterface $urlBuilder
     * @param HelperData $helper
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        RequestInterface $requestInterface,
        CheckoutSession $checkoutSession,
        Response $response,
        ManagerInterface $messageManager,
        UrlInterface $urlBuilder,
        HelperData $helper
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_requestInterface = $requestInterface;
        $this->_checkoutSession = $checkoutSession;
        $this->_response = $response;
        $this->_messageManager = $messageManager;
        $this->_urlBuilder = $urlBuilder;
        $this->_helper = $helper;
    }

    /**
     * Checkout cart product add event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_helper->getStatus()) {
                $params = $this->_requestInterface->getParams();
                $paramProductId = $params['product'];
                $groupData = $this->_sellerGroupRepository->getGroupByProductId(
                    $paramProductId
                );
                $boosterData = $this->sellerBoosterRepository->getBoosterByProductId(
                    $paramProductId
                );
                $items =  $this->_checkoutSession->getQuote()->getAllVisibleItems();
                $groupFlag = 0;
                $boosterFlag = 0;
                $groupProductId = '';
                $boosterProductId = '';
                foreach ($items as $item) {
                    $productId = $item->getProductId();
                    $collection = $this->_sellerGroupRepository->getGroupByProductId(
                        $productId
                    );
                    if ($collection['product_id']) {
                        $groupFlag = 1;
                        $groupProductId = $productId;
                    }

                    $booster = $this->sellerBoosterRepository->getBoosterByProductId(
                        $productId
                    );
                    if ($booster['product_id']) {
                        $boosterFlag = 1;
                        $boosterProductId = $productId;
                    }
                }
                $this->groupProductAddAction(
                    $groupFlag,
                    $groupProductId,
                    $paramProductId,
                    $items,
                    $groupData,
                    $observer
                );
                $this->boosterProductAddAction(
                    $boosterFlag,
                    $boosterProductId,
                    $paramProductId,
                    $items,
                    $boosterData,
                    $observer
                );
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }

    /**
     * Group product add method.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    private function groupProductAddAction($flag, $groupProductId, $paramProductId, $items, $groupData, $observer)
    {
        if ($flag == 1) {
            if ($groupProductId!=$paramProductId) {
                $this->_messageManager->addNotice(
                    __(
                        'Please first complete your seller membership purchase
                         or empty your cart for buying other products'
                    )
                );
            } elseif ($groupData['product_id']==$paramProductId) {
                $this->_messageManager->addNotice(
                    __(
                        'Please first empty your cart for purchasing seller membership.'
                    )
                );
            }
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 0 && !empty($items) && $groupData['product_id']==$paramProductId) {
            $this->_messageManager->addNotice(
                __(
                    'Please first empty your cart for purchasing seller membership.'
                )
            );
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 1 && $groupData['product_id']==$paramProductId) {
            foreach ($items as $item) {
                $productId = $item->getProductId();
                $collection = $this->_sellerGroupRepository->getGroupByProductId($productId);
                if ($collection->getId()) {
                    $this->deleteQuote($item);
                }
            }
           
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        }
    }

    /**
     * Booster product add method.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    private function boosterProductAddAction($flag, $boosterProductId, $paramProductId, $items, $boosterData, $observer)
    {
        if ($flag == 1) {
            if ($boosterProductId != $paramProductId) {
                $this->_messageManager->addNotice(
                    __(
                        'Please first complete your seller membership booster purchase
                         or empty your cart for buying other products'
                    )
                );
            } elseif ($boosterData['product_id'] == $paramProductId) {
                $this->_messageManager->addNotice(
                    __(
                        'Please first empty your cart for purchasing seller membership booster.'
                    )
                );
            }
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 0 && !empty($items) && $boosterData['product_id'] == $paramProductId) {
            $this->_messageManager->addNotice(
                __(
                    'Please first empty your cart for purchasing seller membership booster.'
                )
            );
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 1 && $boosterData['product_id'] == $paramProductId) {
            foreach ($items as $item) {
                $productId = $item->getProductId();
                $collection = $this->sellerBoosterRepository->getBoosterByProductId($productId);
                if ($collection->getId()) {
                    $this->deleteQuote($item);
                }
            }
           
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        }
    }

    /**
     * Delete quote item
     */
    public function deleteQuote($item)
    {
        $item->delete();
    }
}
