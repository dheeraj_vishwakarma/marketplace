<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\HTTP\PhpEnvironment\Response;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\Request\Http as RequestHttp;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelperData;

/**
 * Webkul MpSellerGroup SalesQuoteAddItemObserver Observer.
 */
class SalesQuoteAddItemObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var RequestInterface
     */
    protected $_requestInterface;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var RequestHttp
     */
    protected $_requestHttp;

    /**
     * @var SellerGroupHelperData
     */
    protected $_sellerGroupHelperData;

    /**
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param RequestInterface $requestInterface
     * @param CheckoutSession $checkoutSession
     * @param Response $response
     * @param ManagerInterface $messageManager
     * @param UrlInterface $urlBuilder
     * @param OrderRepositoryInterface $orderRepository
     * @param RequestHttp $requestHttp
     * @param SellerGroupHelperData $sellerGroupHelperData
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        RequestInterface $requestInterface,
        CheckoutSession $checkoutSession,
        Response $response,
        ManagerInterface $messageManager,
        UrlInterface $urlBuilder,
        OrderRepositoryInterface $orderRepository,
        RequestHttp $requestHttp,
        SellerGroupHelperData $sellerGroupHelperData
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_requestInterface = $requestInterface;
        $this->_checkoutSession = $checkoutSession;
        $this->_response = $response;
        $this->_messageManager = $messageManager;
        $this->_urlBuilder = $urlBuilder;
        $this->_orderRepository = $orderRepository;
        $this->_requestHttp = $requestHttp;
        $this->_sellerGroupHelperData = $sellerGroupHelperData;
    }

    /**
     * Sale quote add item event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_sellerGroupHelperData->getStatus()) {
                $params = $this->_requestInterface->getParams();
                $event = $observer->getQuoteItem();
                $product = $event->getProduct();
                $paramProductId = $product->getId();
                $groupData = $this->_sellerGroupRepository->getGroupByProductId(
                    $paramProductId
                );
                $boosterData = $this->_sellerGroupRepository->getGroupByProductId(
                    $paramProductId
                );
                $this->salesOrderReorder($params, $paramProductId, $groupData, $boosterData, $observer);
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }

    /**
     * Group product add method.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    private function groupProductAddAction($flag, $groupProductId, $paramProductId, $items, $groupData, $observer)
    {
        if ($flag == 1) {
            if ($groupProductId!=$paramProductId) {
                $this->_messageManager->addNotice(
                    __(
                        'Please first complete your seller membership 
                        pruchase or empty your cart for buying other products.'
                    )
                );
            }
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 0 && !empty($items) && $groupData['product_id']==$paramProductId) {
            $this->_messageManager->addNotice(
                __(
                    'Please first empty your cart for purchasing seller membership.'
                )
            );
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 1 && $groupData['product_id']==$paramProductId) {
            foreach ($items as $item) {
                $productId = $item->getProductId();
                $collection = $this->_sellerGroupRepository->getGroupByProductId($productId);
                if ($collection->getId()) {
                    $this->deleteQuote($item);
                }
            }
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        }
    }

    /**
     * Delete Seller Group
     */
    public function deleteQuote($item)
    {
        $item->delete();
    }

    /**
     * Sales Order Reorder
     */
    public function salesOrderReorder($params, $paramProductId, $groupData, $boosterData, $observer)
    {
        if ($this->_requestHttp->getFullActionName() == 'sales_order_reorder') {
            $orderId = $params['order_id'];
            $order = $this->_orderRepository->get($orderId);
            $orderItems = $order->getAllItems();
            $items =  $this->_checkoutSession->getQuote()->getAllVisibleItems();
            $remainItems = count($items) - count($orderItems);
            if ($remainItems) {
                $orderItemIds = [];
                foreach ($orderItems as $orderItem) {
                    array_push($orderItemIds, $orderItem->getProductId());
                }
                $totalItemIds = [];
                foreach ($items as $item) {
                    array_push($totalItemIds, $item->getProductId());
                }
                $resultIds = array_diff($totalItemIds, $orderItemIds);

                $flag = 0;
                $groupProductId = '';
                foreach ($resultIds as $resultId) {
                    $productId = $resultId;
                    $collection = $this->_sellerGroupRepository->getGroupByProductId($productId);
                    if ($collection['product_id']) {
                        $flag = 1;
                        $groupProductId = $productId;
                    }
                }
                $this->groupProductAddAction(
                    $flag,
                    $groupProductId,
                    $paramProductId,
                    $items,
                    $groupData,
                    $observer
                );

                $flag = 0;
                $boosterProductId = '';
                foreach ($resultIds as $resultId) {
                    $productId = $resultId;
                    $booster = $this->sellerBoosterRepository->getBoosterByProductId($productId);
                    if ($booster['product_id']) {
                        $flag = 1;
                        $boosterProductId = $productId;
                    }
                }
                $this->boosterProductAddAction(
                    $flag,
                    $boosterProductId,
                    $paramProductId,
                    $items,
                    $boosterData,
                    $observer
                );
            }
        }
    }

    /**
     * Booster product add method.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    private function boosterProductAddAction($flag, $boosterProductId, $paramProductId, $items, $boosterData, $observer)
    {
        if ($flag == 1) {
            if ($boosterProductId != $paramProductId) {
                $this->_messageManager->addNotice(
                    __(
                        'Please first complete your seller membership booster
                        purchase or empty your cart for buying other products.'
                    )
                );
            }
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 0 && !empty($items) && $boosterData['product_id'] == $paramProductId) {
            $this->_messageManager->addNotice(
                __(
                    'Please first empty your cart for purchasing seller membership booster.'
                )
            );
            $observer->getRequest()->setParam('product', false);
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        } elseif ($flag == 1 && $boosterData['product_id'] == $paramProductId) {
            foreach ($items as $item) {
                $productId = $item->getProductId();
                $collection = $this->sellerBoosterRepository->getBoosterByProductId($productId);
                if ($collection->getId()) {
                    $this->deleteQuote($item);
                }
            }
            $url = $this->_urlBuilder->getUrl('checkout/cart');
            $this->_response->setRedirect($url);
            $this->_response->sendResponse();
        }
    }
}
