<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as SellerProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class MpApproveProductAfterObserver implements ObserverInterface
{
    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $_helperData;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerProductCollectionFactory
     */
    protected $_sellerProductCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @param \Webkul\MpSellerGroup\Helper\Data $helperData
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param SellerProductCollectionFactory $sellerProductCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $salesListCollection
     */
    public function __construct(
        \Webkul\MpSellerGroup\Helper\Data $helperData,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        SellerProductCollectionFactory $sellerProductCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $salesListCollection
    ) {
        $this->_helperData = $helperData;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_date = $date;
        $this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->salesListCollection = $salesListCollection;
    }

    /**
     * @param  $sellerId
     * @return array
     */
    public function getSellerGroupData($sellerId)
    {
        $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($sellerId);
        if ($getSellerGroup) {
            return $getSellerGroup;
        } else {
            return [];
        }
    }

    /**
     * Marketplace Product save after observer
     * that checks the remaining products and expired time of partner group.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_helperData->getStatus()) {
            $sellerId = $observer->getSeller()->getId();
            $today = $this->_date->gmtDate();
            $sellerProduct = $observer->getProduct();
            if ($sellerProduct->getMageproductId()) {
                $productId = $sellerProduct->getMageproductId();
                $getSellerGroup = $this->getSellerGroupData($sellerId);
                $salesList = $this->salesListCollection->create()
                    ->addFieldToFilter(
                        'mageproduct_id',
                        ['eq' => $productId]
                    )->addFieldToSelect('mageproduct_id');

                $content = '';
                if ($getSellerGroup
                    && $salesList->getSize()==0
                    && $sellerProduct->getStatus() == 1
                ) {
                    $products = $this->_sellerProductCollectionFactory->create()
                        ->addFieldToFilter(
                            'seller_id',
                            $sellerId
                        )->addFieldToSelect('mageproduct_id');
                    $marketplaceProductIds = $salesList->getColumnValues('mageproduct_id');
                    $allowqty = $getSellerGroup->getNoOfProducts();
                    $createdAt = $getSellerGroup->getTransactionDate();

                    $collection = $this->_productCollectionFactory->create()
                    ->addFieldToFilter(
                        'created_at',
                        ['datetime' => true, 'from' => $createdAt]
                    )->addFieldToFilter(
                        'entity_id',
                        ['in' => $marketplaceProductIds]
                    );

                    $totalproduct = $collection->getSize();
                    $productleft = $allowqty - $totalproduct;
                    if ($productleft > 0) {
                        $this->checkValidation($sellerId);
                    }
                }
            }
        }
    }

    /**
     * [checkValidation used to set updated value of remaining products,
     * according to condition]
     *
     * @return void
     */
    private function checkValidation($sellerId)
    {
        try {
            $getSellerGroup = $this->getSellerGroupData($sellerId);

            $today = $this->_date->gmtDate();

            if (!empty($getSellerGroup)) {
                $allowqty = $getSellerGroup->getNoOfProducts();
                $checkType = $getSellerGroup->getCheckType();
                if ($checkType == 2) {
                    if (strtotime($getSellerGroup->getExpiredOn()) > strtotime($today)
                        && $getSellerGroup->getRemainingProducts() < $allowqty
                    ) {
                        $getSellerGroup->setRemainingProducts(
                            $getSellerGroup->getRemainingProducts()+1
                        );
                        $getSellerGroup->setUpdatedAt($today);
                        $getSellerGroup->save();

                        if ($getSellerGroup->getRemainingProducts()>=$allowqty) {
                            $getSellerGroup->setExpiredStatus(1)->save();
                            $this->_helperData->expireSellerBoosterTypes($sellerId);
                        }
                    }
                } elseif ($checkType == 0) {
                    if ($getSellerGroup->getRemainingProducts() < $allowqty) {
                        $getSellerGroup->setRemainingProducts(
                            $getSellerGroup->getRemainingProducts()+1
                        );
                        $getSellerGroup->setUpdatedAt($today);
                        $getSellerGroup->save();

                        if ($getSellerGroup->getRemainingProducts()>=$allowqty) {
                            $getSellerGroup->setExpiredStatus(1)->save();
                            $this->_helperData->expireSellerBoosterTypes($sellerId);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
