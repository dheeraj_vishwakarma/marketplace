<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelperData;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface;
use Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpSellerGroup\Helper\Email;

/**
 * Webkul MpSellerGroup SalesOrderInvoiceAfterObserver Observer.
 */
class SalesOrderInvoiceAfterObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var SellerGroupHelperData
     */
    protected $_sellerGroupHelperData;

    /**
     * @var SellerGroupTypeRepositoryInterface
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerGroupTypeInterface
     */
    protected $_sellerGroupTypeInterface;

    /**
     * @var SellerBoosterTypeRepositoryInterface
     */
    protected $sellerBoosterTypeRepository;

    /**
     * @var SellerBoosterTypeInterface
     */
    protected $sellerBoosterTypeInterface;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var Email
     */
    protected $_email;

    /**
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param ManagerInterface $messageManager
     * @param OrderRepositoryInterface $orderRepository
     * @param SellerGroupHelperData $sellerGroupHelperData
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param SellerGroupTypeInterface $sellerGroupTypeInterface
     * @param SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository
     * @param SellerBoosterTypeInterface $sellerBoosterTypeInterface
     * @param DateTime $date
     * @param MarketplaceHelper $marketplaceHelper
     * @param Email $email
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        ManagerInterface $messageManager,
        OrderRepositoryInterface $orderRepository,
        SellerGroupHelperData $sellerGroupHelperData,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerGroupTypeInterface $sellerGroupTypeInterface,
        SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository,
        SellerBoosterTypeInterface $sellerBoosterTypeInterface,
        DateTime $date,
        MarketplaceHelper $marketplaceHelper,
        Email $email
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_messageManager = $messageManager;
        $this->_orderRepository = $orderRepository;
        $this->_sellerGroupHelperData = $sellerGroupHelperData;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_sellerGroupTypeInterface = $sellerGroupTypeInterface;
        $this->sellerBoosterTypeRepository = $sellerBoosterTypeRepository;
        $this->sellerBoosterTypeInterface = $sellerBoosterTypeInterface;
        $this->_date = $date;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_email = $email;
    }

    /**
     * Sale order invoice save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_sellerGroupHelperData->getStatus()) {
                $invoice = $observer->getEvent()->getInvoice();
                $orderId = $invoice->getOrderId();
                $order = $this->_orderRepository->get($orderId);

                $adminStoremail = $this->_marketplaceHelper->getAdminEmailId();
                $defaultTransEmailId = $this->_marketplaceHelper->getDefaultTransEmailId();
                $adminEmail = $adminStoremail ? $adminStoremail : $defaultTransEmailId;
                $adminUsername = 'Admin';

                $senderInfo = [];
                $receiverInfo = [];

                $receiverInfo = [
                    'name' => $order->getFirstName(),
                    'email' => $order->getCustomerEmail(),
                ];
                $senderInfo = [
                    'name' => $adminUsername,
                    'email' => $adminEmail,
                ];

                $sellerGroupTypeData = $this->_sellerGroupTypeRepository->getByOrderId($orderId);
                $id = $sellerGroupTypeData->getId();
                if ($id) {
                    $createdDate = $sellerGroupTypeData->getCreatedAt();
                    $timePeriods = $sellerGroupTypeData->getTimePeriods();

                    $sellerGroup = $this->_sellerGroupTypeInterface;
                    $sellerGroup->setId($id);
                    $sellerGroup->setTransactionStatus('paid');
                    if ($invoice->getTransactionId()) {
                        $sellerGroup->setTransactionId($invoice->getTransactionId());
                    } else {
                        $sellerGroup->setTransactionId($invoice->getIncrementId());
                    }
                    if ($sellerGroupTypeData->getCheckType()!=0) {
                        $date = strtotime("+".$timePeriods." days", strtotime($createdDate));
                        $expiredOnDate = date("Y-m-d H:i:s", $date);
                        $sellerGroup->setExpiredOn($expiredOnDate);
                    }
                    $sellerGroup->setUpdatedAt($this->_date->gmtDate());

                    $this->_sellerGroupTypeRepository->save($sellerGroup);
                    $group = $this->_sellerGroupRepository->getById($sellerGroupTypeData['group_id']);
                    $emailTemplateVariables = [];
                    $emailTempVariables['seller_name'] = $order->getCustomerName();
                    $emailTempVariables['group_name'] = $group['group_name'];
                    $emailTempVariables['fee_amount'] = $sellerGroupTypeData['fee_amount'];
                    $emailTempVariables['no_of_products'] = $sellerGroupTypeData['no_of_products'];
                    $emailTempVariables['time_periods'] = $sellerGroupTypeData['time_periods'].__(' Days');
                    if ($sellerGroupTypeData['check_type'] == 0) {
                        $emailTempVariables['time_periods'] = __('N/A');
                    } elseif ($sellerGroupTypeData['check_type'] == 1) {
                        $emailTempVariables['no_of_products'] = __('Unlimited');
                    }
                    $this->_email->sendGroupInvoiceEmail(
                        $emailTempVariables,
                        $senderInfo,
                        $receiverInfo
                    );
                }

                $sellerBoosterTypeData = $this->sellerBoosterTypeRepository->getByOrderId($orderId);
                $id = $sellerBoosterTypeData->getId();
                if ($id) {
                    $sellerBooster = $this->sellerBoosterTypeInterface;
                    $sellerBooster->setId($id);
                    $sellerBooster->setTransactionStatus('paid');
                    if ($invoice->getTransactionId()) {
                        $sellerBooster->setTransactionId($invoice->getTransactionId());
                    } else {
                        $sellerBooster->setTransactionId($invoice->getIncrementId());
                    }
                    $sellerBooster->setUpdatedAt($this->_date->gmtDate());

                    $this->sellerBoosterTypeRepository->save($sellerBooster);
                }
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }
}
