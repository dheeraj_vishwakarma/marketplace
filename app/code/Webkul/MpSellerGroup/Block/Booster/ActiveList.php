<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Block\Booster;

use Webkul\MpSellerGroup\Model\ResourceModel\SellerBoosterType\CollectionFactory;
use Webkul\Marketplace\Model\ControllersRepository;

/**
 * Webkul MpSellerGroup Block for seller purchased boosters
 */
class ActiveList extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\MpSellerGroup to return collection
     */
    private $boosterTypes;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $helper;

    /**
     * @var ControllersRepository
     */
    protected $_controllersRepository;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param CollectionFactory $collectionFactory
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param \Webkul\MpSellerGroup\Model\Booster\Source\TransactionStatus $transactionStatus
     * @param ControllersRepository $controllersRepository
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        CollectionFactory $collectionFactory,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Webkul\MpSellerGroup\Model\Booster\Source\TransactionStatus $transactionStatus,
        ControllersRepository $controllersRepository,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->transactionStatus = $transactionStatus;
        $this->_controllersRepository = $controllersRepository;
        parent::__construct($context, $data);
    }

    /**
     * getAllRecurringProfiles get all recurring profiles for seller
     *
     * @return object [return collection]
     */
    public function getAllActiveBoosters()
    {
        try {
            if (!$this->boosterTypes) {
                $data = $this->getRequest()->getParams();
                $filteredData = $this->getFilteredData($data);

                $boosterTypeCollection = $this->collectionFactory->create();
                $userId = $this->helper->getSellerId();

                $collection = $boosterTypeCollection
                    ->addFieldToFilter(
                        'seller_id',
                        ['eq' => $userId]
                    )->setOrder(
                        'created_at',
                        'DESC'
                    );

                if ($filteredData['filterTransactionId']) {
                    $collection->addFieldToFilter(
                        'transaction_id',
                        ['eq' => $filteredData['filterTransactionId']]
                    );
                }

                if ($filteredData['filterTransactionStatus']) {
                    $collection->addFieldToFilter(
                        'transaction_status',
                        ['eq' => $filteredData['filterTransactionStatus']]
                    );
                }

                $boosterTable = $collection->getTable('marketplace_sellergroup_booster');
                $collection->getSelect()
                    ->join(
                        ['bo'=> $boosterTable],
                        "main_table.booster_id = bo.entity_id",
                        [
                            'booster_name' => 'bo.booster_name',
                        ]
                    );

                if ($filteredData['from'] && $filteredData['to']) {
                    $collection->getSelect()->where(
                        "main_table.created_at BETWEEN '"
                        . $filteredData['from']
                        ."' AND '"
                        . $filteredData['to']
                        ."'"
                    );
                }

                if ($collection->getSize()) {
                    $this->boosterTypes = $collection;
                }
            }
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_Booster_ActiveList getAllActiveBoosters : ".$e->getMessage());
        }
        return $this->boosterTypes;
    }

    /**
     * getFilteredData used to get filtered data
     *
     * @param array $data [contains request data]
     * @return array [returns filtered data]
     */
    public function getFilteredData($data)
    {
        try {
            $filterTransactionId = '';
            $filterTransactionStatus = '';
            $filterFromDate = '';
            $filterToDate = '';
            $from = null;
            $to = null;

            if (!empty($data['transaction_id'])) {
                $filterTransactionId = $data['transaction_id'] != '' ? $data['transaction_id'] : '';
            }

            if (!empty($data['transaction_status'])) {
                $filterTransactionStatus = $data['transaction_status'] != '' ? $data['transaction_status'] : '';
            }

            if (!empty($data['from_date'])) {
                $filterFromDate = $data['from_date'] != '' ? $data['from_date'] : '';
            }

            if (!empty($data['to_date'])) {
                $filterToDate = $data['to_date'] != '' ? $data['to_date'] : '';
            }

            if ($filterToDate) {
                $todate = date_create($filterToDate);
                $to = date_format($todate, 'Y-m-d 23:59:59');
            }

            if (!$to) {
                $to = date('Y-m-d 23:59:59');
            }

            if ($filterFromDate) {
                $fromdate = date_create($filterFromDate);
                $from = date_format($fromdate, 'Y-m-d H:i:s');
            }

            $wholeData = [
                'filterTransactionId' => $filterTransactionId,
                'filterTransactionStatus' => $filterTransactionStatus,
                'from' => $from,
                'to' => $to,
            ];

            return $wholeData;
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_Booster_ActiveList getFilteredData : ".$e->getMessage());
        }
    }

    /**
     * _prepareLayout
     *
     * @return object
     */
    protected function _prepareLayout()
    {
        try {
            parent::_prepareLayout();
            if ($this->getAllActiveBoosters()) {
                $pager = $this->getLayout()->createBlock(
                    \Magento\Theme\Block\Html\Pager::class,
                    'mpsellergroup.booster.list.pager'
                )->setCollection(
                    $this->getAllActiveBoosters()
                );
                $this->setChild('pager', $pager);
                $this->getAllActiveBoosters()->load();
            }
        } catch (\Exception $e) {
            $this->helper->logInfo(
                "Block_Booster_ActiveList_prepareLayout Exception : ".$e->getMessage()
            );
        }
        return $this;
    }

    /**
     * getPagerHtml
     *
     * @return object
     */
    public function getPagerHtml()
    {
        try {
            return $this->getChildHtml('pager');
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_Booster_ActiveList getPagerHtml : ".$e->getMessage());
        }
    }

    /**
     * getParameters get request parameters
     *
     * @return array
     */
    public function getParameters()
    {
        try {
            return $this->getRequest()->getParams();
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_Booster_ActiveList getParameters : ".$e->getMessage());
        }
    }

    /**
     * getAllProfileStates
     *
     * @return array
     */
    public function getAllTransactionStatus()
    {
        return $this->transactionStatus->toOptionArray();
    }

    /**
     * Return Helper
     *
     */

    public function helper()
    {
        return $this->helper;
    }
   
    /**
     * Get Request Params
     */
    public function getRequestParams()
    {
        return $this->getRequest()->getParams();
    }

    public function getAllowedControllersBySetData($allowedModule)
    {
        $allowedModuleArr=[];
        if ($allowedModule && $allowedModule!='all') {
            $allowedModuleControllers = explode(',', $allowedModule);
            foreach ($allowedModuleControllers as $key => $value) {
                $controllersList = $this->_controllersRepository->getByPath($value);
                foreach ($controllersList as $key => $value) {
                    if ($this->helper->isModuleInstalled($value['module_name'])) {
                        array_push($allowedModuleArr, $value['label']);
                    }
                }
            }
        } else {
            $controllersList = $this->_controllersRepository->getList();
            foreach ($controllersList as $key => $value) {
                if ($this->helper->isModuleInstalled($value['module_name'])) {
                    array_push($allowedModuleArr, $value['label']);
                }
            }
        }
        return $allowedModuleArr;
    }
}
