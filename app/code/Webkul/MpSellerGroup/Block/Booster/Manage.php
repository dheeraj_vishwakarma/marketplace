<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Block\Booster;

use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Webkul\Marketplace\Model\ControllersRepository;

class Manage extends \Magento\Framework\View\Element\Template
{
    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var SellerBoosterTypeRepositoryInterface
     */
    protected $sellerBoosterTypeRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var ControllersRepository
     */
    protected $_controllersRepository;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    public $helper;

    /**
     * @var \Magento\Catalog\Helper\Output
     */
    public $catalogHelper;
    
   /**
    * @param \Magento\Catalog\Block\Product\Context $context
    * @param \Magento\Customer\Model\Session $customerSession
    * @param SellerGroupRepositoryInterface $sellerGroupRepository
    * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
    * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
    * @param SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository
    * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
    * @param ProductRepositoryInterface $productRepositoryInterface
    * @param ControllersRepository $controllersRepository
    * @param \Webkul\MpSellerGroup\Helper\Data $helper
    * @param \Magento\Catalog\Helper\Output $catalogHelper
    * @param \Magento\Framework\Message\ManagerInterface $messageManager
    * @param array $data
    */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        ProductRepositoryInterface $productRepositoryInterface,
        ControllersRepository $controllersRepository,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Magento\Catalog\Helper\Output $catalogHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        array $data = []
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->sellerBoosterTypeRepository = $sellerBoosterTypeRepository;
        $this->_customerSession = $customerSession;
        $this->_date = $date;
        $this->_productRepository = $productRepositoryInterface;
        $this->_controllersRepository = $controllersRepository;
        $this->helper = $helper;
        $this->catalogHelper = $catalogHelper;
        $this->messageManager = $messageManager;
        parent::__construct(
            $context,
            $data
        );
    }

    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('Seller Membership Boosters'));
    }

    public function helperData()
    {
        return $this->helper;
    }

    public function catalogHelper()
    {
        return $this->catalogHelper;
    }

    public function getActiveBoosterLists()
    {
        return $this->sellerBoosterRepository->getActiveList();
    }

    public function getBoosterLists()
    {
        return $this->sellerBoosterRepository->getList();
    }

    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    public function getBoosterDataByBoosterId($boosterId)
    {
        return $this->sellerBoosterRepository->getById($boosterId);
    }

    public function getSellerBoosterCollection()
    {
        $sellerId = $this->helper->getSellerId();
        return $this->sellerBoosterTypeRepository->getCollectionBySellerId($sellerId);
    }

    public function getProductById($productId)
    {
        return $this->_productRepository->getById($productId);
    }

    public function getAllowedControllersBySetData($allowedModule)
    {
        $allowedModuleArr=[];
        if ($allowedModule && $allowedModule!='all') {
            $allowedModuleControllers = explode(',', $allowedModule);
            foreach ($allowedModuleControllers as $key => $value) {
                $controllersList = $this->_controllersRepository->getByPath($value);
                foreach ($controllersList as $key => $value) {
                    if ($this->helper->isModuleInstalled($value['module_name'])) {
                        array_push($allowedModuleArr, $value['label']);
                    }
                }
            }
        } else {
            $controllersList = $this->_controllersRepository->getList();
            foreach ($controllersList as $key => $value) {
                if ($this->helper->isModuleInstalled($value['module_name'])) {
                    array_push($allowedModuleArr, $value['label']);
                }
            }
        }
        return $allowedModuleArr;
    }
}
