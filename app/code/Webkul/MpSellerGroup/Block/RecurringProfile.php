<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Block;

use Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile\CollectionFactory;

/**
 * Webkul MpSellerGroup RecurringProfile Create Block for seller recurring profiles
 */
class RecurringProfile extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\MpSellerGroup to return collection
     */
    private $recurringProfiles;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param CollectionFactory $collectionFactory
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param \Webkul\MpSellerGroup\Model\RecurringProfile\Source\ProfileStates $profileStates
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        CollectionFactory $collectionFactory,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Webkul\MpSellerGroup\Model\RecurringProfile\Source\ProfileStates $profileStates,
        array $data = []
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->helper = $helper;
        $this->profileStates = $profileStates;
        parent::__construct($context, $data);
    }

    /**
     * getAllRecurringProfiles get all recurring profiles for seller
     *
     * @return object [return collection]
     */
    public function getAllRecurringProfiles()
    {
        try {
            if (!$this->recurringProfiles) {
                $data = $this->getRequest()->getParams();
                $filteredData = $this->getFilteredData($data);

                $recurringProfileModel = $this->collectionFactory->create();
                $userId = $this->helper->getSellerId();

                $collection = $recurringProfileModel
                    ->addFieldToFilter(
                        'seller_id',
                        ['eq' => $userId]
                    )->addFieldToFilter(
                        'reference_id',
                        ['like' => '%'.$filteredData['filterRefId'].'%']
                    )->setOrder(
                        'created_at',
                        'DESC'
                    );

                if ($filteredData['filterProfileState']) {
                    $collection->addFieldToFilter(
                        'state',
                        ['eq' => $filteredData['filterProfileState']]
                    );
                }

                if ($filteredData['from'] && $filteredData['to']) {
                    $collection->getSelect()->where(
                        "created_at BETWEEN '"
                        . $filteredData['from']
                        ."' AND '"
                        . $filteredData['to']
                        ."'"
                    );
                }

                if ($collection->getSize()) {
                    $this->recurringProfiles = $collection;
                }
            }
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_RecurringProfile getAllRecurringProfiles : ".$e->getMessage());
        }
        return $this->recurringProfiles;
    }

    /**
     * getFilteredData used to get filtered data
     *
     * @param array $data [contains request data]
     * @return array [returns filtered data]
     */
    public function getFilteredData($data)
    {
        try {
            $filterRefId = '';
            $filterProfileState = '';
            $filterFromDate = '';
            $filterToDate = '';
            $from = null;
            $to = null;

            if (!empty($data['ref_id'])) {
                $filterRefId = $data['ref_id'] != '' ? $data['ref_id'] : '';
            }

            if (!empty($data['profile_state'])) {
                $filterProfileState = $data['profile_state'] != '' ? $data['profile_state'] : '';
            }

            if (!empty($data['from_date'])) {
                $filterFromDate = $data['from_date'] != '' ? $data['from_date'] : '';
            }

            if (!empty($data['to_date'])) {
                $filterToDate = $data['to_date'] != '' ? $data['to_date'] : '';
            }

            if ($filterToDate) {
                $todate = date_create($filterToDate);
                $to = date_format($todate, 'Y-m-d 23:59:59');
            }

            if (!$to) {
                $to = date('Y-m-d 23:59:59');
            }

            if ($filterFromDate) {
                $fromdate = date_create($filterFromDate);
                $from = date_format($fromdate, 'Y-m-d H:i:s');
            }

            $wholeData = [
                'filterRefId' => $filterRefId,
                'filterProfileState' => $filterProfileState,
                'from' => $from,
                'to' => $to,
            ];

            return $wholeData;
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_RecurringProfile getFilteredData : ".$e->getMessage());
        }
    }

    /**
     * _prepareLayout
     *
     * @return object
     */
    protected function _prepareLayout()
    {
        try {
            parent::_prepareLayout();
            if ($this->getAllRecurringProfiles()) {
                $pager = $this->getLayout()->createBlock(
                    \Magento\Theme\Block\Html\Pager::class,
                    'mpsellergroup.recurringprofiles.list.pager'
                )->setCollection(
                    $this->getAllRecurringProfiles()
                );
                $this->setChild('pager', $pager);
                $this->getAllRecurringProfiles()->load();
            }
        } catch (\Exception $e) {
            $this->helper->logInfo(
                "Block_RecurringProfile _prepareLayout Exception : ".$e->getMessage()
            );
        }
        return $this;
    }

    /**
     * getPagerHtml
     *
     * @return object
     */
    public function getPagerHtml()
    {
        try {
            return $this->getChildHtml('pager');
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_RecurringProfile getPagerHtml : ".$e->getMessage());
        }
    }

    /**
     * getParameters get request parameters
     *
     * @return array
     */
    public function getParameters()
    {
        try {
            return $this->getRequest()->getParams();
        } catch (\Exception $e) {
            $this->helper->logInfo("Block_RecurringProfile getParameters : ".$e->getMessage());
        }
    }

    /**
     * getConfirmationMessage get confirmation message
     *
     * @return string
     */
    public function getConfirmationMessage()
    {
        return __("Are you sure you want to do this?");
    }

    /**
     * getCancelUrl get cancel Url
     *
     * @return string
     */
    public function getCancelUrl($profileId)
    {
        return $this->getUrl(
            'mpsellergroup/recurring_profile/updateState',
            [
                'profile' => $profileId,
                'action' => 'cancel'
            ]
        );
    }

    /**
     * getSuspendUrl get suspend Url
     *
     * @return string
     */
    public function getSuspendUrl($profileId)
    {
        return $this->getUrl(
            'mpsellergroup/recurring_profile/updateState',
            [
                'profile' => $profileId,
                'action' => 'suspend'
            ]
        );
    }

    /**
     * getActivateUrl get activate Url
     *
     * @return string
     */
    public function getActivateUrl($profileId)
    {
        return $this->getUrl(
            'mpsellergroup/recurring_profile/updateState',
            [
                'profile' => $profileId,
                'action' => 'activate'
            ]
        );
    }

    /**
     * getUpdateUrl get update profile Url
     *
     * @return string
     */
    public function getUpdateUrl($profileId)
    {
        return $this->getUrl(
            'mpsellergroup/recurring_profile/updateProfile',
            [
                'profile' => $profileId
            ]
        );
    }

    /**
     * getAllProfileStates
     *
     * @return array
     */
    public function getAllProfileStates()
    {
        return $this->profileStates->toOptionArray();
    }

    /**
     * Return Helper
     *
     */

    public function helper()
    {
        return $this->helper;
    }
   
    /**
     * Get Request Params
     */
    public function getRequestParams()
    {
        return $this->getRequest()->getParams();
    }
}
