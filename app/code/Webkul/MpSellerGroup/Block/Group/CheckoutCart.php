<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Block\Group;

use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;

class CheckoutCart extends \Magento\Framework\View\Element\Template
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $helper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param SellerGroupRepositoryInterface                    $sellerGroupRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime       $date
     * @param array                                             $data
     * @param \Webkul\MpSellerGroup\Helper\Data                 $helper
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        array $data = []
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_date = $date;
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * Get Group details.
     *
     * @return \Webkul\MpSellerGroup\Model\SellerGroup
     */
    public function getGroupDataByProductId($productId)
    {
        return $getSellerGroup = $this->_sellerGroupRepository->getGroupByProductId(
            $productId
        );
    }

    /**
     * Get Current Date.
     *
     * @return string
     */
    public function getDate()
    {
        $currentDate = $this->helper->getCurrentTime(true);
        $date = strtotime($currentDate);
        return date("M j, Y g:i:s a", $date);
    }
}
