<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Block\Group;

use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as SellerProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;

class AddProduct extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $_helperData;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerProductCollectionFactory
     */
    protected $_sellerProductCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @param Context                                     $context
     * @param \Webkul\MpSellerGroup\Helper\Data           $helperData
     * @param \Magento\Customer\Model\Session             $customerSession
     * @param SellerGroupRepositoryInterface              $sellerGroupRepository
     * @param SellerGroupTypeRepositoryInterface          $sellerGroupTypeRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param SellerProductCollectionFactory              $sellerProductCollectionFactory
     * @param ProductCollectionFactory                    $productCollectionFactory
     * @param array                                       $data
     */
    public function __construct(
        \Webkul\MpSellerGroup\Helper\Data $helperData,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Catalog\Block\Product\Context $context,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        SellerProductCollectionFactory $sellerProductCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        array $data = []
    ) {
        $this->_helperData = $helperData;
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_customerSession = $customerSession;
        $this->_date = $date;
        $this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * Return Helper
     */
    public function helper()
    {
        return $this->_helperData;
    }
}
