<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Helper;

use Webkul\MpSellerGroup\Model\SellerGroup;

/**
 * MpSellerGroup Subscription helper.
 */
class Subscription extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * Logging instance.
     *
     * @var \Webkul\MpSellerGroup\Logger\Logger
     */
    private $logger;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    private $curl;

    /**
     * @param Magento\Framework\App\Helper\Context $context
     * @param \Webkul\MpSellerGroup\Logger\Logger $logger
     * @param \Magento\Framework\HTTP\Client\Curl $curl
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\MpSellerGroup\Logger\Logger $logger,
        \Magento\Framework\HTTP\Client\Curl $curl
    ) {
        parent::__construct($context);
        $this->logger = $logger;
        $this->curl = $curl;
    }

    /**
     * @param  $param
     * @return string
     */
    public function getConfigValue($param)
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/subscription_settings/'.$param,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */

    public function getAccessToken()
    {
        try {
            $url = "https://api.".$this->getSandboxStatus()."paypal.com/v1/oauth2/token";
            $params = [
                'grant_type' => "client_credentials"
            ];
            $options = [
                CURLOPT_USERPWD => $this->getConfigValue("client_id").":".$this->getConfigValue("client_secret")
            ];
            $response = $this->getResponseFromCurl($url, $params, [], $options);
            if (!empty($response['access_token'])) {
                return $response['access_token'];
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("getAccessToken Helper_Subscription : %1", $e->getMessage())
            );
            return false;
        }
    }

    /**
     * @return string
     */

    public function getSandboxStatus()
    {
        $sandbox = "";
        $sandboxstatus = $this->getConfigValue("sandbox");
        if ($sandboxstatus == 1) {
            $sandbox = "sandbox.";
        }
        return $sandbox;
    }

    /**
     * @param  $url
     * @param  array  $params
     * @param  array  $headers
     * @param  array  $options
     * @return array
     */
    public function getResponseFromCurl(
        $url,
        $params = [],
        $headers = [],
        $options = []
    ) {
        try {
            if (!empty($options)) {
                $this->curl->setOptions($options);
            }
            if (!empty($headers)) {
                $this->curl->setHeaders($headers);
            }
            if (!empty($params)) {
                $this->curl->post($url, $params);
            } else {
                $this->curl->get($url);
            }
            $response = json_decode($this->curl->getBody(), true);
            return $response;
        } catch (\Exception $e) {
            $this->logInfo(
                __("getResponseFromCurl Helper_Subscription : %1", $e->getMessage())
            );
            return false;
        }
    }

    /**
     * @param  $params
     * @param  $accessToken
     * @return int
     */
    public function createSubscriptionPlan($params, $accessToken)
    {
        $result = false;
        try {
            if ($accessToken) {
                $url = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-plans";
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $response = $this->getResponseFromCurl($url, json_encode($params), $headers);
                $this->logInfo(
                    __("createSubscriptionPlan Helper_Subscription response : %1", json_encode($response))
                );
                if (!empty($response['id'])) {
                    $result = $response['id'];
                }
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("createSubscriptionPlan Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $subscriptionPlanId
     * @param  $accessToken
     * @return boolean
     */
    public function activateSubscriptionPlan($subscriptionPlanId, $accessToken)
    {
        $result = false;
        try {
            if ($accessToken) {
                $apiUrl = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-plans/";
                $url = $apiUrl.$subscriptionPlanId;
                $params = [
                    [
                        "op" => "replace",
                        "path" => "/",
                        "value" => [
                            "state" => "ACTIVE"
                        ]
                    ]
                ];
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $options = [
                    CURLOPT_CUSTOMREQUEST => "PATCH"
                ];
                $jsonData = json_encode($params, JSON_UNESCAPED_SLASHES);
                $result = $this->getResponseFromCurl($url, $jsonData, $headers, $options);
                if ($result == null) {
                    $result = true;
                }
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("activateSubscriptionPlan Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $subscriptionPlanId
     * @param  $accessToken
     * @return object
     */

    public function getSubscriptionPlan($subscriptionPlanId, $accessToken)
    {
        $result = false;
        try {
            if ($accessToken) {
                $apiUrl = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-plans/";
                $url = $apiUrl.$subscriptionPlanId."/";
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $options = [
                    CURLOPT_CUSTOMREQUEST => null
                ];
                $result = $this->getResponseFromCurl($url, [], $headers, $options);
                $this->logInfo(
                    __("getSubscriptionPlan Helper_Subscription response : %1", json_encode($result))
                );
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("getSubscriptionPlan Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $params
     * @param  $accessToken
     * @return object
     */

    public function createAnAgreement($params, $accessToken)
    {
        $result = false;
        try {
            if ($accessToken) {
                $url = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-agreements";
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $options = [
                    CURLOPT_CUSTOMREQUEST => null
                ];
                $result = $this->getResponseFromCurl($url, json_encode($params), $headers, $options);
                $this->logInfo(
                    __("createAnAgreement Helper_Subscription response : %1", json_encode($result))
                );
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("createAnAgreement Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $paymentToken
     * @return object
     */

    public function executeAnAgreement($paymentToken)
    {
        $result = false;
        try {
            $accessToken = $this->getAccessToken();
            if ($accessToken) {
                $apiUrl = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-agreements/";
                $url = $apiUrl . $paymentToken . "/agreement-execute";
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $params = [
                    "payment_token" => $paymentToken
                ];
                $result = $this->getResponseFromCurl($url, json_encode($params), $headers);
                $this->logInfo(
                    __("executeAnAgreement Helper_Subscription response : %1", json_encode($result))
                );
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("executeAnAgreement Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $referenceId
     * @return object
     */
    public function cancelBillingAgreement($referenceId)
    {
        $result = false;
        try {
            $accessToken = $this->getAccessToken();
            if ($accessToken) {
                $url = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-agreements/"
                    . $referenceId
                    . "/cancel";
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $params = [
                    "note" => __("Canceling the profile.")
                ];
                $result = $this->getResponseFromCurl($url, json_encode($params), $headers);
                $this->logInfo(
                    __("cancelBillingAgreement Helper_Subscription response : %1", json_encode($result))
                );
                if ($result == null) {
                    $result = true;
                }
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("cancelBillingAgreement Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $referenceId
     * @return object
     */
    public function suspendBillingAgreement($referenceId)
    {
        $result = false;
        try {
            $accessToken = $this->getAccessToken();
            if ($accessToken) {
                $url = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-agreements/"
                    . $referenceId
                    . "/suspend";
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $params = [
                    "note" => __("Suspending the profile.")
                ];
                $result = $this->getResponseFromCurl($url, json_encode($params), $headers);
                $this->logInfo(
                    __("suspendBillingAgreement Helper_Subscription response : %1", json_encode($result))
                );
                if ($result == null) {
                    $result = true;
                }
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("suspendBillingAgreement Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $referenceId
     * @return object
     */
    public function activateBillingAgreement($referenceId)
    {
        $result = false;
        try {
            $accessToken = $this->getAccessToken();
            if ($accessToken) {
                $url = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-agreements/"
                    . $referenceId
                    . "/re-activate";
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $params = [
                    "note" => __("Re-activating the profile.")
                ];
                $result = $this->getResponseFromCurl($url, json_encode($params), $headers);
                $this->logInfo(
                    __("activateBillingAgreement Helper_Subscription response : %1", json_encode($result))
                );
                if ($result == null) {
                    $result = true;
                }
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("activateBillingAgreement Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $referenceId
     * @return object
     */
    public function getBillingAgreement($referenceId)
    {
        $result = false;
        try {
            $accessToken = $this->getAccessToken();
            if ($accessToken) {
                $url = "https://api.".$this->getSandboxStatus()."paypal.com/v1/payments/billing-agreements/"
                    . $referenceId;
                $headers = [
                    'Content-Type' => "application/json",
                    'Authorization' => "Bearer ".$accessToken,
                ];
                $result = $this->getResponseFromCurl($url, [], $headers);
                $this->logInfo(
                    __("getBillingAgreement Helper_Subscription response : %1", json_encode($result))
                );
            }
        } catch (\Exception $e) {
            $this->logInfo(
                __("getBillingAgreement Helper_Subscription : %1", $e->getMessage())
            );
        }
        return $result;
    }

    /**
     * @param  $unit
     * @return string
     */
    public function getBillingPeriodUnit($unit)
    {
        if ($unit == SellerGroup::DAY) {
            return "DAY";
        } elseif ($unit == SellerGroup::WEEK) {
            return "WEEK";
        } elseif ($unit == SellerGroup::MONTH) {
            return "MONTH";
        } elseif ($unit == SellerGroup::YEAR) {
            return "YEAR";
        }
    }

    /**
     * @param  $data
     * @return null
     */
    public function logInfo($data)
    {
        $this->logger->info($data);
    }
}
