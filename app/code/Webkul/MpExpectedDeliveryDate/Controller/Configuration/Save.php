<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Configuration;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterface;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ExpddConfigRepoInterface $expddConfigRepoInterface
     * @param ExpddConfigInterface $expddConfigInterface
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ExpddConfigRepoInterface $expddConfigRepoInterface,
        ExpddConfigInterface $expddConfigInterface,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->expddConfigRepoInterface = $expddConfigRepoInterface;
        $this->expddConfigInterface = $expddConfigInterface;
        $this->helper = $helper;
        $this->sellerId = 0;
        parent::__construct($context);
    }
    
    /**
     * Execute
     *
     * @return PageFactory
     */
    public function execute()
    {
        $this->sellerId = $this->helper->getSellerId();
        if ($this->sellerId) {
            $isSellerHasConfig = $this->expddConfigRepoInterface->getBySellerId($this->sellerId);
            $data = $this->captureRequestData($isSellerHasConfig->getId()??0);
            $this->expddConfigRepoInterface->save($data);
            $this->messageManager->addSuccess(__('Settings has been saved successfully.'));
            $this->_redirect("*/*/index");
            return true;
        }
        $this->messageManager->addError(__('Invalid Request.'));
        $this->_redirect("*/*/index");
        return true;
    }

    /**
     * Capture Request Data
     *
     * @param integer $entityId
     * @return ExpddConfigInterface
     */
    private function captureRequestData(int $entityId = 0): ExpddConfigInterface
    {
        $wholeData    = $this->getRequest()->getParams();
        $expddConfigInterface = $this->expddConfigInterface;
        $wholeData['holidays'] =
        $this->getHolidays(
            $wholeData['range_type']??[],
            $wholeData['date_from']??[],
            $wholeData['date_to']??[]
        );

        $wholeData['seller_id'] = $this->sellerId;

        if ($entityId) {
             $expddConfigInterface->setId($entityId);
        }
        foreach ($wholeData as $key => $data) {
            $method = 'set'.str_replace('_', '', ucwords($key, '_'));
            $expddConfigInterface->$method($this->parseData($data));
        }
        
        return $expddConfigInterface;
    }

    /**
     * Get Holidays
     *
     * @param array $rangeTypes
     * @param array $dateFrom
     * @param array $dateTo
     * @return string
     */
    private function getHolidays(array $rangeTypes, array $dateFrom, array $dateTo): string
    {
        $holidays = [];

        if (count($rangeTypes)) {
            foreach ($rangeTypes as $key => $type) {
                $holidays[$key] = [
                    'range_type' => $type,
                    'date_from' => $dateFrom[$key] ?? null,
                    'date_to' => $dateTo[$key] ?? null
                ];
            }
        }

        return $this->helper->jsonEncode($holidays);
    }

    /**
     * Parse Data
     *
     * @param [type] $data
     * @return string
     */
    private function parseData($data): string
    {
        if (is_array($data)) {
            return implode(',', $data);
        }

        if (is_string($data)) {
            return trim($data);
        }

        return $data;
    }
}
