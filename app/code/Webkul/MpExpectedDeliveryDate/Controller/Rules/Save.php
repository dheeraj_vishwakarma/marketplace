<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Rules;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRuleRepoInterface;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

   /**
    * @param Context $context
    * @param PageFactory $resultPageFactory
    * @param ExpddRuleRepoInterface $expddRuleRepoInterface
    * @param ExpddRuleInterface $expddRuleInterface
    * @param \Magento\Framework\Message\ManagerInterface $messageManager
    * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
    */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ExpddRuleRepoInterface $expddRuleRepoInterface,
        ExpddRuleInterface $expddRuleInterface,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->expddRuleRepoInterface = $expddRuleRepoInterface;
        $this->messageManager = $messageManager;
        $this->expddRuleInterface = $expddRuleInterface;
        $this->helper = $helper;
        $this->sellerId = 0;
        parent::__construct($context);
    }
    
    /**
     * Execute
     *
     * @return PageFactory
     */
    public function execute()
    {
        $this->sellerId = $this->helper->getSellerId();
        if ($this->sellerId) {
            $data = $this->captureRequestData();
            $this->expddRuleRepoInterface->save($data);
            $this->messageManager->addSuccess(__('Rule has been saved successfully.'));
            $this->_redirect("*/*/index");
            return true;
        }
        $this->messageManager->addError(__('Invalid Request.'));
        $this->_redirect("*/*/index");
        return true;
    }

    /**
     * Capture Request Data
     *
     * @return ExpddRuleInterface
     */
    private function captureRequestData(): ExpddRuleInterface
    {
        $wholeData    = $this->getRequest()->getParams();
        if (!empty($wholeData['entity_id'])) {
            $wholeData['id'] = $wholeData['entity_id'];
        }
        unset($wholeData['entity_id']);
        $wholeData['seller_id'] = $this->sellerId;
        $expddRuleInterface = $this->expddRuleInterface;

        foreach ($wholeData as $key => $data) {
            $method = 'set'.str_replace('_', '', ucwords($key, '_'));
            $expddRuleInterface->$method($this->parseData($data));
        }
        if ($wholeData['delivery_type'] === 'specific') {
            $expddRuleInterface->setDaysTo('');
        }
        return $expddRuleInterface;
    }

    /**
     * Parse Data
     *
     * @param [type] $data
     * @return string
     */
    private function parseData($data): string
    {
        if (is_array($data)) {
            return implode(',', $data);
        }

        if (is_string($data)) {
            return trim($data);
        }

        return $data;
    }
}
