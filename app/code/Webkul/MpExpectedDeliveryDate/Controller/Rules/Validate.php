<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Rules;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterfaceFactory;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionZipRepoInterfaceFactory;

class Validate extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ExpddRegionZipRepoInterfaceFactory $expddRegionZipRepo
     * @param ExpddConfigRepoInterfaceFactory $expddConfigZipRepo
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ExpddRegionZipRepoInterfaceFactory $expddRegionZipRepo,
        ExpddConfigRepoInterfaceFactory $expddConfigZipRepo,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->expddRegionZipRepo = $expddRegionZipRepo;
        $this->expddConfigZipRepo = $expddConfigZipRepo;
        $this->helper = $helper;
        $this->coreSession = $coreSession;
        $this->jsonResultFactory = $jsonResultFactory;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $request = $this->getRequest()->getParams();
        
        if (empty($request['sellerId']) && !empty($request['fromCheckoutPage']) && $request['fromCheckoutPage']) {
            $sellerId = $this->helper->getSellerIdFromProductId($request['productId']);
        } else {
            $sellerId = $request['sellerId'];
        }
        $config = $this->expddConfigZipRepo->create()->getBySellerId($sellerId);
        if (!$config || !$config->getStatus()) {
            $result = $this->jsonResultFactory->create();
            $data['message'] = "";
            $result->setData($data);
            return $result;
        }
        $hasSellerZipcode = $this->expddRegionZipRepo->create()
                            ->getRuleByZipAndSellerId($sellerId, (int)$request['zipcode'])
                            ->getFirstItem();
        $data = [];
        if (empty($hasSellerZipcode->getData())) {
            $data['message'] = __(trim($config->getTextForNotAvailable()));
        } else {
            $_data = $hasSellerZipcode;
            $deliveryType = $_data->getDeliveryType();
            $dayFrom = $_data->getDaysFrom();
            $dayTo = $_data->getDaysTo();
            $estimationType = $_data->getEstimationType();
            $response = $this->getMessages(
                $deliveryType,
                $dayFrom,
                $dayTo,
                $estimationType,
                $config
            );
            $data['message'] = __(trim($response));
        }
        $this->storeInSession($request['productId'], $request['zipcode'], $data['message']);
        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }

    /**
     * getMessages
     *
     * @param string $deliveryType
     * @param string $dayFrom
     * @param string $dayTo
     * @param string $estimationType
     * @param ExpddConfigRepoInterfaceFactory $config
     * @return string
     */
    private function getMessages($deliveryType, $dayFrom, $dayTo, $estimationType, $config)
    {
        $rangeMsg = $config->getTextForRange();
        $specificMsg = $config->getTextForSpecific();
        $holidays = $config->getHolidays();
        $weekends = explode(',', $config->getWeekends());
        $holidays = is_string($holidays)?$this->helper->jsonDecode($holidays):[];
        $days = $this->validateSkippedDays($holidays, $weekends, $dayFrom, $dayTo);
        $dayFrom = $days['dayFrom'];
        $dayTo = $days['dayTo'];
        if ($estimationType === 'in_dates') {
            $dayFrom = $dayFrom?date("d/M/Y", strtotime("+$dayFrom day")):'';
            $dayTo = $dayTo?date("d/M/Y", strtotime("+$dayTo day")):'';
        }
        if ($deliveryType === 'range') {
            $response = str_replace('%%from%%', $dayFrom, $rangeMsg);
            $finalResponse = str_replace('%%to%%', $dayTo, $response);
            return $finalResponse;
        } else {
            $finalResponse = str_replace('%%to%%', $dayFrom, $specificMsg);
            return $finalResponse;
        }
    }

    /**
     * Store in Session
     *
     * @param string $productId
     * @param string $zipCode
     * @return void
     */
    private function storeInSession($productId, $zipCode, $msg)
    {
        $previousZipEntries =  $this->coreSession->getExpddZip();
        $previousZipEntries[$productId] = $zipCode;
        $this->coreSession->setExpddZip($previousZipEntries);

        $previousMsgEntries =  $this->coreSession->getExpddMsg();
        $previousMsgEntries[$productId] = $msg;
        $this->coreSession->setExpddMsg($previousMsgEntries);
    }

    /**
     * Validate Holidays and Weekend
     *
     * @param array $holidays
     * @param array $weekends
     * @param int $dayFrom
     * @param int $dayTo
     * @return array
     */
    private function validateSkippedDays($holidays, $weekends, $dayFrom, $dayTo)
    {
        $holidayDates = [];
        foreach ($holidays as $holiday) {
            if ($holiday['range_type'] === 'range') {
                $dateFrom = strtotime(str_replace('/', '-', $holiday['date_from']));
                $dateTo = strtotime(str_replace('/', '-', $holiday['date_to']));
                for ($currentDate = $dateFrom; $currentDate <= $dateTo; $currentDate += (86400)) {
                    $holidayDates[] = $currentDate;
                }
            } else {
                $currentDate = strtotime(str_replace('/', '-', $holiday['date_from']));
                $holidayDates[] = $currentDate;
            }
        }
        $holidayDates = array_unique($holidayDates);
        $_dayFrom = $dayFrom-1;
        $_dayTo = $dayTo-1;
        $_dayFrom = strtotime("+$_dayFrom day");
        $_dayTo = $dayTo?strtotime("+$_dayTo day"):null;
        $now = strtotime(date('d-m-Y', time()));
        for ($i = $now; $i <= $_dayFrom; $i = $i + 86400) {
            if (in_array($i, $holidayDates) || in_array(strtolower(date('l', $i)), $weekends)) {
                $_dayFrom+=86400;
                $dayFrom++;
                if ($_dayTo) {
                    $_dayTo+=86400;
                    $dayTo++;
                }
            }
        }
        if ($dayTo) {
            for ($i = $_dayFrom; $i <= $_dayTo; $i = $i + 86400) {
                if (in_array($i, $holidayDates) || in_array(strtolower(date('l', $i)), $weekends)) {
                    $_dayTo+=86400;
                    $dayTo++;
                }
            }
        }
        return [
            'dayFrom' => $dayFrom,
            'dayTo' => $dayTo
        ];
    }
}
