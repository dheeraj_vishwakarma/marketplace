<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Region;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionRepoInterfaceFactory;

class Zipcodes extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param Context $context
     * @param \Webkul\Marketplace\Helper\Data $helperData
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @param ExpddRegionRepoInterfaceFactory $regionRepoFactory
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        \Webkul\Marketplace\Helper\Data $helperData,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        ExpddRegionRepoInterfaceFactory $regionRepoFactory,
        PageFactory $resultPageFactory
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->helperData = $helperData;
        $this->coreSession = $coreSession;
        $this->regionRepoFactory = $regionRepoFactory;
        parent::__construct($context);
    }
    
    /**
     * Execute
     *
     * @return PageFactory
     */
    public function execute()
    {
        $helper = $this->helperData;
        $isPartner = $helper->isSeller();
        $regionId = $this->getRequest()->getParam('region_id');
        $this->coreSession->unsRegionId();
        $this->coreSession->setRegionId($regionId);
        if ($isPartner == 1) {
            /** @var \Magento\Framework\View\Result\Page $resultPage */
            $resultPage = $this->_resultPageFactory->create();
            if ($helper->getIsSeparatePanel()) {
                $resultPage->addHandle('mpexpecteddate_layout2_region_zipcodes');
            }
            $region = $this->regionRepoFactory->create()->getById($regionId);
            $resultPage->getConfig()->getTitle()->set(
                $region->getName(). ' '.__(' Zipcodes')
            );
            
            return $resultPage;
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}
