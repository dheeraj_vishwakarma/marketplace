<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Adminhtml\Configuration;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Index extends Action
{

    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterface $expddConfigRepo
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterface $expddConfigRepo,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->resultForwardFactory = $resultForwardFactory;
        $this->expddConfigRepo = $expddConfigRepo;
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Webkul_MpExpectedDeliveryDate::config");
    }

    /**
     * Execute
     *
     * @return PageFactory
     */
    public function execute()
    {
        $hasData = $this->expddConfigRepo->getBySellerId(0)->getId();
        $id = $this->getRequest()->getParam('id');

        if ($hasData && !$id) {
            $this->_redirect("*/*/index/id/$hasData");
            return true;
        }
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Webkul_MpExpectedDeliveryDate::config');
        $resultPage->getConfig()->getTitle()->prepend(__('General Settings'));
        return $resultPage;
    }
}
