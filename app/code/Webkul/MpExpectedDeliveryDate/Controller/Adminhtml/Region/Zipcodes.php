<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Adminhtml\Region;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionRepoInterfaceFactory;

class Zipcodes extends Action
{

    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ExpddRegionRepoInterfaceFactory $regionRepoFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ExpddRegionRepoInterfaceFactory $regionRepoFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->regionRepoFactory = $regionRepoFactory;
        $this->resultForwardFactory = $resultForwardFactory;
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Webkul_MpExpectedDeliveryDate::region");
    }

    /**
     * Execute
     *
     * @return PageFactory
     */
    public function execute()
    {
        $regionId = $this->getRequest()->getParam('region_id');
        $region = $this->regionRepoFactory->create()->getById($regionId);
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Webkul_MpExpectedDeliveryDate::region');
        $resultPage->getConfig()->getTitle()->prepend($region->getName(). ' '.__('Zipcodes'));
        return $resultPage;
    }
}
