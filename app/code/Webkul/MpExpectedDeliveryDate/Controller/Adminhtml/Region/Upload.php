<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Adminhtml\Region;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Result\PageFactory;

class Upload extends Action
{

    /**
     * Image uploader
     *
     * @var \Magento\Catalog\Model\Uploader
     */
    protected $uploader;

    /**
     * Upload constructor.
     *
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Catalog\Model\Uploader $uploader
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Webkul\MpExpectedDeliveryDate\Model\Uploader $uploader
    ) {
        parent::__construct($context);
        $this->uploader = $uploader;
    }

    /**
     * Check admin permissions for this controller
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpExpectedDeliveryDate::region');
    }

    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $imageArr = (array)$this->getRequest()->getFiles();
        foreach ($imageArr as $name => $data) {
            try {
                $result = $this->uploader->saveFileToTmpDir($name);
    
                $result['cookie'] = [
                    'name' => $this->_getSession()->getName(),
                    'value' => $this->_getSession()->getSessionId(),
                    'lifetime' => $this->_getSession()->getCookieLifetime(),
                    'path' => $this->_getSession()->getCookiePath(),
                    'domain' => $this->_getSession()->getCookieDomain(),
                ];
            } catch (\Exception $e) {
                $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
            }
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
    /**
     * @return \Webkul\MpSellAsBrand\Model\Uploader
     */
    public function returnUploader()
    {
        return $this->uploader;
    }
}
