/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

define([
    'jquery',
    'Magento_Ui/js/form/element/select'
], function ($, Select) {
    'use strict';

    $(document).ready(function(){
        setTimeout(()=> handleEle(), 500)
        $(document).on('change', 'select[name="delivery_type"]', function(){
            handleEle();
        });

        function handleEle(){
            var selectEle = $('select[name="delivery_type"]');
            let ele = $(".estimation_day_to");
            if (selectEle.val() === 'specific') {
                ele.css('display', 'none');
            } else {
                ele.css('display', 'block');
            }
        }
    })

    
    return Select.extend({
    });
});