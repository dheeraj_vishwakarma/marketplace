require([
    'jquery',
    'mage/translate',
    'Magento_Ui/js/lib/validation/validator',
], function ($, $t, validator) {
    $(document).ready(function(){
        validator.addRule(
            'greaterThan',
            function (value, element, param) {
                var $otherElement = $(element);
                return parseInt(value, 10) > parseInt($otherElement.val(), 10);
            },
            $t('Value must be more than Day From')
        );
        
    })
});