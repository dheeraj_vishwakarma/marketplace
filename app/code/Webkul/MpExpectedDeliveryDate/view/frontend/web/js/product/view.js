/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define(
    [
        'jquery',
        'mage/translate',
        "jquery/ui",
        'jquery/validate',
        "mage/calendar"
    ],
    function ($, $t) {
        'use strict';
        
        $.widget('mage.productView', { 
                options: {
                },

                /**
                 * execution starts
                 */
                _create: function () {
                    this.registerEvents();
                },

                /**
                 * register document events
                 */
                registerEvents: function(){
                    let self = this;
                    $("#validateExpdd").on("click", function(){
                        self.validateRequest()
                    });
                    let inputEle =  $("#wk-mpexpdd-regex");
                    let zipcode = self.options.zipcode;

                    if (zipcode) {
                        inputEle.val(zipcode);
                        self.validateRequest();
                    }
                },

                /**
                 * validate request
                 */

                validateRequest: function(){
                    var zipcode = $("#wk-mpexpdd-regex").val();
                    let regexp = /^[a-z\d\-_\s]+$/i;
                    $('.error-block').html("");
                    if (zipcode == "") {   
                        $('.error-block').html($.mage.__('Zip code is required'));
                        return;
                    } else if (!regexp.test(zipcode)) {
                        wkZcvRegexElement.innerHTML = $.mage.__('Invalid format');
                        return true;
                    } else if (zipcode.length > 6) {
                        wkZcvRegexElement.innerHTML = $.mage.__('Invalid Zipcode. Should be only of 6 digits')
                    }

                    this.httpRequest(zipcode);
                },

                httpRequest : function(zipcode = null) {
                    let loader = $(".loader");
                    loader.css('display', 'inline-block');
                    let self = this;
                    let url = self.options.url;
                    let data = {
                        sellerId : self.options.sellerId,
                        zipcode: zipcode,
                        productId : self.options.productId
                    };
                    $.ajax(
                        {
                            url : url,
                            data : data,
                            form_key: jQuery.cookie('form_key'),
                            type : 'POST',

                            success : function(result) {
                                $("#wk-mpexpdd-delivery-detail").html(result.message);
                                loader.css('display', 'none');
                            }
                        }
                    )
                }
        });
        return $.mage.productView;
    }
);