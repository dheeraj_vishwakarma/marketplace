/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define(
    [
        'jquery',
        'mage/translate',
        "jquery/ui",
        'jquery/validate',
        "mage/calendar"
    ],
    function ($, $t) {
        'use strict';
        
        $.widget('mage.productView', { 
                options: {
                },

                /**
                 * execution starts
                 */
                _create: function () {
                    this.registerEvents();
                },

                /**
                 * register document events
                 */
                registerEvents: function(){
                    let self = this;
                    $('.blocks').each(function (k, item) {
                        let productId = $(this).data('productid');
                        let sellerId = $(this).data('sellerid');
                        let inputEle =  $(`#wk-mpexpdd-zipform-${productId}`);
                        let zipcode = inputEle.val();
                        if (zipcode) {
                            self.validateRequest(productId, sellerId);
                        }
                    })

                    $(document).on("click", '.validateExpdd', function(){
                        let productId = $(this).data('productid');
                        let sellerId = $(this).data('sellerid');
                        self.validateRequest(productId, sellerId)
                    });
                },

                /**
                 * validate request
                 */

                validateRequest: function(productId, sellerId){
                    let self = this;
                    var zipcode = $(`#wk-mpexpdd-zipform-${productId}`).val();
                    let regexp = /^[a-z\d\-_\s]+$/i;
                    let errorBlockEle = $(`#error-block-${productId}`);
                    errorBlockEle.html("");
                    if (zipcode == "") {   
                        errorBlockEle.html($.mage.__('Zip code is required'));
                        return;
                    } else if (!regexp.test(zipcode)) {
                        errorBlockEle.html($.mage.__('Invalid format'));
                        return true;
                    } else if (zipcode.length > 6) {
                        errorBlockEle.html($.mage.__('Invalid Zipcode. Should be only of 6 digits'));
                    }

                    this.httpRequest(zipcode, productId, sellerId);
                },

                httpRequest : function(zipcode = null, productId, sellerId) {
                    let self = this;
                    let loader = $(`#loader-${productId}`);
                    loader.css('display', 'inline-block');
                    let url = self.options.url;
                    let data = {
                        sellerId : sellerId,
                        zipcode: zipcode,
                        productId : productId
                    };
                    $.ajax(
                        {
                            url : url,
                            data : data,
                            form_key: jQuery.cookie('form_key'),
                            type : 'POST',

                            success : function(result) {
                                $(`#wk-mpexpdd-delivery-detail-${productId}`).html(result.message);
                                loader.css('display', 'none');
                            }
                        }
                    )
                }
        });
        return $.mage.productView;
    }
);