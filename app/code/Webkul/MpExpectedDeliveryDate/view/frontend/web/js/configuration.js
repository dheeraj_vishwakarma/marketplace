/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define(
    [
        'jquery',
        'mage/translate',
        "jquery/ui",
        'jquery/validate',
        "mage/calendar",
        "domReady!"
    ],
    function ($, $t) {
        'use strict';
        
        $.widget('mage.expddConfiguration', { 
                options: {
                    removeText : $t("Remove"),
                },

                /**
                 * execution starts
                 */
                _create: function () {
                    this.registerEvents();
                    this.renderSavedData();
                },
                /**
                 * 
                 * initialize calender element
                 */
                initCalenderElement: function(elements, dates, format='dd/mm/yy', isChangeYear=true){
                    var self = this;
                    $.each(elements, function(key, element){
                        var minDate = new Date();
                        if (element.includes('-to')) {
                            let dateFrom = dates[(key-1)];
                            dateFrom = dateFrom||self.getTodayDate(true, true);
                            dateFrom = dateFrom.split('/');
                            minDate = new Date(dateFrom[1]+'/'+dateFrom[0]+'/'+dateFrom[2]);
                            if (minDate.getTime() < new Date().getTime()) {
                                minDate = new Date();
                            }
                        }
                        $(element).calendar({
                            changeMonth: true,
                            changeYear: isChangeYear,
                            showButtonPanel: true,
                            dateFormat: format,
                            minDate : minDate,
                            currentText: $t('Go Today'),
                            closeText: $t('Close')
                          });
                          $(element).val(dates[key]||self.getTodayDate(isChangeYear, element.includes('-to')));
                    });
                },
                
                /**
                 * 
                 * register elements events 
                 */
                registerEvents: function(){
                    let self = this;

                    $("form").validate();
                    $('textarea').tooltip({trigger :'focus', title: 'Password tooltip', placement: "top"});
                    $(".add-row").on('click', function(event){
                        event.preventDefault();
                        var ele = $(this);
                        let records = Number(ele.attr('data-records')||0);
                        self.addNewRow(records)
                        ele.attr('data-records', records+1);
                    });

                    $(document).on('change', '.range-types', function(){
                        self.rangeTypeTriggered(this);
                    })

                    $(document).on('click', '.remove', function(event){
                        event.preventDefault();
                        self.removeItemTriggered(this);
                    });

                    $('#save-btn').click(function (event) {
                        self.validateAndSubmit(event);
                    });
                },

                /**
                 * add new row
                 */
                addNewRow: function(records, data = {}){
                    let row = `<tr><td>${this.getRangeTypesSelect(records, data.range_type||null)}</td><td class='date-selector-section'>${this.getDateSelectors(records, data)}</td><td>${this.getActionButton(records)}</td></tr>`;
                    $("#holiday-records").append(row);
                },

                /**
                 * creating range types HTML
                 */
                getRangeTypesSelect: function(index, selectedType='specific'){
                    let rangeTypes = JSON.parse(this.options.rangeTypes);
                    let options = 
                    Object.keys(rangeTypes).map(
                        (key) => 
                           `<option ${key===selectedType?'selected="selected"':''} value="${key}">${rangeTypes[key]}</option>`
                        );
                    return `<select class="range-types" data-form-part="config_index_form" name="range_type[${index}]" data-index='${index}' id="range-types-${index}">${options}</select>`
                },

                 /**
                 * creating Action Button HTML
                 */
                getActionButton: function(index){
                    return `<button class="remove" id='remove-${index}'>${this.options.removeText}</button>`
                },

                /**
                 * creating Date inputs HTML
                 */
                getDateSelectors: function(index, data){
                    let self = this;
                    let type = data.range_type||'specific';
                    let start_from = data.date_from||null;
                    let start_to = data.date_to||null;
                    let element = '<div class="date-selector-block">';
                    switch (type) {
                        case 'specific':
                            element+=`<input data-form-part="config_index_form" type="text" name="date_from[${index}]" class="date-selector" data-index='${index}' id='date-selector-${index}'/></div>`;
                            setTimeout(() => self.initCalenderElement([`#date-selector-${index}`], [start_from]), 200)
                            break;

                        case 'range':  
                            element+=`<input data-form-part="config_index_form" type="text" name="date_from[${index}]" class="date-selector date-selector-from" data-index='${index}' id='date-selector-from-${index}'/><input data-form-part="config_index_form" type="text" name="date_to[${index}]" class="date-selector date-selector-to" data-index='${index}' id='date-selector-to-${index}'/></div>`;
                            setTimeout(() => self.initCalenderElement([
                                `#date-selector-from-${index}`,
                                `#date-selector-to-${index}`
                            ], [start_from, start_to]), 200)
                            break;  
                            
                        case 'recurring': 
                            element+=`<input data-form-part="config_index_form" type="text" name="date_from[${index}]" class="date-selector" data-index='${index}' id='date-selector-${index}'/></div>`;
                            setTimeout(() => self.initCalenderElement([`#date-selector-${index}`], [start_from], 'dd/mm', false), 200);
                            break; 
                    }

                    return element;
                },

                /**
                 * changing range types
                 */
                rangeTypeTriggered: function(element){
                    let dateSelector = $(element).parent().parent().find('.date-selector-section');
                    dateSelector.html("");
                    let index = $(element).attr('data-index');
                    let html = this.getDateSelectors(index, {range_type:element.value});
                    dateSelector.html(html);
                },

                /**
                 * removing row
                 */
                removeItemTriggered: function(element){
                    $(element).parent().parent().remove()
                },

                /**
                 * 
                 * @param {boolean} isShowYear 
                 * @param {boolean} isDateRangeToElement 
                 */
                getTodayDate: function(isShowYear, isDateRangeToElement){
                    let date = isDateRangeToElement?new Date(new Date().getTime() + 24 * 60 * 60 * 1000):new Date;
                    return ("0" + date.getDate()).slice(-2)+'/'+
                            (("0" + (date.getMonth() + 1)).slice(-2))+
                            (isShowYear?'/'+date.getFullYear():'');
                },

                /**
                 * render saved data
                 */
                renderSavedData: function(){
                    let self = this;
                    var holidays = JSON.parse(self.options.holidays);
                    if (typeof holidays === 'object') {
                        holidays = Object.values(holidays)
                    }
                    if (holidays.length > 0) {
                        $.each(holidays, function(key, holiday){
                            self.addNewRow(key, holiday);
                        })
    
                        $(".add-row").attr('data-records', (holidays.length));
                    }
                },

                /**
                 * validate and submit
                 */
                validateAndSubmit: function(event){
                    let form = $("#expdd-configuration");
                    let isValid = form.valid();
                    if(isValid){
                        form.submit();
                    }else {
                        event.preventDefault();
                    }
                },

                /* setDateRangeMinDate : function(){
                    $(document).on('change', '.date-selector-from', function() {
                        let toEle = $(this).next('.date-selector-to');
                        let dateFrom = $(this).val().split('/');
                        let minDate = new Date(dateFrom[1]+'/'+dateFrom[0]+'/'+dateFrom[2]);
                        $(toEle).calendar().minDate(new Date(minDate));
                    })
                } */
        });
        return $.mage.expddConfiguration;
    }
);