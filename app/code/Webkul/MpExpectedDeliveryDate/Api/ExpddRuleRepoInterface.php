<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Api;

/**
 * ExpddRuleRepo Interface
 * Expected Delivery Date Rule Repository
 */
interface ExpddRuleRepoInterface
{

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRule
     */
    public function getById($id);
    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRule
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddRule $subject);
    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria);
    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRule $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddRule $subject);
    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id);

    /**
     * get Seller rules
     *
     * @param int $id
     * @return \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRule\Collection
     */
    public function getBySellerId($sellerId);

    /**
     * get all
     *
     * @param int $id
     * @return \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRule\Collection
     */
    public function getAll();
}
