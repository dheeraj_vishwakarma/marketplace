<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Api\Data;

interface ExpddRegionZipcodeInterface
{

    const ENTITY_ID = 'entity_id';

    const SELLER_ID = 'seller_id';

    const REGION_ID = 'region_id';

    const ZIP_FROM = 'zip_from';

    const ZIP_TO = 'zip_to';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
     */
    public function setEntityId($entityId);
    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId();
    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
     */
    public function setSellerId($sellerId);
    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId();
    /**
     * Set RegionId
     *
     * @param int $regionId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
     */
    public function setRegionId($regionId);
    /**
     * Get RegionId
     *
     * @return int
     */
    public function getRegionId();
    /**
     * Set ZipFrom
     *
     * @param string $zipFrom
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
     */
    public function setZipFrom($zipFrom);
    /**
     * Get ZipFrom
     *
     * @return string
     */
    public function getZipFrom();
    /**
     * Set ZipTo
     *
     * @param string $zipTo
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
     */
    public function setZipTo($zipTo);
    /**
     * Get ZipTo
     *
     * @return string
     */
    public function getZipTo();
    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
     */
    public function setCreatedAt($createdAt);
    /**
     * Get CreatedAt
     *
     * @return string
     */
    public function getCreatedAt();
    /**
     * Set UpdatedAt
     *
     * @param string $updatedAt
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
     */
    public function setUpdatedAt($updatedAt);
    /**
     * Get UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt();
}
