<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 */

namespace Webkul\MpExpectedDeliveryDate\Model\Config\Source;

class Weekends implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ["value"=>'sunday', "label"=>__("Sunday")],
            ["value"=>'monday', "label"=>__("Monday")],
            ["value"=>'tuesday', "label"=>__("Tuesday")],
            ["value"=>'wednesday', "label"=>__("Wednesday")],
            ["value"=>'thursday', "label"=>__("Thursday")],
            ["value"=>'friday', "label"=>__("Friday")],
            ["value"=>'saturday', "label"=>__("Saturday")]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'sunday'=>__("Sunday"),
            'monday'=>__("Monday"),
            'tuesday'=>__("Tuesday"),
            'wednesday'=>__("Wednesday"),
            'thursday'=>__("Thursday"),
            'friday'=>__("Friday"),
            'saturday'=>__("Saturday")
        ];
    }
}
