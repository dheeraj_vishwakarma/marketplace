<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 */

namespace Webkul\MpExpectedDeliveryDate\Model\Config\Source;

class Rangetype implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ["value"=>'specific', "label"=>__("Specific")],
            ["value"=>'range', "label"=>__("Range from-to")],
            ["value"=>'recurring', "label"=>__("Recurring")]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'specific'=>__("Specific"),
            'range'=>__("Range from-to"),
            'recurring'=>__("Recurring")
        ];
    }
}
