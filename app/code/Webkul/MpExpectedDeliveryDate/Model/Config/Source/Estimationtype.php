<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 */

namespace Webkul\MpExpectedDeliveryDate\Model\Config\Source;

class Estimationtype implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ["value"=>'in_days', "label"=>__("Estimation in Days")],
            ["value"=>'in_dates', "label"=>__("Estimation in Dates")]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'in_days'=>__("Estimation in Days"),
            'in_dates'=>__("Estimation in Dates")
        ];
    }
}
