<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddConfig;

/**
 * Collection Class
 * Expected Delivery Date Cofig Collection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{

    protected $_idFieldName = 'entity_id';

    /**
     * Initialize resource model
     *
     * @return void
     */
    public function _construct()
    {
        $this->_init(
            \Webkul\MpExpectedDeliveryDate\Model\ExpddConfig::class,
            \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddConfig::class
        );
        $this->_map['fields']['entity_id'] = 'main_table.entity_id';
    }
}
