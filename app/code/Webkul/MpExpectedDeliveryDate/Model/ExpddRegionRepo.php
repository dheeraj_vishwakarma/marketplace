<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Model;

use \Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;

class ExpddRegionRepo implements \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionRepoInterface
{

    protected $modelFactory = null;

    protected $collectionFactory = null;

    /**
     * initialize
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRegionFactory $modelFactory
     * @param
     * Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegion\CollectionFactory
     * $collectionFactory
     * @return void
     */
    public function __construct(
        \Webkul\MpExpectedDeliveryDate\Model\ExpddRegionFactory $modelFactory,
        \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegion\CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->modelFactory = $modelFactory;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegion
     */
    public function getById($id)
    {
        $model = $this->modelFactory->create()->load($id);
        if (!$model->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
                __('The CMS block with the "%1" ID doesn\'t exist.', $id)
            );
        }
        return $model;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegion
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegion $subject)
    {
        try {
            $subject->save();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($exception->getMessage()));
        }
         return $subject;
    }

    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria)
    {
        $collection = $this->collectionFactory->create();
        
        return $this->collectionProcessor->process($creteria, $collection);
    }

    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRegion $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegion $subject)
    {
        try {
            $subject->delete();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
}
