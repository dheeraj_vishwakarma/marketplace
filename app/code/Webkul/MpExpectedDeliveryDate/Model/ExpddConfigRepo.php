<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Model;

/**
 * ExpddConfigRepo Class
 * Config Repo
 */
class ExpddConfigRepo implements \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterface
{

    protected $modelFactory = null;

    protected $collectionFactory = null;

    /**
     * initialize
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddConfigFactory $modelFactory
     * @param
     * Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddConfig\CollectionFactory
     * $collectionFactory
     * @return void
     */
    public function __construct(
        \Webkul\MpExpectedDeliveryDate\Model\ExpddConfigFactory $modelFactory,
        \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddConfig\CollectionFactory $collectionFactory
    ) {
        $this->modelFactory = $modelFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddConfig
     */
    public function getById($id)
    {
        $model = $this->modelFactory->create()->load($id);
        if (!$model->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
                __('The CMS block with the "%1" ID doesn\'t exist.', $id)
            );
        }
        return $model;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddConfig
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddConfig $subject)
    {
        try {
             $subject->save();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($exception->getMessage()));
        }
         return $subject;
    }

    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria)
    {
        $collection = $this->collectionFactory->create();
         return $collection;
    }

    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddConfig $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddConfig $subject)
    {
        try {
            $subject->delete();
        } catch (\Exception $exception) {
             throw new \Magento\Framework\Exception\CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * Get Configuration By Seller Id
     *
     * @param integer $sellerId
     * @return \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddConfig\Collection|null
     */
    public function getBySellerId($sellerId)
    {
        try {
            $collection = $this->collectionFactory->create();
            return $collection->addFieldToFilter('seller_id', ['eq'=>$sellerId])
            ->getFirstItem();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\LocalizedException($exception->getMessage());
        }
    }
}
