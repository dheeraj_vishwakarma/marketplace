<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Plugin\Quote;

use Closure;

class QuoteToOrderItem
{
    /**
     * @param \Magento\Framework\Logger\Monolog $logger
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     */
    public function __construct(
        \Magento\Framework\Logger\Monolog $logger,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
    ) {
        $this->coreSession = $coreSession;
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item\ToOrderItem $subject
     * @param callable $proceed
     * @param \Magento\Quote\Model\Quote\Item\AbstractItem $item
     * @param array $additional
     * @return \Magento\Sales\Model\Order\Item
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function aroundConvert(
        \Magento\Quote\Model\Quote\Item\ToOrderItem $subject,
        Closure $proceed,
        \Magento\Quote\Model\Quote\Item\AbstractItem $item,
        $additional = []
    ) {
        /** @var $orderItem \Magento\Sales\Model\Order\Item */
        $orderItem = $proceed($item, $additional);
        $productId = $item->getProduct()->getId();
        $data = $this->coreSession->getExpddMsg();
        if (!empty($data[$productId])) {
            $orderItem->setAdditionalData($this->helper->jsonEncode(["mpexpdd" => $data[$productId]]));
        }
        $this->coreSession->unsExpddMsg();
        $this->coreSession->unsExpddZip();
        return $orderItem;
    }
}
