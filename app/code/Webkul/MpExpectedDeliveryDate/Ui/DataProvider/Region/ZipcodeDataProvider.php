<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 */

namespace Webkul\MpExpectedDeliveryDate\Ui\DataProvider\Region;

use Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegionZipcode\CollectionFactory;

class ZipcodeDataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $session;
    protected $collection;
    protected $loadedData;
    /**
     * DataProvider
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface,
        \Magento\Framework\App\RequestInterface $request,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->collection->addFieldToSelect("*");
        $this->helper = $helper;
        $this->sessionManagerInterface = $sessionManagerInterface;
        $this->request = $request;
    }

    /**
     * Get Session
     *
     * @return \Magento\Framework\Session\SessionManagerInterface
     */
    protected function getSession()
    {
        if ($this->session === null) {
            $this->session = $this->sessionManagerInterface;
        }
        return $this->session;
    }

    /**
     * get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $this->loadedData = [];
        $sellerId = $this->helper->getSellerId();
        $regionId = $this->getSession()->getRegionId();
        $this->loadedData = $this->collection
                    ->addFieldToFilter('seller_id', ["eq"=>$sellerId])
                    ->addFieldToFilter('region_id', ["eq"=>$regionId])->toArray();
                    
        $data = $this->getSession()->getConfigFormData();
        if (!empty($data)) {
            $id = !empty($data['zip_listing']['entity_id'])
            ? $data['zip_listing']['entity_id'] : null;
            $this->loadedData[$id] = $data;
            $this->getSession()->unsDataFormData();
        }
        return $this->loadedData;
    }
}
