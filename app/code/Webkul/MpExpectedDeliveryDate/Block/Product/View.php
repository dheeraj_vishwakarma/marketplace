<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Product;

use \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterfaceFactory;

use Magento\Catalog\Block\Product\AbstractProduct;

class View extends AbstractProduct
{
    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param ExpddConfigRepoInterfaceFactory $expddConfigRepo
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param \Magento\Customer\Model\Address $address
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        ExpddConfigRepoInterfaceFactory $expddConfigRepo,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        \Magento\Customer\Model\Address $address,
        array $data = []
    ) {
        $this->expddConfigRepo = $expddConfigRepo;
        $this->address = $address;
        $this->helper = $helper;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Get Product Seller Id
     *
     * @return void
     */
    public function getProductSellerId()
    {
        $id = $this->getProduct()->getId();
        return $this->helper->getSellerIdFromProductId($id);
    }

    /**
     * is Cofgurations enabled for seller
     *
     * @return boolean
     */
    public function isEnable($sellerId)
    {
        $config = $this->expddConfigRepo->create();

        $status = $config->getBySellerId($sellerId);
        if (!empty($status)) {
            return $status->getStatus();
        }

        return false;
    }

    /**
     * Get Postal code
     *
     * @return void
     */
    public function getPostalCode()
    {
        $customerAddressId = $this->helper->getCustomerAddress();
        return $customerAddressId?$this->address->load($customerAddressId)->getPostcode():null;
    }
}
