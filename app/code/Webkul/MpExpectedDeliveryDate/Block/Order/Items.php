<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

/**
 * @author     Magento Core Team <core@magentocommerce.com>
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Order;

/**
 * Sales order view items block.
 *
 * @api
 * @since 100.0.2
 */
class Items extends \Magento\Sales\Block\Order\Items
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param array $data
     * @param \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $itemCollectionFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        array $data = [],
        \Magento\Sales\Model\ResourceModel\Order\Item\CollectionFactory $itemCollectionFactory = null
    ) {
        $this->helper = $helper;
        parent::__construct($context, $registry, $data, $itemCollectionFactory);
    }
}
