<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Adminhtml\RegionZipcodes;

use Webkul\MpExpectedDeliveryDate\Block\Adminhtml\Configuration\GenericButton;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class SaveButton
 * Save Button
 */
class EditButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $id = $this->request->getParam('region_id');
        return [
            'label' => __('Edit Region'),
            'class' => 'edit primary',
            'on_click' => sprintf("location.href = '%s';", $this->getUrl("*/*/edit/id/".$id)),
            'sort_order' => 90,
        ];
    }
}
