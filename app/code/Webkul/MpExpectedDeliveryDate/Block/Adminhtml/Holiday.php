<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Adminhtml;

use \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterface;

class Holiday extends \Magento\Backend\Block\Template
{
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Rangetype $rangeType
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param ExpddConfigRepoInterface $expddConfigRepoInterface
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Rangetype $rangeType,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        ExpddConfigRepoInterface $expddConfigRepoInterface,
        array $data = []
    ) {
        $this->rangeType = $rangeType;
        $this->helper = $helper;
        $this->expddConfigRepoInterface = $expddConfigRepoInterface;
        parent::__construct($context, $data);
    }

    /**
     * Get Range Type
     *
     * @return array
     */
    public function getRangeTypes(): array
    {
        return $this->rangeType->toArray();
    }

    /**
     * Get Holidays
     *
     * @return void
     */
    public function getHolidays()
    {
        $id = $this->getRequest()->get('id')??0;
        return $id?$this->expddConfigRepoInterface->getById($id)->getHolidays():'[]';
    }
}
