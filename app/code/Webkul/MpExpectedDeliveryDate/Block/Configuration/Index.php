<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Configuration;

use \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterface;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Rangetype $rangeType
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param ExpddConfigRepoInterface $expddConfigRepoInterface
     * @param \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Weekends $weekends
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Rangetype $rangeType,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        ExpddConfigRepoInterface $expddConfigRepoInterface,
        \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Weekends $weekends,
        array $data = []
    ) {
        $this->rangeType = $rangeType;
        $this->helper = $helper;
        $this->expddConfigRepoInterface = $expddConfigRepoInterface;
        $this->weekends = $weekends;
        parent::__construct($context, $data);
    }

    /**
     * Get Range Type
     *
     * @return array
     */
    public function getRangeTypes(): array
    {
        return $this->rangeType->toArray();
    }

    /**
     * Get Holidays
     *
     * @return string
     */
    public function getHolidays(): ?string
    {
        $id = $this->helper->getSellerId();
        $response = null;
        if ($id) {
            $response = $this->expddConfigRepoInterface->getBySellerId($id)->getHolidays();
        }
        return $response??'[]';
    }
    
    /**
     * Get Holidays
     *
     * @return array
     */
    public function getFormData(): array
    {
        $id = $this->helper->getSellerId();
        return $id?$this->expddConfigRepoInterface->getBySellerId($id)->getData():[];
    }

    /**
     * Get Weekends
     *
     * @return array
     */
    public function getWeekends(): array
    {
        return $this->weekends->toArray();
    }
}
