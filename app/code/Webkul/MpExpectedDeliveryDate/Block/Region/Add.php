<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Region;

use \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionRepoInterface;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRuleRepoInterface;

class Add extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param ExpddRegionRepoInterface $expddRegionRepoInterface
     * @param ExpddRuleRepoInterface $expddRuleRepoInterface
     * @param \Magento\Directory\Block\Data $directoryBlock
     * @param \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory
     * @param \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Estimationtype $estimationType
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        ExpddRegionRepoInterface $expddRegionRepoInterface,
        ExpddRuleRepoInterface $expddRuleRepoInterface,
        \Magento\Directory\Block\Data $directoryBlock,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Estimationtype $estimationType,
        \Magento\Framework\Data\Form\FormKey $formKey,
        array $data = []
    ) {
        $this->estimationType = $estimationType;
        $this->helper = $helper;
        $this->expddRegionRepoInterface = $expddRegionRepoInterface;
        $this->expddRuleRepoInterface = $expddRuleRepoInterface;
        $this->directoryBlock = $directoryBlock;
        $this->formKey = $formKey;
        $this->countryCollectionFactory = $countryCollectionFactory;
        parent::__construct($context, $data);
    }

    /**
     * Get Estimation Type
     *
     * @return array
     */
    public function getRules(): array
    {
        $sellerId = $this->helper->getSellerId();
        return $this->expddRuleRepoInterface->getBySellerId($sellerId)
        ->addFieldToFilter('status', ["eq" => 1])
        ->addFieldToSelect('name')->addFieldToSelect('entity_id')->getData();
    }

    /**
     * Get countries
     *
     * @return void
     */
    public function getCountryCollection()
    {
        $collection = $this->countryCollectionFactory->create()->loadByStore();
        return $collection;
    }

    /**
     * Retrieve list of top destinations countries
     *
     * @return array
     */
    protected function getTopDestinations()
    {
        $destinations = (string)$this->_scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }

    /**
     * Retrieve list of countries in array option
     *
     * @return array
     */
    public function getCountries()
    {
        return $this->getCountryCollection()
                ->setForegroundCountries($this->getTopDestinations())
                    ->toOptionArray();
    }
    
    /**
     * Get Holidays
     *
     * @return array
     */
    public function getFormData(): array
    {
        $id = $this->getRequest()->getParam('id');
        if (!$id) {
            return [];
        }
        $sellerId = $this->helper->getSellerId();
        $data = $this->expddRegionRepoInterface->getById($id);
        if ($data->getSellerId() != $sellerId) {
            return [];
        }
        return $data->getData();
    }

    /**
     * get form key
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
