<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\Header;

/**
 * Xtremo Homepage Block
 */
class Cms extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Cms\Model\ResourceModel\Page\CollectionFactory
     */
    private $cmsPageCollection;

    /**
     * @param \Magento\Framework\View\Element\Template\Context        $context
     * @param \Webkul\Xtremo\Helper\Data                              $helper
     * @param \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $cmsPageCollection
     * @param array                                                   $data = []
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Xtremo\Helper\Data $helper,
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $cmsPageCollection,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->cmsPageCollection = $cmsPageCollection;
        parent::__construct($context, $data);
    }

    public function getCmsPagesLinks()
    {
        $links = [];
        try {
            $store = $this->_storeManager->getStore();
            $cmsPagesConfig = $this->helper->getHeaderCmsPagesConfig();
            if ($cmsPagesConfig && $cmsPagesConfig !== "" && $this->helper->isXtremoEnabled()) {
                $cmsPagesArray = explode(",", $cmsPagesConfig);
                if (!empty($cmsPagesArray)) {
                    $cmsPagesArray = array_diff($cmsPagesArray, ['no-route', 'enable-cookies']);
                    $collection = $this->cmsPageCollection->create();
                    $collection->addFieldToFilter('identifier', ['in'=>$cmsPagesArray]);
                    $collection->addStoreFilter($store);
                    if ($collection->getSize()) {
                        $links = [];
                        foreach ($collection as $value) {
                            $links[$value->getId()]['title'] = $value->getTitle();
                            $links[$value->getId()]['url'] = $this->helper->getBaseUrl().$value->getIdentifier();
                        }
                    }
                }
            }
            return $links;
        } catch (\Exception $e) {
            return $links;
        }
    }
}
