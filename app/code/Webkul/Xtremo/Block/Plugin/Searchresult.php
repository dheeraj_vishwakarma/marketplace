<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\Plugin;

/**
 * Xtremo Block Plugin Searchresult
 */
class Searchresult
{
    /**
     * @var \Magento\Framework\App\Response\Http
     */
    private $response;

    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    private $redirect;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * __construct
     *
     * @param LoggerInterface $logger
     * @param State           $appState
     * @param Source          $assetSource
     * @param Temporary       $temporaryFile
     * @param Data            $helper
     */
    public function __construct(
        \Magento\Framework\App\Response\Http $response,
        \Magento\Framework\App\Response\RedirectInterface $redirect,
        \Webkul\Xtremo\Helper\Data $helper
    ) {
        $this->response = $response;
        $this->redirect = $redirect;
        $this->helper = $helper;
    }

    /**
     * afterGetResultCount calls after getResultCount
     *
     * @return object
     */
    public function afterGetResultCount(
        \Magento\CatalogSearch\Block\Result $subject,
        $result
    ) {
        if ($this->helper->checkCurrentTheme() && $result==0) {
            $this->redirect->redirect($this->response, 'xtremo/noresult');
        } else {
            return $result;
        }
    }
}
