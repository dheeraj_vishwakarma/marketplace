<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\Html;

use Magento\Catalog\Helper\Output as CatalogOutputHelper;

/**
 * Xtremo Homepage Block
 */
class Homepage extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * @var CatalogOutputHelper
     */
    protected $catalogOutputHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param CatalogOutputHelper $catalogOutputHelper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Xtremo\Helper\Data $helper,
        CatalogOutputHelper $catalogOutputHelper,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->catalogOutputHelper = $catalogOutputHelper;
        parent::__construct($context, $data);
    }

    public function getFeaturedProducts()
    {
        try {
            if ($this->helper->isXtremoEnabled()) {
                return $this->helper->getFilteredProductsCollectionByAttribute('is_featured_product', 1);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getSaleProducts()
    {
        try {
            if ($this->helper->isXtremoEnabled()) {
                return $this->helper->getFilteredProductsCollectionByAttribute('is_in_sale', 1);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getTopRatedProducts()
    {
        try {
            if ($this->helper->isXtremoEnabled()) {
                return $this->helper->getFilteredProductsCollectionByAttribute('is_in_top_rated', 1);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getTailoredProducts()
    {
        try {
            if ($this->helper->isXtremoEnabled()) {
                return $this->helper->getFilteredProductsCollectionByAttribute('is_tailored_product', 1);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getPopularCategories()
    {
        try {
            if ($this->helper->isXtremoEnabled()) {
                return $this->helper->getFilteredCategoriesCollectionByAttribute('is_popular_category', 1);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getPopularImage($image)
    {
        $imageUrl = $this->helper->getMediaUrl().'catalog/category/xtremo/resized/'.$image;
        return $imageUrl;
    }

    public function getDefaultCategoryImage()
    {
        return $this->helper->getConfigCategoryImage();
    }

    /**
     * Get Catalog Output Helper
     *
     * @return object \Magento\Catalog\Helper\Output
     */
    public function getCatalogOutputHelper()
    {
        return $this->catalogOutputHelper;
    }
}
