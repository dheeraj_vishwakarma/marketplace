<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\View\Element\Html\Link;

/**
 * Xtremo Block View Element Html Link Current
 */
class Current extends \Magento\Framework\View\Element\Html\Link\Current
{
    /**
     * Get href URL
     *
     * @return string
     */
    public function getHref()
    {
        return $this->getPath();
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }

        $highlight = '';

        if ($this->getIsHighlighted()) {
            $highlight = ' current';
        }

        if ($this->isCurrent()) {
            $html = '<li class="nav item current '.$this->getFooterSocial().'">';
            $html .= '<strong>'
                . $this->escapeHtml((string)new \Magento\Framework\Phrase($this->getLabel()))
                . '</strong>';
            $html .= '</li>';
        } else {
            if ($this->getHref()) {
                $html = '<li class="nav item' ." ".$this->getFooterSocial(). $highlight . '">
                            <a href="' . $this->escapeHtml($this->getHref()) . '"';
            } else {
                $html = '<li class="nav item' ." ".$this->getFooterSocial(). $highlight . '">
                            <a';
            }

            $html .= $this->getTitle()
                ? ' title="' . $this->escapeHtml((string)new \Magento\Framework\Phrase($this->getTitle())) . '"'
                : '';
            $html .= $this->getAttributesHtml() . '>';

            if ($this->getIsHighlighted()) {
                $html .= '<strong>';
            }

            $html .= $this->escapeHtml((string)new \Magento\Framework\Phrase($this->getLabel()));

            if ($this->getIsHighlighted()) {
                $html .= '</strong>';
            }

            $html .= '</a></li>';
        }

        return $html;
    }
}
