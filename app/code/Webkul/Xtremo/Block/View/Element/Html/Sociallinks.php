<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\View\Element\Html;

/**
 * Xtremo Block View Element Html Followlinks
 */
class Sociallinks extends \Magento\Framework\View\Element\Html\Links
{
    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        $helper = \Magento\Framework\App\ObjectManager::getInstance()->get(
            \Webkul\Xtremo\Helper\Data::class
        );

        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }

        $html = '';
        if ($this->getLinks() && $helper->isXtremoEnabled()) {
            $html = '<div' . ($this->hasCssClass() ? ' class="' . $this->escapeHtml(
                $this->getCssClass()
            ) . '"' : '') . '>';
            $html .= '<div class="block-title"><strong><span>'
                .($this->hasDivClass() ?
                    $this->escapeHtml(
                        $this->getDivClass()
                    ) : __('Company'))
                .'</span></strong></div>';

            $html .= '<ul' . ($this->hasCssClass() ? ' class="' . $this->escapeHtml(
                $this->getCssClass()
            ) . '"' : '') . '>';
            foreach ($this->getLinks() as $link) {
                if (isset($link['footer_social'])) {
                    if ($link['footer_social'] == "twitter_link social_links") {
                        $link['path'] = $helper->getFooterSocialLinkConfig("twitter_link");
                    } elseif ($link['footer_social'] == "google_link social_links") {
                        $link['path'] = $helper->getFooterSocialLinkConfig("google_link");
                    } elseif ($link['footer_social'] == "facebook_link social_links") {
                        $link['path'] = $helper->getFooterSocialLinkConfig("facebook_link");
                    } elseif ($link['footer_social'] == "linkedin_link social_links") {
                        $link['path'] = $helper->getFooterSocialLinkConfig("linkedin_link");
                    } elseif ($link['footer_social'] == "instagram_link social_links") {
                        $link['path'] = $helper->getFooterSocialLinkConfig("instagram_link");
                    }
                    $html .= $this->renderLink($link);
                }
            }
            $html .= '</ul></div>';
        }
        return $html;
    }
}
