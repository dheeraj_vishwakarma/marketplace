<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\Adminhtml\Bannerimages\Edit;

/**
 * Adminhtml Xtremo Bannerimages Edit Form
 */
class Form extends \Magento\Backend\Block\Widget\Form\Generic
{
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $systemStore;

    /**
     * @var \Webkul\Xtremo\Model\Bannerimages
     */
    protected $xtremoBannerimages;
    protected $helper;

    /**
     * [__construct]
     *
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry             $registry
     * @param \Magento\Framework\Data\FormFactory     $formFactory
     * @param \Magento\Store\Model\System\Store       $systemStore
     * @param \Webkul\Xtremo\Model\Bannerimages       $xtremoBannerimages
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Webkul\Xtremo\Model\Bannerimages $xtremoBannerimages,
        \Webkul\Xtremo\Helper\Data $helper,
        array $data = []
    ) {
        $this->systemStore = $systemStore;
        $this->xtremoBannerimages = $xtremoBannerimages;
        $this->helper = $helper;
        parent::__construct($context, $registry, $formFactory, $data);
    }

    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('bannerimages_form');
        $this->setTitle(__('Image Information'));
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        $model = $this->_coreRegistry->registry('xtremo_bannerimages');
        $form = $this->_formFactory->create(
            ['data' => [
                        'id' => 'edit_form',
                        'enctype' => 'multipart/form-data',
                        'action' => $this->getData('action'),
                        'method' => 'post']
                    ]
        );
        $form->setHtmlIdPrefix('bannerimages_');
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('Image Information'), 'class' => 'fieldset-wide']
        );
        if ($model->getId()) {
            $fieldset->addField('entity_id', 'hidden', ['name' => 'id']);
        }
        $fieldset->addField(
            'title',
            'text',
            [
                'name' => 'title',
                'label' => __('Image Title'),
                'title' => __('Image Title'),
                'required' => true
            ]
        );
        $fieldset->addField(
            'filename',
            'image',
            [
                'name' => 'filename',
                'label' => __('Image'),
                'title' => __('Image'),
                'required' => true,
                'class' => "required-entry"
            ]
        );
        $storeIds = [];
        if ($model->getStoreIds()) {
            $storeIds = $this->helper->jsonDecodeData($model->getStoreIds());
        }
        $model->setData('store_ids', $storeIds);
        $fieldset->addField(
            'store_ids',
            'multiselect',
            [
              'name'     => 'store_ids',
              'label'    => __('Store Views'),
              'title'    => __('Store Views'),
              'required' => true,
              'values'   => $this->systemStore->getStoreValuesForForm(false, true),
            ]
        );
        $categoryOptionArray = $this->helper->getBannerCategories();
        $categories = $this->_getExistingCategories($model);
        $model->setData('category_ids', $categories);
        $fieldset->addField(
            'category_ids',
            'multiselect',
            [
                'name' => 'category_ids',
                'label' => __('Categories'),
                'title' => __('Categories'),
                'required' => true,
                'values' => $categoryOptionArray,
            ]
        );
        $fieldset->addField(
            'status',
            'select',
            [
                'label' => __('Status'),
                'title' => __('Status'),
                'name' => 'status',
                'required' => true,
                'options' => $this->xtremoBannerimages->getAvailableStatuses()
            ]
        );
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
        return parent::_prepareForm();
    }

    /**
     * Prepare form
     *
     * @return $this
     */
    private function _getExistingCategories($model)
    {
        $itemList = [];
        if ($model->getCategoryIds()) {
            // Get our collection
            $existingCategories = $this->helper->jsonDecodeData($model->getCategoryIds());
            if (!empty($existingCategories)) {
                $itemList = $existingCategories;
            }
        }
        return $itemList;
    }
}
