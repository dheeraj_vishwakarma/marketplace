<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Xtremo\Controller\Adminhtml\Bannerimages;

use Webkul\Xtremo\Controller\Adminhtml\Bannerimages as BannerimagesController;
use Magento\Framework\Controller\ResultFactory;

/**
 * Xtremo Adminhtml Bannerimages NewAction Controller
 */
class NewAction extends BannerimagesController
{
    /**
     * @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    {
        /**
 * @var \Magento\Backend\Model\View\Result\Forward $resultForward
*/
        $resultForward = $this->resultFactory->create(ResultFactory::TYPE_FORWARD);
        $resultForward->forward('edit');
        return $resultForward;
    }
}
