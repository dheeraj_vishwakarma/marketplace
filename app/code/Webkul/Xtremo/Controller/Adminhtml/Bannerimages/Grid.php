<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Controller\Adminhtml\Bannerimages;

/**
 * Xtremo Adminhtml Bannerimages Grid Controller
 */
class Grid extends Index
{
    /**
     * set page data
     *
     * @return $this
     */
    public function setPageData()
    {
        return $this;
    }
}
