<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Controller\Adminhtml\Bannerimages;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

/**
 * Xtremo Adminhtml Bannerimages massEnable Controller
 */
class MassEnable extends \Magento\Backend\App\Action
{
    const ENABLE = true;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    protected $helper;

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    private $filterModel;

    /**
     * @var \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection
     */
    protected $bannerImagesCollection;

    /**
     * @param Action\Context $context
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param \Magento\Ui\Component\MassAction\Filter $filterModel
     * @param \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection
     */
    public function __construct(
        Action\Context $context,
        \Webkul\Xtremo\Helper\Data $helper,
        \Magento\Ui\Component\MassAction\Filter $filterModel,
        \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection
    ) {
        $this->helper = $helper;
        $this->filterModel = $filterModel;
        $this->bannerImagesCollection = $bannerImagesCollection;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Xtremo::bannerimages');
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $model = $this->filterModel;
        $collection = $model->getCollection($this->bannerImagesCollection->create());

        foreach ($collection as $image) {
            $this->updateItem($image, self::ENABLE);
        }

        // clear cache
        $this->helper->clearCache();
        $this->messageManager->addSuccess(__('Status set to "enabled" successfully.'));
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * @param object $item
     * @param int $status
     */
    private function updateItem($item, $status)
    {
        $item->setStatus($status)->save();
    }
}
