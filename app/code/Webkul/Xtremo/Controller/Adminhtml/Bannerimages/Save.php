<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Controller\Adminhtml\Bannerimages;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;

/**
 * Xtremo Adminhtml Bannerimages Save Controller
 */
class Save extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    private $fileUploaderFactory;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;
    private $bannerImages;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $fileSystem;

    /**
     * Constructor
     *
     * @param Action\Context $context
     * @param \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param \Webkul\Xtremo\Model\Bannerimages $bannerImages
     */
    public function __construct(
        Action\Context $context,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Webkul\Xtremo\Helper\Data $helper,
        \Magento\Framework\Filesystem $fileSystem,
        \Webkul\Xtremo\Model\Bannerimages $bannerImages
    ) {
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->helper = $helper;
        $this->fileSystem = $fileSystem;
        $this->bannerImages = $bannerImages;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Xtremo::bannerimages');
    }

    private function checkErrorImage($image)
    {
        $error      = 0;
        $msg        = "";
        $result     = [];
        $isDelete   = false;
        if (isset($image['error']) && $image['error']==1) {
            $error = 1;
            $msg = $image['msg'];
        } elseif (isset($image['image']['tmp_name'])
            && $image['image']['tmp_name'] !== ''
            && $image['image']['tmp_name'] !== null
        ) {
            $isDelete = $image['delete'];
            $result = $this->uploadImageForBanner($image['image']);
        }
        if (isset($result['error']) && (int)$result['error']==1) {
            $error = 1;
            $msg = $result['msg'];
        }
        return [
            $error,
            $msg,
            $result,
            $isDelete
        ];
    }

    /**
     * Delete the file if isdelete value is true
     *
     * @param array $data
     * @param bool $isDelete
     * @param array $imageData
     * @return array
     */
    private function deletIfIsDelete(
        $data,
        $isDelete,
        $imageData
    ) {
        if ($isDelete && isset($data['filename']['value'])) {
            $this->helper->deleteUploadedFile($isDelete, $data['filename']['value']);
        }

        if (!empty($imageData['filename'])) {
            $data['filename'] = $imageData['filename'];
        } else {
            unset($data['filename']);
        }

        if (!empty($imageData['link'])) {
            $data['link'] = $imageData['link'];
        } else {
            unset($data['link']);
        }
        return $data;
    }

    /**
     * Save action.
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $time = date('Y-m-d H:i:s');
        $resultRedirect = $this->resultRedirectFactory->create();
        $imageData = [];
        $error = 0;
        $isDelete = false;
        $msg = "";
        $data = $this->getRequest()->getParams();

        try {
            $image = $this->checkFileUpload($data);
            list(
                $error,
                $msg,
                $imageData,
                $isDelete
            ) = $this->checkErrorImage($image);
            if ($error == 1) {
                $this->messageManager->addError($msg);
                if (isset($data['id']) && $data['id']!==0) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['id' => $data['id'], '_current' => true]
                    );
                }
                return $resultRedirect->setPath('*/*/');
            } else {
                if ($data) {
                    $data = $this->deletIfIsDelete($data, $isDelete, $imageData);
                    
                    $model = $this->bannerImages;
                    $data['update_time'] = $time;

                    if (isset($data['category_ids']) && !empty($data['category_ids'])) {
                        $data['category_ids'] = $this->helper->jsonEncodeData($data['category_ids']);
                    } else {
                        $data['category_ids'] = '';
                    }

                    if (isset($data['store_ids']) && !empty($data['store_ids'])) {
                        $data['store_ids'] = $this->helper->jsonEncodeData($data['store_ids']);
                    } else {
                        $data['store_ids'] = '';
                    }

                    if (isset($data['id']) && $data['id']!==0) {
                        $model->addData($data)->setId($data['id'])->save();
                    } else {
                        $data['created_time'] = $time;
                        $model->setData($data)->save();
                    }
                }

                // clear cache
                $this->helper->clearCache();
                $this->messageManager->addSuccess(__('Image saved successfully.'));

                if (isset($data['id']) && $data['id']!==0) {
                    return $resultRedirect->setPath(
                        '*/*/edit',
                        ['id' => $data['id'], '_current' => true]
                    );
                }

                return $resultRedirect->setPath('*/*/');
            }
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());

            if (isset($data['id']) && $data['id']!==0) {
                return $resultRedirect->setPath(
                    '*/*/edit',
                    ['id' => $data['id'], '_current' => true]
                );
            }

            return $resultRedirect->setPath('*/*/');
        }
    }

    /**
     * [checkFileUpload used to validate file]
     *
     * @param  array $data
     * @return array
     */
    private function checkFileUpload($data)
    {
        $errors = [];
        $isDelete = false;

        if (isset($data['id']) && isset($data['filename'])) {
            if (isset($data['filename']['delete'])
                && $data['filename']['delete']==1
            ) {
                $isDelete = true;
                $image = $this->getFileUploader();

                if ($image['tmp_name'] !== "") {
                    return [
                        'delete' => $isDelete,
                        'image' => $image
                    ];
                } else {
                    $errors['error'] = 1;
                    $errors['msg'] = __('Please upload an image for Banner');
                    return $errors;
                }
            }
        } else {
            $image = $this->getFileUploader();
            if ($image['tmp_name'] !== "") {
                return [
                    'delete' => $isDelete,
                    'image' => $image
                ];
            } else {
                $errors['error'] = 1;
                $errors['msg'] = __('Please upload an image for Banner');
                return $errors;
            }
        }
    }

    /**
     * [getFileUploader used to get File uploader]
     *
     * @return object
     */
    private function getFileUploader()
    {
        $uploader = $this->fileUploaderFactory
            ->create(
                ['fileId' => 'filename']
            );
        $uploader->setAllowedExtensions(['jpg', 'jpeg', 'gif', 'png']);
        $uploader->setAllowRenameFiles(false);
        $uploader->setFilesDispersion(false);
        $image = $uploader->validateFile();

        return $image;
    }

    /**
     * Upload images for Banner on category list
     *
     * @param  array $image
     * @return array
     */
    private function uploadImageForBanner($image)
    {
        $errors = [];
        $imageData = [];
        $file = $this->helper->getFileDriver();
        $check = getimagesize($image['tmp_name']);
        if (!$check) {
            $errors['error'] = 1;
            $errors['msg'] = __('Invalid File.');
            return $errors;
        } else {
            $splitname = explode('.', $image['name']);
            $ext = end($splitname);
            $allowedExtensions = $this->helper->getAllowedExtensions(false);
            if (in_array($ext, $allowedExtensions)) {
                $imageUploadPath = $this->fileSystem
                    ->getDirectoryRead(DirectoryList::MEDIA)
                    ->getAbsolutePath($this->helper->getBannerImagePath());

                if (!$file->isDirectory($imageUploadPath)) {
                    $file->createDirectory($imageUploadPath, 0755);
                }
                $imageName = 'File-'.time().'.'.$ext;
                $uploader = $this->fileUploaderFactory
                    ->create(
                        ['fileId' => 'filename']
                    );
                $uploader->setAllowedExtensions($allowedExtensions);
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(false);
                $result = $uploader->save($imageUploadPath, $imageName);
                $imageData['link'] = $this->helper->getBannerImagePath().$imageName;
                $imageData['filename'] = $imageName;
                $file->changePermissions($imageUploadPath.$imageName, 0644);

                if (isset($result['error'])
                    && $result['error']!==0
                ) {
                    $errors['error'] = 1;
                    $errors['msg'] = __(
                        '%1 Image Not Uploaded',
                        $image['name']
                    );
                    return $errors;
                } else {
                    return $imageData;
                }
            } else {
                $errors['error'] = 1;
                $errors['msg'] = __(
                    'Invalid File Type. Please upload valid file type from %1',
                    implode(",", $allowedExtensions)
                );
                return $errors;
            }
        }
    }
}
