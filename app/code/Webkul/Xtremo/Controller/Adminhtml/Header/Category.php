<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Controller\Adminhtml\Header;

use Magento\Backend\App\Action;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;

/**
 * Webkul Xtremo Adminhtml Header Category controller
 */
class Category extends Action
{
    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    /**
     * @var CollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\Json\Helper\Data   $jsonHelper
     * @param CollectionFactory                     $categoryCollectionFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        CollectionFactory $categoryCollectionFactory
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($context);
    }

    /**
     * retrieve parent id from multiple category Ids
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $level = 2;
        $categoryIds = [];

        if ($params && isset($params['level'])) {
            $level = $params['level'];
        }
        if ($params && isset($params['ids'])) {
            $categoryIds = $params['ids'];
        }

        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addAttributeToFilter('level', $level);
        $collection->addAttributeToFilter('entity_id', ['in'=>$categoryIds]);
        $collection->addFieldToSelect('entity_id');
        $parentCategoryIds = $collection->getColumnValues('entity_id');

        $this->getResponse()->representJson(
            $this->jsonHelper->jsonEncode($parentCategoryIds)
        );
    }
}
