<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Controller\Adminhtml;

use Magento\Backend\App\Action;

/**
 * Webkul Xtremo admin Bannerimages controller
 */
abstract class Bannerimages extends \Magento\Backend\App\Action
{

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Xtremo::bannerimages');
    }
}
