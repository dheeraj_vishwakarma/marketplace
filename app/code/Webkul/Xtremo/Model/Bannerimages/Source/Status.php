<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model\Bannerimages\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Webkul Xtremo Bannerimages Source Status Model
 */
class Status implements OptionSourceInterface
{
    /**
     * @var \Webkul\Xtremo\Model\Bannerimages
     */
    private $bannerimages;

    /**
     * __construct
     *
     * @param \Webkul\Xtremo\Model\Bannerimages $bannerimages
     */
    public function __construct(\Webkul\Xtremo\Model\Bannerimages $bannerimages)
    {
        $this->bannerimages = $bannerimages;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $availableOptions = $this->bannerimages->getAvailableStatuses();
        $options = [];
        foreach ($availableOptions as $key => $value) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }
        return $options;
    }
}
