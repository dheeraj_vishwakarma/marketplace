<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

class Categorylist implements ArrayInterface
{
    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * @param \Webkul\Xtremo\Helper\Data $helper
     */
    public function __construct(
        \Webkul\Xtremo\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    public function toOptionArray()
    {
        $arr = $this->_toArray();
        $categoryList = [];

        foreach ($arr as $key => $value) {
            $categoryList[] = [
                'value' => $key,
                'label' => $value
            ];
        }

        return $categoryList;
    }

    private function _toArray()
    {
        $catagoryList = [];
        $secondLevelCategories = $this->helper->getCategoryCollection(true, 2);

        if ($secondLevelCategories->getSize()) {
            foreach ($secondLevelCategories as $category) {
                if ($category->getChildrenCount()>0) {
                    $catagoryList[$category->getEntityId()] = $category->getName();
                    $thirdLevelCategories = $this->helper->getCategoryCollection(
                        true,
                        3,
                        'parent_id',
                        false,
                        $category->getId()
                    );
                    if ($thirdLevelCategories->getSize()) {
                        foreach ($thirdLevelCategories as $thirdCategory) {
                            $catagoryList[$thirdCategory->getEntityId()] = html_entity_decode(
                                "&#160;&#160;&#160;&#160;".$thirdCategory->getName()
                            );
                        }
                    }
                } else {
                    $catagoryList[$category->getEntityId()] = $category->getName();
                }
            }
        }
        return $catagoryList;
    }
}
