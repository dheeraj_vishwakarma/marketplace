<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_Xtremo
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model\Config\Source\Cms;

use Magento\Cms\Model\ResourceModel\Page\CollectionFactory;

/**
 * Webkul Xtremo Cms Pages
 */
class Page extends \Magento\Cms\Model\Config\Source\Page
{
    /**
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\App\RequestInterface $request
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        \Magento\Framework\App\RequestInterface $request
    ) {
        $this->request = $request;
        parent::__construct($collectionFactory);
    }

    /**
     * To option array
     *
     * @return array
     */
    public function toOptionArray()
    {
        $store = $this->request->getParam('store', 0);
        if (!$this->options) {
            $this->options = $this->collectionFactory->create()->addStoreFilter($store)->toOptionIdArray();
        }
        return $this->options;
    }
}
