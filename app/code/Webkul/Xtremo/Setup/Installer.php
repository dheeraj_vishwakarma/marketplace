<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Setup;

use Magento\Framework\Setup;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\Store;
use Magento\Theme\Model\Theme\Registration;

class Installer implements Setup\SampleData\InstallerInterface
{
    /**
     * @var \Magento\WidgetSampleData\Model\CmsBlock
     */
    protected $cmsBlock;

    /**
     * @var \Magento\CmsSampleData\Model\Page
     */
    private $page;

    /**
     * @var \Magento\CmsSampleData\Model\Block
     */
    private $block;

    /**
     * @var \Webkul\Xtremo\Model\CatalogSampleData\Category
     */
    private $category;

    /**
     * @var CollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollection;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    private $productStatus;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    private $productVisibility;

    /**
     * Setup class for products
     *
     * @var \Magento\CatalogSampleData\Model\Product
     */
    private $productSetup;

    /**
     * @var \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var Registration
     */
    private $themeRegistration;

    /**
     * @var \Magento\Theme\Model\Config
     */
    private $config;

    /**
     * @var \Webkul\Xtremo\Helper\Data $helper
     */
    private $helper;

    /**
     * @param \Webkul\Xtremo\Model\CmsBlock $cmsBlock
     * @param \Webkul\Xtremo\Model\Page $page
     * @param \Webkul\Xtremo\Model\Block $block
     * @param \Webkul\Xtremo\Model\CatalogSampleData\Category $category
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Webkul\Xtremo\Model\CatalogSampleData\Product $productSetup
     * @param \Magento\Theme\Model\Config $config
     * @param \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $collectionFactory
     * @param Registration $themeRegistration
     * @param \Webkul\Xtremo\Helper\Data $helper
     */
    public function __construct(
        \Webkul\Xtremo\Model\CmsBlock $cmsBlock,
        \Webkul\Xtremo\Model\Page $page,
        \Webkul\Xtremo\Model\Block $block,
        \Webkul\Xtremo\Model\CatalogSampleData\Category $category,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Webkul\Xtremo\Model\CatalogSampleData\Product $productSetup,
        \Magento\Theme\Model\Config $config,
        \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $collectionFactory,
        Registration $themeRegistration,
        \Webkul\Xtremo\Helper\Data $helper
    ) {
        $this->cmsBlock = $cmsBlock;
        $this->page = $page;
        $this->block = $block;
        $this->category = $category;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->productCollection = $productCollection;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->productSetup = $productSetup;
        $this->collectionFactory = $collectionFactory;
        $this->config = $config;
        $this->themeRegistration = $themeRegistration;
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function install()
    {
        try {
            $this->themeRegistration->register();
            $this->assignTheme();

            $this->page->install(['Webkul_Xtremo::fixtures/pages.csv']);
            $this->block->install(['Webkul_Xtremo::fixtures/pages_static_blocks.csv']);

            $i = 0;
            $collection = $this->getCategoryCollection();
            if ($collection->getSize() <= 0) {
                $this->category->install(['Webkul_Xtremo::fixtures/categories.csv']);
                $collection = $this->getCategoryCollection();
            }
            if ($collection->getSize()) {
                $collection = $this->getCategoryCollection();
                $collection->addAttributeToFilter('is_popular_category', 1);
                if ($collection->getSize() < 5) {
                    $i = $collection->getSize();
                }
                $collection = $this->getCategoryCollection();

                if ($collection->getSize()) {
                    foreach ($collection as $model) {
                        if ($i < 5) {
                            $this->saveCategory($model);
                        }
                        $i++;
                    }
                }
            }

            $productCollection = $this->productCollection->create()
                ->addAttributeToSelect('*')
                ->addStoreFilter()
                ->addAttributeToFilter(
                    'status',
                    ['in' => $this->productStatus->getVisibleStatusIds()]
                )->addAttributeToFilter(
                    'visibility',
                    ['in' => $this->productVisibility->getVisibleInSiteIds()]
                );
            if ($productCollection->getSize()<=0) {
                $this->productSetup->install(
                    [
                        'Webkul_Xtremo::fixtures/SimpleProduct/products_gear_bags.csv'
                    ],
                    [
                        'Webkul_Xtremo::fixtures/SimpleProduct/images_gear_bags.csv'
                    ]
                );
            }

            $this->cmsBlock->install(['Webkul_Xtremo::fixtures/cmsblock.csv']);
        } catch (\Exception $e) {
            $this->helper->logData('Webkul_Xtremo::Setup_Installer :'.$e);
        }
    }

    private function saveCategory($model)
    {
        $model->setIsPopularCategory(1)->save();
    }

    /**
     * get category collection
     *
     * @return object
     */
    protected function getCategoryCollection()
    {
        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        $collection->addIsActiveFilter();
        $collection->addAttributeToFilter('level', ['neq'=>1]);

        return $collection;
    }

    /**
     * Assign Theme
     *
     * @return void
     */
    protected function assignTheme()
    {
        $themes = $this->collectionFactory->create()->loadRegisteredThemes();
        /**
         * @var \Magento\Theme\Model\Theme $theme
        */
        foreach ($themes as $theme) {
            if ($theme->getCode() == 'Webkul/xtremo') {
                $this->config->assignToStore(
                    $theme,
                    [Store::DEFAULT_STORE_ID],
                    ScopeConfigInterface::SCOPE_TYPE_DEFAULT
                );
            }
        }
    }
}
