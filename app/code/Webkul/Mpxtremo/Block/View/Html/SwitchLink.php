<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpxtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Block\View\Html;

class SwitchLink extends \Magento\Framework\View\Element\Html\Link
{
    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $helper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Marketplace\Helper\Data $helper
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Marketplace\Helper\Data $helper,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
        $this->logger = $logger;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        try {
            if (method_exists($this->helper, "getIsSeparatePanel")) {
                if ($this->helper->getIsSeparatePanel()) {
                    if (false != $this->getTemplate()) {
                        return parent::_toHtml();
                    }
                    $label = $this->escapeHtml($this->getLabel());
                    return '<li><a ' . $this->getLinkAttributes() . ' >' . $label . '</a></li>';
                }
            }
        } catch (\Exception $e) {
            $this->logger->error('Mpxtremo_Block_View_Html_SwitchLink'.$e->getMessage());
        }
    }
}
