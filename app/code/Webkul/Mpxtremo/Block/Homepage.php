<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpxtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Block;

/**
 * Mpxtremo Homepage Block
 */
class Homepage extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\Mpxtremo\Helper\Data
     */
    private $mpHelper;

     /**
      * @var \Webkul\Xtremo\Helper\Data
      */
    private $xtremoHelper;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Xtremo\Helper\Data                       $xtremoHelper
     * @param \Webkul\Marketplace\Helper\Data                  $mpHelper
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Xtremo\Helper\Data $xtremoHelper,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        array $data = []
    ) {
        $this->mpHelper = $mpHelper;
        $this->xtremoHelper = $xtremoHelper;
        parent::__construct($context, $data);
    }

    public function getSellerShopUrl()
    {
        $data = [
            'label' => __('Open Account')
        ];
        if ($this->xtremoHelper->isLoggedIn()) {
            if ($this->mpHelper->isSeller()) {
                $sellerCollection = $this->mpHelper->getSellerData();
                $sellerCollection->getSelect()->group('seller_id');
                if ($sellerCollection && $sellerCollection->getSize()) {
                    foreach ($sellerCollection as $seller) {
                        $data['url'] = $this->mpHelper->getRewriteUrl(
                            'marketplace/seller/profile/shop/'.$seller->getShopUrl()
                        );
                        $data['label'] = __('View Shop');
                    }
                } else {
                    $data['url'] = $this->getUrl(
                        '/b2bmarketplace/supplier/create/',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }
            } else {
                $data['url'] = $this->getUrl(
                    '/b2bmarketplace/supplier/create/',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            $data['url'] = $this->xtremoHelper->getCustomerModelUrl()->getRegisterUrl();
        }

        return $data;
    }

    /**
     * Get Xtremo Helper
     *
     * @return object \Webkul\Xtremo\Helper\Data
     */
    public function getXtremoHelper()
    {
        return $this->xtremoHelper;
    }
}
