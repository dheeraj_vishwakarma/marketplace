<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_Mpxtremo
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\View\Result\Page as ResultPage;

class AfterSellerPublicPage implements ObserverInterface
{
    /**
     * @var \Webkul\Mpxtremo\Helper\Data
     */
    protected $helper;
    protected $resultPage;

    /**
     * @param \Webkul\Mpxtremo\Helper\Data $helper
     */
    public function __construct(
        \Webkul\Mpxtremo\Helper\Data $helper,
        ResultPage $resultPage
    ) {
        $this->helper = $helper;
        $this->resultPage = $resultPage;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $pageConfig = $this->resultPage->getConfig();
        $pageConfig->getTitle()->set(
            __('Marketplace Seller Profile')
        );
        if (!$this->helper->getSellerPublicPageConfig()) {
            $pageConfig->setPageLayout("1column");
        }
    }
}
