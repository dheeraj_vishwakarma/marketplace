<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_Mpxtremo
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * This class is used to remove seller top block from seller pages
 */
class RemoveSellerTopBlock implements ObserverInterface
{
    /**
     * @var \Webkul\Mpxtremo\Helper\Data
     */
    protected $helper;

    /**
     * @param \Webkul\Mpxtremo\Helper\Data $helper
     */
    public function __construct(
        \Webkul\Mpxtremo\Helper\Data $helper
    ) {
        $this->helper = $helper;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $layout = $observer->getLayout();
        $block = $layout->getBlock('marketplace_seller_top_block');
        if ($block) {
            if ($this->helper->getSellerPublicPageConfig()) {
                $layout->unsetElement('marketplace_seller_top_block');
            }
        }
    }
}
