<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpxtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Model\Plugin\Account;

class Diagrams
{
    /**
     * Seller statistics graph width.
     *
     * @var string
     */
    private $width = '800';

    /**
     * Seller statistics graph height.
     *
     * @var string
     */
    private $height = '320';

    /**
     * @var \Webkul\Marketplace\Helper\Dashboard\Data
     */
    private $helper;

    /**
     * @var \Webkul\Mpxtremo\Helper\Data
     */
    private $mpXtremoHelper;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $xtremoHelper;

    /**
     * @param \Webkul\Marketplace\Helper\Dashboard\Data $helper
     * @param \Webkul\Mpxtremo\Helper\Data              $mpXtremoHelper
     */
    public function __construct(
        \Webkul\Marketplace\Helper\Dashboard\Data $helper,
        \Webkul\Mpxtremo\Helper\Data $mpXtremoHelper,
        \Webkul\Xtremo\Helper\Data $xtremoHelper
    ) {
        $this->helper = $helper;
        $this->mpXtremoHelper = $mpXtremoHelper;
        $this->xtremoHelper = $xtremoHelper;
    }

    public function afterGetSellerStatisticsGraphUrl(
        \Webkul\Marketplace\Block\Account\Dashboard\Diagrams $subject,
        $result
    ) {
        $params = [
            'cht' => 'bvg',
            'chf' => 'bg,s,ffffff',
            'chxt' => 'x,y',
            'chds' => 'a',
            'chbh' => 'a,1,1',
            'chco' => 'FE705D,27B75E',
            'chxr' => '0,1'
        ];
        $getData = $subject->getSale(true);
        $getSale = $getData['values'];

        $expenses = $this->mpXtremoHelper->getTotalExpenses(
            $this->mpXtremoHelper->getSellerId()
        );

        if (isset($getData['arr'])) {
            $arr = $getData['arr'];
            $indexid = 0;
            $tmpstring = implode('|', $arr);
            $valueBuffer[] = $indexid.':|'.$tmpstring;
            $valueBuffer = implode('|', $valueBuffer);
            $params['chxl'] = $valueBuffer;
        } else {
            $params['chxl'] = $getData['chxl'];
        }

        $params['chd'] = 't:'.implode(',', $getSale).'|'.implode(',', $expenses);
        $params['chg'] = (100/count($getSale)).',10,4,1';

        // seller statistics graph size
        $params['chs'] = $this->width.'x'.$this->height;

        // return the encoded graph image url
        $getParamData = urlencode(base64_encode($this->xtremoHelper->jsonEncodeData($params)));
        $getEncryptedHashData = $this->helper->getChartEncryptedHashData($getParamData);
        $params = [
            'param_data' => $getParamData,
            'encrypted_data' => $getEncryptedHashData,
        ];
        return $subject->getUrl(
            '*/*/dashboard_tunnel',
            ['_query' => $params, '_secure' => $subject->getRequest()->isSecure()]
        );
    }
}
