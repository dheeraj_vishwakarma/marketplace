<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_ProductAttachment
 * @author     Webkul
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\ViewModel;

use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\Marketplace\Helper\Orders as MpOrderHelper;
use Webkul\Xtremo\Helper\Data as XtremoHelper;
use Webkul\Mpxtremo\Helper\Data as MpxtremoHelper;
use Magento\Wishlist\Helper\Data as WishlistHelper;
use Magento\Search\Helper\Data as SearchHelper;
use Magento\Framework\Json\Helper\Data as JsonHelper;
use Magento\Customer\Helper\Address as CustomerAddressHelper;
use Magento\Directory\Helper\Data as DirectoryHelper;
use Magento\Catalog\Helper\Data as CatalogHelper;
use Magento\Catalog\Helper\Category as CategoryHelper;
use Magento\Catalog\Helper\Output as CatalogOutputHelper;
use Magento\Framework\Data\Helper\PostHelper;
use Magento\Catalog\Helper\Product\Compare as ProductCompareHelper;

/**
 * Mpxtremo View Model
 */
class Mpxtremo implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var MarketplaceHelper
     */
    protected $mpHelper;

    /**
     * @var MarketplaceHelper
     */
    protected $mpOrderHelper;

    /**
     * @var XtremoHelper
     */
    protected $xtremoHelper;

    /**
     * @var MpxtremoHelper
     */
    protected $mpxtremoHelper;

    /**
     * @var SearchHelper
     */
    protected $searchHelper;

    /**
     * @var WishlistHelper
     */
    protected $wishlistHelper;

    /**
     * @var CustomerAddressHelper
     */
    protected $customerAddressHelper;

    /**
     * @var DirectoryHelper
     */
    protected $directoryHelper;

    /**
     * @var CatalogHelper
     */
    protected $catalogHelper;

    /**
     * @var CategoryHelper
     */
    protected $categoryHelper;

    /**
     * @var CatalogOutputHelper
     */
    protected $catalogOutputHelper;

    /**
     * @var PostHelper
     */
    protected $postHelper;

    /**
     * @var ProductCompareHelper
     */
    protected $productCompareHelper;

    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @param XtremoHelper $xtremoHelper
     * @param SearchHelper $searchHelper
     * @param WishlistHelper $wishlistHelper
     * @param CustomerAddressHelper $customerAddressHelper
     * @param DirectoryHelper $directoryHelper
     * @param CatalogHelper $catalogHelper
     * @param CatalogOutputHelper $catalogOutputHelper
     * @param PostHelper $postHelper
     * @param ProductCompareHelper $productCompareHelper
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        MarketplaceHelper $mpHelper,
        MpOrderHelper $mpOrderHelper,
        XtremoHelper $xtremoHelper,
        MpxtremoHelper $mpxtremoHelper,
        SearchHelper $searchHelper,
        WishlistHelper $wishlistHelper,
        CustomerAddressHelper $customerAddressHelper,
        DirectoryHelper $directoryHelper,
        CatalogHelper $catalogHelper,
        CategoryHelper $categoryHelper,
        CatalogOutputHelper $catalogOutputHelper,
        PostHelper $postHelper,
        ProductCompareHelper $productCompareHelper,
        JsonHelper $jsonHelper
    ) {
        $this->mpHelper = $mpHelper;
        $this->mpOrderHelper = $mpOrderHelper;
        $this->xtremoHelper  = $xtremoHelper;
        $this->mpxtremoHelper  = $mpxtremoHelper;
        $this->searchHelper  = $searchHelper;
        $this->wishlistHelper = $wishlistHelper;
        $this->customerAddressHelper = $customerAddressHelper;
        $this->directoryHelper = $directoryHelper;
        $this->catalogHelper = $catalogHelper;
        $this->categoryHelper = $categoryHelper;
        $this->catalogOutputHelper = $catalogOutputHelper;
        $this->postHelper = $postHelper;
        $this->productCompareHelper = $productCompareHelper;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * Get Marketplace Helper
     *
     * @return object \Webkul\Marketplace\Helper\Data
     */
    public function getMarketplaceHelper()
    {
        return $this->mpHelper;
    }

    /**
     * Get Marketplace Order Helper
     *
     * @return object \Webkul\Marketplace\Helper\Orders
     */
    public function getMpOrderHelper()
    {
        return $this->mpOrderHelper;
    }

    /**
     * Get Xtremo Helper
     *
     * @return object \Webkul\Xtremo\Helper\Data
     */
    public function getXtremoHelper()
    {
        return $this->xtremoHelper;
    }

    /**
     * Get Mpxtremo Helper
     *
     * @return object \Webkul\Mpxtremo\Helper\Data
     */
    public function getMpxtremoHelper()
    {
        return $this->mpxtremoHelper;
    }

    /**
     * Get Search Helper
     *
     * @return object \Magento\Search\Helper\Data
     */
    public function getSearchHelper()
    {
        return $this->searchHelper;
    }

    /**
     * Get Wishlist Helper
     *
     * @return object \Magento\Wishlist\Helper\Data
     */
    public function getWishlistHelper()
    {
        return $this->wishlistHelper;
    }

    /**
     * Get CustomerAddress Helper
     *
     * @return object \Magento\Customer\Helper\Address
     */
    public function getCustomerAddressHelper()
    {
        return $this->customerAddressHelper;
    }

    /**
     * Get Directory Helper
     *
     * @return object \Magento\Customer\Helper\Address
     */
    public function getDirectoryHelper()
    {
        return $this->directoryHelper;
    }

    /**
     * Get Catalog Helper
     *
     * @return object \Magento\Catalog\Helper\Data
     */
    public function getCatalogHelper()
    {
        return $this->catalogHelper;
    }

    /**
     * Get Category Helper
     *
     * @return object \Magento\Catalog\Helper\Category
     */
    public function getCategoryHelper()
    {
        return $this->categoryHelper;
    }

    /**
     * Get Catalog Output Helper
     *
     * @return object \Magento\Catalog\Helper\Output
     */
    public function getCatalogOutputHelper()
    {
        return $this->catalogOutputHelper;
    }

    /**
     * Get Framework Post Helper
     *
     * @return object \Magento\Framework\Data\Helper\PostHelper
     */
    public function getPostHelper()
    {
        return $this->postHelper;
    }

    /**
     * Get Product Compare Helper
     *
     * @return object \Magento\Catalog\Helper\Product\Compare
     */
    public function getProductCompareHelper()
    {
        return $this->productCompareHelper;
    }

    /**
     * Get Json Helper
     *
     * @return object \Magento\Framework\Json\Helper\Data
     */
    public function getJsonHelper()
    {
        return $this->jsonHelper;
    }
}
