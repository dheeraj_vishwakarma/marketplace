<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_B2BMarketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\B2BMarketplace\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class UpgradeSchema implements UpgradeSchemaInterface
{
    /**
     * {@inheritdoc}
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $setup->getConnection()->addColumn(
            $setup->getTable('wk_b2b_requestforquote_sent_quotes'),
            'currency_rate',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => false,
                'default' => '1',
                'comment' => 'Current Currency Rate'
            ]
        );

        $setup->getConnection()->addColumn(
            $setup->getTable('wk_b2b_requestforquote_quote_item'),
            'currency_rate',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                'length' => '12,4',
                'nullable' => false,
                'default' => '1',
                'comment' => 'Current Currency Rate'
            ]
        );

        /**
         * Drop column attachments from tables 'wk_b2b_requestforquote_quote'
         */
        $setup->getConnection()->dropColumn(
            $setup->getTable('wk_b2b_requestforquote_quote'),
            'attachments'
        );

        /*
         * Create table 'wk_b2b_quote_attachment' for saving wk_b2b_requestforquote_quote attachments
         */
        $table = $setup->getConnection()
            ->newTable($setup->getTable('wk_b2b_quote_attachment'))
            ->addColumn(
                'attachment_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Attachment Id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Quote Id'
            )
            ->addColumn(
                'file_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'File Name'
            )
            ->addColumn(
                'file_path',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'File Path'
            )
            ->addIndex(
                $setup->getIdxName('wk_b2b_quote_attachment', ['quote_id']),
                ['quote_id']
            )
            ->addForeignKey(
                $setup->getFkName(
                    'wk_b2b_quote_attachment',
                    'quote_id',
                    'wk_b2b_requestforquote_quote',
                    'entity_id'
                ),
                'quote_id',
                $setup->getTable('wk_b2b_requestforquote_quote'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->setComment('wk_b2b_quote_attachment');
        $setup->getConnection()->createTable($table);

        /**
         * Drop column samples from tables 'wk_b2b_requestforquote_quote_item'
         */
        $setup->getConnection()->dropColumn(
            $setup->getTable('wk_b2b_requestforquote_quote_item'),
            'samples'
        );

        /*
         * Create table 'wk_b2b_quote_item_attachment' for saving wk_b2b_requestforquote_quote_item samples
         */
        $table = $setup->getConnection()
            ->newTable($setup->getTable('wk_b2b_quote_item_attachment'))
            ->addColumn(
                'attachment_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Attachment Id'
            )
            ->addColumn(
                'quote_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Quote Item Id'
            )
            ->addColumn(
                'file_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'File Name'
            )
            ->addColumn(
                'file_path',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'File Path'
            )
            ->addIndex(
                $setup->getIdxName('wk_b2b_quote_item_attachment', ['quote_item_id']),
                ['quote_item_id']
            )
            ->addForeignKey(
                $setup->getFkName(
                    'wk_b2b_quote_item_attachment',
                    'quote_item_id',
                    'wk_b2b_requestforquote_quote_item',
                    'entity_id'
                ),
                'quote_item_id',
                $setup->getTable('wk_b2b_requestforquote_quote_item'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->setComment('wk_b2b_quote_item_attachment');
        $setup->getConnection()->createTable($table);

        /**
         * Drop column sample_images from tables 'wk_b2b_requestforquote_quote_conversation'
         */
        $setup->getConnection()->dropColumn(
            $setup->getTable('wk_b2b_requestforquote_quote_conversation'),
            'sample_images'
        );

        /*
         * Create table 'wk_b2b_quote_conversation_attachment' for saving conversation samples
         */
        $table = $setup->getConnection()
            ->newTable($setup->getTable('wk_b2b_quote_conversation_attachment'))
            ->addColumn(
                'attachment_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Attachment Id'
            )
            ->addColumn(
                'quote_conversation_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Quote Conversation Id'
            )
            ->addColumn(
                'file_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'File Name'
            )
            ->addColumn(
                'file_path',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'File Path'
            )
            ->addIndex(
                $setup->getIdxName('wk_b2b_quote_conversation_attachment', ['quote_conversation_id']),
                ['quote_conversation_id']
            )
            ->addForeignKey(
                $setup->getFkName(
                    'wk_b2b_quote_conversation_attachment',
                    'quote_conversation_id',
                    'wk_b2b_requestforquote_quote_conversation',
                    'entity_id'
                ),
                'quote_conversation_id',
                $setup->getTable('wk_b2b_requestforquote_quote_conversation'),
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            )
            ->setComment('wk_b2b_quote_conversation_attachment');
        $setup->getConnection()->createTable($table);

        /**
         * Update tables 'wk_message_attachments'
         */
        $setup->getConnection()->addColumn(
            $setup->getTable('wk_message_attachments'),
            'file_name',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'nullable' => false,
                'comment' => 'file_name'
            ]
        );
        $setup->getConnection()->addIndex(
            $setup->getTable('wk_message_attachments'),
            $setup->getIdxName('wk_message_attachments', ['msg_id']),
            ['msg_id']
        );
        try {
            $setup->getConnection()->addForeignKey(
                $setup->getFkName(
                    'wk_message_attachments',
                    'msg_id',
                    'wk_b2b_message_history',
                    'id'
                ),
                $setup->getTable('wk_message_attachments'),
                'msg_id',
                $setup->getTable('wk_b2b_message_history'),
                'id',
                \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
            );
        } catch (\Exception $e) {
            $e->getMessage();
        }

        $setup->endSetup();
    }
}
