<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_B2BMarketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\B2BMarketplace\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        
        /*
         * Create table 'wk_b2b_message_history'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_message_history'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Id'
            )
            ->addColumn(
                'list_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'List Id'
            )
            ->addColumn(
                'sender_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Sender Id'
            )
            ->addColumn(
                'receiver_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Receiver Id'
            )
            ->addColumn(
                'parent_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Parent Product Id'
            )
            ->addColumn(
                'seen_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Seen Status'
            )
            ->addColumn(
                'msg',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                '4M',
                ['nullable' => false],
                'Message'
            )
            ->addColumn(
                'type',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Type'
            )
            ->addColumn(
                'response_rate_cal_flag',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                1,
                ['nullable' => false, 'default' => 1, 'unsigned' => true],
                'response_rate_cal_flag'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created At'
            )
            ->setComment('Message History');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_b2b_message_list'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_message_list'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Id'
            )
            ->addColumn(
                'supplier_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Supplier Id'
            )
            ->addColumn(
                'customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Customer Id'
            )
            ->addColumn(
                'customer_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Customer Status'
            )
            ->addColumn(
                'supplier_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Supplier Status'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created At'
            )
            ->setComment('Message List');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_message_attachments'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_message_attachments'))
            ->addColumn(
                'id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                ],
                'Id'
            )
            ->addColumn(
                'msg_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Message Id'
            )
            ->addColumn(
                'type',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['nullable' => false, 'unsigned' => true],
                'Type'
            )
            ->addColumn(
                'value',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                ['nullable' => false],
                'Value'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created At'
            )
            ->setComment('Message List');
        $installer->getConnection()->createTable($table);

        /**
         * Update table 'marketplace_userdata' to add token column
         */
        $installer->getConnection()->addColumn(
            $installer->getTable('marketplace_userdata'),
            'token',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                'default' => '',
                'comment' => 'Token'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('marketplace_userdata'),
            'default_address_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'comment' => 'Default Address Id'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('marketplace_userdata'),
            'operational_address_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'comment' => 'Operational Address Id'
            ]
        );

        $installer->getConnection()->addColumn(
            $installer->getTable('marketplace_userdata'),
            'corporate_address_id',
            [
                'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                'comment' => 'Corporate Address Id'
            ]
        );

        /*
         * Create table 'wk_b2b_requestforquote_buying_leads'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_requestforquote_buying_leads'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quote Id'
            )
            ->addColumn(
                'quote_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quote Item Id'
            )
            ->addColumn(
                'seller_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'seller id'
            )
            ->addColumn(
                'expiry_date',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Expiry Date'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created At'
            )
            ->setComment('wk_b2b_requestforquote_buying_leads');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_b2b_requestforquote_quote'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_requestforquote_quote'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'customer_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Customer Id'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Status'
            )
            ->addColumn(
                'quote_title',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'quote_title'
            )
            ->addColumn(
                'quote_desc',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'quote_desc'
            )
            ->addColumn(
                'customer_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'customer_name'
            )
            ->addColumn(
                'customer_company_name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'customer_company_name'
            )
            ->addColumn(
                'customer_address',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'customer_address'
            )
            ->addColumn(
                'customer_contact_no',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'customer_contact_no'
            )
            ->addColumn(
                'attachments',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'attachments'
            )
            ->addColumn(
                'forward_quote',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'forward_quote'
            )
            ->addColumn(
                'is_buying_lead',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'is_buying_lead'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created At'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Updated Date'
            )
            ->setComment('wk_b2b_requestforquote_quote');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_b2b_requestforquote_quote_conversation'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_requestforquote_quote_conversation'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'seller_quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Seller Quote Id'
            )
            ->addColumn(
                'response',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Response'
            )
            ->addColumn(
                'sample_images',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Sample Images'
            )
            ->addColumn(
                'sender_type',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'sender_type'
            )
            ->addColumn(
                'item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Item Id'
            )
            ->addColumn(
                'sender_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Sender Id'
            )
            ->addColumn(
                'receiver_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Receiver Id'
            )
            ->addColumn(
                'quote_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'quote_status'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created Date'
            )
            ->addColumn(
                'updated_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Updated At'
            )
            ->setComment('wk_b2b_requestforquote_quote_conversation');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_b2b_requestforquote_quote_item'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_requestforquote_quote_item'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quote Id'
            )
            ->addColumn(
                'seller_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'seller id'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Product Id'
            )
            ->addColumn(
                'name',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'Name'
            )
            ->addColumn(
                'description',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Description'
            )
            ->addColumn(
                'qty',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quantity'
            )
            ->addColumn(
                'price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => false, 'default' => '0.0000'],
                'Price'
            )
            ->addColumn(
                'has_samples',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'has_samples'
            )
            ->addColumn(
                'samples',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Samples'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Status'
            )
            ->addColumn(
                'buying_lead_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'buying_lead_status'
            )
            ->addColumn(
                'forward_quote',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'forward_quote'
            )
            ->addColumn(
                'categories',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => true, 'default' => null],
                'categories'
            )
            ->setComment('wk_b2b_requestforquote_quote_item');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_b2b_requestforquote_quote_products'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_requestforquote_quote_products'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quote Id'
            )
            ->addColumn(
                'product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Product Id'
            )
            ->addColumn(
                'approved_quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Approved Quote Id'
            )
            ->addColumn(
                'original_product_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Original Product Id'
            )
            ->addColumn(
                'mage_quote_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'mage_quote_item_id'
            )
            ->addColumn(
                'mage_order_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'mage_order_item_id'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created At'
            )
            ->addIndex(
                $installer->getIdxName('wk_b2b_requestforquote_quote_products', ['product_id']),
                ['product_id']
            )
            ->setComment('wk_b2b_requestforquote_quote_products');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_b2b_requestforquote_sent_quotes'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_requestforquote_sent_quotes'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'request_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'request_id'
            )
            ->addColumn(
                'quote_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quote Id'
            )
            ->addColumn(
                'quote_item_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quote Item Id'
            )
            ->addColumn(
                'seller_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'seller id'
            )
            ->addColumn(
                'price',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => false, 'default' => '0.0000'],
                'Price'
            )
            ->addColumn(
                'qty',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Quantity'
            )
            ->addColumn(
                'status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Status'
            )
            ->addColumn(
                'customer_status',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                2,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'Status'
            )
            ->addColumn(
                'has_samples',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                null,
                [],
                'Has Samples'
            )
            ->addColumn(
                'sample_unit',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'sample_unit'
            )
            ->addColumn(
                'sample_charges',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => false, 'default' => '0.0000'],
                'sample_charges'
            )
            ->addColumn(
                'sample_charge_per_unit',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => false, 'default' => '0.0000'],
                'sample_charge_per_unit'
            )
            ->addColumn(
                'shipping_days',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'shipping_days'
            )
            ->addColumn(
                'shipping_charges',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => false, 'default' => '0.0000'],
                'shipping_charges'
            )
            ->addColumn(
                'shipping_charge_per_unit',
                \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
                '12,4',
                ['nullable' => false, 'default' => '0.0000'],
                'shipping_charge_per_unit'
            )
            ->addColumn(
                'created_at',
                \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                null,
                ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE],
                'Created At'
            )
            ->setComment('wk_b2b_requestforquote_sent_quotes');
        $installer->getConnection()->createTable($table);

        /*
         * Create table 'wk_b2b_supplier_categories'
         */
        $table = $installer->getConnection()
            ->newTable($installer->getTable('wk_b2b_supplier_categories'))
            ->addColumn(
                'entity_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Id'
            )
            ->addColumn(
                'supplier_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'supplier_id'
            )
            ->addColumn(
                'category_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                10,
                ['unsigned' => true, 'nullable' => false, 'default' => '0'],
                'category_id'
            )
            ->setComment('wk_b2b_supplier_categories');
        $installer->getConnection()->createTable($table);
        try {
            $this->addForeignKeys($installer);
        } catch (\Exception $e) {
            $e->getMessage();
        }

        $installer->endSetup();
    }

    /**
     *
     * @param SchemaSetupInterface $installer
     * @return void
     */
    private function addForeignKeys(SchemaSetupInterface $installer)
    {
        /**
         * Add foreign keys for Product ID
         */
        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_requestforquote_quote_products',
                'product_id',
                'catalog_product_entity',
                'entity_id'
            ),
            $installer->getTable('wk_b2b_requestforquote_quote_products'),
            'product_id',
            $installer->getTable('catalog_product_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_message_history',
                'sender_id',
                $installer->getTable('customer_entity'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_message_history'),
            'sender_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_message_list',
                'supplier_id',
                $installer->getTable('customer_entity'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_message_list'),
            'supplier_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_requestforquote_quote',
                'customer_id',
                $installer->getTable('customer_entity'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_requestforquote_quote'),
            'customer_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_requestforquote_quote_conversation',
                'sender_id',
                $installer->getTable('customer_entity'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_requestforquote_quote_conversation'),
            'sender_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_requestforquote_quote_item',
                'quote_id',
                $installer->getTable('wk_b2b_requestforquote_quote'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_requestforquote_quote_item'),
            'quote_id',
            $installer->getTable('wk_b2b_requestforquote_quote'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_requestforquote_sent_quotes',
                'seller_id',
                $installer->getTable('customer_entity'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_requestforquote_sent_quotes'),
            'seller_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_requestforquote_buying_leads',
                'seller_id',
                $installer->getTable('customer_entity'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_requestforquote_buying_leads'),
            'seller_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_supplier_categories',
                'supplier_id',
                $installer->getTable('customer_entity'),
                'entity_id'
            ),
            $installer->getTable('wk_b2b_supplier_categories'),
            'supplier_id',
            $installer->getTable('customer_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );

        $installer->getConnection()->addForeignKey(
            $installer->getFkName(
                'wk_b2b_supplier_categories',
                'category_id',
                'catalog_category_entity',
                'entity_id'
            ),
            $installer->getTable('wk_b2b_supplier_categories'),
            'category_id',
            $installer->getTable('catalog_category_entity'),
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::ACTION_CASCADE
        );
    }
}
