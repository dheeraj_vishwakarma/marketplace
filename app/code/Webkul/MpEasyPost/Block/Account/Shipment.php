<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpEasyPost\Block\Account;

/**
 * Webkul MpEasyPost Shipment Block
 */
class Shipment extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\MpEasyPost\Helper\Data
     */
    private $currentHelper;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var \Webkul\Marketplace\Model\OrdersFactory
     */
    private $mpOrdersFactory;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    private $mpHelperData;

    /**
     * @var \Webkul\MpEasyPost\Logger\Logger
     */
    private $logger;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\MpEasyPost\Helper\Data $currentHelper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\Marketplace\Model\OrdersFactory $mpOrdersFactory
     * @param \Webkul\Marketplace\Helper\Data $mpHelperData
     * @param \Webkul\MpEasyPost\Logger\Logger $logger
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\MpEasyPost\Helper\Data $currentHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Model\OrdersFactory $mpOrdersFactory,
        \Webkul\Marketplace\Helper\Data $mpHelperData,
        \Webkul\MpEasyPost\Logger\Logger $logger,
        array $data = []
    ) {
        $this->currentHelper = $currentHelper;
        $this->customerSession = $customerSession;
        $this->mpOrdersFactory = $mpOrdersFactory;
        $this->mpHelperData = $mpHelperData;
        $this->logger = $logger;
        parent::__construct($context, $data);
    }

    /**
     * Return current customer session.
     * @return \Magento\Customer\Model\Session
     */
    public function _getCustomerData()
    {
        return $this->customerSession->getCustomer();
    }

    /**
     * Get current module helper.
     *
     * @return \Webkul\MpDHLShipping\Helper\Data
     */
    public function getHelper()
    {
        return $this->currentHelper;
    }

    /**
     * Get current module helper.
     *
     * @return \Webkul\MpEasyPost\Helper\Data
     */
    public function isEasyPostShipment()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        return $this->currentHelper->isEasyPostShipment($orderId);
    }

    /**
     * GetShipmentLabelUrl
     */
    public function getShipmentLabelUrl()
    {
        try {
            $params = $this->getRequest()->getParams();
            $sellerId = $this->customerSession->getCustomerId();
            if (!empty($params) && isset($params['order_id']) && isset($params['shipment_id'])) {
                $orderData = $this->mpOrdersFactory->create()
                                            ->getCollection()
                                            ->addFieldToFilter('seller_id', ['eq' => $sellerId])
                                            ->addFieldToFilter('order_id', ['eq' => $params['order_id']])
                                            ->addFieldToFilter('shipment_id', ['eq' => $params['shipment_id']])
                                            ->setPageSize(1)->getFirstItem();
                return $orderData->getTrackingNumber() ? $orderData->getShipmentLabel() : false;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $this->logger->info('getShipmentLabelUrl-'.$e->getMessage());
        }
    }

    /**
     * IsSeller
     * @return bool
     */
    public function isSeller()
    {
        return $this->mpHelperData->isSeller();
    }
}
