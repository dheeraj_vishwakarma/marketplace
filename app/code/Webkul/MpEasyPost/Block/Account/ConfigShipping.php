<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpEasyPost\Block\Account;

/**
 * Webkul MpEasyPost Config Shipping Block
 */
class ConfigShipping extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\MpEasyPost\Helper\Data
     */
    private $currentHelper;

    /**
     * @var Session
     */
    private $customerSession;

    /**
     * @var \Magento\Config\Model\Config\Source\Yesno
     */
    private $yesNo;

    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry = null;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\App\Response\Http $response
     * @param \Webkul\MpEasyPost\Helper\Data $currentHelper
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Config\Model\Config\Source\Yesno $yesNo
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Response\Http $response,
        \Webkul\MpEasyPost\Helper\Data $currentHelper,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Config\Model\Config\Source\Yesno $yesNo,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        array $data = []
    ) {
        $this->response = $response;
        $this->currentHelper = $currentHelper;
        $this->customerSession = $customerSession;
        $this->yesNo = $yesNo;
        $this->coreRegistry = $coreRegistry;
        $this->encryptor = $encryptor;
        parent::__construct($context, $data);
    }

    /**
     * Return current customer EasypostApiKey.
     * @return \Magento\Customer\Model\Session
     */
    public function _getCustomerEtsyApiData()
    {
        $etsyApiKey = $this->customerSession->getCustomer()->getEasypostApiKey();
        return $this->encryptor->decrypt($etsyApiKey);
    }

    /**
     * IsConfigSettingAllowed
     * @return bool|\Magento\Framework\App\Response\Http
     */
    public function isConfigSettingAllowed()
    {
        if ($this->getConfigData('active') && $this->getConfigData('allow_seller')) {
            return true;
        } else {
            $this->response->setRedirect($this->getUrl('marketplace/account/dashboard'));
        }
    }

    /**
     * Retrieve information from carrier configuration.
     * @param string $field
     * @return void|false|string
     */
    public function getConfigData($field)
    {
        return $this->getHelper()->getConfigData($field);
    }

    /**
     * Get current module helper.
     *
     * @return \Webkul\MpDHLShipping\Helper\Data
     */
    public function getHelper()
    {
        return $this->currentHelper;
    }

    public function yesNoData()
    {
        return $this->yesNo->toOptionArray();
    }

    /**
     * Retrieve current order model instance
     *
     */
    public function getOrder()
    {
        return $this->coreRegistry->registry('sales_order');
    }

    /**
     * Get Currency Code for Custom Value
     *
     * @return string
     */
    public function getCustomValueCurrencyCode()
    {
        $orderInfo = $this->getOrder();
        return $orderInfo->getBaseCurrency()->getCurrencyCode();
    }
}
