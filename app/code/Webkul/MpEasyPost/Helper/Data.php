<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpEasyPost\Helper;

use Magento\Store\Model\ScopeInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\Marketplace\Model\OrdersFactory as MpOrders;
use Webkul\MpEasyPost\Logger\Logger;

/**
 * Webkul MpEasyPost Helper Data.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var Magento\Sales\Api\OrderRepositoryInterface;
     */
    private $orderRepository;

    /**
     * @var \Webkul\Marketplace\Model\OrdersFactory
     */
    private $mpOrders;

    /**
     * @var \Webkul\MpEasyPost\Logger\Logger;
     */
    private $logger;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\App\Helper\Context $context,
     * @param \Magento\Customer\Model\Session $customerSession,
     * @param OrderRepositoryInterface $orderRepository,
     * @param MpOrders $mpOrders,
     * @param Logger $logger
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        OrderRepositoryInterface $orderRepository,
        MpOrders $mpOrders,
        Logger $logger
    ) {
        $this->customerSession = $customerSession;
        $this->orderRepository = $orderRepository;
        $this->mpOrders = $mpOrders;
        $this->logger = $logger;
        parent::__construct($context);
    }

    /**
     * GetConfigData
     *
     * @param string $tag
     * @return string
     */
    public function getConfigData($tag)
    {
        return  $this->scopeConfig->getValue('carriers/mpeasypost/'.$tag, ScopeInterface::SCOPE_STORE);
    }

    /**
     * IsAuspostShipment
     *
     * @param int $orderId
     * @return bool

     */
    public function isEasyPostShipment($orderId)
    {
        try {
            $order = $this->orderRepository->get($orderId);
            $customerId = $this->customerSession->getCustomerId();
            $shippingmethod = $order->getShippingMethod();
            $sellerOrders = $this->mpOrders->create()->getCollection()
                                    ->addFieldToFilter('seller_id', ['eq' => $customerId])
                                    ->addFieldToFilter('order_id', ['eq' => $orderId])
                                    ->setPageSize(1)->getFirstItem();
            if (strpos($shippingmethod, 'mpeasypost') !== false) {
                return $sellerOrders->getShipmentLabel() != '' ? true : false;
            } elseif (strpos($shippingmethod, 'mp_multishipping') !== false) {
                return ($sellerOrders->getShipmentLabel() != ''
                    && strpos($row->getMultishipMethod(), 'mpeasypost') !== false) ? true : false;
            }
            return false;
        } catch (\Exception $e) {
            $this->logger->info('isEasyPostShipment '.$e->getMessage());
        }
    }
}
