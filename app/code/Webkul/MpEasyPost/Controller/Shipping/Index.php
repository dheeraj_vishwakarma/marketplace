<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpEasyPost\Controller\Shipping;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;

/**
 * Webkul MpEasyPost  Controller Index
 */
class Index extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Customer\Model\Url
     */
    private $customerUrl;

    /**
     * Initialize dependencies
     *
     * @param Context $context,
     * @param CustomerRepositoryInterface $customerRepository,
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Customer\Model\Url $customerUrl
     */
    public function __construct(
        Context $context,
        CustomerRepositoryInterface $customerRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Url $customerUrl
    ) {
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->customerUrl = $customerUrl;
        parent::__construct($context);
    }

    /**
     * Check customer authentication.
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->customerUrl->getLoginUrl();
        if (!$this->customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     * Save Seller's EasyPost configuration Data.
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        if ($this->getRequest()->isPost()) {
            try {
                $customerData = $this->getRequest()->getParams();
                if (isset($customerData['easypost_api_change']) && $customerData['easypost_api_change']) {
                    $customerId = $this->customerSession->getCustomerId();
                    $customer = $this->customerRepository->getById($customerId);
                    $customer->setCustomAttribute('easypost_api_key', $customerData['easypost_api_key']);
                    $this->customerRepository->save($customer);
                }
                $this->messageManager->addSuccess(__('EasyPost shipping API key saved successfully.'));
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());
            }

            return $this->resultRedirectFactory->create()
                            ->setPath('easypost/shipping/view', ['_secure' => $this->getRequest()->isSecure()]);
        }
         return $this->resultRedirectFactory->create()
                            ->setPath('easypost/shipping/view', ['_secure' => $this->getRequest()->isSecure()]);
    }
}
