
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
define([
    'jquery'
], function ($) {
    'use strict';

    $('#easypost_api_key').on('change', function () {
        $('#easypost_api_change').val(1);
    });
});
