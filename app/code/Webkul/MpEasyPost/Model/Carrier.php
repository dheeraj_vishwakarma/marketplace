<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpEasyPost\Model;

use Magento\Quote\Model\Quote\Address\RateResult\Error;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Session\SessionManager;
use Magento\Quote\Model\Quote\Item\OptionFactory;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Encryption\EncryptorInterface;
use Magento\Shipping\Helper\Carrier as CarrierHelper;
use Magento\Shipping\Model\Rate\Result;
use Magento\Framework\Xml\Security;
use Magento\Framework\Module\Dir;
use Magento\Shipping\Model\Shipping\LabelGenerator;
use Webkul\MarketplaceBaseShipping\Model\Carrier\AbstractCarrierOnline;
use Webkul\MarketplaceBaseShipping\Model\ShippingSettingRepository;
use Magento\Dhl\Model\Carrier as Dhlcarrier;
use Magento\Framework\Filesystem\Driver\File;

/**
 * Marketplace MpEasyPost shipping.
 */
class Carrier extends AbstractCarrierOnline implements \Magento\Shipping\Model\Carrier\CarrierInterface
{
    /**
     * Code of the carrier.
     *
     * @var string
     */
    const CODE = 'mpeasypost';

    /**
     * Code of the carrier.
     *
     * @var string
     */
    protected $_code = self::CODE;

    /**
     * Error flag
     * @var boolean
     */
    protected $flag = false;

    /**
     * Error string return from Api
     * @var string
     */
    protected $errorMsg = '';

    /**
     * @var array
     */
    protected $_sellerCredentials = false;

    /**
     * @var Magento\Catalog\Model\ProductFactory
     */
    protected $productFactory;

    /**
     * @var \Magento\Framework\ObjectManagerInterface
     */
    protected $objectManager;

    /**
     * @var \Magento\Shipping\Model\Rate\ResultFactory
     */
    protected $rateFactory;

    /**
     * @var \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory
     */
    protected $rateMethodFactory;

    /**
     * @var Magento\Framework\Session\SessionManager
     */
    protected $_coreSession;

    /**
     * @var Magento\Quote\Model\Quote\Item\OptionFactory
     */
    protected $itemOptionModel;

    /**
     * @var Magento\Customer\Api\CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var Magento\Sales\Api\OrderRepositoryInterface
     */
    protected $orderRepository;

    /**
     * @var Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var Magento\Directory\Model\RegionFactory
     */
    protected $region;

    /**
     * @var Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\HTTP\Client\Curl
     */
    protected $curl;

    /**
     * @var \Webkul\Marketplace\Model\ProductFactory
     */
    protected $mpProductFactory;

    /**
     * @var \Webkul\MpEasyPost\Logger\Logger
     */
    protected $easyLogger;

    /**
     * @var \Webkul\Marketplace\Helper\Orders
     */
    protected $mpOrderHelper;

    /**
     * @var array
     */
    protected $totalPriceArr = [];

    /**
     *
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Sales\Model\Order $modelOrder
     * @param ManagerInterface $messageManager
     * @param Security $xmlSecurity
     * @param \Magento\Framework\Encryption\EncryptorInterface $encryptor
     * @param \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory
     * @param \Magento\Shipping\Model\Rate\ResultFactory $rateFactory
     * @param \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory
     * @param \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory
     * @param \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory
     * @param \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param \Magento\Directory\Model\CountryFactory $countryFactory
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Directory\Helper\Data $directoryData
     * @param \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry
     * @param \Webkul\Marketplace\Helper\Orders $marketplaceOrderHelper
     * @param \Webkul\Marketplace\Model\ProductFactory $marketplaceProductFactory
     * @param \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory
     * @param ShippingSettingRepository $shippingSettingRepository
     * @param \Magento\Catalog\Model\ProductFactory $productFactory
     * @param \Magento\Customer\Model\AddressFactory $addressFactory
     * @param \Magento\Customer\Model\CustomerFactory $customerFactory
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Magento\Framework\App\Request\Http $requestParam
     * @param \Magento\Quote\Model\Quote\Item\OptionFactory $quoteOptionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param \Magento\Framework\Module\Dir\Reader $configReader
     * @param \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory
     * @param CarrierHelper $carrierHelper
     * @param LabelGenerator $labelGenerator
     * @param SessionManager $_coreSession
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\MpEasyPost\Logger\Logger $easyLogger
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Sales\Model\Order $modelOrder,
        ManagerInterface $messageManager,
        Security $xmlSecurity,
        \Magento\Framework\Encryption\EncryptorInterface $encryptor,
        \Magento\Shipping\Model\Simplexml\ElementFactory $xmlElFactory,
        \Magento\Shipping\Model\Rate\ResultFactory $rateFactory,
        \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
        \Magento\Shipping\Model\Tracking\ResultFactory $trackFactory,
        \Magento\Shipping\Model\Tracking\Result\ErrorFactory $trackErrorFactory,
        \Magento\Shipping\Model\Tracking\Result\StatusFactory $trackStatusFactory,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        \Magento\Directory\Model\CountryFactory $countryFactory,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Directory\Helper\Data $directoryData,
        \Magento\CatalogInventory\Api\StockRegistryInterface $stockRegistry,
        \Webkul\Marketplace\Helper\Orders $marketplaceOrderHelper,
        \Webkul\Marketplace\Model\ProductFactory $marketplaceProductFactory,
        \Webkul\Marketplace\Model\SaleslistFactory $saleslistFactory,
        ShippingSettingRepository $shippingSettingRepository,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Customer\Model\AddressFactory $addressFactory,
        \Magento\Customer\Model\CustomerFactory $customerFactory,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\App\Request\Http $requestParam,
        \Magento\Quote\Model\Quote\Item\OptionFactory $quoteOptionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\App\RequestInterface $requestInterface,
        \Magento\Framework\Module\Dir\Reader $configReader,
        \Magento\Framework\HTTP\ZendClientFactory $httpClientFactory,
        CarrierHelper $carrierHelper,
        LabelGenerator $labelGenerator,
        SessionManager $_coreSession,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\MpEasyPost\Logger\Logger $easyLogger,
        File $file,
        array $data = []
    ) {
        $this->file = $file;
        $this->encryptor = $encryptor;
        $this->jsonHelper = $jsonHelper;
        $this->easyLogger = $easyLogger;
        $this->modelOrder = $modelOrder;
        $this->messageManager = $messageManager;
        parent::__construct(
            $scopeConfig,
            $rateErrorFactory,
            $logger,
            $xmlSecurity,
            $xmlElFactory,
            $rateFactory,
            $rateMethodFactory,
            $trackFactory,
            $trackErrorFactory,
            $trackStatusFactory,
            $regionFactory,
            $countryFactory,
            $currencyFactory,
            $directoryData,
            $stockRegistry,
            $marketplaceOrderHelper,
            $marketplaceProductFactory,
            $saleslistFactory,
            $shippingSettingRepository,
            $productFactory,
            $addressFactory,
            $customerFactory,
            $customerSession,
            $requestParam,
            $quoteOptionFactory,
            $storeManager,
            $requestInterface,
            $httpClientFactory,
            $carrierHelper,
            $labelGenerator,
            $_coreSession,
            $data
        );
    }

    /**
     * Collect and get rates.
     *
     * @param RateRequest $request
     *
     * @return \Magento\Quote\Model\Quote\Address\RateResult\Error|bool|Result
     */
    public function collectRates(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {
        try {
            if (!$this->canCollectRates() || $this->isMultiShippingActive() || $this->isStorePickupActive()) {
                return false;
            }
            $this->setRequest($request);
            $this->_result = $this->getShippingPricedetail($this->_rawRequest);
            return $this->_result;
        } catch (\Exception $e) {
            $this->easyLogger->addError('collectRates - '.$e->getMessage());
        }
    }

    /**
     * Get shipping url
     * @return string
     *
     * @return void
     */
    protected function _getShippingUrl()
    {
        return $this->_shipServiceWsdl;
    }

    /**
     * GetSellerIdOfItemId
     * @param \Magento\Quote\Model\Quote\Item $item,
     * @return int
     */
    public function getSellerIdOfItemId($item)
    {
        try {
            $partner = 0;
            $itemOption = $this->itemOptionModel->create()->getCollection()
                                                    ->addFieldToFilter('item_id', ['eq' => $item->getId()])
                                                    ->addFieldToFilter('code', ['eq' => 'info_buyRequest'])
                                                    ->setPageSize(1)->getFirstItem();
            $optionValue = $itemOption->getEntityId() ? $itemOption->getValue() : '';
            $mpassignproductId = 0;
            if ($optionValue != '') {
                $temp = $this->jsonHelper->jsonDecode($optionValue);
                $mpassignproductId = isset($temp['mpassignproduct_id']) ? $temp['mpassignproduct_id'] : 0;
                if (!$mpassignproductId) {
                    foreach ($item->getOptions() as $option) {
                        if (isset($option['value']) && $option['value']) {
                            $temp =  $this->jsonHelper->jsonDecode($option['value']);
                            $mpassignproductId = isset($temp['mpassignproduct_id']) ? $temp['mpassignproduct_id'] : 0;
                        }
                    }
                }
            }
            if ($mpassignproductId) {
                $mpassignModel = $this->objectManager->create(\Webkul\MpAssignProduct\Model\Items::class)
                                                        ->load($mpassignproductId);
                $partner = $mpassignModel->getSellerId();
            } else {
                $sellerProducts = $this->mpProductFactory->create()->getCollection()
                                                ->addFieldToFilter('mageproduct_id', ['eq' => $item->getProductId()])
                                                ->setPageSize(1)->getFirstItem();
                $partner = $sellerProducts->getEntityId() ? $sellerProducts->getSellerId() : 0;
            }
            return $partner;
        } catch (\Exception $e) {
            $this->easyLogger->addError('getSellerIdOfItemId - '.$e->getMessage());
            return false;
        }
    }

    /**
     * CalculateWeightForProduct
     * @param \Magento\Quote\Model\Quote\Item $item
     * @return float
     */
    public function calculateWeightForProduct($item)
    {
        try {
            $childWeight = 0;
            if ($item->getHasChildren()) {
                $_product = $item->getProduct();
                if ($_product->getTypeId() == 'bundle') {
                    if ($_product->getWeightType()) {
                        $childWeight = $_product->getWeight();
                    } else {
                        foreach ($item->getChildren() as $child) {
                            $productWeight = $this->productFactory->create()->load($child->getProductId())->getWeight();
                            $childWeight += $productWeight * $child->getQty();
                        }
                    }
                    $weight = $childWeight * $item->getQty();
                } elseif ($_product->getTypeId() == 'configurable') {
                    foreach ($item->getChildren() as $child) {
                        $productWeight = $this->productFactory->create()->load($child->getProductId())->getWeight();
                        $weight = $productWeight * $item->getQty();
                    }
                }
            } else {
                $productWeight = $item->getProduct()->getWeight();
                $weight = $productWeight * $item->getQty();
            }
            return $weight;
        } catch (\Exception $e) {
            $this->easyLogger->addError('calculateWeightForProduct - '.$e->getMessage());
            return 0;
        }
    }

    /**
     * Makes remote request to the carrier and returns a response.
     *
     * @param string $purpose
     *
     * @return mixed
     */
    protected function _createRatesRequest($shipdetail)
    {
        $queryParams = [];
        try {
            $this->_setOriginAddress($shipdetail['seller_id']);
            $rawRequest = $this->_rawRequest;
            foreach ($rawRequest->getAllItems() as $item) {
                $address = $item->getQuote()->getShippingAddress();
                if ($address) {
                    $rawRequest->setName($address->getFirstname()." ".$address->getLastname());
                }
                break;
            }
            $originAddress = $rawRequest->getShippingDetails();
            $destAddress = [
                "name" => $rawRequest->getName(),
                "street1" => $rawRequest->getStreet1(),
                "street2" => $rawRequest->getStreet2(),
                "city" => $rawRequest->getDestCity(),
                "state" => $rawRequest->getDestCity(),
                "zip" => $rawRequest->getDestPostcode(),
                "country" => $rawRequest->getDestCountryId(),
                "phone" => $rawRequest->getPhone(),
                "email" => $rawRequest->getEmail()
            ];
            $sellerId = $shipdetail['seller_id'];
            if ($sellerId) {
                $seller = $this->customerFactory->create()->load($sellerId);
                $rawRequest->setSellerName($seller->getFirstname()." ".$seller->getLastname());
            }
            $originAddress = [
                "name" => $rawRequest->getSellerName(),
                "street1" => $rawRequest->getStreet1(),
                "street2" => $rawRequest->getStreet2(),
                "city" => $rawRequest->getOriginCity(),
                "state" => $rawRequest->getOriginCity(),
                "zip" => $rawRequest->getOriginPostcode(),
                "country" => $rawRequest->getOriginCountryId(),
                "phone" => $rawRequest->getPhone(),
                "email" => $rawRequest->getEmail()
            ];
            $unit = $this->getConfigData('unit_of_measure');
            $weight = ($unit == 'KGS') ? $shipdetail['items_weight'] : (float)$shipdetail['items_weight'] * 0.453592;
            $parcel = [
                "length" => $this->getConfigData('length'),
                "width" => $this->getConfigData('width'),
                "height" => $this->getConfigData('height'),
                "weight" => $weight
            ];
            $queryParams = [
                "to_address" => $destAddress,
                "from_address" => $originAddress,
                "parcel" => $parcel
            ];
        } catch (\Exception $e) {
            $this->easyLogger->addError('_createRatesRequest - '.$e->getMessage());
        }
        return $queryParams;
    }

    /**
     * GetPriceRateFromEasyPost
     * @param array $param
     */
    private function getPriceRateFromEasyPost($param)
    {
        $priceList = [];
        try {
            $request = $this->_rawRequest;
            \EasyPost\EasyPost::setApiKey($request->getApiKey());
            $shipment = \EasyPost\Shipment::create($param);
            if ($shipment) {
                $shipment =  $this->jsonHelper->jsonDecode($shipment);
            }
            $errorMsg = '';
            if (is_array($shipment['rates']) && !empty($shipment['rates'])) {
                foreach ($shipment['rates'] as $method) {
                    $serviceName = implode(" ", preg_split('/(?=[A-Z])/', $method['service']));
                    $priceList[$method['service'].'-'.$method['carrier']] = [
                        'label' => $serviceName.' ('.$method['carrier'].')',
                        'amount' => $method['retail_rate'],
                        'shipment_id' => $method['shipment_id'],
                        'rate_id' => $method['id']
                    ];
                }
            } elseif (is_array($shipment['messages']) && !empty($shipment['messages'])) {
                foreach ($shipment['messages'] as $key => $value) {
                    if (isset($value['type']) && $value['type'] == 'rate_error') {
                        $errorMsg = $errorMsg."\r\n".$value['carrier']." (".$value['message'].")";
                    }
                }
            }
            $this->errorMsg = $errorMsg;
        } catch (\Exception $e) {
            $this->easyLogger->addError('getPriceRateFromEasyPost - '.$e->getMessage());
        }
        return $priceList;
    }

    /**
     * _filterSellerRate
     * @param array $priceArr
     * @return void
     */
    protected function _filterSellerRate($priceArr)
    {
        try {
            if (!empty($this->totalPriceArr)) {
                foreach ($priceArr as $method => $price) {
                    if (array_key_exists($method, $this->totalPriceArr)) {
                        $this->_check = true;
                        $this->totalPriceArr[$method]['amount'] = $this->totalPriceArr[$method]['amount'] +
                        $priceArr[$method]['amount'];
                    } else {
                        unset($priceArr[$method]);
                        $this->flag = $this->_check == true ? false : true;
                    }
                }
            } else {
                $this->totalPriceArr = $priceArr;
            }
            if (!empty($priceArr)) {
                foreach ($this->totalPriceArr as $method => $price) {
                    if (!array_key_exists($method, $priceArr)) {
                        unset($this->totalPriceArr[$method]);
                    }
                }
            } else {
                $this->totalPriceArr = [];
                $this->flag = true;
            }
        } catch (\Exception $e) {
            $this->easyLogger->addError('_filterSellerRate - '.$e->getMessage());
        }
    }

    /**
     * GetShippingPricedetail.
     * @param \Magento\Framework\DataObject $request
     * @return Result
     */
    public function getShippingPricedetail(\Magento\Framework\DataObject $request)
    {
        try {
            $this->_rawRequest = $request;
            $this->setRawRequest($request);
            $r = $request;
            $submethod = [];
            $shippinginfo = [];
            $totalpric = [];
            $totalPriceArr = [];
            $serviceCodeToActualNameMap = [];
            $costArr = [];
            $debugData = [];
            $itemPriceDetails = [];
            $price = 0;
            foreach ($r->getShippingDetails() as $shipdetail) {
                $priceArr = [];
                $this->_isSellerHasOwnCredentials($request, $shipdetail['seller_id']);

                $priceArr = $this->getShippingDetail($shipdetail, $r);
                $newShipdetails = $shipdetail;
                $items = explode(',', $shipdetail['item_id']);
                if (isset($shipdetail['item_weight_details']) &&
                    isset($shipdetail['item_product_price_details']) &&
                    isset($shipdetail['item_qty_details'])
                    ) {
                    $itemPriceArr = [];
                    $itemCostArr = [];
                    foreach ($items as $itemId) {
                        $newShipdetails['items_weight'] = $shipdetail['item_weight_details'][$itemId];
                        $id =  $shipdetail['item_product_price_details'][$itemId];
                        $qty = $shipdetail['item_qty_details'][$itemId];
                        $newShipdetails['price'] = $id * $qty;
                        $itemPriceArr = $this->getShippingDetail($newShipdetails);
                        $itemPriceDetails[$itemId] = $itemPriceArr;
                    }
                }
                $this->_filterSellerRate($priceArr);

                if ($this->flag) {
                    $response['type'] = 'error';
                    if ($this->isMultiShippingActive() || $this->isStorePickupActive()) {
                        return [];
                    } else {
                        return $this->_parseResponse($response);
                    }
                }
                $submethod = [];

                foreach ($priceArr as $index => $price) {
                    $submethod[$index] = [
                        'method' => $price['label'],
                        'cost' => $price['amount'],
                        'base_amount' => $price['amount'],
                        'shipment_id' => $price['shipment_id'],
                        'rate_id' => $price['rate_id'],
                        'error' => 0,
                    ];
                    $costArr[$index] = $price['amount'];
                }
                $shipdetail = $this->checkAndUpdateShippingDetails($shipdetail);
                array_push(
                    $shippinginfo,
                    [
                        'sellerCredentials' => $this->_sellerCredentials,
                        'seller_id' => $shipdetail['seller_id'],
                        'methodcode' => $this->_code,
                        'shipping_ammount' => $price,
                        'product_name' => $shipdetail['product_name'],
                        'submethod' => $submethod,
                        'item_ids' => $shipdetail['item_id'],
                        'item_price_details' => $itemPriceDetails,
                        'item_id_details' => $shipdetail['item_id_details'],
                        'item_name_details' => $shipdetail['item_name_details'],
                        'item_qty_details' => $shipdetail['item_qty_details']
                    ]
                );
            }
            $totalpric = ['totalprice' => $this->totalPriceArr, 'costarr' => $costArr];
            $debugData['result'] = $totalpric;
            $result = ['handlingfee' => $totalpric, 'shippinginfo' => $shippinginfo];
            $shippingAll = $this->_coreSession->getShippingInfo();
            $shippingAll[$this->_code] = $result['shippinginfo'];
            $this->setShippingInformation($shippingAll);
            if ($this->isMultiShippingActive() || $this->isStorePickupActive()) {
                return $result;
            } else {
                return $this->_parseResponse($totalpric);
            }
        } catch (\Exception $e) {
            $this->easyLogger->addError('getShippingPricedetail - '.$e->getMessage());
        }
    }

    /**
     * Get Shipping Detail
     *
     * @param  array $shipdetail
     */
    public function getShippingDetail($shipdetail)
    {
        $requestParam = $this->_createRatesRequest($shipdetail);
        return $this->getPriceRateFromEasyPost($requestParam);
    }

    /**
     * Check and update Shipping Details
     *
     * @param array $shipdetail
     * @return void
     */
    public function checkAndUpdateShippingDetails($shipdetail)
    {
        if (!isset($shipdetail['item_id_details'])) {
            $shipdetail['item_id_details'] = [];
        }
        if (!isset($shipdetail['item_name_details'])) {
            $shipdetail['item_name_details'] = [];
        }
        if (!isset($shipdetail['item_qty_details'])) {
            $shipdetail['item_qty_details'] = [];
        }
        return $shipdetail;
    }

    /**
     * Parse calculated rates.
     * @param string $response
     * @return Result
     */
    protected function _parseResponse($response)
    {
        try {
            $result = $this->_rateFactory->create();
            if (isset($response['type']) && $response['type'] == 'error') {
                $error = $this->_rateErrorFactory->create();
                $error->setCarrier('mpeasypost');
                $error->setCarrierTitle($this->getConfigData('title'));
                $errorMsg = $this->getConfigData('error_custom_type') ?
                                $this->getConfigData('specificerrmsg') : $this->errorMsg;
                $error->setErrorMessage($errorMsg);
                $result->append($error);
            } else {
                $totalPriceArr = $response['totalprice'];
                $costArr = $response['costarr'];
                foreach ($totalPriceArr as $method => $price) {
                    $cost = $price['amount'];
                    $rate = $this->_rateMethodFactory->create();
                    $rate->setCarrier('mpeasypost');
                    $rate->setCarrierTitle($this->getConfigData('title'));
                    /** $rate->setMethod(str_replace('-USPS' , '', $method)); */
                    $rate->setMethod($method);
                    $rate->setMethodTitle($price['label']);
                    $rate->setCost($costArr[$method]);
                    $rate->setPrice($cost);
                    $result->append($rate);
                }
            }
            return $result;
        } catch (\Exception $e) {
            $this->easyLogger->info('Easypost _parseResponse - '.$e->getMessage());
            $error = $this->_rateErrorFactory->create();
            $error->setCarrier($this->_code);
            $error->setCarrierTitle($this->getConfigData('title'));
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
            return $result;
        }
    }

    /**
     * GetConfigData
     * @param string $tag
     * @return string
     */
    public function getConfigData($tag)
    {
        return $this->_scopeConfig->getValue('carriers/mpeasypost/'.$tag);
    }

    /**
     * Get allowed shipping methods.
     *
     * @return string[]
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getAllowedMethods()
    {
        return ['mpeasypost' => $this->getConfigData('name')];
    }

    /**
     * Set seller credentials.
     *
     * @param \Magento\Framework\DataObject $request
     * @param int $sellerId
     * @return \Magento\Framework\DataObject
     */
    private function _isSellerHasOwnCredentials(\Magento\Framework\DataObject $request, $sellerId)
    {
        try {
            $apiKey = '';
            if ($sellerId) {
                $customer = $this->customerFactory->create()->load($sellerId);
                $easypostApiKey = $customer['easypost_api_key'];
                if ($easypostApiKey && $this->getConfigData('allow_seller')) {
                    $apiKey = $easypostApiKey;
                    $this->_sellerCredentials = true;
                }
            }
            if ($apiKey == '') {
                $apiKey = $this->getConfigData('api_key');
                $apiKey = $this->encryptor->decrypt($apiKey);
            }
            $request->setData('api_key', $apiKey);
            $this->setRawRequest($request);
            return $request;
        } catch (\Exception $e) {
            $this->easyLogger->addError('_isSellerHasOwnCredentials - '.$e->getMessage());
        }
    }

    /**
     * Do shipment request to carrier web service,.
     * @param int $request
     * @return \Magento\Framework\DataObject
     */
    public function _doShipmentRequest(\Magento\Framework\DataObject $request)
    {
        try {
            $orderId = $this->requestParam->getParam('order_id');
            $this->_order = $this->modelOrder->load($orderId);
            $this->_prepareShipmentRequest($request);
            $this->_mapRequestToShipment($request);

            return $response = $this->_createShipmentRequest($orderId);
        } catch (\Exception $e) {
            $this->easyLogger->addError('_doShipmentRequest - '.$e->getMessage());
            return false;
        }
    }

    /**
     * Map request to shipment
     *
     * @param \Magento\Framework\DataObject $request
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _mapRequestToShipment(\Magento\Framework\DataObject $rowRequest)
    {
        $rowRequest->setOrigCountryId($rowRequest->getShipperAddressCountryCode());
        $this->setRawRequest($rowRequest);
        $customsValue = 0;
        $packageWeight = 0;
        $totalValue = 0;
        $packages = $rowRequest->getPackages();
        if (!is_array($packages) || empty($packages)) {
            throw new \Magento\Framework\Exception\LocalizedException(__('No packages for request'));
        }
        foreach ($packages as &$piece) {
            $params = $piece['params'];
            if ($params['width'] || $params['length'] || $params['height']) {
                $minValue = $params['dimension_units'] == "CENTIMETER" ?
                Dhlcarrier::DIMENSION_MIN_CM : Dhlcarrier::DIMENSION_MIN_IN;
                if ($params['width'] < $minValue || $params['length'] < $minValue || $params['height'] < $minValue) {
                    $message = __('Height, width and length should be equal or greater than %1', $minValue);
                    throw new \Magento\Framework\Exception\LocalizedException($message);
                }
            }

            $weightUnit = $piece['params']['weight_units'];
            $customsValue += $piece['params']['customs_value'];
            $packageWeight += $piece['params']['weight'];
            foreach ($piece['items'] as $value) {
                $totalValue += $value['price'];
            }
        }
        $this->customsValue = $customsValue;
        $rowRequest->setPackages($packages)
            ->setPackageValue($totalValue)
            ->setTotalValue($totalValue)
            ->setValueWithDiscount($customsValue)
            ->setPackageCustomsValue(1)
            ->setFreeMethodWeight(0);
    }

    /**
     * Make EasyPost Shipment Request
     * @return string xml
     */
    private function _createShipmentRequest($orderId)
    {
        try {
            $request = $this->_rawRequest;
            $orderId = $this->_order->getId();
            $this->setConfigData($request);
            $apiKey = $this->getEasyPostApiKeyForOrder($this->_order);
            $mpOrder = $this->mpOrder->getShipmentTmpData();
            $shipmentData = explode('-', $mpOrder);
            \EasyPost\EasyPost::setApiKey($apiKey);
            $shipment = \EasyPost\Shipment::retrieve($shipmentData[0], $apiKey);
            $rate = \EasyPost\Rate::retrieve($shipmentData[1], $apiKey);
            $shipment->buy(['rate' => $rate]);
            $shipmentData = ['api_name' => 'EasyPost', 'tracking_number' => $shipment->tracking_code];
            $result = new \Magento\Framework\DataObject();
            $labelContent = $this->file->fileGetContents($shipment->postage_label->label_url);
            $result->setShippingLabelContent($labelContent);
            $result->setTrackingNumber($shipment->tracking_code);
            return $result;
        } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
            $this->easyLogger->addError('_createShipmentRequest - '.$e->getMessage());
        }
    }

    /**
     * Disable shipments for module
     *
     * @return boolean
     */
    public function isShippingLabelsAvailable()
    {
        return false;
    }

    /**
     * Get easy post credentials for order
     *
     * @param object $order
     * @return string $apiKey
     */
    public function getEasyPostApiKeyForOrder($order)
    {
        $sellerId = 0;
        $apiKey = "";
        $orderId = $order->getId();
        $sellerOrder = $this->marketplaceOrderHelper->getOrderinfo($orderId);
        $this->mpOrder = $sellerOrder;
        $sellerId = $sellerOrder->getSellerId();
        if ($sellerId) {
            $apiKey = $this->getSellerEasyPostKey($sellerId);
        }
        if (!$apiKey) {
            $apiKey = $this->encryptor->decrypt($this->getConfigData("api_key"));
        }
        return $apiKey;
    }

    /**
     * Get sellelr easy post key
     *
     * @param int $sellerId
     * @return void
     */
    public function getSellerEasyPostKey($sellerId)
    {
        $customer = $this->customerFactory->create()->load($sellerId);
        $apiKey = $customer->getEasypostApikey();
        return $apiKey;
    }
}
