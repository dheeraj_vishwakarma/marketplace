<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpEasyPost\Model\Config;

use Magento\Config\Model\Config\CommentInterface;

/**
 * Webkul MpEasyPost Configuration Referall
 */
class Referall implements CommentInterface
{
    /**
     * Get Comment Text funcion
     *
     * @param string $elementValue
     * @return string
     */
    public function getCommentText($elementValue)
    {
        return "<a href='https://www.easypost.com/?utm_source=webkul'>Get your easypost credentials</a>";
    }
}
