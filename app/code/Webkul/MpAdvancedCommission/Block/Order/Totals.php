<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAdvancedCommission
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpAdvancedCommission\Block\Order;

use Webkul\Marketplace\Model\ResourceModel\Saleslist\Collection;

class Totals extends \Magento\Sales\Block\Order\Totals
{
    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $helper;

    /**
     * @var Collection
     */
    protected $orderCollection;

    /**
     * Associated array of seller order totals
     * array(
     *  $totalCode => $totalObject
     * )
     *
     * @var array
     */
    protected $_totals;

    /**
     * @param \Webkul\Marketplace\Helper\Data                   $helper
     * @param \Magento\Framework\Registry                       $coreRegistry
     * @param Collection                                        $orderCollection
     * @param \Magento\Framework\View\Element\Template\Context  $context
     * @param array                                             $data
     */
    public function __construct(
        \Webkul\Marketplace\Helper\Data $helper,
        \Magento\Framework\Registry $coreRegistry,
        Collection $orderCollection,
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Marketplace\Model\OrdersFactory $ordersFactory,
        array $data = []
    ) {
        $this->helper = $helper;
        $this->orderCollection = $orderCollection;
        $this->mpOrders = $ordersFactory;
        parent::__construct(
            $context,
            $coreRegistry,
            $data
        );
    }
    public function getOrderCollection()
    {
        $collection = $this->mpOrders->create()
                    ->getCollection()
                    ->addFieldToFilter('order_id', ['eq'=>$this->getOrder()->getId()])
                    ->addFieldToFilter('seller_id', ['eq'=>$this->helper->getCustomerId()])
                    ->getFirstItem();
        return $collection;
    }


    /**
     * add FPT details
     *
     * @return void
     */
    public function initTotals()
    {
        $source = $this->getSource();
        $order = $this->getOrder();
        $items = $order->getAllItems();
        $store = $order->getStore();
        // echo"<pre>";print_r($this->getOrderCollection()->getData());exit;
        $orderCommission = $this->getOrderCollection()->getOrderCommission();
        if ($orderCommission) {
            $totals = $this->getParentBlock()->getTotals();
            $currencyRate = $source[0]['currency_rate'];
            // $totalOrdered = $this->getParentBlock()->getOrderedAmount($source[0])+$initialFee;
            $total = new \Magento\Framework\DataObject(
                [
                    'code' => 'order_commission',
                    'label' => __('Order Commission'),
                    'value' => $this->helper->getCurrentCurrencyPrice($currencyRate, $orderCommission),
                    'base_value' => $orderCommission
                ]
            );
        
            if (isset($totals['admin_commission'])) {
                $this->getParentBlock()->addTotalBefore($total, 'admin_commission');
            } else {
                $this->getParentBlock()->addTotalBefore($total, $this->getBeforeCondition());
            }
            $this->_initOrderedTotal();
            $this->_initBaseOrderedTotal();
            $this->_initVendorSubTotal();
            
        }
        
        return $this;
    }
    /**
     * calculate order total with initial fee
     *
     * @return void
     */
    protected function _initOrderedTotal()
    {
        $parent = $this->getParentBlock();
        $initialFee = $parent->getTotal('order_commission');
        $total = $parent->getTotal('ordered_total');
        if (!$total) {
            return $this;
        }
        $total->setValue($total->getValue() );
        return $this;
    }
    
    /**
     * calculate base total with initial fee
     *
     * @return void
     */
    protected function _initBaseOrderedTotal()
    {
        $parent = $this->getParentBlock();
        $initialFee = $parent->getTotal('order_commission');
        $total = $parent->getTotal('base_ordered_total');
       
        if (!$total) {
            return $this;
        }
        $total->setValue($total->getValue());
        return $this;
    }
    protected function _initVendorSubTotal()
    {
        $parent = $this->getParentBlock();
        $initialFee = $parent->getTotal('order_commission');
        $total = $parent->getTotal('vendor_total');
       
        if (!$total) {
            return $this;
        }
        $total->setValue($total->getValue() - $initialFee->getBaseValue());
        return $this;
    }
}
