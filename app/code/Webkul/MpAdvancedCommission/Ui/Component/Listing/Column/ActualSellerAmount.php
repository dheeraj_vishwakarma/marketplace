<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAdvancedCommission
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpAdvancedCommission\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory;

class ActualSellerAmount extends Column
{
    /**
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Webkul\Marketplace\Helper\Data $helper
     * @param CollectionFactory $orderCollection
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Weee\Helper\Data $weeData
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Webkul\Marketplace\Helper\Data $helper,
        CollectionFactory $orderCollection,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Framework\Pricing\PriceCurrencyInterface $currencyInterface,
        \Webkul\MpAdvancedCommission\Helper\Data $commissionHelper,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        array $components = [],
        array $data = []
    ) {
        $this->helper = $helper;
        $this->orderCollection = $orderCollection;
        $this->orderRepository = $orderRepository;
        $this->currencyFactory = $currencyFactory;
        $this->currencyInterface = $currencyInterface;
        $this->commissionHelper = $commissionHelper;
        $this->priceHelper = $priceHelper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            
            foreach ($dataSource['data']['items'] as &$item) {
                $percentage = 0;
                $totalSellerAmount = 0;
                $collection = $this->orderCollection->create()
                ->addFieldToFilter('seller_id',$item['seller_id'])
                ->addFieldToFilter('order_id',$item['order_id']);
                    foreach($collection as $itemData){
                        $totalSellerAmount += $itemData->getActualSellerAmount();
                    }
                    if($totalSellerAmount > 0){
                        $percentage = ($item['actual_seller_amount']/ $totalSellerAmount)*100;
                    }
                    
                    $orderCommission = $this->commissionHelper->getOrderCommissionRate();
                    $orderCommissionPrice = ($orderCommission*$percentage)/100;
               
        
                if ($item['actual_seller_amount']>0 ) {
                    $price = $item['actual_seller_amount'] - $orderCommissionPrice;
                    $item[$fieldName] = $this->priceHelper->currency(
                        $price,
                        true,false
                    );
                    
                    
                } else {
                    $price = $item['actual_seller_amount'] ;
                    $item[$fieldName] = $this->priceHelper->currency(
                        $price,
                        true,false
                    );
                }
            }
        }

        return $dataSource;
    }
}
