<?php
/**
 * Webkul Software
 *
 * @category Webkul
 * @package Webkul_Recurring
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

namespace Webkul\MpAdvancedCommission\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Sales\Model\OrderFactory;
use Magento\Framework\DB\Transaction;
use Magento\Quote\Model\QuoteRepository;
use Webkul\Marketplace\Model\OrdersFactory;

class SalesOrderPlaceAfterObserver implements ObserverInterface
{
    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var Magento\Sales\Model\OrderFactory;
     */
    protected $orderModel;

    /**
     * @var QuoteRepository
     */
    protected $quoteRepository;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    /**
     * @var \Webkul\Recurring\Helper\Data
     */
    private $helper;

    /**
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param OrderFactory $orderModel
     * @param \Webkul\Recurring\Helper\Data $helper
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param QuoteRepository $quoteRepository
     */
    public function __construct(
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Checkout\Model\Session $checkoutSession,
        OrderFactory $orderModel,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        QuoteRepository $quoteRepository,
        OrdersFactory $mpOrdersFactory,
        \Webkul\MpAdvancedCommission\Helper\Data $mpHelper    
    ) {
        $this->date             = $date;
        $this->jsonHelper       = $jsonHelper;
        $this->checkoutSession  = $checkoutSession;
        $this->orderModel       = $orderModel;
        $this->quoteRepository  = $quoteRepository;
        $this->mpOrdersFactory = $mpOrdersFactory;
        $this->mpHelper = $mpHelper;
    }

    /**
     * Observer action for Sales order place after.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $quoteId = $this->checkoutSession->getLastQuoteId();
            $quote = $this->quoteRepository->get($quoteId);
            $order = $observer->getOrder();
            $lastOrderId = $observer->getOrder()->getId();
            $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/saleslist.log');
            $logger = new \Zend\Log\Logger();
            $logger->addWriter($writer);
            $logger->info($lastOrderId);
            $orderlist = $this->mpOrdersFactory->create()->getCollection()
                    ->addFieldToFilter('order_id',$lastOrderId);
            $orderCommission = $this->mpHelper->getOrderCommissionRate();
            foreach ($orderlist as $key => $orderData) {
                $this->mpOrdersFactory->create()->setEntityId($orderData->getEntityId())
                        ->setOrderCommission($orderCommission)
                        ->save();
            }
            
        } catch (\LocalizedException $e) {
            throw new \Magento\Framework\Exception\LocalizedException(__($e->getMessage()));
        }
    }
    
}
