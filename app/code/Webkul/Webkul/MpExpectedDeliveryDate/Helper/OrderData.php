<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Helper;

/**
 * helper class.
 */
class OrderData extends \Magento\Framework\App\Helper\AbstractHelper implements
    \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param \Magento\GiftMessage\Helper\Message $giftMessageHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\Json\Helper\Data $jsonHelper,
        \Magento\GiftMessage\Helper\Message $giftMessageHelper
    ) {
        $this->jsonHelper = $jsonHelper;
        $this->giftMessageHelper = $giftMessageHelper;
        parent::__construct($context);
    }

    /**
     * Json Encode
     *
     * @param array $data
     * @return string
     */
    public function jsonEncode(array $data): string
    {
        return $this->jsonHelper->jsonEncode($data);
    }

    /**
     * Json Decode
     *
     * @param string $data
     * @return array
     */
    public function jsonDecode(string $data): array
    {
        return $this->jsonHelper->jsonDecode($data);
    }

    /**
     * Get Mp Expp Data
     *
     * @param mixed $data
     * @return void
     */
    public function getMpExpddData($data)
    {
        $response = '';
        if ($data) {
            $data = (array) $this->jsonDecode($data);
            if (!empty($data['mpexpdd'])) {
                $response = $data['mpexpdd'];
            }
        }
        return $response;
    }

    public function getGiftMessageHelper()
    {
        return $this->giftMessageHelper;
    }
}
