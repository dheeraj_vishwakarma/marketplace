<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Helper;

use Magento\Customer\Model\SessionFactory as CustomerSession;

/**
 * helper class.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * Customer session.
     *
     * @var CustomerSession
     */
    protected $customerSession;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;
   
    /**
     * @param CustomerSession $customerSession
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Model\CustomerFactory $customer
     * @param \Webkul\Marketplace\Model\ProductFactory $marketplaceProduct
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     */
    public function __construct(
        CustomerSession $customerSession,
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Model\CustomerFactory $customer,
        \Webkul\Marketplace\Model\ProductFactory $marketplaceProduct,
        \Magento\Framework\Json\Helper\Data $jsonHelper
    ) {
        $this->marketplaceProduct = $marketplaceProduct;
        $this->customerSession = $customerSession;
        $this->_storeManager = $storeManager;
        $this->customer = $customer;
        $this->scopeConfig = $scopeConfig;
        $this->jsonHelper = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * Get value from config
     *
     * @param string $key
     * @return string
     */
    public function getCoreConfigValue(string $key): string
    {
        return $this->scopeConfig->getValue($key, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * Json Encode
     *
     * @param array $data
     * @return string
     */
    public function jsonEncode(array $data): string
    {
        return $this->jsonHelper->jsonEncode($data);
    }

    /**
     * Json Decode
     *
     * @param string $data
     * @return array
     */
    public function jsonDecode(string $data): array
    {
        return $this->jsonHelper->jsonDecode($data);
    }

    /**
     * Get Seller Id
     *
     * @return int|null
     */
    public function getSellerId()
    {
        return $this->customerSession->create()->getCustomer()->getId();
    }

    /**
     * Get Customer Default Shipping Address
     *
     * @return int
     */
    public function getCustomerAddress()
    {
        return $this->customerSession->create()->getCustomer()->getDefaultShipping();
    }

    /**
     * Get Seller Id From Product Id
     *
     * @param string $productId
     * @return string
     */
    public function getSellerIdFromProductId($productId)
    {
        return $this->marketplaceProduct->create()
        ->getCollection()
        ->addFieldToFilter('mageproduct_id', ['eq'=>$productId])
        ->getFirstItem()->getSellerId();
    }

    /**
     * Get Seller Name
     *
     * @param int $sellerId
     * @return string
     */
    public function getSellerName($sellerId)
    {
        return $this->customer->create()->load($sellerId)->getFirstname();
    }
}
