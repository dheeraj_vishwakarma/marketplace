<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Api;

interface ExpddRegionZipRepoInterface
{

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode
     */
    public function getById($id);
    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode $subject);
    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria);
    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode $subject);
    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id);

    /**
     * delete by region id
     *
     * @param int $regionId
     * @return boolean
     */
    public function deleteByRegionId($regionId);

    /**
     * get by region id
     *
     * @param int $regionId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode
     */
    public function getByRegionId($regionId);

    /**
     * get by seller id
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode
     */
    public function getBySellerId($sellerId);
}
