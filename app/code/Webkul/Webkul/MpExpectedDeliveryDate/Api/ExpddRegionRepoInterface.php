<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Api;

interface ExpddRegionRepoInterface
{

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegion
     */
    public function getById($id);
    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegion
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegion $subject);
    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria);
    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRegion $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegion $subject);
    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id);
}
