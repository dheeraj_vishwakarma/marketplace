<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Api;

/**
 * ExpddConfigRepo Interface
 * Expected Delivery Date Config Repository
 */
interface ExpddConfigRepoInterface
{

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddConfig
     */
    public function getById($id);
    /**
     * save by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddConfig
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddConfig $subject);
    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria);
    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddConfig $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddConfig $subject);
    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id);

    /**
     * Get Configuration By Seller Id
     *
     * @param integer $sellerId
     * @return \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddConfig\Collection|null
     */
    public function getBySellerId($sellerId);
}
