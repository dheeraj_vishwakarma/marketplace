<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Api\Data;

interface ExpddConfigInterface
{

    const ID = 'entity_id';

    const STATUS = 'status';

    const SELLER_ID = 'seller_id';

    const WEEKENDS = 'weekends';

    const HOLIDAYS = 'holidays';

    const TEXT_FOR_RANGE = 'text_for_range';

    const TEXT_FOR_SPECIFIC = 'text_for_specific';

    const TEXT_FOR_NOT_AVAILABLE = 'text_for_not_available';

    /**
     * Set Id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setId($id);
    /**
     * Get Id
     *
     * @return int
     */
    public function getId();
    /**
     * Set Status
     *
     * @param int $status
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setStatus($status);
    /**
     * Get Status
     *
     * @return int
     */
    public function getStatus();
    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setSellerId($sellerId);
    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId();
    /**
     * Set Weekends
     *
     * @param string $weekends
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setWeekends($weekends);
    /**
     * Get Weekends
     *
     * @return string
     */
    public function getWeekends();
    /**
     * Set Holidays
     *
     * @param string $holidays
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setHolidays($holidays);
    /**
     * Get Holidays
     *
     * @return string
     */
    public function getHolidays();
    /**
     * Set TextForRange
     *
     * @param string $textForRange
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setTextForRange($textForRange);
    /**
     * Get TextForRange
     *
     * @return string
     */
    public function getTextForRange();
    /**
     * Set TextForSpecific
     *
     * @param string $textForSpecific
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setTextForSpecific($textForSpecific);
    /**
     * Get TextForSpecific
     *
     * @return string
     */
    public function getTextForSpecific();
    /**
     * Set TextForNotAvailable
     *
     * @param string $textForNotAvailable
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setTextForNotAvailable($textForNotAvailable);
    /**
     * Get TextForNotAvailable
     *
     * @return string
     */
    public function getTextForNotAvailable();
}
