<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Api\Data;

interface ExpddRegionInterface
{

    const ENTITY_ID = 'entity_id';

    const STATUS = 'status';

    const SELLER_ID = 'seller_id';

    const NAME = 'name';

    const RULE_ID = 'rule_id';

    const COUNTRY = 'country';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setEntityId($entityId);
    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId();
    /**
     * Set Status
     *
     * @param int $status
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setStatus($status);
    /**
     * Get Status
     *
     * @return int
     */
    public function getStatus();
    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setSellerId($sellerId);
    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId();
    /**
     * Set Name
     *
     * @param string $name
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setName($name);
    /**
     * Get Name
     *
     * @return string
     */
    public function getName();
    /**
     * Set RuleId
     *
     * @param int $ruleId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setRuleId($ruleId);
    /**
     * Get RuleId
     *
     * @return int
     */
    public function getRuleId();
    /**
     * Set Country
     *
     * @param string $country
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setCountry($country);
    /**
     * Get Country
     *
     * @return string
     */
    public function getCountry();
    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setCreatedAt($createdAt);
    /**
     * Get CreatedAt
     *
     * @return string
     */
    public function getCreatedAt();
    /**
     * Set UpdatedAt
     *
     * @param string $updatedAt
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
     */
    public function setUpdatedAt($updatedAt);
    /**
     * Get UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt();
}
