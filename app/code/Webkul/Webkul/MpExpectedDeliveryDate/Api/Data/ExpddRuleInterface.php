<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Api\Data;

interface ExpddRuleInterface
{

    const ID = 'entity_id';

    const STATUS = 'status';

    const SELLER_ID = 'seller_id';

    const NAME = 'name';

    const DESCRIPTION = 'description';

    const ESTIMATION_TYPE = 'estimation_type';

    const DELIVERY_TYPE = 'delivery_type';

    const DAYS_FROM = 'days_from';

    const DAYS_TO = 'days_to';

    /**
     * Set Id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setId($id);
    /**
     * Get Id
     *
     * @return int
     */
    public function getId();
    /**
     * Set Status
     *
     * @param int $status
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setStatus($status);
    /**
     * Get Status
     *
     * @return int
     */
    public function getStatus();
    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setSellerId($sellerId);
    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId();
    /**
     * Set Name
     *
     * @param string $name
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setName($name);
    /**
     * Get Name
     *
     * @return string
     */
    public function getName();
    /**
     * Set Description
     *
     * @param string $description
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDescription($description);
    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription();
    /**
     * Set EstimationType
     *
     * @param string $estimationType
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setEstimationType($estimationType);
    /**
     * Get EstimationType
     *
     * @return string
     */
    public function getEstimationType();
    /**
     * Set DeliveryType
     *
     * @param string $deliveryType
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDeliveryType($deliveryType);
    /**
     * Get DeliveryType
     *
     * @return string
     */
    public function getDeliveryType();
    /**
     * Set DaysFrom
     *
     * @param int $daysFrom
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDaysFrom($daysFrom);
    /**
     * Get DaysFrom
     *
     * @return int
     */
    public function getDaysFrom();
    /**
     * Set DaysTo
     *
     * @param int $daysTo
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDaysTo($daysTo);
    /**
     * Get DaysTo
     *
     * @return int
     */
    public function getDaysTo();
}
