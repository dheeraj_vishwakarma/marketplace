<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Model;

use \Magento\Framework\Api\SearchCriteria\CollectionProcessor;
use \Magento\Framework\Api\Search\SearchResultInterfaceFactory;

class ExpddRegionZipRepo implements \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionZipRepoInterface
{

    protected $modelFactory = null;

    protected $collectionFactory = null;

    /**
     * @param \Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeFactory $modelFactory
     * @param \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegionZipcode\CollectionFactory $collectionFactory
     * @param CollectionProcessor $collectionProcessor
     * @param \Magento\Framework\App\ResourceConnection $resourceConnection
     * @param SearchResultInterfaceFactory $searchResultFactory
     */
    public function __construct(
        \Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeFactory $modelFactory,
        \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegionZipcode\CollectionFactory $collectionFactory,
        CollectionProcessor $collectionProcessor,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        SearchResultInterfaceFactory $searchResultFactory
    ) {
        $this->modelFactory = $modelFactory;
        $this->collectionFactory = $collectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->resourceConnection = $resourceConnection;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode
     */
    public function getById($id)
    {
        $model = $this->modelFactory->create()->load($id);
        if (!$model->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
                __('The CMS block with the "%1" ID doesn\'t exist.', $id)
            );
        }
        return $model;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode $subject)
    {
        try {
            $subject->save();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($exception->getMessage()));
        }
         return $subject;
    }

    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria)
    {
        $collection = $this->collectionFactory->create();
        $searchResult = $this->searchResultFactory->create();
        $this->collectionProcessor->process($creteria, $collection);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcode $subject)
    {
        try {
            $subject->delete();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }
    
    /**
     * delete by region id
     *
     * @param int $regionId
     * @return boolean
     */
    public function deleteByRegionId($regionId)
    {
        $collection = $this->getByRegionId($regionId);

        if ($collection && !empty($collection->getData())) {
            foreach ($collection as $item) {
                $this->delete($item);
            }
        }
        return true;
    }

    /**
     * Get Zipcodes By Seller Id
     *
     * @param integer $sellerId
     * @return \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegionZipcode\Collection
     */
    public function getBySellerId($sellerId)
    {
        try {
            $collection = $this->collectionFactory->create();
            return $collection->addFieldToFilter('seller_id', ['eq'=>$sellerId]);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\LocalizedException($exception->getMessage());
        }
    }

    /**
     * getRuleByZipAndSellerId
     *
     * @param string $sellerId
     * @param string $zipCode
     * @return void
     */
    public function getRuleByZipAndSellerId($sellerId, $zipCode)
    {
        $regionTable = $this->resourceConnection->getTableName('wk_expdd_regions');
        $ruleTable = $this->resourceConnection->getTableName('wk_expdd_rules');

        try {
            $collection = $this->collectionFactory->create();
            $collection->addFieldToFilter('main_table.seller_id', ['eq'=>$sellerId])
            ->addFieldToFilter('zip_from', ['lteq' => $zipCode])
            ->addFieldToFilter('zip_to', ['gteq' => $zipCode])
            ->getSelect()
            ->join(
                ['regions' => $regionTable],
                'main_table.region_id = regions.entity_id',
                [
                    'rule_id' => 'regions.rule_id'
                ]
            )
            ->where("regions.status = 1")
            ->join(
                ['rule' => $ruleTable],
                "regions.rule_id = rule.entity_id",
                [
                    "estimation_type" => "rule.estimation_type",
                    "delivery_type" => "rule.delivery_type",
                    "days_from" => "rule.days_from",
                    "days_to" => "rule.days_to"
                ]
            )
            ->where("rule.status = 1");
            return $collection;
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\LocalizedException($exception->getMessage());
        }
    }
    /**
     * getByRegionId
     *
     * @param string $regionId
     * @return void
     */
    public function getByRegionId($regionId)
    {
        $collection = $this->collectionFactory->create();
        return $collection->addFieldToFilter('region_id', $regionId);
    }
}
