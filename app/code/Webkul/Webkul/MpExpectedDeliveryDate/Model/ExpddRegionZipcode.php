<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Model;

/**
 * Model Expected Delivery Date Regions Zipcode
 */
class ExpddRegionZipcode extends \Magento\Framework\Model\AbstractModel implements
    \Magento\Framework\DataObject\IdentityInterface,
    \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterface
{

    const NOROUTE_ENTITY_ID = 'no-route';

    const CACHE_TAG = 'webkul_mpexpecteddeliverydate_expddregionzipcode';

    protected $_cacheTag = 'webkul_mpexpecteddeliverydate_expddregionzipcode';

    protected $_eventPrefix = 'webkul_mpexpecteddeliverydate_expddregionzipcode';

    /**
     * set resource model
     */
    public function _construct()
    {
        $this->_init(\Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegionZipcode::class);
    }

    /**
     * Load No-Route Indexer.
     *
     * @return $this
     */
    public function noRouteReasons()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return []
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeInterface
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId()
    {
        return parent::getData(self::SELLER_ID);
    }

    /**
     * Set RegionId
     *
     * @param int $regionId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeInterface
     */
    public function setRegionId($regionId)
    {
        return $this->setData(self::REGION_ID, $regionId);
    }

    /**
     * Get RegionId
     *
     * @return int
     */
    public function getRegionId()
    {
        return parent::getData(self::REGION_ID);
    }

    /**
     * Set ZipFrom
     *
     * @param string $zipFrom
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeInterface
     */
    public function setZipFrom($zipFrom)
    {
        return $this->setData(self::ZIP_FROM, $zipFrom);
    }

    /**
     * Get ZipFrom
     *
     * @return string
     */
    public function getZipFrom()
    {
        return parent::getData(self::ZIP_FROM);
    }

    /**
     * Set ZipTo
     *
     * @param string $zipTo
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeInterface
     */
    public function setZipTo($zipTo)
    {
        return $this->setData(self::ZIP_TO, $zipTo);
    }

    /**
     * Get ZipTo
     *
     * @return string
     */
    public function getZipTo()
    {
        return parent::getData(self::ZIP_TO);
    }

    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set UpdatedAt
     *
     * @param string $updatedAt
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionZipcodeInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::UPDATED_AT);
    }
}
