<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Model;

/**
 * ExpddRule Class
 * Model for Adding Rules for Expected Delivery Date
 */
class ExpddRule extends \Magento\Framework\Model\AbstractModel implements
    \Magento\Framework\DataObject\IdentityInterface,
    \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
{

    const NOROUTE_ENTITY_ID = 'no-route';

    const CACHE_TAG = 'webkul_mpexpecteddeliverydate_expddrule';

    protected $_cacheTag = 'webkul_mpexpecteddeliverydate_expddrule';

    protected $_eventPrefix = 'webkul_mpexpecteddeliverydate_expddrule';

    /**
     * set resource model
     */
    public function _construct()
    {
        $this->_init(\Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRule::class);
    }

    /**
     * Load No-Route Indexer.
     *
     * @return $this
     */
    public function noRouteReasons()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return []
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Set Id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ID);
    }

    /**
     * Set Status
     *
     * @param int $status
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Status
     *
     * @return int
     */
    public function getStatus()
    {
        return parent::getData(self::STATUS);
    }

    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId()
    {
        return parent::getData(self::SELLER_ID);
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return parent::getData(self::NAME);
    }

    /**
     * Set Description
     *
     * @param string $description
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDescription($description)
    {
        return $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * Get Description
     *
     * @return string
     */
    public function getDescription()
    {
        return parent::getData(self::DESCRIPTION);
    }

    /**
     * Set EstimationType
     *
     * @param string $estimationType
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setEstimationType($estimationType)
    {
        return $this->setData(self::ESTIMATION_TYPE, $estimationType);
    }

    /**
     * Get EstimationType
     *
     * @return string
     */
    public function getEstimationType()
    {
        return parent::getData(self::ESTIMATION_TYPE);
    }

    /**
     * Set DeliveryType
     *
     * @param string $deliveryType
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDeliveryType($deliveryType)
    {
        return $this->setData(self::DELIVERY_TYPE, $deliveryType);
    }

    /**
     * Get DeliveryType
     *
     * @return string
     */
    public function getDeliveryType()
    {
        return parent::getData(self::DELIVERY_TYPE);
    }

    /**
     * Set DaysFrom
     *
     * @param int $daysFrom
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDaysFrom($daysFrom)
    {
        return $this->setData(self::DAYS_FROM, $daysFrom);
    }

    /**
     * Get DaysFrom
     *
     * @return int
     */
    public function getDaysFrom()
    {
        return parent::getData(self::DAYS_FROM);
    }

    /**
     * Set DaysTo
     *
     * @param int $daysTo
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface
     */
    public function setDaysTo($daysTo)
    {
        return $this->setData(self::DAYS_TO, $daysTo);
    }

    /**
     * Get DaysTo
     *
     * @return int
     */
    public function getDaysTo()
    {
        return parent::getData(self::DAYS_TO);
    }
}
