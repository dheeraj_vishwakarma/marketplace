<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpExpectedDeliveryDate\Model;

/**
 * ExpddRuleRepo Class
 * Rules Repo
 */
class ExpddRuleRepo implements \Webkul\MpExpectedDeliveryDate\Api\ExpddRuleRepoInterface
{

    protected $modelFactory = null;

    protected $collectionFactory = null;

    /**
     * initialize
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRuleFactory $modelFactory
     * @param
     * Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRule\CollectionFactory
     * $collectionFactory
     * @return void
     */
    public function __construct(
        \Webkul\MpExpectedDeliveryDate\Model\ExpddRuleFactory $modelFactory,
        \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRule\CollectionFactory $collectionFactory
    ) {
        $this->modelFactory = $modelFactory;
        $this->collectionFactory = $collectionFactory;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRule
     */
    public function getById($id)
    {
        $model = $this->modelFactory->create()->load($id);
        if (!$model->getId()) {
            throw new \Magento\Framework\Exception\NoSuchEntityException(
                __('The CMS block with the "%1" ID doesn\'t exist.', $id)
            );
        }
        return $model;
    }

    /**
     * get by id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRule
     */
    public function save(\Webkul\MpExpectedDeliveryDate\Model\ExpddRule $subject)
    {
        try {
            $subject->save();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($exception->getMessage()));
        }
         return $subject;
    }

    /**
     * get list
     *
     * @param Magento\Framework\Api\SearchCriteriaInterface $creteria
     * @return Magento\Framework\Api\SearchResults
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $creteria)
    {
        $collection = $this->collectionFactory->create();
         return $collection;
    }

    /**
     * delete
     *
     * @param Webkul\MpExpectedDeliveryDate\Model\ExpddRule $subject
     * @return boolean
     */
    public function delete(\Webkul\MpExpectedDeliveryDate\Model\ExpddRule $subject)
    {
        try {
            $subject->delete();
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * delete by id
     *
     * @param int $id
     * @return boolean
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * Get Rules By Seller Id
     *
     * @param integer $sellerId
     * @return \Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRule\Collection|null
     */
    public function getBySellerId($sellerId)
    {
        try {
            $collection = $this->collectionFactory->create();
            return $collection->addFieldToFilter('seller_id', ['eq'=>$sellerId]);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\LocalizedException($exception->getMessage());
        }
    }

    public function getAll()
    {
        return $this->collectionFactory->create();
    }
}
