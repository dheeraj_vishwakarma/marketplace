<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Model;

/**
 * Model of Regions
 */
class ExpddRegion extends \Magento\Framework\Model\AbstractModel implements
    \Magento\Framework\DataObject\IdentityInterface,
    \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterface
{

    const NOROUTE_ENTITY_ID = 'no-route';

    const CACHE_TAG = 'webkul_mpexpecteddeliverydate_expddregion';

    protected $_cacheTag = 'webkul_mpexpecteddeliverydate_expddregion';

    protected $_eventPrefix = 'webkul_mpexpecteddeliverydate_expddregion';

    /**
     * set resource model
     */
    public function _construct()
    {
        $this->_init(\Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegion::class);
    }

    /**
     * Load No-Route Indexer.
     *
     * @return $this
     */
    public function noRouteReasons()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return []
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Set EntityId
     *
     * @param int $entityId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setEntityId($entityId)
    {
        return $this->setData(self::ENTITY_ID, $entityId);
    }

    /**
     * Get EntityId
     *
     * @return int
     */
    public function getEntityId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set Status
     *
     * @param int $status
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Status
     *
     * @return int
     */
    public function getStatus()
    {
        return parent::getData(self::STATUS);
    }

    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId()
    {
        return parent::getData(self::SELLER_ID);
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }

    /**
     * Get Name
     *
     * @return string
     */
    public function getName()
    {
        return parent::getData(self::NAME);
    }

    /**
     * Set RuleId
     *
     * @param int $ruleId
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setRuleId($ruleId)
    {
        return $this->setData(self::RULE_ID, $ruleId);
    }

    /**
     * Get RuleId
     *
     * @return int
     */
    public function getRuleId()
    {
        return parent::getData(self::RULE_ID);
    }

    /**
     * Set Country
     *
     * @param string $country
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setCountry($country)
    {
        return $this->setData(self::COUNTRY, $country);
    }

    /**
     * Get Country
     *
     * @return string
     */
    public function getCountry()
    {
        return parent::getData(self::COUNTRY);
    }

    /**
     * Set CreatedAt
     *
     * @param string $createdAt
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get CreatedAt
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return parent::getData(self::CREATED_AT);
    }

    /**
     * Set UpdatedAt
     *
     * @param string $updatedAt
     * @return Webkul\MpExpectedDeliveryDate\Model\ExpddRegionInterface
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get UpdatedAt
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return parent::getData(self::UPDATED_AT);
    }
}
