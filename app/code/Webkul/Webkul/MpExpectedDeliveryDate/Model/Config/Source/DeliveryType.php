<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 */

namespace Webkul\MpExpectedDeliveryDate\Model\Config\Source;

class DeliveryType implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ["value"=>'range', "label"=>__("Delivery in Range")],
            ["value"=>'specific', "label"=>__("Delivery at Specific Day")]
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'range'=>__("Delivery in Range"),
            'specific'=>__("Delivery at Specific Day")
        ];
    }
}
