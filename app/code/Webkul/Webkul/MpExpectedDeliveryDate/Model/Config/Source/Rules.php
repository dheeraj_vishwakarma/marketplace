<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 */

namespace Webkul\MpExpectedDeliveryDate\Model\Config\Source;

class Rules implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @param \Webkul\MpExpectedDeliveryDate\Api\ExpddRuleRepoInterface $rules
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     */
    public function __construct(
        \Webkul\MpExpectedDeliveryDate\Api\ExpddRuleRepoInterface $rules,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
    ) {
        $this->rules = $rules;
        $this->helper = $helper;
    }
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $rules = $this->getSellerRules();
        $options = [];

        foreach ($rules as $rule) {
            $options[] = [
                'value' => $rule['entity_id'],
                'label' => __($rule['name'])
            ];
        }
        return $options;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $rules = $this->getSellerRules();
        $options = [];

        foreach ($rules as $rule) {
            $options[$rule['entity_id']]  = __($rule['name']);
        }
        return $options;
    }

    /**
     * Get seller Rules
     *
     * @return void
     */
    private function getSellerRules()
    {
        $collection = $this->rules->getAll();
        return $collection->addFieldToFilter('status', ["eq" => 1])
        ->addFieldToSelect('name')->addFieldToSelect('entity_id')->getData();
    }
}
