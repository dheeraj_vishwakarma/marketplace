<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html ASL Licence
 */

namespace Webkul\MpExpectedDeliveryDate\Model\Config\Source;

class Zippatterntype implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ["value"=>'start_with', "label"=>__("Starting With")],
            ["value"=>'end_with', "label"=>__("Ending With")],
            ["value"=>'same', "label"=>__("Same as")],
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'start_with'=>__("Starting With"),
            'end_with'=>__("Ending With"),
            'same'=>__("Same as")
        ];
    }
}
