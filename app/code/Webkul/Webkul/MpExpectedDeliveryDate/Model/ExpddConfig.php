<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Model;

/**
 * ExpddConfig Class
 * Expected Delivery Date Model
 */
class ExpddConfig extends \Magento\Framework\Model\AbstractModel implements
    \Magento\Framework\DataObject\IdentityInterface,
    \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
{

    const NOROUTE_ENTITY_ID = 'no-route';

    const CACHE_TAG = 'webkul_mpexpecteddeliverydate_expddconfig';

    protected $_cacheTag = 'webkul_mpexpecteddeliverydate_expddconfig';

    protected $_eventPrefix = 'webkul_mpexpecteddeliverydate_expddconfig';

    /**
     * set resource model
     */
    public function _construct()
    {
        $this->_init(\Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddConfig::class);
    }

    /**
     * Load No-Route Indexer.
     *
     * @return $this
     */
    public function noRouteReasons()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Get identities.
     *
     * @return []
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Set Id
     *
     * @param int $id
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ID, $id);
    }

    /**
     * Get Id
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ID);
    }

    /**
     * Set Status
     *
     * @param int $status
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setStatus($status)
    {
        return $this->setData(self::STATUS, $status);
    }

    /**
     * Get Status
     *
     * @return int
     */
    public function getStatus()
    {
        return parent::getData(self::STATUS);
    }

    /**
     * Set SellerId
     *
     * @param int $sellerId
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setSellerId($sellerId)
    {
        return $this->setData(self::SELLER_ID, $sellerId);
    }

    /**
     * Get SellerId
     *
     * @return int
     */
    public function getSellerId()
    {
        return parent::getData(self::SELLER_ID);
    }

    /**
     * Set Weekends
     *
     * @param string $weekends
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setWeekends($weekends)
    {
        return $this->setData(self::WEEKENDS, $weekends);
    }

    /**
     * Get Weekends
     *
     * @return string
     */
    public function getWeekends()
    {
        return parent::getData(self::WEEKENDS);
    }

    /**
     * Set Holidays
     *
     * @param string $holidays
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setHolidays($holidays)
    {
        return $this->setData(self::HOLIDAYS, $holidays);
    }

    /**
     * Get Holidays
     *
     * @return string
     */
    public function getHolidays()
    {
        return parent::getData(self::HOLIDAYS);
    }

    /**
     * Set TextForRange
     *
     * @param string $textForRange
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setTextForRange($textForRange)
    {
        return $this->setData(self::TEXT_FOR_RANGE, $textForRange);
    }

    /**
     * Get TextForRange
     *
     * @return string
     */
    public function getTextForRange()
    {
        return parent::getData(self::TEXT_FOR_RANGE);
    }

    /**
     * Set TextForSpecific
     *
     * @param string $textForSpecific
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setTextForSpecific($textForSpecific)
    {
        return $this->setData(self::TEXT_FOR_SPECIFIC, $textForSpecific);
    }

    /**
     * Get TextForSpecific
     *
     * @return string
     */
    public function getTextForSpecific()
    {
        return parent::getData(self::TEXT_FOR_SPECIFIC);
    }

    /**
     * Set TextForNotAvailable
     *
     * @param string $textForNotAvailable
     * @return Webkul\MpExpectedDeliveryDate\Api\Data\ExpddConfigInterface
     */
    public function setTextForNotAvailable($textForNotAvailable)
    {
        return $this->setData(self::TEXT_FOR_NOT_AVAILABLE, $textForNotAvailable);
    }

    /**
     * Get TextForNotAvailable
     *
     * @return string
     */
    public function getTextForNotAvailable()
    {
        return parent::getData(self::TEXT_FOR_NOT_AVAILABLE);
    }
}
