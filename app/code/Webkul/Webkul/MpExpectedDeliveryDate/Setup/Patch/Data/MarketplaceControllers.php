<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Webkul\MpExpectedDeliveryDate\Setup\Patch\Data;

use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\SchemaPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Webkul\Marketplace\Model\ControllersRepository;

/**
 * Patch is mechanism, that allows to do atomic upgrade data changes
 */
class MarketplaceControllers implements DataPatchInterface
{
    /**
     * @var ModuleDataSetupInterface $moduleDataSetup
     */
    private $moduleDataSetup;

    /**
     * @var ControllersRepository
     */
    private $controllersRepository;

    /**
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param ControllersRepository $controllersRepository
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        ControllersRepository $controllersRepository
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->controllersRepository = $controllersRepository;
    }

    /**
     * Do Upgrade
     *
     * @return void
     */
    public function apply()
    {
        $data = [];
        $connection = $this->moduleDataSetup->getConnection();
        if (!($this->controllersRepository->getByPath('mpexpecteddate/configuration/index')->getSize())) {
            $data[] = [
                'module_name' => 'Webkul_MpExpectedDeliveryDate',
                'controller_path' => 'mpexpecteddate/configuration/index',
                'label' => 'Expected Delivery Date Configuration',
                'is_child' => '0',
                'parent_id' => '0',
            ];
        }

        if (!($this->controllersRepository->getByPath('mpexpecteddate/rules/index')->getSize())) {
            $data[] = [
                'module_name' => 'Webkul_MpExpectedDeliveryDate',
                'controller_path' => 'mpexpecteddate/rules/index',
                'label' => 'Expected Delivery Date Rules',
                'is_child' => '0',
                'parent_id' => '0',
            ];
        }

        if (!($this->controllersRepository->getByPath('mpexpecteddate/regions/index')->getSize())) {
            $data[] = [
                'module_name' => 'Webkul_MpExpectedDeliveryDate',
                'controller_path' => 'mpexpecteddate/regions/index',
                'label' => 'Expected Delivery Date Regions',
                'is_child' => '0',
                'parent_id' => '0',
            ];
        }

        



        $connection->insertMultiple($this->moduleDataSetup->getTable('marketplace_controller_list'), $data);
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [

        ];
    }
}
