<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Cart;

use \Webkul\MpExpectedDeliveryDate\Api\ExpddConfigRepoInterfaceFactory;

class View extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param ExpddConfigRepoInterfaceFactory $expddConfigRepo
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
     * @param \Magento\Customer\Model\Address $address
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        ExpddConfigRepoInterfaceFactory $expddConfigRepo,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Customer\Model\Address $address,
        array $data = []
    ) {
        $this->expddConfigRepo = $expddConfigRepo;
        $this->coreSession = $coreSession;
        $this->address = $address;
        $this->helper = $helper;

        parent::__construct(
            $context,
            $data
        );
    }

    /**
     * Get Product Seller Id
     *
     * @return void
     */
    public function getProductSellerId()
    {
        $id = $this->getProductid();
        return $this->helper->getSellerIdFromProductId($id);
    }

    /**
     * is Cofgurations enabled for seller
     *
     * @return boolean
     */
    public function isEnable($sellerId)
    {
        $config = $this->expddConfigRepo->create();

        $status = $config->getBySellerId($sellerId);
        if (!empty($status)) {
            return $status->getStatus();
        }

        return false;
    }

    /**
     * Get Postal code
     *
     * @return void
     */
    public function getPostalCode()
    {
        $postalFromSession = $this->getPostalCodeFromSession();
        if ($postalFromSession) {
            return $postalFromSession;
        }
        $customerAddressId = $this->helper->getCustomerAddress();
        return $customerAddressId?$this->address->load($customerAddressId)->getPostcode():null;
    }

    /**
     * Get Product ID
     *
     * @return int
     */
    public function getProductId()
    {
        return $this->getItem()->getProduct()->getId();
    }

    /**
     * Get Postal Code from Session
     *
     * @return string|null
     */
    private function getPostalCodeFromSession()
    {
        $productId = $this->getProductId();
        $zipcodes = $this->coreSession->getExpddZip();

        return $zipcodes[$productId]??null;
    }
}
