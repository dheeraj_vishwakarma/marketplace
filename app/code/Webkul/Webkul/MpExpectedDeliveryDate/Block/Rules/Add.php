<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Block\Rules;

use \Webkul\MpExpectedDeliveryDate\Api\ExpddRuleRepoInterface;

class Add extends \Magento\Framework\View\Element\Template
{
    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     * @param ExpddRuleRepoInterface $expddRuleRepoInterface
     * @param \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Estimationtype $estimationType
     * @param \Webkul\MpExpectedDeliveryDate\Model\Config\Source\DeliveryType $deliveryType
     * @param \Magento\Framework\Data\Form\FormKey $formKey
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        ExpddRuleRepoInterface $expddRuleRepoInterface,
        \Webkul\MpExpectedDeliveryDate\Model\Config\Source\Estimationtype $estimationType,
        \Webkul\MpExpectedDeliveryDate\Model\Config\Source\DeliveryType $deliveryType,
        \Magento\Framework\Data\Form\FormKey $formKey,
        array $data = []
    ) {
        $this->estimationType = $estimationType;
        $this->deliveryType = $deliveryType;
        $this->helper = $helper;
        $this->expddRuleRepoInterface = $expddRuleRepoInterface;
        $this->formKey = $formKey;
        parent::__construct($context, $data);
    }

    /**
     * Get Estimation Type
     *
     * @return array
     */
    public function getEstimationTypes(): array
    {
        return $this->estimationType->toArray();
    }

    /**
     * Get Delivery Type
     *
     * @return array
     */
    public function getDeliveryTypes()
    {
        return $this->deliveryType->toArray();
    }
    
    /**
     * Get Holidays
     *
     * @return array
     */
    public function getFormData(): array
    {
        $id = $this->getRequest()->getParam('id');
        $sellerId = $this->helper->getSellerId();
        return $id?$this->expddRuleRepoInterface->getBySellerId($sellerId)
        ->addFieldToFilter('entity_id', ["eq"=> $id])->getFirstItem()->getData():[];
    }

    /**
     * get form key
     *
     * @return string
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }
}
