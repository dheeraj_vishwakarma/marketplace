/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            "validation": "mage/validation/validation",
            expddConfiguration: 'Webkul_MpExpectedDeliveryDate/js/configuration',
            productView : 'Webkul_MpExpectedDeliveryDate/js/product/view',
            cartItemView : 'Webkul_MpExpectedDeliveryDate/js/cart/itemView',
            'Magento_Checkout/js/view/shipping': 'Webkul_MpExpectedDeliveryDate/js/view/shipping',
            expddNavigation : 'Webkul_MpExpectedDeliveryDate/js/navigation',
        }
    }
};
