/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint jquery:true*/
define([
    "jquery",
    'mage/translate',
    "mage/template",
    "mage/mage",
    'Magento_Ui/js/modal/modal'
], function ($, $t,mageTemplate, alert) {
    'use strict';
    $.widget('mage.expddNavigation', {
        options: {
            mainMenu : '.main-dropdown-nav',
            subMenu : '.child-dropdown-menu',
            otherMenu : '.dropdown-item'
        },
        _create: function () {
            var self = this;
            $(self.options.subMenu).hide();
            $(self.options.mainMenu).on('mouseover', function () {
                $(self.options.subMenu).show();
            });
            $(self.options.otherMenu).on('mouseleave', function () {
                $(self.options.subMenu).hide();
            });
        },
    });
    return $.mage.expddNavigation;
});
