<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

 $data = $block->getFormData();
 $weekends = $block->getWeekends();
?>
<form action="<?= $block->escapeUrl($block->getUrl(
    'mpexpecteddate/configuration/save',
    ['_secure' => $block->getRequest()->isSecure()]
)) ?>" 
enctype="multipart/form-data" 
method="post" id="expdd-configuration" data-form="expdd-configuration">
    <div class="wk-mp-design" id="wk-bodymain">
        <fieldset class="fieldset info wk-mp-fieldset">
            <div data-mage-init='{"formButtonAction": {}}' class="wk-mp-page-title legend">
                <span><?= /* @noEscape */  __('General Settings') ?></span>
                <button class="button wk-mp-btn" title="<?= /* @noEscape */  __('Save') ?>" 
                    type="submit" id="save-btn">
                    <span><span><?= /* @noEscape */  __('Save') ?></span></span>
                </button>
            </div>
            <?= $block->getBlockHtml('formkey')?>
            <?= $block->getBlockHtml('seller.formkey')?>

            <div class="field required">
                <label class="label"><?= /* @noEscape */  __('Enable') ?>:</label>
                <div class="control">
                    <select id="" class="select" name="status" required="true">
                        <option value="1" <?php if (!empty($data['status']) && $data['status'] == 1) {
                            echo "selected='selected'";}?>>
                            <?= /* @noEscape */ __("Yes"); ?>
                        </option>
                        <option value="0" <?php if (empty($data['status']) || $data['status'] == 0) {
                            echo "selected='selected'";}?>>
                            <?= /* @noEscape */ __("No"); ?>
                        </option>
                    </select>
                </div>
            </div>

            <div class="field required">
                <label class="label"><?= /* @noEscape */ __(' Delivery Day/Date range message') ?>:</label>
                <div class="control">
                    <textarea type="text" class="input-text" name="text_for_range" id="text_for_range" 
                    required="true"><?= $block->escapeHtml(!empty($data['text_for_range'])?
                        $data['text_for_range']:'Product will be Delivered in %%from%% To %%to%% days')?></textarea>
                    <span class="tip-content">
                        <?= /*@noEscape*/__("Example: Product will be Delivered in %%from%% To %%to%% days")?>
                    </span>
                </div>
            </div>
            <div class="field required">
                <label class="label"><?= /*@noEscape*/ __('Specific Delivery Day/Date message') ?>:</label>
                <div class="control">
                    <textarea class="textarea" id="text_for_specific" name="text_for_specific" 
                    required="true"><?= $block->escapeHtml(!empty($data['text_for_specific'])?
                        $data['text_for_specific']:'Product will be Delivered in %%to%% day(s)')?></textarea>
                        <span class="tip-content">
                        <?= /*@noEscape*/__("Example: Product will be Delivered in %%to%% day(s)")?>
                    </span>
                </div>
            </div>
            <div class="field required">
                <label class="label"><?= /*@noEscape*/ __('Delivery not available message') ?>:</label>
                <div class="control">
                    <textarea class="textarea" id="text_for_not_available" name="text_for_not_available" 
                    required="true"><?= $block->escapeHtml(
                        !empty($data['text_for_not_available'])?
                        $data['text_for_not_available']:'Delivery not Available'
                    )?></textarea>
                    <span class="tip-content"><?= /*@noEscape*/__("Example: Delivery not available message")?></span>
                </div>
            </div>

            <div class="field">
                <label class="label"><?= /* @noEscape */ __('Weekends') ?>:</label>
                <div class="control">
                    <select  class="select" name="weekends[]" multiple>
                            <?php
                            foreach ($weekends as $key => $weekend) {
                                ?>
                                <option value="<?= /*@noEscape*/ $key ?>"
                                    <?php
                                    if (!empty($data) && in_array($key, explode(',', $data['weekends']))) {
                                        echo "selected='selected'";
                                    }
                                    ?>
                                >
                                    <?= $block->escapeHtml($weekend) ?>
                                </option>
                                <?php
                            }
                            ?>
                    </select>
                </div>
            </div>

            <div class="field">
                <label class='label'> <?= /*@noEscape*/ __("Holidays") ?>:</label>
                <div id="holiday-section" class="control">
                    <table class="data-grid">
                        <thead>
                            <tr>
                                <th class="data-grid-th">
                                    <?= /* @noEscape */ __('Duration Type') ?>
                                </th>
                                <th class="data-grid-th">
                                <?= /* @noEscape */ __('Duration') ?>
                                </th>
                                <th class="data-grid-th">
                                    <?= /* @noEscape */ __('Action') ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="holiday-records">
                        </tbody>
                    </table>
                    <button class="add-row" data-records="0"><?= /* @noEscape */ __('Add Row') ?></button>
                </div>
            </div>
        </fieldset>
    </div>
</form>
<?php
$formData = [
    'holidays' => $block->getHolidays(),
    'rangeTypes' => $block->helper->jsonEncode($block->getRangeTypes())
];
$serializedFormData = $block->helper->jsonEncode($formData);
?>

<script type="text/x-magento-init">
    {
        "*": {
            "expddConfiguration": <?= /* @noEscape */ $serializedFormData; ?>
        }
    }
</script>