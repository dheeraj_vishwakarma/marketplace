<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */


namespace Webkul\MpExpectedDeliveryDate\Ui\DataProvider\Region;

use Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegion\CollectionFactory;

class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    protected $session;
    protected $collection;
    protected $loadedData;
    /**
     * DataProvider
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface
     * @param \Magento\Framework\Json\Helper\Data $jsonHelper
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        \Magento\Framework\Session\SessionManagerInterface $sessionManagerInterface,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $collectionFactory->create();
        $this->collection->addFieldToSelect("*");
        $this->sessionManagerInterface = $sessionManagerInterface;
        $this->helper = $helper;
    }

    /**
     * Get Session
     *
     * @return \Magento\Framework\Session\SessionManagerInterface
     */
    protected function getSession()
    {
        if ($this->session === null) {
            $this->session = $this->sessionManagerInterface;
        }
        return $this->session;
    }

    /**
     * get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $this->loadedData = [];
        $sellerId = $this->helper->getSellerId();
        $this->loadedData = $this->collection->addFieldToFilter('seller_id', ["eq"=>$sellerId])->toArray();
        $data = $this->getSession()->getConfigFormData();
        if (!empty($data)) {
            $id = !empty($data['config_index_form_settings']['id'])
            ? $data['config_index_form_settings']['id'] : null;
            $this->loadedData[$id] = $data;
            $this->getSession()->unsDataFormData();
        }
        return $this->loadedData;
    }
}
