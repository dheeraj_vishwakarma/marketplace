<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Region;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Upload extends Action
{
    /**
     * Image uploader
     *
     * @var \Magento\Catalog\Model\Uploader
     */
    protected $uploader;

   /**
    * Upload constructor.
    *
    * @param Context $context
    * @param \Webkul\MpExpectedDeliveryDate\Model\Uploader $uploader
    * @param \Magento\Framework\Session\SessionManagerInterface $coreSession
    */
    public function __construct(
        Context $context,
        \Webkul\MpExpectedDeliveryDate\Model\Uploader $uploader,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    ) {
        $this->uploader = $uploader;
        $this->coreSession = $coreSession;
        parent::__construct($context);
    }
    
    /**
     * Upload file controller action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $imageArr = (array)$this->getRequest()->getFiles();
        foreach ($imageArr as $name => $data) {
            try {
                $result = $this->uploader->saveFileToTmpDir($name);

                $result['data'] = [
                    'name' => $this->coreSession->getName(),
                    'value' => $this->coreSession->getSessionId(),
                    'lifetime' => $this->coreSession->getCookieLifetime(),
                    'path' => $this->coreSession->getCookiePath(),
                    'domain' => $this->coreSession->getCookieDomain(),
                ];
            } catch (\Exception $e) {
                $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
            }
        }
        
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
    /**
     * @return \Webkul\MpSellAsBrand\Model\Uploader
     */
    public function returnUploader()
    {
        return $this->uploader;
    }
}
