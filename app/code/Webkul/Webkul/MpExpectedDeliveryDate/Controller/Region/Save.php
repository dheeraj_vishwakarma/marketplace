<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Region;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionInterfaceFactory;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionRepoInterfaceFactory;
use \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRegionZipcodeInterfaceFactory;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRegionZipRepoInterfaceFactory;

class Save extends Action
{
    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ExpddRegionRepoInterfaceFactory $regionRepo
     * @param ExpddRegionZipRepoInterfaceFactory $zipRepo
     * @param ExpddRegionInterfaceFactory $region
     * @param ExpddRegionZipcodeInterfaceFactory $zip
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ExpddRegionRepoInterfaceFactory $regionRepo,
        ExpddRegionZipRepoInterfaceFactory $zipRepo,
        ExpddRegionInterfaceFactory $region,
        ExpddRegionZipcodeInterfaceFactory $zip,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
    ) {
        $this->_resultPageFactory = $resultPageFactory;
        $this->zip = $zip;
        $this->zipRepo = $zipRepo;
        $this->region = $region;
        $this->regionRepo = $regionRepo;
        $this->file = $file;
        $this->errors = [];
        $this->csvFilePath = null;
        $this->helper = $helper;
        $this->sellerId = 0;
        parent::__construct($context);
    }
    
    public function execute()
    {
        $this->sellerId = $this->helper->getSellerId();
        if ($this->sellerId) {
            $data = $this->captureRegionData();
            $_region = $this->regionRepo->create()->save($data);
            $regionId = $_region->getId();
            $this->messageManager->addSuccess(__('Region Stored Successfully'));
            
            $response = $this->processCsvData($regionId);
            if (!empty($this->csvFilePath) && $this->file->isFile($this->csvFilePath)) {
                $this->file->deleteFile($this->csvFilePath);
            }
            if (!$response['status'] && !empty($this->errors)) {
                foreach ($this->errors as $error) {
                    $this->messageManager->addError($error);
                }
                $this->_redirect("*/*/edit/id/$regionId");
                return true;
            }
            
            $this->_redirect("*/*/index");
            return true;
        }
        $this->messageManager->addError(__('Invalid Request.'));
        $this->_redirect("*/*/index");
        return true;
    }

    /**
     * Capture Request Data
     *
     * @return ExpddRegionInterface
     */
    private function captureRegionData()
    {
        
        $wholeData    = $this->getRequest()->getParams();
        if (!empty($wholeData['entity_id'])) {
            $wholeData['id'] = $wholeData['entity_id'];
        } else {
            unset($wholeData['entity_id']);
        }
        /* if (!empty($wholeData['country_id'])) {
            $wholeData['country'] = $wholeData['country_id'];
        } */
        $wholeData['seller_id'] = $this->sellerId;
        $region = $this->region->create();

        foreach ($wholeData as $key => $data) {
            if ($key==='zipuploder') {
                continue;
            }
            $method = 'set'.str_replace('_', '', ucwords($key, '_'));
            $region->$method($this->parseData($data));
        }
        return $region;
    }

    /**
     * Parse Data
     *
     * @param [type] $data
     * @return string
     */
    private function parseData($data): string
    {
        if (is_array($data)) {
            return implode(',', $data);
        }

        if (is_string($data)) {
            return trim($data);
        }

        return $data;
    }

    /**
     * Process csv data
     *
     * @param integer $regionId
     * @return array $msg
     */
    public function processCsvData($regionId)
    {
        $response = [];
        // Checking csv file
        $this->csvFilePath = $this->getRequest()->getParam('zipuploder');
        $csvData = [];
        try {
            if (!empty($this->csvFilePath) && $this->file->isFile($this->csvFilePath)) {
                // Reading csv file
                $file = $this->file->fileOpen($this->csvFilePath, 'r');
                while (!$this->file->endOfFile($file)) {
                    $csvData[] = $this->file->fileGetCsv($file, 1024);
                }
                $this->file->fileClose($file);
            } else {
                $response['status'] = false;
                return $response;
            }
        } catch (\Exception $e) {
            $response['status'] = false;
            $this->setErrors(__($e->getMessage()));
            return $response;
        }
        return $this->saveCsvData($csvData, $regionId);
    }

    /**
     * Save Zip code from CSV
     *
     * @param array $csvData
     * @param integer $regionId
     * @return array
     */
    private function saveCsvData(array $csvData, int $regionId): array
    {
        $response = [];
        $response['status'] = true;
        if (empty($csvData)) {
            $response['status'] = false;
            $this->setErrors(__("Invalid Data"));
            return $response;
        }

        foreach ($csvData as $key => $zipCodes) {
            if ($key === 0) {
                continue;
            }
            $isValid = $this->validateZipcode($zipCodes, $key+1);
            if (!$isValid['status']) {
                $response['status'] = false;
                $this->setErrors($isValid['errors']);
                continue;
            }

            $zipData = $this->captureZipData($zipCodes, $regionId);
            
            $zipRepo = $this->zipRepo->create();
            $zipRepo->save($zipData);
        }

        return $response;
    }

    /**
     * Capture Zip data
     *
     * @param array $data
     * @param integer $regionId
     * @return ExpddRegionZipcodeInterface
     */
    private function captureZipData(array $data, int $regionId)
    {
        $zip = $this->zip->create();
        $zip->setZipFrom($data[0]);
        $zip->setZipTo($data[1]);
        $zip->setSellerId($this->sellerId);
        $zip->setRegionId($regionId);
        return $zip;
    }

    /**
     * Validate Zip code
     *
     * @param array $zipCodes
     * @return void
     */
    private function validateZipcode($zipCodes, $row)
    {
        $response = [];
        if (empty($zipCodes)) {
            $response['status'] = false;
            $response['errors'] = __('No record found at row %1', $row);

            return $response;
        }

        if (count($zipCodes)<=0) {
            $response['status'] = false;
            $response['errors'] = __('Zip from and Zip to both are required. Invalid record found at row %1', $row);
            return $response;
        }

        foreach ($zipCodes as $key => $zip) {
            if (preg_match('/[^a-z_\-0-9\s]/i', $zip)) {
                $response['status'] = false;
                $response['errors'] =
                    __('Zipcode should contains numbers and alphabet only. Invalid format found at row %1', $row);
                return $response;
            }
            $length = strlen($zip);
            if ($length>6 || $length<4) {
                $l = $length;
                $r = $row;
                $response['status'] = false;
                $response['errors'] =
                __("Zipcode should be more than 4 digits and less 7 found %1. Invalid format found at row %2", $l, $r);
                return $response;
            }

            $isExist = $this->isExist($zip);

            if ($isExist) {
                $response['status'] = false;
                $response['errors'] =
                __("Records %1 already exists. Skipped row %2", $zip, $row);
                return $response;
            }
        }
        $response['status'] = true;
        return $response;
    }

    private function setErrors($error)
    {
        array_push($this->errors, $error);
    }

    /**
     * is ZIp Exist
     *
     * @param string $zipcode
     * @return boolean
     */
    private function isExist($zipcodes)
    {
        $zipRepo = $this->zipRepo->create();
        
        $results = $zipRepo->getBySellerId($this->sellerId)
            ->addFieldToFilter('zip_from', ['lteq' => $zipcodes])
            ->addFieldToFilter('zip_to', ['gteq' => $zipcodes]);

        return $results->count();
    }
}
