<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Webkul_MpExpectedDeliveryDate
 * @author Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Region;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\MpExpectedDeliveryDate\Model\ResourceModel\ExpddRegion\CollectionFactory;
use Webkul\MpExpectedDeliveryDate\Api\ExpddRegionZipRepoInterfaceFactory;

class MassDelete extends Action
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

   /**
    * @param Context $context
    * @param Filter $filter
    * @param CollectionFactory $collectionFactory
    * @param ExpddRegionZipRepoInterfaceFactory $regionRepoFactory
    */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        ExpddRegionZipRepoInterfaceFactory $regionRepoFactory
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->regionRepoFactory = $regionRepoFactory;
        parent::__construct($context);
    }
    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $countRecord = $collection->getSize();
        foreach ($collection as $item) {
            $this->regionRepoFactory->create()->deleteByRegionId($item->getId());
            $item->delete();
        }
        $this->messageManager->addSuccess(
            __(
                'A total of %1 record(s) have been deleted.',
                $countRecord
            )
        );

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('*/*/index');
    }
}
