<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpExpectedDeliveryDate
 * @author    Webkul <support@webkul.com>
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpExpectedDeliveryDate\Controller\Adminhtml\Rules;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use \Webkul\MpExpectedDeliveryDate\Api\Data\ExpddRuleInterface;
use \Webkul\MpExpectedDeliveryDate\Api\ExpddRuleRepoInterface;
use \Magento\Framework\Controller\ResultFactory;

class Save extends Action
{

    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $resultForwardFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     * @param ExpddRuleRepoInterface $expddRuleRepoInterface
     * @param ExpddRuleInterface $expddRuleInterface
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory,
        ExpddRuleRepoInterface $expddRuleRepoInterface,
        ExpddRuleInterface $expddRuleInterface,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Webkul\MpExpectedDeliveryDate\Helper\Data $helper
    ) {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->expddRuleRepoInterface = $expddRuleRepoInterface;
        $this->expddRuleInterface = $expddRuleInterface;
        $this->helper = $helper;
        $this->messageManager = $messageManager;
        $this->resultForwardFactory = $resultForwardFactory;
    }

    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed("Webkul_MpExpectedDeliveryDate::rules");
    }

    /**
     * Execute
     *
     * @return PageFactory
     */
    public function execute()
    {
        $data = $this->captureRequestData();
        $this->expddRuleRepoInterface->save($data);
        $this->messageManager->addSuccess(__('Rule has been saved successfully.'));
        $this->_redirect("*/*/index");
        return true;
    }

    /**
     * Capture Request Data
     *
     * @return ExpddRuleInterface
     */
    private function captureRequestData(): ExpddRuleInterface
    {
        $wholeData    = $this->getRequest()->getParams();
        if (!empty($wholeData['entity_id'])) {
            $wholeData['id'] = $wholeData['entity_id'];
        }
        $expddRuleInterface = $this->expddRuleInterface;

        foreach ($wholeData as $key => $data) {
            $method = 'set'.str_replace('_', '', ucwords($key, '_'));
            $expddRuleInterface->$method($this->parseData($data));
        }
        if ($wholeData['delivery_type'] === 'specific') {
            $expddRuleInterface->setDaysTo('');
        }
        return $expddRuleInterface;
    }

    /**
     * Parse Data
     *
     * @param [type] $data
     * @return string
     */
    private function parseData($data): string
    {
        if (is_array($data)) {
            return implode(',', $data);
        }

        if (is_string($data)) {
            return trim($data);
        }

        return $data;
    }
}
