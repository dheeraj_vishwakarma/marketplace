<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Xtremo\Observer;

use Magento\Framework\Event\ObserverInterface;

/**
 * Webkul Xtremo BeforeAddProduct Observer
 */
class BeforeAddProduct implements ObserverInterface
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlInterface;

    /**
     * @param \Magento\Framework\UrlInterface $urlInterface
     */
    public function __construct(
        \Magento\Framework\UrlInterface $urlInterface
    ) {
        $this->urlInterface = $urlInterface;
    }

    /**
     * [executes on checkout_cart_add_product_complete event]
     *
     * @param  \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $data = $observer->getEvent()->getRequest()->getParams();
        if (isset($data['buy_btn'])) {
            $request = $observer->getRequest();
            $response = $observer->getResponse();
            $product  = $observer->getProduct();

            if (!$request->isAjax()) {
                $url = $this->urlInterface->getUrl('checkout/cart');
                $request->setParam('return_url', $url);
            }
        }
    }
}
