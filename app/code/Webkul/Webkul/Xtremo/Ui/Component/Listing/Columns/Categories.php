<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Xtremo\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Webkul Xtremo Ui Listing Categories
 */
class Categories extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    protected $helper;

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface $urlBuilder
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        \Webkul\Xtremo\Helper\Data $helper,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->helper = $helper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $categoryArray = [];
                if (isset($item['entity_id'])) {
                    if (!empty($item['category_ids'])) {
                        $assignedCategories = $this->helper->jsonDecodeData($item['category_ids'], true);
                        $categoryArray = $this->getCategoryArray($assignedCategories);
                    }
                    $item[$fieldName] = $categoryArray;
                }
            }
        }
        return $dataSource;
    }

    /**
     * Get array of category names
     *
     * @param array $assignedCategories
     * @return array
     */
    private function getCategoryArray($assignedCategories)
    {
        $categoryArray = [];
        if (!empty($assignedCategories)) {
            foreach ($assignedCategories as $catId) {
                $category = $this->helper->getCategory($catId);
                if ($category && $category->getId()) {
                    $categoryArray[] = $category->getName();
                }
            }
        }
        return $categoryArray;
    }
}
