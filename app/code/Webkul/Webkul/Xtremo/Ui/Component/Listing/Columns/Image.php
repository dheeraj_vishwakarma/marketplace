<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Xtremo Ui Component Listing Columns Image
 */
class Image extends \Magento\Ui\Component\Listing\Columns\Column
{
    const NAME = 'filename';

    const ALT_FIELD = 'name';

    private $bannerImage;

    /**
     * @param ContextInterface      $context
     * @param UiComponentFactory    $uiComponentFactory
     * @param StoreManagerInterface $storemanager
     * @param array                 $components
     * @param array                 $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        StoreManagerInterface $storemanager,
        \Webkul\Xtremo\Model\Bannerimages $bannerImage,
        array $components = [],
        array $data = []
    ) {
        parent::__construct($context, $uiComponentFactory, $components, $data);
        $this->bannerImage = $bannerImage;
        $this->_storeManager = $storemanager;
    }

    /**
     * prepareDataSource
     *
     * @param  array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        $mediaDirectory = $this->_storeManager
            ->getStore()
            ->getBaseUrl(
                \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
            ).'xtremo/category/banner/images/';

        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as & $item) {
                $image = $this->loadImageBanner($item['entity_id']);

                $imageName = $image->getFilename();
                $imageTitle = $image->getTitle();
                $item[$fieldName . '_src'] = $mediaDirectory.$imageName;
                $item[$fieldName . '_alt'] = $this->getAlt($item) ?: $imageTitle;
                $item[$fieldName . '_orig_src'] = $mediaDirectory.$imageName;
            }
        }
        return $dataSource;
    }

    private function loadImageBanner($id)
    {
        return $this->bannerImage->load($id);
    }
}
