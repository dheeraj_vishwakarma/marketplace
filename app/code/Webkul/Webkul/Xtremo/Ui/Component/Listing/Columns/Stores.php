<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\Xtremo\Ui\Component\Listing\Columns;

use Magento\Framework\UrlInterface;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Magento\Store\Model\StoreRepository;

/**
 * Class Categories.
 */
class Stores extends Column
{
    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var Magento\Store\Model\StoreRepository
     */
    protected $storeRepository;

    /**
     * Constructor.
     *
     * @param ContextInterface   $context
     * @param UiComponentFactory $uiComponentFactory
     * @param UrlInterface       $urlBuilder
     * @param array              $components
     * @param array              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        UrlInterface $urlBuilder,
        \Webkul\Xtremo\Helper\Data $helper,
        StoreRepository $storeRepository,
        array $components = [],
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->helper = $helper;
        $this->storeRepository = $storeRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $storeArray = [];
                if (isset($item['entity_id'])) {
                    if (!empty($item['store_ids'])) {
                        $assignedStores = $this->helper->jsonDecodeData($item['store_ids'], true);
                        $storeArray = $this->getStoreArray($assignedStores);
                    }
                    $item[$fieldName] = $storeArray;
                }
            }
        }
        return $dataSource;
    }
    
    private function getStoreArray($assignedStores)
    {
        $storeArray = [];
        if (!empty($assignedStores)) {
            foreach ($assignedStores as $storeId) {
                if ($storeId == 0) {
                    $storeArray[] = "All Store Views";
                } else {
                    $store = $this->storeRepository->getById($storeId);
                    if ($store && $store->getId()) {
                        $storeArray[] = $store->getData('name');
                    }
                }
            }
        }
        return $storeArray;
    }
}
