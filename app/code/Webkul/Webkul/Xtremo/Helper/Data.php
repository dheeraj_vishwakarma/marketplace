<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Helper;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Json\Helper\Data as JsonHelper;

/**
 * Webkul Xtremo Helper Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CollectionFactory
     */
    private $categoryCollectionFactory;

    /**
     * @var \Magento\Customer\Block\Account\Customer
     */
    private $customerBlock;

    /**
     * @var \Magento\Customer\Model\Session
     */
    private $customerSession;

    /**
     * @var \Magento\Catalog\Model\CategoryRepository
     */
    private $categoryRepository;

    /**
     * @var \Magento\Directory\Model\CurrencyFactory
     */
    private $currencyFactory;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    private $productCollection;

    /**
     * @var \Magento\Catalog\Model\Product\Attribute\Source\Status
     */
    private $productStatus;

    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    private $productVisibility;

    /**
     * @var DirectoryList
     */
    private $directory;

    /**
     * @var \Magento\Customer\Model\Url
     */
    private $customerUrl;

    /**
     * @var \Magento\Framework\Registry
     */
    private $registry;

    /**
     * @var \Magento\Framework\View\Design\Theme\ThemeProviderInterface
     */
    private $themeProvider;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $file;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $fileSystem;

    /**
     * @var \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory
     */
    private $bannerImagesCollection;

    /**
     * @var \Magento\Framework\Image\AdapterFactory
     */
    private $imageFactory;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $filesystemFile;

    /**
     * @var JsonHelper
     */
    private $jsonHelper;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \Magento\Framework\Serialize\Serializer\Serialize
     */
    private $serialize;

    /**
     * @var \Magento\Framework\Shell
     */
    private $shell;

    /**
     * @var \Magento\Framework\App\Cache\ManagerFactory
     */
    protected $cacheManager;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Customer\Block\Account\Customer $customerBlock
     * @param CollectionFactory $categoryCollectionFactory
     * @param \Magento\Customer\Model\SessionFactory $customerSession
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Catalog\Model\CategoryRepository $categoryRepository
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection
     * @param \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param DirectoryList $directory
     * @param \Magento\Customer\Model\Url $customerUrl
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\View\Design\Theme\ThemeProviderInterface $themeProvider
     * @param JsonHelper $jsonHelper
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection
     * @param \Magento\Framework\Image\AdapterFactory $imageFactory
     * @param \Magento\Framework\Filesystem\Io\File $filesystemFile
     * @param \Magento\Framework\Serialize\Serializer\Serialize $serialize
     * @param \Magento\Framework\Shell $shell
     * @param \Psr\Log\LoggerInterface $logger
     * @param \Magento\Framework\App\Cache\ManagerFactory $cacheManagerFactory
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Customer\Block\Account\Customer $customerBlock,
        CollectionFactory $categoryCollectionFactory,
        \Magento\Customer\Model\SessionFactory $customerSession,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Catalog\Model\CategoryRepository $categoryRepository,
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollection,
        \Magento\Catalog\Model\Product\Attribute\Source\Status $productStatus,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        DirectoryList $directory,
        \Magento\Customer\Model\Url $customerUrl,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\View\Design\Theme\ThemeProviderInterface $themeProvider,
        JsonHelper $jsonHelper,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\Filesystem $fileSystem,
        \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection,
        \Magento\Framework\Image\AdapterFactory $imageFactory,
        \Magento\Framework\Filesystem\Io\File $filesystemFile,
        \Magento\Framework\Serialize\Serializer\Serialize $serialize,
        \Magento\Framework\Shell $shell,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Cache\ManagerFactory $cacheManagerFactory
    ) {
        parent::__construct($context);
        $this->storeManager = $storeManager;
        $this->customerBlock = $customerBlock;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->customerSession = $customerSession;
        $this->currencyFactory = $currencyFactory;
        $this->categoryRepository = $categoryRepository;
        $this->productCollection = $productCollection;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->directory = $directory;
        $this->customerUrl = $customerUrl;
        $this->registry = $registry;
        $this->themeProvider = $themeProvider;
        $this->jsonHelper = $jsonHelper;
        $this->file = $file;
        $this->fileSystem = $fileSystem;
        $this->bannerImagesCollection = $bannerImagesCollection;
        $this->imageFactory = $imageFactory;
        $this->filesystemFile = $filesystemFile;
        $this->serialize = $serialize;
        $this->shell = $shell;
        $this->logger = $logger;
        $this->cacheManager = $cacheManagerFactory;
    }

    /**
     * This function will decode the array to json format
     *
     * @param array $data
     * @return json
     */
    public function jsonDecodeData($data)
    {
        return $this->jsonHelper->jsonDecode($data, true);
    }

    /**
     * This function will return json encoded data
     *
     * @param json $data
     * @return Array
     */
    public function jsonEncodeData($data)
    {
        return $this->jsonHelper->jsonEncode($data, true);
    }

    /**
     * getHeaderPhoneNumberConfig
     *
     * @return string
     */
    public function getHeaderPhoneNumberConfig()
    {
        return $this->scopeConfig->getValue(
            'xtremo/header/phone_number',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * getHeaderCmsPagesConfig
     *
     * @return string
     */
    public function getHeaderCmsPagesConfig()
    {
        return $this->scopeConfig->getValue(
            'xtremo/header/cms_pages',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * getHeaderSearchCategoryIsEnableConfig
     *
     * @return string
     */
    public function getHeaderSearchCategoryIsEnableConfig()
    {
        return $this->scopeConfig->getValue(
            'xtremo/header/is_category_search',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * getHeaderSearchCategories
     *
     * @return string
     */
    public function getHeaderSearchCategories()
    {
        return $this->scopeConfig->getValue(
            'xtremo/header/categories_to_search',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * getFooterSocialLinkConfig
     *
     * @param string $linkMark
     * @return string
     */
    public function getFooterSocialLinkConfig($linkMark)
    {
        return $this->scopeConfig->getValue(
            'xtremo/footer/'.$linkMark,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * getHomepageCategoryPlaceholderConfig
     *
     * @return string
     */
    public function getHomepageCategoryPlaceholderConfig()
    {
        $defaultImage = '';

        if ($this->isXtremoEnabled()) {
            $defaultImage = $this->scopeConfig->getValue(
                'xtremo/content/category_placeholder',
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }

        if ($defaultImage == "" || !$defaultImage) {
            $defaultImage = 'placeholder.jpg';
        }
        return $defaultImage;
    }

    /**
     * getBaseUrl
     *
     * @return string
     */
    public function getBaseUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl();
    }

    /**
     * getCategoryCollection
     *
     * @param boolean $isActive
     * @param boolean $level
     * @param boolean $sortBy
     * @param boolean $pageSize
     * @param boolean $parentFilter
     * @return object
     */
    public function getCategoryCollection(
        $isActive = true,
        $level = false,
        $sortBy = false,
        $pageSize = false,
        $parentFilter = false
    ) {
        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*');

        // select only active categories
        if ($isActive) {
            $collection->addIsActiveFilter();
        }

        // select categories of certain level
        if ($level) {
            $collection->addAttributeToFilter('level', ['eq'=>$level]);
        }

        if ($parentFilter) {
            $collection->addAttributeToFilter('parent_id', ['eq'=>$parentFilter]);
        }

        // sort categories by some value
        if ($sortBy) {
            $collection->addOrderField($sortBy);
        }

        // select certain number of categories
        if ($pageSize) {
            $collection->setPageSize($pageSize);
        }

        return $collection;
    }

    /**
     * getCategorySearchListToHtml
     *
     * @return string
     */
    public function getCategorySearchListToHtml()
    {
        $html = '';
        $categoryIds = $this->getHeaderSearchCategories();

        if (!$categoryIds || empty($categoryIds) || $categoryIds == "") {
            $secondLevelCategories = $this->getCategoryCollection(true, 2);
        } else {
            $categoryIds = explode(",", $categoryIds);
            $secondLevelCategories = $this->getCategoryCollection(true, 2);
            $secondLevelCategories->addAttributeToFilter('entity_id', ['in'=>$categoryIds]);
        }

        $html .= '<div class="xtremo-cat-search-wrapper">';
        $html .= '<select name="cat" class="xtremo-cat-search">';
        $html .= '<option value="">'.__('All').'</option>';

        if ($secondLevelCategories->getSize()) {
            foreach ($secondLevelCategories as $category) {
                $html .= '<option value="'.$category->getId().'">'.$category->getName().'</option>';
                $thirdLevelCategories = $this->getCategoryCollection(
                    true,
                    3,
                    'parent_id',
                    false,
                    $category->getId()
                );
                $thirdLevelCategories->addAttributeToFilter('entity_id', ['in'=>$categoryIds]);
                if ($thirdLevelCategories->getSize()) {
                    foreach ($thirdLevelCategories as $thirdCategory) {
                        $html .= '<option value="'
                                    .$thirdCategory->getId()
                                .'">'
                                    ."&nbsp;&nbsp;&nbsp;&nbsp;"
                                    .$thirdCategory->getName()
                            .'</option>';
                    }
                }
            }
        }

        $html .= '</select>';
        $html .= '</div>';

        return $html;
    }

    /**
     * isModuleEnabled checks a given module is enabled or not
     *
     * @param  string $moduleName
     * @return boolean
     */
    public function isModuleEnabled($moduleName)
    {
        return $this->_moduleManager->isEnabled($moduleName);
    }

    /**
     * isOutputEnabled checks a given module is enabled or not
     *
     * @param  string $moduleName
     * @return boolean
     */
    public function isOutputEnabled($moduleName)
    {
        return $this->_moduleManager->isOutputEnabled($moduleName);
    }

    /**
     * isXtremoEnabled
     *
     * @return boolean
     */
    public function isXtremoEnabled()
    {
        if ($this->isModuleEnabled('Webkul_Xtremo') && $this->isOutputEnabled('Webkul_Xtremo')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * checkCustomerLoggedIn
     *
     * @return boolean
     */
    public function checkCustomerLoggedIn()
    {
        return $this->customerBlock->customerLoggedIn();
    }

    /**
     * getLoggedInCustomer get customer data for logged in customer
     *
     * @return object
     */
    public function getLoggedInCustomer()
    {
        return $this->customerSession->create()->getCustomer();
    }

    /**
     * getCurrencySymbol
     *
     * @param string $code
     * @return string
     */
    public function getCurrencySymbol($code)
    {
        return $this->currencyFactory->create()->load($code)->getCurrencySymbol();
    }

    /**
     * getCategoryImage get category Image URL by Id
     *
     * @param  int $id category Id
     * @return int | string
     */
    public function getCategoryImage($id)
    {
        $category = $this->getCategory($id);
        if (isset($category['image'])) {
            return $category->getImageUrl();
        } else {
            return $this->getConfigCategoryImage();
        }
    }

    /**
     * getCategory get category by Id
     *
     * @param  int $id category Id
     * @return int | string
     */
    public function getCategory($id)
    {
        $category = $this->categoryRepository->get($id);
        return $category;
    }

    /**
     * getConfigCategoryImage
     *
     * @return string|boolean
     */
    public function getConfigCategoryImage()
    {
        $mediaDirectory = $this->getDirectoryPath('media');
        $defaultImage = $this->getHomepageCategoryPlaceholderConfig();
        $imageFile = $mediaDirectory.'/xtremo/homepage/category/'. $defaultImage;

        if ($this->file->isExists($imageFile) && getimagesize($imageFile)!==false) {
            return $this->getMediaUrl().'xtremo/homepage/category/'. $defaultImage;
        } elseif ($this->file->isExists($mediaDirectory.'/xtremo/homepage/category/placeholder.jpg')
            && getimagesize($mediaDirectory.'/xtremo/homepage/category/placeholder.jpg')!==false
        ) {
            return $this->getMediaUrl().'xtremo/homepage/category/placeholder.jpg';
        } else {
            return false;
        }
    }

    /**
     * getFeaturedProductsList used to get featured products
     *
     * @return object
     */
    public function getFilteredProductsCollectionByAttribute($attributeCode, $attributeValue)
    {
        $model = $this->productCollection->create()
            ->addAttributeToSelect('*')
            ->addStoreFilter()
            ->addAttributeToFilter(
                'status',
                ['in' => $this->productStatus->getVisibleStatusIds()]
            )->addAttributeToFilter(
                'visibility',
                ['in' => $this->productVisibility->getVisibleInSiteIds()]
            )->addFieldToFilter(
                $attributeCode,
                $attributeValue
            );

        return $model;
    }

    /**
     * getFilteredCategoriesCollectionByAttribute
     *
     * @param string $attributeCode
     * @param mixed $attributeValue
     * @return object
     */
    public function getFilteredCategoriesCollectionByAttribute($attributeCode, $attributeValue)
    {
        $collection = $this->getCategoryCollection();
        $collection->addAttributeToFilter('level', ['neq'=>1]);
        $collection->addAttributeToFilter($attributeCode, $attributeValue);
        $collection->addOrderField('level');
        $collection->setOrder('entity_id', 'ASC');

        if ($collection->getSize() > 5) {
            $collection->setPageSize(5);
        }

        return $collection;
    }

    /**
     * getMediaUrl
     *
     * @return string
     */
    public function getMediaUrl()
    {
        return $this->storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        );
    }

    /**
     * getDirectoryPath
     *
     * @param string $dir
     * @return string
     */
    public function getDirectoryPath($dir)
    {
        return $this->directory->getPath($dir);
    }

    /**
     * isLoggedIn
     *
     * @return boolean
     */
    public function isLoggedIn()
    {
        return $this->customerSession->create()->isLoggedIn();
    }

    /**
     * setCustomerAuthUrl
     *
     * @param string $url
     * @return mixed
     */
    public function setCustomerAuthUrl($url)
    {
        return $this->customerSession->create()->setBeforeAuthUrl($url);
    }

    /**
     * getCustomerModelUrl
     *
     * @return string
     */
    public function getCustomerModelUrl()
    {
        return $this->customerUrl;
    }

    /**
     * getCurrentCategory
     *
     * @return \Magento\Catalog\Model\Category
     */
    public function getCurrentCategory()
    {
        return $this->registry->registry('current_category');
    }

    /**
     * displayDiscountLabel
     *
     * @param \Magento\Catalog\Model\Product $_product
     * @return mixed
     */
    public function displayDiscountLabel($_product)
    {
        $originalPrice = $_product->getPrice();
        $finalPrice = $_product->getFinalPrice();

        $percentage = 0;
        if ($originalPrice > $finalPrice) {
            $percentage = number_format(($originalPrice - $finalPrice) * 100 / $originalPrice, 0);
        }

        if ($percentage) {
            return $percentage;
        }
    }

    /**
     * checkCurrentTheme
     *
     * @return boolean
     */
    public function checkCurrentTheme()
    {
        $themeId = $this->scopeConfig->getValue(
            \Magento\Framework\View\DesignInterface::XML_PATH_THEME_ID,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->storeManager->getStore()->getId()
        );
        $flag = false;

        /**
         * @var $theme \Magento\Framework\View\Design\ThemeInterface
        */
        $theme = $this->themeProvider->getThemeById($themeId);

        if (($theme->getCode() == "Webkul/xtremo" ||
        $theme->getCode() == "Webkul/xtremoRTL") && $theme->getArea() == "frontend") {
            $flag = true;
        }

        return $flag;
    }

    /**
     * getBannerCategories
     *
     * @return array
     */
    public function getBannerCategories()
    {
        $categoryOptionArray = [];
        $i = 0;
        $collection = $this->getCategoryCollection(true, 2);
        if ($collection->getSize()) {
            foreach ($collection as $category) {
                $categoryOptionArray[$i]['label'] = $category->getName();
                $categoryOptionArray[$i]['value'] = $category->getId();
                $i++;
            }
        }
        return $categoryOptionArray;
    }

    /**
     * getCategoryBannerImage
     *
     * @param array $categoriesArray
     * @param int $id
     * @return string|boolean
     */
    public function getCategoryBannerImage($categoriesArray, $id)
    {
        if (!empty($categoriesArray) && isset($categoriesArray[$id]) && array_key_exists($id, $categoriesArray)) {
            return $categoriesArray[$id];
        } else {
            return $this->getConfigCategoryImage();
        }
    }

    /**
     * getCategoryBannerImages
     *
     * @return array
     */
    public function getCategoryBannerImages()
    {
        $collection = $this->bannerImagesCollection->create()
            ->addFieldToFilter('status', 1)
            ->setOrder('entity_id', 'DESC');
        $currentStoreId = $this->storeManager->getStore()->getId();
        $categoriesArray = [];
        if ($collection->getSize()) {
            $categoriesArray = $this->prepareCategoriesArray($collection, $categoriesArray, $currentStoreId);
            $categoriesArray = $this->prepareCategoriesArray($collection, $categoriesArray);
        }
        return $categoriesArray;
    }

    /**
     *
     * @param object $collection
     * @param array $categoriesArray
     * @param string $storeId
     * @return array
     */
    private function prepareCategoriesArray($collection, $categoriesArray, $storeId = '0')
    {
        foreach ($collection as $data) {
            $mediaDirectory = $this->getDirectoryPath('media');
            $link = $mediaDirectory."/".$data->getLink();
            $info = $this->shell->execute("file -iL $link 2>/dev/null");
            $infoArray = explode(" ", $info);
            $countInfoArray = $this->getArrayCount($infoArray);
            if ($data->getCategoryIds() !== ""
                && $data->getLink() !== ""
                && $countInfoArray < 4
                && !empty($categoryIds = $this->jsonDecodeData($data->getCategoryIds()))
            ) {
                $storeIds = $data->getStoreIds();
                $storeIdsArray = [];
                if ($storeIds !== "") {
                    $storeIdsArray = $this->jsonDecodeData($storeIds);
                    foreach ($categoryIds as $catId) {
                        if (in_array($storeId, $storeIdsArray)
                            && !isset($categoriesArray[$catId])
                            && !array_key_exists($catId, $categoriesArray)
                        ) {
                            $categoriesArray[$catId] = $this->getMediaUrl().$data->getLink();
                        }
                    }
                }
            }
        }
        return $categoriesArray;
    }

    /**
     * @param [array] $array
     * @return Integer|0
     */
    public function getArrayCount($array)
    {
        return count($array);
    }

    /**
     * getCategoryThumbUrl
     *
     * @param \Magento\Catalog\Model\Category $category
     * @return string|boolean
     */
    public function getCategoryThumbUrl($category)
    {
        $url   = false;
        $image = $category->getPopularCategoryImage();
        if ($image) {
            if (is_string($image)) {
                $url = $this->storeManager->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ) . 'catalog/category/xtremo/' . $image;
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Something went wrong while getting the image url.')
                );
            }
        }

        return $url;
    }

    /**
     * getBannerImagePath get upload path for agorae slider images
     *
     * @return string
     */
    public function getBannerImagePath()
    {
        return 'xtremo/category/banner/images/';
    }

    /**
     * getAllowedExtensions used to get allowed extension for image
     *
     * @param  boolean $flag
     * @return array
     */
    public function getAllowedExtensions($flag)
    {
        if ($flag) {
            return ['\'gif\'','\'png\'','\'jpg\'','\'jpeg\''];
        } else {
            return ['gif','png','jpg','jpeg'];
        }
    }

    /**
     * getFileDriver
     *
     * @return object
     */
    public function getFileDriver()
    {
        return $this->file;
    }

    /**
     * getFilesystemFile
     *
     * @return object
     */
    public function getFilesystemFile()
    {
        return $this->filesystemFile;
    }

    /**
     * getSerializer
     *
     * @return object
     */
    public function getSerializer()
    {
        return $this->serialize;
    }

    /**
     * deleteUploadedFile deletes uploaded file
     *
     * @param  boolean $delete
     * @param  string  $data
     * @return void
     */
    public function deleteUploadedFile($delete, $data)
    {
        if ($delete) {
            $file = $this->getFileDriver();
            $mediaPath = $this->fileSystem
                ->getDirectoryRead(DirectoryList::MEDIA)
                ->getAbsolutePath();
            $deletePath = $mediaPath.$data;
            if ($file->isExists($deletePath)) {
                $file->deleteFile($deletePath);
            }
        }
    }

    /**
     * resize
     *
     * @param string $image
     * @param int $width
     * @param int $height
     * @return void
     */
    public function resize($image, $width = null, $height = null)
    {
        $absolutePath = $this->fileSystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath('catalog/category/xtremo/tmp/').$image;

        $imageResized = $this->fileSystem->getDirectoryRead(
            DirectoryList::MEDIA
        )->getAbsolutePath('catalog/category/xtremo/resized/');

        $isDirectoryExists = $this->filesystemFile->checkAndCreateFolder($imageResized);
        if ($isDirectoryExists) {
            //create image factory...
            $imageResize = $this->imageFactory->create();
            $imageResize->open($absolutePath);
            $imageResize->constrainOnly(true);
            $imageResize->keepTransparency(true);
            $imageResize->keepFrame(false);
            $imageResize->keepAspectRatio(false);

            // TRUE if want to resize image according to original image height and width
            $imageResize->backgroundColor([000, 000, 000]);

            $imageResize->resize($width, $height);

            //destination folder
            $destination = $imageResized . $image;

            //save image
            $imageResize->save($destination);

            $resizedURL = $this->getMediaUrl().'catalog/category/xtremo/resized/'.$image;
        }
    }

    /**
     * This function will write the data into the log file
     *
     * @param [mix] $data
     * @return void
     */
    public function logData($data)
    {
        $this->logger->info($data);
    }

    /**
     * Clean Cache
     */
    public function clearCache()
    {
        $cacheManager = $this->cacheManager->create();
        $availableTypes = $cacheManager->getAvailableTypes();
        $cacheManager->clean($availableTypes);
    }
}
