<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Controller\Adminhtml\Bannerimages;

use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Backend\App\Action;
use Magento\TestFramework\ErrorLog\Logger;

/**
 * Xtremo Adminhtml Bannerimages massDelete Controller
 */
class MassDelete extends \Magento\Backend\App\Action
{
    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    protected $helper;

    /**
     * @var \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection
     */
    protected $bannerImagesCollection;

    /**
     * @var \Magento\Ui\Component\MassAction\Filter
     */
    private $filterModel;
    
    /**
     * @param Action\Context $context
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param \Magento\Ui\Component\MassAction\Filter $filterModel
     * @param \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection
     */
    public function __construct(
        Action\Context $context,
        \Webkul\Xtremo\Helper\Data $helper,
        \Magento\Ui\Component\MassAction\Filter $filterModel,
        \Webkul\Xtremo\Model\ResourceModel\Bannerimages\CollectionFactory $bannerImagesCollection
    ) {
        $this->helper = $helper;
        $this->filterModel = $filterModel;
        $this->bannerImagesCollection = $bannerImagesCollection;
        parent::__construct($context);
    }

    /**
     * {@inheritdoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_Xtremo::bannerimages');
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $model = $this->filterModel;
        $collection = $model->getCollection($this->bannerImagesCollection->create());

        foreach ($collection as $image) {
            $this->helper->deleteUploadedFile(true, $image['link']);
        }
        $collection->walk('delete');

        // clear cache
        $this->helper->clearCache();
        $this->messageManager->addSuccess(__('Image(s) deleted successfully.'));
        $resultRedirect = $this->resultRedirectFactory->create();
        return $resultRedirect->setPath('*/*/');
    }
}
