<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Controller\Adminhtml\Bannerimages;

use Webkul\Xtremo\Controller\Adminhtml\Bannerimages as BannerimagesController;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action;
use Magento\Framework\Json\Helper\Data as JsonHelper;

/**
 * Xtremo Adminhtml Bannerimages Edit Controller
 */
class Edit extends BannerimagesController
{
    /**
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $helper;

    private $bannerImage;

    /**
     *
     * @var \Magento\Framework\Registry
     */
    private $registry;
    
    /**
     *
     * @var \Magento\Backend\Model\Session
     */
    private $backendSession;

    /**
     * @var Magento\Framework\Json\Helper\Data
     */
    private $jsonHelper;

    /**
     * @param Action\Context             $context
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param JsonHelper                                $jsonHelper
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Backend\Model\Session $backendSession,
        \Webkul\Xtremo\Helper\Data $helper,
        JsonHelper $jsonHelper,
        \Webkul\Xtremo\Model\Bannerimages $bannerImage
    ) {
        $this->helper           = $helper;
        $this->backendSession   = $backendSession;
        $this->registry         = $registry;
        $this->bannerImage      = $bannerImage;
        $this->jsonHelper       = $jsonHelper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        $bannerImagesModel = $this->bannerImage;

        if (isset($params['id'])) {
            $bannerImagesModel->load($params['id']);
        }

        $data = $this->backendSession->getFormData(true);
        $bannerImagesModel['filename'] = $bannerImagesModel['link'];

        if (isset($data['category_ids']) && !empty($data['category_ids'])) {
            $data['category_ids'] = $this->jsonHelper->jsonDecode($data['category_ids']);
        }

        if (isset($data['store_ids']) && !empty($data['store_ids'])) {
            $data['store_ids'] = $this->jsonHelper->jsonDecode($data['store_ids']);
        }

        if (!empty($data)) {
            $bannerImagesModel->setData($data);
        }

        $this->registry->register('xtremo_bannerimages', $bannerImagesModel);

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->prepend(__('Banner Image'));
        $resultPage->getConfig()->getTitle()->prepend(
            $bannerImagesModel->getId() ? $bannerImagesModel->getTitle() : __('New Image')
        );

        $resultPage->addBreadcrumb(__('Manage Category Banner Images'), __('Manage Category Banner Images'));
        $blockClassName = \Webkul\Xtremo\Block\Adminhtml\Bannerimages\Edit::class;
        $content = $resultPage->getLayout()->createBlock($blockClassName, "xtremobanner");
        $resultPage->addContent($content);
        
        return $resultPage;
    }
}
