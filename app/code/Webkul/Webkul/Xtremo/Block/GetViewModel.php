<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block;

/**
 * Mpxtremo GetViewModel Block
 */
class GetViewModel extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\Mpxtremo\ViewModel\Mpxtremo
     */
    protected $xtremoViewModel;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Xtremo\ViewModel\Xtremo $xtremoViewModel
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Mpxtremo\ViewModel\Mpxtremo $xtremoViewModel,
        array $data = []
    ) {
        $this->xtremoViewModel = $xtremoViewModel;
        parent::__construct($context, $data);
    }

    /**
     * Get Mpxtremo View Model
     *
     * @return object \Webkul\Mpxtremo\ViewModel\Mpxtremo
     */
    public function getXtremoViewModel()
    {
        return $this->xtremoViewModel;
    }
}
