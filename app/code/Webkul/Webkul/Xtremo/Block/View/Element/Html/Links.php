<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\View\Element\Html;

/**
 * Links list block
 */
class Links extends \Magento\Framework\View\Element\Html\Links
{
    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

     /**
      * @param \Magento\Framework\View\Element\Template\Context $context
      * @param \Webkul\Xtremo\Helper\Data                       $helper
      * @param array                                            $data    = []
      */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Xtremo\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->helper = $helper;
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    protected function _toHtml()
    {
        if (false != $this->getTemplate()) {
            return parent::_toHtml();
        }

        $html = '';
        if ($this->helper->checkCurrentTheme()) {
            $topLinkSwitcher = false;
            $footerLinks = false;
            $topLinkClass = "";

            if ($this->getNameInLayout() == "top.links") {
                $topLinkSwitcher = true;
            }

            if (strpos($this->getNameInLayout(), "footer_links")!==false) {
                $footerLinks = true;
            }

            $customerLoggedIn = $this->helper->isLoggedIn();
            if ($customerLoggedIn) {
                $customerName = $this->helper->getLoggedInCustomer()->getFirstname() . " "
                 . $this->helper->getLoggedInCustomer()->getLastname();
                $welcomeMessage = __("Hello %1!", $customerName);
                $loginText = __("Sign out");
            } else {
                $welcomeMessage = __("Hello User!");
                $loginText = __("Sign in");
            }

            if ($this->getLinks()) {
                if ($topLinkSwitcher) {
                    $html .= '<div class="switcher toplink switcher-toplink"
            					id="switcher-toplink">
    					        <strong class="label switcher-label">
    					            <span>'. $welcomeMessage . '</span>
    					        </strong>
            					<div class="actions dropdown options switcher-options">
    					            <div class="action toggle switcher-trigger" id="switcher-toplink-trigger">
    					                <strong class="switcher-toplink-strong">
    					                    <span>' . $welcomeMessage . '</span>
    					                </strong>
    					                <span class="wk-cat-text">'. $loginText . '</span>
    					            </div>';
                    $topLinkClass = " dropdown switcher-dropdown";
                }
                if ($footerLinks) {
                    $html = '<div' . ($this->hasCssClass() ? ' class="' . $this->escapeHtml(
                        $this->getCssClass()
                    ) . '"' : '') . '>';
                    $html .= '<div class="block-title"><strong><span>'
                        .($this->hasDivClass() ?
                            $this->escapeHtml(
                                $this->getDivClass()
                            ) : __('Information'))
                        .'</span></strong></div>';
                }
                $html = $this->getLinksHtml($html, $topLinkClass, $topLinkSwitcher, $welcomeMessage, $customerLoggedIn);

                if ($footerLinks) {
                    $html .= '</div>';
                }
            }
        } else {
            $html = $this->getDefaultHtml();
        }

        return $html;
    }

    private function getLinksHtml($html, $topLinkClass, $topLinkSwitcher, $welcomeMessage, $customerLoggedIn)
    {
        $html .= '<ul' . ($this->hasCssClass() ? ' class="' . $this->escapeHtml(
            $this->getCssClass()
        ) . $topLinkClass .'"' : '') . ($topLinkSwitcher ? ' data-mage-init=\''. '{"dropdownDialog":{
                "appendTo":"#switcher-toplink > .options",
                "triggerTarget":"#switcher-toplink-trigger",
                "closeOnMouseLeave": false,
                "triggerClass":"active",
                "parentClass":"active",
                "buttons":null}}' .'\''  : '') . '>';
        if ($topLinkSwitcher) {
            $html .= '<li class="welcome-msg"><span>' .$welcomeMessage. '</span></li>';
            if ($customerLoggedIn) {
                $html .= '<li>' . $this->getChildHtml('authorization-link') . '</li>';
            } else {
                $html .= '<li class="reg-sign-wrapper"><ul>'
                        . $this->getChildHtml('authorization-link')
                        . $this->getChildHtml('register-link')
                    . '</ul></li>';
            }
        }

        foreach ($this->getLinks() as $link) {
            if ($topLinkSwitcher) {
                if ($link->getNameInLayout() !== "authorization-link"
                    && $link->getNameInLayout() !== "register-link"
                    && $link->getNameInLayout() !== "forgot-password-link"
                ) {
                    $html .= $this->renderLink($link);
                }
            } else {
                $html .= $this->renderLink($link);
            }
        }

        if ($topLinkSwitcher && !$customerLoggedIn) {
            $html .= '<li>' . $this->getChildHtml('forgot-password-link') . '</li>';
        }
        $html .= '</ul>';

        if ($topLinkSwitcher) {
            $html .= '</div></div>';
        }

        return $html;
    }

    /**
     * @return string
     */
    private function getDefaultHtml()
    {
        $html = '';
        if ($this->getLinks()) {
            $html = '<ul' . ($this->hasCssClass() ? ' class="' . $this->escapeHtml(
                $this->getCssClass()
            ) . '"' : '') . '>';
            foreach ($this->getLinks() as $link) {
                $html .= $this->renderLink($link);
            }
            $html .= '</ul>';
        }
        return $html;
    }
}
