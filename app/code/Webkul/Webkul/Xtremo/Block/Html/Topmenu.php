<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\Html;

use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\Data\Tree\NodeFactory;
use Magento\Framework\Data\TreeFactory;

/**
 * Adminhtml Xtremo Topmenu Block
 */
class Topmenu extends \Magento\Theme\Block\Html\Topmenu
{
    private $newHtml = '';

    /**
     * @var  \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * @var array
     */
    private $categoryBannerImages = [];

    /**
     * @param Context                    $context
     * @param NodeFactory                $nodeFactory
     * @param TreeFactory                $treeFactory
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param array                      $data        = []
     */
    public function __construct(
        Context $context,
        NodeFactory $nodeFactory,
        TreeFactory $treeFactory,
        \Webkul\Xtremo\Helper\Data $helper,
        array $data = []
    ) {
        parent::__construct($context, $nodeFactory, $treeFactory, $data);
        $this->helper = $helper;
        $this->categoryBannerImages = $this->helper->getCategoryBannerImages();
    }

    /**
     * Recursively generates top menu html from data that is specified in $menuTree
     *
     * @param  \Magento\Framework\Data\Tree\Node $menuTree
     * @param  string                            $childrenWrapClass
     * @param  int                               $limit
     * @param  array                             $colBrakes
     * @return string
     */
    protected function _getHtml(
        \Magento\Framework\Data\Tree\Node $menuTree,
        $childrenWrapClass,
        $limit,
        array $colBrakes = []
    ) {
        $html = '';

        $children = $menuTree->getChildren();
        $parentLevel = $menuTree->getLevel();
        $childLevel = $parentLevel === null ? 0 : $parentLevel + 1;

        $counter = 1;
        $itemPosition = 1;
        $childrenCount = $children->count();

        $parentPositionClass = $menuTree->getPositionClass();
        $itemPositionClassPrefix = $parentPositionClass ? $parentPositionClass . '-' : 'nav-';

        foreach ($children as $child) {
            $child->setLevel($childLevel);
            $child->setIsFirst($counter == 1);
            $child->setIsLast($counter == $childrenCount);
            $child->setPositionClass($itemPositionClassPrefix . $counter);

            $outermostClassCode = '';
            $outermostClass = $menuTree->getOutermostClass();

            if ($childLevel == 0 && $outermostClass) {
                $outermostClassCode = ' class="' . $outermostClass . '" ';
                $child->setClass($outermostClass);
            }

            if (!empty($colBrakes) && $colBrakes[$counter]['colbrake']) {
                $html .= '</ul></li><li class="column"><ul>';
            }

            if ($childLevel == 0) {
                $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
                $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                    $child->getName()
                ) . '</span></a>' ;

                $this->newHtml .= $this->_addSubMenu(
                    $child,
                    $childLevel,
                    $childrenWrapClass,
                    $limit
                );

                $html .= '</li>';
            } else {
                $html .= '<li ' . $this->_getRenderedMenuItemAttributes($child) . '>';
                $html .= '<a href="' . $child->getUrl() . '" ' . $outermostClassCode . '><span>' . $this->escapeHtml(
                    $child->getName()
                ) . '</span></a>' . $this->_addSubMenu(
                    $child,
                    $childLevel,
                    $childrenWrapClass,
                    $limit
                ) . '</li>';
            }
            $itemPosition++;
            $counter++;
        }

        if (!empty($colBrakes) && $limit) {
            $html = '<li class="column"><ul>' . $html . '</ul></li>';
        }

        return $html;
    }

    /**
     * Add sub menu HTML code for current menu item
     *
     * @param  \Magento\Framework\Data\Tree\Node $child
     * @param  string                            $childLevel
     * @param  string                            $childrenWrapClass
     * @param  int                               $limit
     * @return string HTML code
     */
    protected function _addSubMenu($child, $childLevel, $childrenWrapClass, $limit)
    {
        $html = '';
        if (!$child->hasChildren()) {
            return $html;
        }

        $colStops = [];
        if ($childLevel == 0 && $limit) {
            $colStops = $this->_columnBrake($child->getChildren(), $limit);
        }

        $categoryNode = explode("-", $child->getId());
        $categoryId = $categoryNode[2];
        $categoryImage = $this->helper->getCategoryBannerImage($this->categoryBannerImages, $categoryId);

        $html .= '<ul class="level'
                . $childLevel . ' '
                . $child->getPositionClass() . ' '
                . $child->getClass()
                . ' submenu">';
        $html .= $this->_getHtml($child, $childrenWrapClass, $limit, $colStops);
        if ($childLevel == 0 && $categoryImage) {
            $html .= '<div class="wk-cat-image"><img class="img-cat-0" src="'.$categoryImage.'"/></div>';
        }
        $html .= '</ul>';

        return $html;
    }

    /**
     * Returns html code for Top search categories
     *
     * @return string
     */
    public function newHtml()
    {
        return $this->newHtml;
    }
}
