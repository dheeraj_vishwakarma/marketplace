<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\Config;

/**
 * Adminhtml Xtremo Block Config Category
 */
class Category extends \Magento\Config\Block\System\Config\Form\Field
{
    /**
     * @var  \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @var  \Magento\Backend\Helper\Data
     */
    private $backendHelper;

    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param Registry                                $coreRegistry
     * @param \Magento\Backend\Helper\Data            $backendHelper
     * @param array                                   $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Backend\Helper\Data $backendHelper,
        array $data = []
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->backendHelper = $backendHelper;
        parent::__construct($context, $data);
    }

    /**
     * [_getElementHtml used to get HTML of an element in system config]
     *
     * @param  \Magento\Framework\Data\Form\Element\AbstractElement $element
     * @return string
     */
    protected function _getElementHtml(\Magento\Framework\Data\Form\Element\AbstractElement $element)
    {
        $html = $element->getElementHtml();
        $html .= '<script type="text/javascript">
            var categoryIds = [];
            require(["jquery", "jquery/ui"], function () {
                jQuery(document).ready(function () {
                    jQuery("#'.$element->getHtmlId().' option").each(function(){
                        var categoryId = jQuery(this).val();
                        categoryIds.push(categoryId);
                    });
                    if(categoryIds.length>0){
                        jQuery.ajax({
                            url: "'.$this->backendHelper->getUrl("xtremo/header/category").'",
                            type: "POST",
                            dataType: "json",
                            data:{
                                level : 2,
                                ids   : categoryIds
                            },
                            success: function(data){
                                if(data.length>0){
                                    jQuery.each(data,function(key, val){

                                        jQuery("#'.$element->getHtmlId().' option[value="+val+"]").css({
                                            "font-weight":"bold",
                                        });
                                    });
                                }
                            }
                        });
                    }
                });
            });
            </script>';
        return $html;
    }
}
