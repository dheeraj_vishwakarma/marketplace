<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Block\Adminhtml\Bannerimages\Edit;
 
class AdditionalBlock extends \Magento\Framework\View\Element\Template
{
    const JS_TEMPLATE = 'images/js.phtml';
 
    /**
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param array                                 $data
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    ) {
        $this->coreRegistry = $registry;
        parent::__construct($context, $data);
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->setTemplate(static::JS_TEMPLATE);
        return $this;
    }

    public function getImageData()
    {
        $model = $this->coreRegistry->registry('xtremo_bannerimages');
        if ($model->getId()) {
            if ($model->getFilename()) {
                return false;
            }
        }
        return true;
    }
}
