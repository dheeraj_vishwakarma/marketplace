<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model\Layer\Filter;

use Magento\Catalog\Model\Layer\Filter\AbstractFilter;
use Magento\Catalog\Model\Layer\Filter\DataProvider\Category as CategoryDataProvider;

/**
 * Layer category filter
 */
class Category extends AbstractFilter
{
    /**
     * @var \Magento\Framework\Escaper
     */
    private $escaper;

    /**
     * @var CategoryDataProvider
     */
    private $dataProvider;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;
    
    /**
     * @param \Magento\Catalog\Model\Layer\Filter\ItemFactory                  $filterItemFactory
     * @param \Magento\Store\Model\StoreManagerInterface                       $storeManager
     * @param \Magento\Catalog\Model\Layer                                     $layer
     * @param \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder             $itemDataBuilder
     * @param \Magento\Framework\Escaper                                       $escaper
     * @param \Magento\Catalog\Model\Layer\Filter\DataProvider\CategoryFactory $categoryDataProviderFactory
     * @param \Webkul\Xtremo\Helper\Data                                       $helper
     * @param \array                                                           $data
     */
    public function __construct(
        \Magento\Catalog\Model\Layer\Filter\ItemFactory $filterItemFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Catalog\Model\Layer $layer,
        \Magento\Catalog\Model\Layer\Filter\Item\DataBuilder $itemDataBuilder,
        \Magento\Framework\Escaper $escaper,
        \Magento\Catalog\Model\Layer\Filter\DataProvider\CategoryFactory $categoryDataProviderFactory,
        \Webkul\Xtremo\Helper\Data $helper,
        \Magento\Framework\App\RequestInterface $request,
        array $data = []
    ) {
        parent::__construct(
            $filterItemFactory,
            $storeManager,
            $layer,
            $itemDataBuilder,
            $data
        );
        $this->escaper = $escaper;
        $this->_requestVar = 'cat';
        $this->dataProvider = $categoryDataProviderFactory->create(['layer' => $this->getLayer()]);
        $this->helper = $helper;
        $this->request = $request;
    }

    /**
     * Apply category filter to product collection
     *
     * @param  \Magento\Framework\App\RequestInterface $request
     * @return $this
     */
    public function apply(\Magento\Framework\App\RequestInterface $request)
    {
        $categoryId = $request->getParam($this->_requestVar) ?: $request->getParam('id');
        if (empty($categoryId)) {
            return $this;
        }

        $this->dataProvider->setCategoryId($categoryId);

        $category = $this->dataProvider->getCategory();

        $this->getLayer()->getProductCollection()->addCategoryFilter($category);

        if ($request->getParam('id') != $category->getId() && $this->dataProvider->isValid()) {
            $this->getLayer()->getState()->addFilter($this->_createItem($category->getName(), $categoryId));
        }
        return $this;
    }

    /**
     * Get filter value for reset current filter state
     *
     * @return mixed|null
     */
    public function getResetValue()
    {
        return $this->dataProvider->getResetValue();
    }

    /**
     * Get filter name
     *
     * @return \Magento\Framework\Phrase
     */
    public function getName()
    {
        return __('Category');
    }

    /**
     * Get data array for building category filter items
     *
     * @return array
     */
    protected function _getItemsData()
    {
        /**
         * @var \Magento\CatalogSearch\Model\ResourceModel\Fulltext\Collection $productCollection
        */
        try {
            $productCollection = $this->getLayer()->getProductCollection();
            $optionsFacetedData = $productCollection->getFacetedData('category');
            $category = $this->dataProvider->getCategory();

            $filter = trim($this->request->getParam('shop'));
            if ($filter == "") {
                $categories = $category->getParentCategory()->getChildrenCategories();
                if ($category->getIsActive()) {
                    foreach ($categories as $category) {
                        if ($category->getIsActive()
                            && $category->getProductCollection()->getSize()
                        ) {
                            $this->itemDataBuilder->addItemData(
                                $this->escaper->escapeHtml($category->getName()),
                                $category->getId(),
                                $category->getProductCollection()->getSize()
                            );
                        }
                    }
                }
            } else {
                $categories = $category->getChildrenCategories();
                $collectionSize = $productCollection->getSize();
                if ($category->getIsActive()) {
                    foreach ($categories as $category) {
                        if ($category->getIsActive()
                            && isset($optionsFacetedData[$category->getId()])
                            && $this->isOptionReducesResults(
                                $optionsFacetedData[$category->getId()]['count'],
                                $collectionSize
                            )
                        ) {
                            $this->itemDataBuilder->addItemData(
                                $this->escaper->escapeHtml($category->getName()),
                                $category->getId(),
                                $optionsFacetedData[$category->getId()]['count']
                            );
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $this->helper->logData('Xtremo_Model_Layer_Filter_Category :'.$e);
        }
        return $this->itemDataBuilder->build();
    }

    public function getAllParentCategories()
    {
        $parentCats = [];
        $category = $this->dataProvider->getCategory();
        foreach ($category->getParentCategories() as $parent) {
            if ($parent->getId() !== $category->getId()) {
                $parentCats[$parent->getName()] = $parent->getUrl();
            }
        }
        return $parentCats;
    }

    public function getCurrentCat()
    {
        return $this->dataProvider->getCategory()->getId();
    }
}
