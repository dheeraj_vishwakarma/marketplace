<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model;

use Webkul\Xtremo\Api\Data\BannerimagesInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * Webkul Xtremo Bannerimages Model
 */
class Bannerimages extends \Magento\Framework\Model\AbstractModel implements BannerimagesInterface, IdentityInterface
{
    /**
     * No route page id
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**#@+
     * Bannerimages's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    /**#@-*/

    /**
     * Xtremo Bannerimages cache tag
     */
    const CACHE_TAG = 'xtremo_bannerimages';

    /**
     * @var string
     */
    protected $_cacheTag = 'xtremo_bannerimages';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'xtremo_bannerimages';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\Xtremo\Model\ResourceModel\Bannerimages::class
        );
    }

    /**
     * Load object data
     *
     * @param  int|null $id
     * @param  string   $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteBannerimages();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route Bannerimages
     *
     * @return \Webkul\Xtremo\Model\Bannerimages
     */
    public function noRouteBannerimages()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Prepare Bannerimages's statuses.
     * Available event xtremo_bannerimages_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [
            self::STATUS_ENABLED => __('Enabled'),
            self::STATUS_DISABLED => __('Disabled')
        ];
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param  int $id
     * @return \Webkul\Xtremo\Api\Data\BannerimagesInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
