<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model;

use Magento\Framework\Setup\SampleData\Context as SampleDataContext;

class Page
{
    /**
     * @var \Magento\Framework\Setup\SampleData\Context
     */
    private $sampleDataContext;

    /**
     * @var \Magento\Cms\Model\PageFactory
     */
    private $pageFactory;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * @param SampleDataContext              $sampleDataContext
     * @param \Magento\Cms\Model\PageFactory $pageFactory
     * @param \Webkul\Xtremo\Helper\Data     $helper
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Webkul\Xtremo\Helper\Data $helper
    ) {
        $this->sampleDataContext = $sampleDataContext;
        $this->pageFactory = $pageFactory;
        $this->helper = $helper;
    }

    /**
     * @param array $fixtures
     * @throws \Exception
     */
    public function install(array $fixtures)
    {
        $fixtureManager = $this->sampleDataContext->getFixtureManager();
        $csvReader      = $this->sampleDataContext->getCsvReader();
        $file           = $this->helper->getFileDriver();
        foreach ($fixtures as $fileName) {
            $fileName = $fixtureManager->getFixture($fileName);
            if (!$file->isExists($fileName)) {
                continue;
            }

            $rows = $csvReader->getData($fileName);
            $header = array_shift($rows);

            foreach ($rows as $row) {
                $data = [];
                foreach ($row as $key => $value) {
                    $data[$header[$key]] = $value;
                }
                $row = $data;
                $this->updateRow($row);
            }
        }
    }

    private function updateRow($row)
    {
        $this->pageFactory->create()
            ->load($row['identifier'], 'identifier')
            ->addData($row)
            ->setStores([\Magento\Store\Model\Store::DEFAULT_STORE_ID])
            ->save();
    }
}
