<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model\Category;

use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Catalog\Model\Attribute\ScopeOverriddenValue;
use Magento\Catalog\Model\Category;
use Magento\Catalog\Model\Category\Attribute\Backend\Image as ImageBackendModel;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CategoryCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute as EavAttribute;
use Magento\Eav\Api\Data\AttributeInterface;
use Magento\Eav\Model\Config;
use Magento\Eav\Model\Entity\Type;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Filesystem;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\Component\Form\Field;
use Magento\Ui\DataProvider\EavValidationRules;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

class DataProvider extends \Magento\Catalog\Model\Category\DataProvider
{
    /**
     * @var Config
     */
    private $eavConfig;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CategoryFactory
     */
    private $categoryFactory;

    /**
     * @var ScopeOverriddenValue
     */
    private $scopeOverriddenValue;

    /**
     * @var ArrayManager
     */
    private $arrayManager;

    /**
     * @var Filesystem
     */
    private $fileInfo;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * DataProvider constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param EavValidationRules $eavValidationRules
     * @param CategoryCollectionFactory $categoryCollectionFactory
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $registry
     * @param Config $eavConfig
     * @param \Magento\Framework\App\RequestInterface $request
     * @param CategoryFactory $categoryFactory
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        EavValidationRules $eavValidationRules,
        CategoryCollectionFactory $categoryCollectionFactory,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $registry,
        Config $eavConfig,
        \Magento\Framework\App\RequestInterface $request,
        CategoryFactory $categoryFactory,
        \Webkul\Xtremo\Helper\Data $helper,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->eavValidationRules = $eavValidationRules;
        $this->collection = $categoryCollectionFactory->create();
        $this->collection->addAttributeToSelect('*');
        $this->eavConfig = $eavConfig;
        $this->registry = $registry;
        $this->storeManager = $storeManager;
        $this->request = $request;
        $this->helper = $helper;
        $this->categoryFactory = $categoryFactory;

        parent::__construct(
            $name,
            $primaryFieldName,
            $requestFieldName,
            $eavValidationRules,
            $categoryCollectionFactory,
            $storeManager,
            $registry,
            $eavConfig,
            $request,
            $categoryFactory,
            $meta,
            $data,
            $pool
        );
    }

    /**
     * Add use Default Settings
     *
     * @param array $category
     * @param array $categoryData
     * @return array
     */
    protected function addUseDefaultSettings($category, $categoryData)
    {
        $data = parent::addUseDefaultSettings($category, $categoryData);

        if (isset($data['popular_category_image'])) {
            unset($data['popular_category_image']);
            $helper = $this->helper;

            $data['popular_category_image'][0]['name'] = $category->getData('popular_category_image');
            $data['popular_category_image'][0]['url']  = $helper->getCategoryThumbUrl($category);
        }
        return $data;
    }

    /**
     * Get fields map
     *
     * @return array
     */
    protected function getFieldsMap()
    {
        $fields = parent::getFieldsMap();
        $fields['content'][] = 'popular_category_image'; // NEW FIELD

        return $fields;
    }

     /**
      * Get data
      *
      * @return array
      * @since 101.0.0
      */
    public function getData()
    {
        $this->loadedData = parent::getData();
        $category = $this->getCurrentCategory();
        $helper = $this->helper;
        if ($this->loadedData[$category->getId()]) {
            $result = [];
            $categoryThumbUrl = $helper->getCategoryThumbUrl($category);
            if ($categoryThumbUrl) {
                $result = getimagesize($categoryThumbUrl);
            }
            $this->loadedData[$category->getId()]["popular_category_image"] =  [
                [
                    "name" => $category->getData('popular_category_image'),
                    "url"  => $categoryThumbUrl,
                    "size" => isset($result['0']) ? $result['0'] : "",
                    "type" => isset($result['mime']) ? $result['mime'] : ""
                ]
            ];
        }
        return $this->loadedData;
    }
}
