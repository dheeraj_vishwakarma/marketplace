<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Model;

use Magento\Framework\Setup\SampleData\Context as SampleDataContext;

/**
 * Launches setup of sample data for Widget module
 */
class CmsBlock
{
    /**
     * @var \Magento\Framework\Setup\SampleData\Context
     */
    private $sampleDataContext;

    /**
     * @var \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory
     */
    private $categoryFactory;

    /**
     * @var \Magento\Widget\Model\Widget\InstanceFactory
     */
    private $widgetFactory;

    /**
     * @var \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory
     */
    private $themeCollectionFactory;

    /**
     * @var \Magento\Cms\Model\BlockFactory
     */
    private $cmsBlockFactory;

    /**
     * @var \Magento\Widget\Model\ResourceModel\Widget\Instance\CollectionFactory
     */
    private $appCollectionFactory;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $helper;

    /**
     * @var \Magento\Framework\App\State
     */
    private $appState;

    /**
     * @param SampleDataContext $sampleDataContext
     * @param \Magento\Widget\Model\Widget\InstanceFactory $widgetFactory
     * @param \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $themeCollectionFactory
     * @param \Magento\Cms\Model\BlockFactory $cmsBlockFactory
     * @param \Magento\Widget\Model\ResourceModel\Widget\Instance\CollectionFactory $appCollectionFactory
     * @param \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory
     * @param \Webkul\Xtremo\Helper\Data $helper
     * @param \Magento\Framework\App\State $appState
     */
    public function __construct(
        SampleDataContext $sampleDataContext,
        \Magento\Widget\Model\Widget\InstanceFactory $widgetFactory,
        \Magento\Theme\Model\ResourceModel\Theme\CollectionFactory $themeCollectionFactory,
        \Magento\Cms\Model\BlockFactory $cmsBlockFactory,
        \Magento\Widget\Model\ResourceModel\Widget\Instance\CollectionFactory $appCollectionFactory,
        \Magento\Catalog\Model\ResourceModel\Category\CollectionFactory $categoryFactory,
        \Webkul\Xtremo\Helper\Data $helper,
        \Magento\Framework\App\State $appState
    ) {
        $this->sampleDataContext = $sampleDataContext;
        $this->widgetFactory = $widgetFactory;
        $this->themeCollectionFactory = $themeCollectionFactory;
        $this->cmsBlockFactory = $cmsBlockFactory;
        $this->appCollectionFactory = $appCollectionFactory;
        $this->categoryFactory = $categoryFactory;
        $this->helper = $helper;
        $this->appState = $appState;
    }

    private function getCmsBlock($id, $blockIdentifier)
    {
        return $this->cmsBlockFactory->create()->load($id, $blockIdentifier);
    }
    
    /**
     * {@inheritdoc}
     */
    public function install(array $fixtures)
    {
        $fixtureManager = $this->sampleDataContext->getFixtureManager();
        $csvReader      = $this->sampleDataContext->getCsvReader();
        $file           = $this->helper->getFileDriver();
        $pageGroupConfig = [
            'pages' => [
                'block' => '',
                'for' => 'all',
                'layout_handle' => 'default',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
            'all_pages' => [
                'block' => '',
                'for' => 'all',
                'layout_handle' => 'default',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
            'anchor_categories' => [
                'entities' => '',
                'block' => '',
                'for' => 'all',
                'is_anchor_only' => 0,
                'layout_handle' => 'catalog_category_view_type_layered',
                'template' => 'widget/static_block/default.phtml',
                'page_id' => '',
            ],
        ];

        foreach ($fixtures as $fileName) {
            $fileName = $fixtureManager->getFixture($fileName);
            if (!$file->isExists($fileName)) {
                continue;
            }

            $rows = $csvReader->getData($fileName);
            $header = array_shift($rows);

            foreach ($rows as $row) {
                $data = [];
                foreach ($row as $key => $value) {
                    $data[$header[$key]] = $value;
                }
                $row = $data;
                /**
                 * @var \Magento\Widget\Model\ResourceModel\Widget\Instance\Collection $instanceCollection
                */
                $instanceCollection = $this->appCollectionFactory->create();
                $instanceCollection->addFilter('title', $row['title']);
                if ($instanceCollection->getSize() > 0) {
                    continue;
                }
                /**
                 * @var \Magento\Cms\Model\Block $block
                */
                $block = $this->getCmsBlock($row['block_identifier'], 'identifier');
                if (!$block) {
                    continue;
                }
                $widgetInstance = $this->widgetFactory->create();

                $code = $row['type_code'];
                $themeId = $this->themeCollectionFactory->create()->getThemeByFullPath($row['theme_path'])->getId();
                $type = $widgetInstance->getWidgetReference('code', $code, 'type');
                $pageGroup = [];
                $group = $row['page_group'];
                $pageGroup['page_group'] = $group;
                $pageGroup[$group] = $this->getMergedArray(
                    $pageGroupConfig[$group],
                    $this->helper->getSerializer()->unserialize($row['group_data'])
                );
                if (!empty($pageGroup[$group]['entities'])) {
                    $pageGroup[$group]['entities'] = $this->getCategoryByUrlKey(
                        $pageGroup[$group]['entities']
                    )->getId();
                }

                $widgetInstance->setType($type)->setCode($code)->setThemeId($themeId);
                $widgetInstance->setTitle($row['title'])
                    ->setStoreIds([\Magento\Store\Model\Store::DEFAULT_STORE_ID])
                    ->setWidgetParameters(['block_id' => $block->getId()])
                    ->setPageGroups([$pageGroup]);
                $this->appState->emulateAreaCode(
                    "frontend",
                    [$this, "updateRow"],
                    [$widgetInstance]
                );
            }
        }
    }

    /**
     * Get merged array from two arrays
     *
     * @param array $pageConfig
     * @param array $rowData
     * @return array
     */
    private function getMergedArray($pageConfig, $rowData)
    {
        return array_merge($pageConfig, $rowData);
    }

    /**
     * Update widget instance
     *
     * @param object $widgetInstance
     * @return void
     */
    public function updateRow($widgetInstance)
    {
        $widgetInstance->save();
    }
    
    /**
     * Get category by url
     *
     * @param string $urlKey
     * @return \Magento\Framework\DataObject
     */
    protected function getCategoryByUrlKey($urlKey)
    {
        $category = $this->categoryFactory->create()
            ->addAttributeToFilter('url_key', $urlKey)
            ->addUrlRewriteToResult()
            ->getFirstItem();
        return $category;
    }
}
