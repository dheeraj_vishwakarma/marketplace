<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Setup;

use Magento\Framework\Setup;
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface as ScopedAttribute;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;

/**
 * Xtremo InstallData Setup
 */
class InstallData implements InstallDataInterface
{
    /**
     * @var Setup\SampleData\Executor
     */
    protected $executor;

    /**
     * @var Installer
     */
    protected $installer;

    /**
     * @var EavSetupFactory $eavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * @var \Magento\Framework\Module\Dir\Reader
     */
    private $reader;

    /**
     * @var \Magento\Framework\Filesystem
     */
    private $fileSystem;

    /**
     * @var \Magento\Framework\Filesystem\Io\File
     */
    private $filesystemFile;
    private $driver;

    /**
     * @var \Webkul\Xtremo\Helper\Data $helper
     */
    private $helper;

    /**
     * @param Setup\SampleData\Executor $executor
     * @param \Webkul\Xtremo\Setup\Installer $installer
     * @param EavSetupFactory $eavSetupFactory
     * @param \Magento\Framework\Module\Dir\Reader $reader
     * @param \Magento\Framework\Filesystem $fileSystem
     * @param \Magento\Framework\Filesystem\Io\File $filesystemFile
     * @param \Magento\Framework\Filesystem\Driver\File $driver
     * @param \Webkul\Xtremo\Helper\Data $helper
     */
    public function __construct(
        Setup\SampleData\Executor $executor,
        \Webkul\Xtremo\Setup\Installer $installer,
        EavSetupFactory $eavSetupFactory,
        \Magento\Framework\Module\Dir\Reader $reader,
        \Magento\Framework\Filesystem $fileSystem,
        \Magento\Framework\Filesystem\Io\File $filesystemFile,
        \Magento\Framework\Filesystem\Driver\File $driver,
        \Webkul\Xtremo\Helper\Data $helper
    ) {
        $this->executor = $executor;
        $this->installer = $installer;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->reader = $reader;
        $this->fileSystem = $fileSystem;
        $this->filesystemFile = $filesystemFile;
        $this->driver = $driver;
        $this->helper = $helper;
    }

    /**
     * {@inheritdoc}
     */
    public function install(Setup\ModuleDataSetupInterface $setup, Setup\ModuleContextInterface $context)
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);
        $eavSetup->removeAttribute(\Magento\Catalog\Model\Category::ENTITY, 'is_popular_category');

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'is_featured_product',
            [
                'type' => 'int',
                'label' => __('Is Featured'),
                'input' => 'boolean',
                'global' => ScopedAttribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '0',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'group' => 'Product Details'
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'is_in_sale',
            [
                'type' => 'int',
                'label' => __('Is In Sale'),
                'input' => 'boolean',
                'global' => ScopedAttribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '0',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'group' => 'Product Details'
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'is_tailored_product',
            [
                'type' => 'int',
                'label' => __('Is Tailored Product'),
                'input' => 'boolean',
                'global' => ScopedAttribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '0',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'group' => 'Product Details'
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Product::ENTITY,
            'is_in_top_rated',
            [
                'type' => 'int',
                'label' => __('Is In Top Rated Product'),
                'input' => 'boolean',
                'global' => ScopedAttribute::SCOPE_GLOBAL,
                'visible' => true,
                'required' => false,
                'user_defined' => true,
                'default' => '0',
                'searchable' => false,
                'filterable' => false,
                'comparable' => false,
                'visible_on_front' => false,
                'used_in_product_listing' => true,
                'unique' => false,
                'group' => 'Product Details'
            ]
        );

        $eavSetup->addAttribute(
            \Magento\Catalog\Model\Category::ENTITY,
            'is_popular_category',
            [
                'type'                  => 'int',
                'group'                 => 'General Information',
                'input'                 => 'select',
                'label'                 => __('Is Popular Category'),
                'note'                  => __('Is Popular Category'),
                'source'                => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                'global'                => ScopedAttribute::SCOPE_GLOBAL,
                'visible'               => true,
                'user_defined'          => false,
                'default'               => 0,
                'required'              => false,
                "searchable"            => false,
                "filterable"            => true,
                "comparable"            => false,
                'visible_on_front'      => false,
                'unique'                => false
            ]
        );

        $this->moveDirToMediaDir();

        $this->executor->exec($this->installer);
    }

    private function moveDirToMediaDir()
    {
        try {
            $mediaPath = $this->fileSystem->getDirectoryRead(
                \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
            )->getAbsolutePath('xtremo/homepage/category');
            if (!$this->filesystemFile->fileExists($mediaPath)) {
                $this->filesystemFile->mkdir($mediaPath, 0777, true);
                $categoryPlaceholderImage = $this->reader->getModuleDir(
                    '',
                    'Webkul_Xtremo'
                ).'/view/base/web/images/xtremo/homepage/category/placeholder.jpg';
                $this->filesystemFile->cp($categoryPlaceholderImage, $mediaPath.'/placeholder.jpg');
            }

            $mediaPath = $this->fileSystem->getDirectoryRead(
                \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
            )->getAbsolutePath('xtremo/homepage/banner/');
            if (!$this->filesystemFile->fileExists($mediaPath)) {
                $this->filesystemFile->mkdir($mediaPath, 0777, true);
                $bannerImage = $this->reader->getModuleDir(
                    '',
                    'Webkul_Xtremo'
                ).'/view/base/web/images/xtremo/homepage/banner/';
                $contents = $this->driver->readDirectoryRecursively($bannerImage);
                foreach ($contents as $path) {
                    $partsOfPath = explode('/', $path);
                    $this->filesystemFile->cp($path, $mediaPath.end($partsOfPath));
                }
            }

            $catalogImagesName = [
                '/m/b'=>['/mb01-blue-0.jpg','/mb03-black-0.jpg','/mb03-black-0_alt1.jpg',
                        '/mb02-gray-0.jpg','/mb02-blue-0.jpg'],
                '/u/b'=>['/ub02-black-0.jpg'],
                '/w/b'=>['/wb01-black-0.jpg','/wb02-green-0.jpg','/wb03-purple-0.jpg']
            ];
            foreach ($catalogImagesName as $path => $imagesName) {
                $catalogMediaPath = $this->fileSystem->getDirectoryRead(
                    \Magento\Framework\App\Filesystem\DirectoryList::MEDIA
                )->getAbsolutePath('catalog/product'.$path);
                if (!$this->filesystemFile->fileExists($catalogMediaPath)) {
                    $this->filesystemFile->mkdir($catalogMediaPath, 0777, true);

                    foreach ($imagesName as $img) {
                        $catalogImages = $this->reader->getModuleDir(
                            '',
                            'Webkul_Xtremo'
                        ).'/view/base/web/images/catalog/product'.$path.$img;
                        $this->filesystemFile->cp($catalogImages, $catalogMediaPath.$img);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->helper->logData('Webkul_Xtremo::Setup_InstallData :'.$e);
        }
    }
}
