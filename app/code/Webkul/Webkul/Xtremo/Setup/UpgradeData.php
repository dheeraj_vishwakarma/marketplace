<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Xtremo\Setup;

use Magento\Cms\Model\Page;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Cms\Model\PageFactory;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{
    /**
     * @var PageFactory
     */
    private $pageFactory;

     /**
      * @var \Magento\Framework\App\Config\ScopeConfigInterface
      */
    private $scopeConfig;

     /**
      * @var EavSetupFactory $eavSetupFactory
      */
    private $eavSetupFactory;

     /**
      * @var Setup\SampleData\Executor
      */
    protected $executor;

     /**
      * @var Installer
      */
    protected $installer;

    /**
     * @var \Webkul\Xtremo\Helper\Data $helper
     */
    private $helper;

    /**
     * @param PageFactory $pageFactory
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param EavSetupFactory $eavSetupFactory
     * @param Setup\SampleData\Executor $executor
     * @param Installer $installer
     * @param \Webkul\Xtremo\Helper\Data $helper
     */
    public function __construct(
        PageFactory $pageFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        EavSetupFactory $eavSetupFactory,
        Setup\SampleData\Executor $executor,
        Installer $installer,
        \Webkul\Xtremo\Helper\Data $helper
    ) {
        $this->pageFactory = $pageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->eavSetupFactory = $eavSetupFactory;
        $this->executor = $executor;
        $this->installer = $installer;
        $this->helper = $helper;
    }

    /**
     * Create page
     *
     * @return Page
     */
    private function createPage()
    {
        return $this->pageFactory->create();
    }

    /**
     * Upgrades data for a module
     *
     * @param                                         ModuleDataSetupInterface $setup
     * @param                                         ModuleContextInterface   $context
     * @return                                        void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        try {
            $setup->startSetup();
            $eavSetup = $this->eavSetupFactory->create(['setup' => $setup]);

            if (version_compare($context->getVersion(), '2.0.1', '<')) {
                $pageId = $this->scopeConfig->getValue(
                    \Magento\Cms\Helper\Page::XML_PATH_NO_ROUTE_PAGE,
                    \Magento\Store\Model\ScopeInterface::SCOPE_STORE
                );

                $data = $this->createPage()->load($pageId);
                if ($data->getPageId()) {
                    $blockClass = \Magento\Framework\View\Element\Template::class;
                    $blockTemplate = "Magento_Cms::html/no-route.phtml";
                    $content = '{{block class="'.$blockClass.'" template="'.$blockTemplate.'"}}';
                    $data->setContent($content)->setPageLayout('1column')->save();
                }
            }

            if (version_compare($context->getVersion(), '2.0.2', '<')) {
                $eavSetup->removeAttribute(\Magento\Catalog\Model\Category::ENTITY, 'is_popular_category');

                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Category::ENTITY,
                    'is_popular_category',
                    [
                        'type'                  => 'int',
                        'group'                 => 'General Information',
                        'input'                 => 'select',
                        'label'                 => __('Is Popular Category'),
                        'note'                  => __('Is Popular Category'),
                        'source'                => \Magento\Eav\Model\Entity\Attribute\Source\Boolean::class,
                        'global'                => ScopedAttributeInterface::SCOPE_GLOBAL,
                        'visible'               => true,
                        'user_defined'          => false,
                        'default'               => 0,
                        'required'              => false,
                        "searchable"            => false,
                        "filterable"            => true,
                        "comparable"            => false,
                        'visible_on_front'      => false,
                        'unique'                => false
                    ]
                );
            }
            if (version_compare($context->getVersion(), '2.0.3', '<')) {
                $scopeGlobal = ScopedAttributeInterface::SCOPE_GLOBAL;
                $eavSetup->addAttribute(
                    \Magento\Catalog\Model\Category::ENTITY,
                    'popular_category_image',
                    [
                        'type'                  => 'varchar',
                        'input'                 => 'image',
                        'label'                 => __('Popular Category Image'),
                        'source'                => '',
                        'backend'                => \Webkul\Xtremo\Model\Category\Attribute\Backend\Image::class,
                        'global'                => $scopeGlobal,
                        'visible'               => true,
                        'user_defined'          => false,
                        'default'               => null,
                        'required'              => false,
                        'sort_order'            => 333,
                    ]
                );
            }
            $this->executor->exec($this->installer);
            $setup->endSetup();
        } catch (\Exception $e) {
            $this->helper->logData('Webkul_Xtremo::Setup_UpgradeData :'.$e);
        }
    }
}
