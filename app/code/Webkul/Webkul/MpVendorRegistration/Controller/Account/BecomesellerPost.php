<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpVendorRegistration
 * @author    Webkul
 * @copyright Copyright (c)  Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpVendorRegistration\Controller\Account;

use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Api\Data\CustomerInterfaceFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\FormKey\Validator as FormKeyValidator;
use Magento\Framework\Filesystem;

/**
 * Webkul MpVendorRegistration Account BecomesellerPost Controller.
 */
class BecomesellerPost extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $date;

    /**
     * @var \Magento\Framework\Filesystem\Directory\WriteInterface
     */
    protected $mediaDirectory;

    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;

    /**
     * @var CustomerInterfaceFactory
     */
    protected $customerDataFactory;

    /**
     * File Uploader factory.
     *
     * @var \Magento\MediaStorage\Model\File\UploaderFactory
     */
    protected $fileUploaderFactory;

    /**
     * @var \Magento\Customer\Model\Customer\Mapper
     */
    protected $customerMapper;

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    protected $urlBuilder;

    protected $sellerCollection;

    protected $mpHelper;

    /**
     * @param Context          $context
     * @param Session          $customerSession
     * @param CustomerInterfaceFactory                $customerDataFactory
     * @param \Magento\Customer\Model\Customer\Mapper $customerMapper
     * @param DataObjectHelper                        $dataObjectHelper
     * @param FormKeyValidator $formKeyValidator
     * @param CustomerRepositoryInterface             $customerRepository
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        FormKeyValidator $formKeyValidator,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        CustomerInterfaceFactory $customerDataFactory,
        Filesystem $filesystem,
        CustomerRepositoryInterface $customerRepository,
        DataObjectHelper $dataObjectHelper,
        \Magento\Customer\Model\Customer\Mapper $customerMapper,
        \Magento\MediaStorage\Model\File\UploaderFactory $fileUploaderFactory,
        \Magento\Customer\Model\Url $urlBuilder,
        \Webkul\Marketplace\Model\Seller $sellerCollection,
        \Webkul\Marketplace\Helper\Data $mpHelper
    ) {
        $this->customerSession = $customerSession;
        $this->customerRepository = $customerRepository;
        $this->customerDataFactory = $customerDataFactory;
        $this->formKeyValidator = $formKeyValidator;
        $this->customerMapper = $customerMapper;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->date = $date;
        $this->mediaDirectory = $filesystem->getDirectoryWrite(
            DirectoryList::MEDIA
        );
        $this->fileUploaderFactory = $fileUploaderFactory;
        $this->urlBuilder = $urlBuilder;
        $this->sellerCollection = $sellerCollection;
        $this->mpHelper = $mpHelper;
        parent::__construct(
            $context
        );
    }

    /**
     * Retrieve customer session object.
     *
     * @return \Magento\Customer\Model\Session
     */
    protected function _getSession()
    {
        return $this->customerSession;
    }

    /**
     * Check customer authentication.
     *
     * @param RequestInterface $request
     *
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->urlBuilder->getLoginUrl();

        if (!$this->customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }

        return parent::dispatch($request);
    }

    /**
     * BecomesellerPost action.
     *
     * @return \Magento\Framework\Controller\Result\RedirectFactory
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     */
    public function execute()
    {
        /**
         * @var \Magento\Framework\Controller\Result\Redirect
         */
        $resultRedirect = $this->resultRedirectFactory->create();

        if ($this->getRequest()->isPost()) {
            try {
                if (!$this->formKeyValidator->validate($this->getRequest())) {
                    return $this->resultRedirectFactory->create()->setPath(
                        'marketplace/account/becomeseller',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }

                $paramData = $this->getRequest();
                $customerId = $this->_getSession()->getCustomerId();
                $customData = $paramData->getParams();

                $savedCustomerData = $this->customerRepository->getById($customerId);
                $customer = $this->customerDataFactory->create();
                $customData = array_merge(
                    $this->customerMapper->toFlatArray($savedCustomerData),
                    $customData
                );
                $customData['id'] = $customerId;
                if (!isset($customData['is_vendor_group'])) {
                    $customData['is_vendor_group'] = 0;
                }
                $this->dataObjectHelper->populateWithArray(
                    $customer,
                    $customData,
                    \Magento\Customer\Api\Data\CustomerInterface::class
                );
                try {
                    $this->customerRepository->save($customer);
                } catch (\Exception $e) {
                    $this->messageManager->addError($e->getMessage());
                } catch (\Magento\Framework\Exception\LocalizedException $e) {
                    $this->messageManager->addError($e->getMessage());
                }

                $fields = $this->getRequest()->getParams();

                $profileurlcount = $this->sellerCollection->getCollection()
                    ->addFieldToFilter(
                        'shop_url',
                        $fields['profileurl']
                    );
                $this->saveSellerData($profileurlcount, $fields);
            } catch (\Exception $e) {
                $this->messageManager->addError($e->getMessage());

                return $this->resultRedirectFactory->create()->setPath(
                    'marketplace/account/becomeseller',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
    /**
     * @param $profileurlcount
     * @param $feild
     * save the seller data
     */
    protected function saveSellerData($profileurlcount, $fields)
    {
        if (!count($profileurlcount)) {
            $sellerId = $this->_getSession()->getCustomerId();
            $status = $this->mpHelper->getIsPartnerApproval() ? 0 : 1;
            $model = $this->sellerCollection->getCollection()
                ->addFieldToFilter('shop_url', $fields['profileurl']);
            if (!count($model)) {
                if (isset($fields['is_seller']) && $fields['is_seller']) {
                    $autoId = 0;
                    $collection = $this->sellerCollection->getCollection()
                        ->addFieldToFilter('seller_id', $sellerId);
                    $this->getSellerAutoId($collection, $autoId);
                    $value = $this->sellerCollection->load($autoId);
                    $value->setData('is_seller', $status);
                    $value->setData('shop_url', $fields['profileurl']);
                    $value->setData('seller_id', $sellerId);
                    $value->setCreatedAt($this->date->gmtDate());
                    $value->setUpdatedAt($this->date->gmtDate());
                    $value->setAdminNotification(1);
                    $value->save();
                    $this->errorMessages($errors);
                    return $this->resultRedirectFactory->create()->setPath(
                        'marketplace/account/becomeseller',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                } else {
                    $this->messageManager->addError(
                        __('Please confirm that you want to become seller.')
                    );

                    return $this->resultRedirectFactory->create()->setPath(
                        'marketplace/account/becomeseller',
                        ['_secure' => $this->getRequest()->isSecure()]
                    );
                }
            } else {
                $this->messageManager->addError(
                    __('Shop URL already exist please set another.')
                );

                return $this->resultRedirectFactory->create()->setPath(
                    'marketplace/account/becomeseller',
                    ['_secure' => $this->getRequest()->isSecure()]
                );
            }
        } else {
            $this->messageManager->addError(
                __('Shop URL already exist please set another.')
            );

            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
    /**
     * @param Seller Collection
     * @param Reference of autoid
     * get seller autoId
     */
    public function getSellerAutoId($collection, &$autoId)
    {
        foreach ($collection as $value) {
            $autoId = $value->getId();
        }
    }
    /**
     * @param $error
     * Display Error
     */
    public function errorMessages($errors)
    {
        try {
            if (!empty($errors)) {
                foreach ($errors as $message) {
                    $this->messageManager->addError($message);
                }
            } else {
                $this->messageManager->addSuccess(
                    __('Profile information was successfully saved')
                );
            }

            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        } catch (\Exception $e) {
            $this->messageManager->addException(
                $e,
                __('We can\'t save the customer.')
            );
        } catch (\Magento\Framework\Exception\LocalizedException $e) {
            $this->messageManager->addError($e->getMessage());
        }
    }
}
