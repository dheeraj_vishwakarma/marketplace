<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpxtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Helper;

use Magento\Cms\Model\Wysiwyg\Config;

/**
 * Webkul Mpxtremo Helper Data
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{
    /**
     * @var Webkul\Marketplace\Helper\Data
     */
    private $mpHelper;

    /**
     * @var \Webkul\Marketplace\Block\Profile
     */
    private $profile;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $xtremoHelper;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory
     */
    private $feedbackCollectionFactory;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory
     */
    private $mpProductCollection;

    /**
     * @var \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory
     */
    private $salesListCollection;

    /**
     * @var \Magento\Customer\Api\CustomerRepositoryInterface
     */
    private $customerRepository;

    /**
     * @var \Magento\Framework\Pricing\Helper\Data
     */
    private $pricingHelper;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\Xtremo\Helper\Data $xtremoHelper
     * @param \Webkul\Marketplace\Block\Profile $profile
     * @param \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory $feedbackCollectionFactory
     * @param \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollection
     * @param \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $salesListCollection
     * @param \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
     * @param \Magento\Framework\Pricing\Helper\Data $pricingHelper
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\Xtremo\Helper\Data $xtremoHelper,
        \Webkul\Marketplace\Block\Profile $profile,
        \Webkul\Marketplace\Model\ResourceModel\Feedback\CollectionFactory $feedbackCollectionFactory,
        \Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory $mpProductCollection,
        \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $salesListCollection,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Pricing\Helper\Data $pricingHelper
    ) {
        parent::__construct($context);
        $this->mpHelper = $mpHelper;
        $this->xtremoHelper = $xtremoHelper;
        $this->profile = $profile;
        $this->feedbackCollectionFactory = $feedbackCollectionFactory;
        $this->mpProductCollection = $mpProductCollection;
        $this->salesListCollection = $salesListCollection;
        $this->customerRepository = $customerRepository;
        $this->pricingHelper = $pricingHelper;
    }

    /**
     * Check whether Wysiwyg is enabled or not
     *
     * @return bool
     */
    public function isWysiwygEnabled()
    {
        $wysiwygState = $this->scopeConfig->getValue(
            Config::WYSIWYG_STATUS_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->mpHelper->getCurrentStoreId()
        );

        if ($wysiwygState == Config::WYSIWYG_DISABLED) {
            return false;
        }

        return true;
    }

    /**
     * getSellerRatingHtml
     *
     * @param int $sellerId
     * @return string|boolean
     */
    public function getSellerRatingHtml($sellerId)
    {
        $collection = $this->feedbackCollectionFactory->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            )->addFieldToFilter(
                'status',
                ['neq' => 0]
            );
        if ($collection->getSize()) {
            $ratings = $this->mpHelper->getSelleRating($sellerId);
            $ratingdata = $this->mpHelper->getFeedTotal($sellerId);
            $totalFeed = '';
            if (is_array($ratingdata) && isset($ratingdata['feedcount'])) {
                if ($ratingdata['feedcount'] == 1) {
                    $totalFeed = __("%1 Review", $ratingdata['feedcount']);
                } else {
                    $totalFeed = __("%1 Reviews", $ratingdata['feedcount']);
                }
            }

            return $html = '<div class="rating-summary">
                        <div class="rating-result" title="'.(($ratings*100)/5).'%">
                            <span style="width:'.(($ratings*100)/5).'%"></span>
                        </div>'.
                        '<p class="rating-links">'
                            .$totalFeed.
                        '</p>
                    </div>';
        } else {
            return false;
        }
    }

    /**
     * getFeedbackCollection
     *
     * @return object
     */
    public function getFeedbackCollection()
    {
        $partner = $this->getProfileDetail();
        $collection = [];
        if (!empty($partner)) {
            $collection = $this->feedbackCollectionFactory->create()
                ->addFieldToFilter(
                    'status',
                    ['neq' => 0]
                )
                ->addFieldToFilter(
                    'seller_id',
                    ['eq' => $partner->getSellerId()]
                )
                ->setOrder('entity_id', 'DESC');
        }
        return $collection;
    }

    public function getProfileDetail()
    {
        return $this->profile->getProfileDetail();
    }

    /**
     * addStockFilter
     *
     * @param object $collection
     * @return object
     */
    public function addStockFilter($collection)
    {
        $catalogInventoryStockItem = $this->mpProductCollection->create()
            ->getTable('cataloginventory_stock_item');

        $collection->getSelect()->join(
            ['ccp' => $catalogInventoryStockItem],
            'ccp.product_id = e.entity_id',
            ['qty' => 'qty']
        );

        return $collection;
    }

    /**
     * getSellerTodayIncome
     *
     * @param int $sellerId
     * @return float
     */
    public function getSellerTodayIncome($sellerId)
    {
        $temp = 0;
        $curryear = date('Y');
        $currMonth = date('m');
        $currDays = date('d');
        $currTime = date('G');

        $fromDate = $curryear.'-'.$currMonth.'-'.$currDays.' 00:00:00';
        $toDate = $curryear.'-'.$currMonth.'-'.$currDays.' 23:59:59';

        return $price = $this->getSellerIncome($sellerId, $fromDate, $toDate);
    }

    /**
     * getSellerWeeklyIncome
     *
     * @param int $sellerId
     * @return float
     */
    public function getSellerWeeklyIncome($sellerId)
    {
        $temp = 0;
        $curryear = date('Y');
        $currMonth = date('m');
        $currDays = date('d');
        $currWeekDay = date('N');
        $currentDayOfMonth = date('j');
        $currWeekStartDay = $currDays - $currWeekDay;
        $currWeekEndDay = $currWeekStartDay + 7;
        $currWeekStartDay++;

        $currWeekStartMonth = $currMonth -1;
        if ($currWeekStartMonth < 1) {
            $currWeekStartMonth = '12';
            $currWeekStartyear = $curryear - 1;
        } else {
            $currWeekStartyear = $curryear;
        }
        if ($currWeekStartDay < 1) {
            $previousMonthDays = cal_days_in_month(CAL_GREGORIAN, $currWeekStartMonth, $currWeekStartyear);
            $currWeekStartDay = $previousMonthDays + $currWeekStartDay;
        }

        if ($currWeekEndDay > $currentDayOfMonth) {
            $currWeekEndDay = $currentDayOfMonth;
        }

        $fromDate = $currWeekStartyear.'-'.$currWeekStartMonth.'-'.$currWeekStartDay.' 00:00:00';
        $toDate = $curryear.'-'.$currMonth.'-'.$currWeekEndDay.' 23:59:59';

        return $price = $this->getSellerIncome($sellerId, $fromDate, $toDate);
    }
    
    /**
     * getSellerMonthlyIncome
     *
     * @param int $sellerId
     * @return float
     */
    public function getSellerMonthlyIncome($sellerId)
    {
        $temp = 0;
        $curryear = date('Y');
        $currMonth = date('m');
        $currDays = date('d');

        $fromDate = $curryear.'-'.$currMonth.'-1 00:00:00';
        $toDate = $curryear.'-'.$currMonth.'-'.$currDays.' 23:59:59';

        return $price = $this->getSellerIncome($sellerId, $fromDate, $toDate);
    }

    /**
     * getSellerIncome
     *
     * @param int $sellerId
     * @param date $fromDate
     * @param date $toDate
     * @return float
     */
    public function getSellerIncome($sellerId, $fromDate, $toDate)
    {
        $price = 0;
        $collection = $this->salesListCollection->create()
            ->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            )
            ->addFieldToFilter(
                'order_id',
                ['neq' => 0]
            )->addFieldToFilter(
                'created_at',
                ['datetime' => true, 'from' => $fromDate, 'to' => $toDate]
            );

        foreach ($collection as $record) {
            $price += $record->getActualSellerAmount();
        }
        return $price;
    }

    /**
     * getTotalQtyOfSellerProduct
     *
     * @param int $sellerId
     * @param int $orderId
     * @return int
     */
    public function getTotalQtyOfSellerProduct($sellerId, $orderId)
    {
        $collection = $this->salesListCollection->create()
            ->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            )
            ->addFieldToFilter(
                'order_id',
                ['eq' => $orderId]
            )->addFieldToSelect('magequantity');

        $qtyArray = $collection->getColumnValues('magequantity');
        return array_sum($qtyArray);
    }

    /**
     * getTopCustomers
     *
     * @param int $sellerId
     * @return array
     */
    public function getTopCustomers($sellerId)
    {
        $collection = $this->salesListCollection->create()
            ->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            )->addFieldToFilter(
                'magebuyer_id',
                ['neq' => 0]
            );

        $buyerIds = $collection->addFieldToSelect('magebuyer_id')
            ->getColumnValues('magebuyer_id');

        $topBuyers = array_count_values($buyerIds);

        return $topBuyers;
    }

    /**
     * getCustomerData
     *
     * @param int $customerId
     * @return \Magento\Customer\Model\Customer|boolean
     */
    public function getCustomerData($customerId)
    {
        try {
            return $data = $this->customerRepository->getById($customerId);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * getTotalAmountEarned
     *
     * @param int $sellerId
     * @param int $buyerId
     * @return int|float
     */
    public function getTotalAmountEarned($sellerId, $buyerId)
    {
        $collection = $this->salesListCollection->create()
            ->addFieldToFilter(
                'seller_id',
                ['eq' => $sellerId]
            )->addFieldToFilter(
                'magebuyer_id',
                ['eq' => $buyerId]
            )->addFieldToSelect('actual_seller_amount');

        return array_sum($collection->getColumnValues('actual_seller_amount'));
    }

    /**
     * getTotalExpenses
     *
     * @param int $sellerId
     * @return array
     */
    public function getTotalExpenses($sellerId)
    {
        $data = [];
        $curryear = date('Y');
        $currMonth = date('m');
        $monthsArr = [
            '',
            __('January'),
            __('February'),
            __('March'),
            __('April'),
            __('May'),
            __('June'),
            __('July'),
            __('August'),
            __('September'),
            __('October'),
            __('November'),
            __('December'),
        ];
        for ($i = 1; $i <= $currMonth; ++$i) {
            $date1 = $curryear.'-'.$i.'-01 00:00:00';
            $date2 = $curryear.'-'.$i.'-31 23:59:59';
            $collection = $this->salesListCollection->create()
                ->addFieldToFilter(
                    'seller_id',
                    ['eq' => $sellerId]
                )->addFieldToFilter(
                    'order_id',
                    ['neq' => 0]
                )->addFieldToFilter(
                    'created_at',
                    ['datetime' => true, 'from' => $date1, 'to' => $date2]
                );
            $temp = 0;
            foreach ($collection as $record) {
                $temp += $record->getTotalCommission();
            }
            $data[$i] = $temp;
        }
        return $data;
    }

    /**
     * getSellerId
     *
     * @return int
     */
    public function getSellerId()
    {
        return $this->mpHelper->getCustomerId();
    }

    /**
     * formatPrice
     *
     * @param float $price
     * @return mixed
     */
    public function formatPrice($price)
    {
        return $this->pricingHelper->currency($price, true, false);
    }

    /**
     * getSellerPublicPageConfig
     *
     * @return array
     */
    public function getSellerPublicPageConfig()
    {
        return $this->scopeConfig->getValue(
            'xtremo/seller_page/public',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * getSellerCategoryTreeSettings
     *
     * @return boolean
     */
    public function getSellerCategoryTreeSettings()
    {
        try {
            if (method_exists($this->mpHelper, "getIsAdminViewCategoryTree")) {
                return $this->mpHelper->getIsAdminViewCategoryTree();
            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}
