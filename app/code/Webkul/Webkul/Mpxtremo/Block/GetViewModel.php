<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpxtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Block;

/**
 * Mpxtremo GetViewModel Block
 */
class GetViewModel extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\Mpxtremo\ViewModel\Mpxtremo
     */
    protected $mpxtremoViewModel;

    /**
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\Mpxtremo\ViewModel\Mpxtremo $mpxtremoViewModel
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\Mpxtremo\ViewModel\Mpxtremo $mpxtremoViewModel,
        array $data = []
    ) {
        $this->mpxtremoViewModel = $mpxtremoViewModel;
        parent::__construct($context, $data);
    }

    /**
     * Get Mpxtremo View Model
     *
     * @return object \Webkul\Mpxtremo\ViewModel\Mpxtremo
     */
    public function getMpxtremoViewModel()
    {
        return $this->mpxtremoViewModel;
    }
}
