<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpxtremo
 * @author    Webkul Software Private Limited
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\Mpxtremo\Model\Plugin\Account;

class LocationChart
{
    /**
     * Seller statistics graph width.
     *
     * @var string
     */
    private $width = '800';

    /**
     * Seller statistics graph height.
     *
     * @var string
     */
    private $height = '320';

    /**
     * @var \Webkul\Marketplace\Helper\Dashboard\Data
     */
    private $helper;

    /**
     * @var \Webkul\Xtremo\Helper\Data
     */
    private $xtremoHelper;

    /**
     * @param \Webkul\Marketplace\Helper\Dashboard\Data $helper
     */
    public function __construct(
        \Webkul\Marketplace\Helper\Dashboard\Data $helper,
        \Webkul\Xtremo\Helper\Data $xtremoHelper
    ) {
        $this->helper       = $helper;
        $this->xtremoHelper = $xtremoHelper;
    }

    public function afterGetSellerStatisticsGraphUrl(
        \Webkul\Marketplace\Block\Account\Dashboard\LocationChart $subject,
        $result
    ) {
        $params = [
            'cht' => 'map:fixed=-60,-180,85,180',
            'chma' => '0,110,0,0',
            'chf' => 'bg,s,1179F9',
            'chdls' => '000000,14',
        ];
        $getSale = $subject->getSale(true);
        $totalContrySale = $getSale['country_sale_arr'];
        $countryOrderCountArr = $getSale['country_order_count_arr'];
        $countryRegionArr = $getSale['country_region_arr'];

        $chmArr = [];
        $chcoArr = [];
        $chdlArr = [];
        $i = 0;

        array_push($chcoArr, '50A0F8');
        foreach ($countryRegionArr as $key => $value) {
            foreach ($value as $key2 => $value2) {
                $count = $countryOrderCountArr[$key][$key2];
                $amount = $totalContrySale[$key][$key2];
                $chmVal = 'f'.$value2.':Orders-'.$count.' Sales-'.$amount.',000000,0,'.$i.',10';
                array_push($chmArr, $chmVal);
                array_push($chdlArr, $value2);
                $randomColor = $subject->randString();
                while ($randomColor == "50A0F8" || $randomColor == "1179F9") {
                    $randomColor = $subject->randString();
                }
                array_push($chcoArr, $randomColor);
                $i++;
            }
        }
        $params['chm'] = implode('|', $chmArr);
        $params['chld'] = implode('|', $chdlArr);
        $params['chdl'] = implode('|', $chdlArr);
        $params['chco'] = implode('|', $chcoArr);

        // seller statistics graph size
        $params['chs'] = $this->width.'x'.$this->height;
        // return the encoded graph image url

        $getParamData = urlencode(base64_encode($this->xtremoHelper->jsonEncodeData($params)));
        $getEncryptedHashData = $this->helper->getChartEncryptedHashData($getParamData);
        $params = [
            'param_data' => $getParamData,
            'encrypted_data' => $getEncryptedHashData,
        ];

        return $subject->getUrl(
            '*/*/dashboard_tunnel',
            ['_query' => $params, '_secure' => $subject->getRequest()->isSecure()]
        );
    }
}
