<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpAdvancedCommission
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpAdvancedCommission\Ui\Component\Listing\Column;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory;

class PurchasedPrice extends Column
{
    /**

     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param \Webkul\Marketplace\Helper\Data $helper
     * @param CollectionFactory $orderCollection
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Weee\Helper\Data $weeData
     * @param \Magento\Directory\Model\CurrencyFactory $currencyFactory
     * @param \Magento\Framework\Pricing\Helper\Data $priceHelper
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        \Webkul\MpAdvancedCommission\Helper\Data $helper,
        CollectionFactory $orderCollection,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Directory\Model\CurrencyFactory $currencyFactory,
        \Magento\Framework\Pricing\Helper\Data $priceHelper,
        \Magento\Framework\Pricing\PriceCurrencyInterface $currencyInterface,
        array $components = [],
        array $data = []
    ) {
        $this->helper = $helper;
        $this->orderCollection = $orderCollection;
        $this->orderRepository = $orderRepository;
        $this->currencyFactory = $currencyFactory;
        $this->_priceHelper = $priceHelper;
        $this->currencyInterface = $currencyInterface;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            $orderCommission = $this->helper->getOrderCommissionRate();
            foreach ($dataSource['data']['items'] as &$item) {
                
                if ($item['purchased_actual_seller_amount']>0) {
                    $price = $item['purchased_actual_seller_amount'] - $orderCommission;
                    
                    $item[$fieldName] = $this->currencyInterface->format(
                        $price,
                        false,
                        2,
                        null,
                        $item['order_currency_code']
                    );
                   
                } else {
                    $price = $item['purchased_actual_seller_amount'] ;
                    $item[$fieldName] = $this->currencyInterface->format(
                        $price,
                        false,
                        2,
                        null,
                        $item['order_currency_code']
                    );
                }
            }
        }

        return $dataSource;
    }
}
