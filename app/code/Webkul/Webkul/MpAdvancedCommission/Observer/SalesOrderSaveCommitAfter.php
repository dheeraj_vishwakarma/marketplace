<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpAdvancedCommission\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\Marketplace\Model\OrdersFactory;
use Webkul\Marketplace\Model\SaleperpartnerFactory;
use Webkul\Marketplace\Model\SaleslistFactory;
use Webkul\Marketplace\Model\FeedbackcountFactory;

/**
 * Webkul Marketplace SalesOrderSaveCommitAfterObserver Observer Model.
 */
class SalesOrderSaveCommitAfter implements ObserverInterface
{
    /**
     * @var eventManager
     */
    protected $_eventManager;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var MarketplaceHelper
     */
    protected $marketplaceHelper;

    /**
     * @var OrdersFactory
     */
    protected $ordersFactory;

    /**
     * @var SaleperpartnerFactory
     */
    protected $saleperpartnerFactory;

    /**
     * @var SaleslistFactory
     */
    protected $saleslistFactory;

    /**
     * @var FeedbackcountFactory
     */
    protected $feedbackcountFactory;

    /**
     * @param \Magento\Framework\Event\Manager            $eventManager
     * @param \Magento\Customer\Model\Session             $customerSession
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param MarketplaceHelper                           $marketplaceHelper
     * @param OrdersFactory                               $ordersFactory
     * @param SaleperpartnerFactory                       $saleperpartnerFactory
     * @param SaleslistFactory                            $saleslistFactory
     * @param FeedbackcountFactory                        $feedbackcountFactory
     */
    public function __construct(
        \Magento\Framework\Event\Manager $eventManager,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        MarketplaceHelper $marketplaceHelper,
        OrdersFactory $ordersFactory,
        SaleperpartnerFactory $saleperpartnerFactory,
        SaleslistFactory $saleslistFactory,
        FeedbackcountFactory $feedbackcountFactory,
        \Webkul\MpAdvancedCommission\Helper\Data $helper
    ) {
        $this->_eventManager = $eventManager;
        $this->_customerSession = $customerSession;
        $this->_date = $date;
        $this->marketplaceHelper = $marketplaceHelper;
        $this->ordersFactory = $ordersFactory;
        $this->saleperpartnerFactory = $saleperpartnerFactory;
        $this->saleslistFactory = $saleslistFactory;
        $this->feedbackcountFactory = $feedbackcountFactory;
        $this->helper = $helper;
    }

    /**
     * Sales order save commmit after on order complete state event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $orderInstance Order */
        $writer = new \Zend\Log\Writer\Stream(BP . '/var/log/observe.log');
        $logger = new \Zend\Log\Logger();
        $logger->addWriter($writer);
        $logger->info('Your text message');

        $order = $observer->getOrder();
        $lastOrderId = $observer->getOrder()->getId();
        $logger->info($lastOrderId);
        $helper = $this->marketplaceHelper;
        if ($order->getState() == 'complete') {
            $logger->info('if');
            $ordercollection = $this->saleslistFactory->create()
            ->getCollection()
            ->addFieldToFilter('order_id', $lastOrderId)
            ->addFieldToFilter(
                'cpprostatus',
                \Webkul\Marketplace\Model\Saleslist::PAID_STATUS_COMPLETE
            );
            $logger->info('dfghj');
            foreach ($ordercollection as $item) {
                $sellerId = $item->getSellerId();
                $logger->info($sellerId);
                $orderCommission = $this->helper->getOrderCommissionRate();
                $logger->info($orderCommission);
                $collectionverifyread = $this->saleperpartnerFactory->create()
                ->getCollection();
                $collectionverifyread->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
                $logger->info('fghj');
                if ($collectionverifyread->getSize() >= 1) {
                    $logger->info('if');
                    foreach ($collectionverifyread as $verifyrow) {
                        $id = $verifyrow->getId();
                        $amountRemaining = $verifyrow->getAmountRemain() - $orderCommission;
                        $logger->info($amountRemaining);
                        $verifyrow->setEntityId($verifyrow->getEntityId());
                        $verifyrow->setAmountRemain($amountRemaining);
                        $verifyrow->setOrderCommissionRate($orderCommission);
                        $verifyrow->save();
                    }
                   
                } else {
                    $logger->info('else');
                    $collectionf = $this->saleperpartnerFactory->create();
                    $amountRemaining = $collectionf->getAmountRemain() - $orderCommission;
                    $logger->info($amountRemaining);
                    $collectionf->setEntityId($collectionf->getEntityId());
                    $collectionf->setAmountRemain($amountRemaining);
                    $collectionf->setOrderCommissionRate($orderCommission);
                    $collectionf->save();
                    $logger->info('sdfgh');
                }
                
            }
        }
    }
}
