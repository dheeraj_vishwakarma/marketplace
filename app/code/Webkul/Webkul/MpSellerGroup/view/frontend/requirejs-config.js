/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
 
var config = {
    map: {
        '*': {
            recurringProfile: 'Webkul_MpSellerGroup/js/recurringProfile',
        }
    }
};
