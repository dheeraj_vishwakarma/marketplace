<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Cron;

use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory as SellerCollection;
use Webkul\Marketplace\Model\ResourceModel\Product\Collection as SellerProductCollection;
use Magento\Catalog\Model\Product\Action as ProductAction;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\Indexer\Product\Price\Processor as IndexerProductPriceProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelper;
use Magento\Framework\Registry;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Webkul\MpSellerGroup\Helper\Email;

class ProductActionOnGroupExpire
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var CollectionFactory
     */
    protected $_sellerGroupTypeCollection;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @var SellerCollection
     */
    protected $_sellerCollection;

    /**
     * @var SellerProductCollection
     */
    protected $_sellerProductCollection;

    /**
     * @var ProductAction
     */
    protected $_productAction;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * Store manager.
     *
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var IndexerProductPriceProcessor
     */
    protected $_productPriceIndexerProcessor;

    /**
     * @var SellerGroupHelper
     */
    protected $_sellerGroupHelper;

    /**
     * Core registry.
     *
     * @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var CustomerRepositoryInterface
     */
    
    protected $_customerRepository;

    /**
     * @var Email
     */
    protected $_email;

    /**
     * @param SellerGroupRepositoryInterface        $sellerGroupRepository
     * @param SellerGroupTypeRepositoryInterface    $sellerGroupTypeRepository
     * @param CollectionFactory                     $sellerGroupTypeCollection
     * @param DateTime                              $date
     * @param SellerCollection                      $sellerCollection
     * @param SellerProductCollection               $sellerProductCollection
     * @param ProductAction                         $productAction
     * @param ProductCollectionFactory              $productCollectionFactory
     * @param StoreManagerInterface                 $storeManager
     * @param IndexerProductPriceProcessor          $productPriceIndexerProcessor
     * @param SellerGroupHelper                     $sellerGroupHelper
     * @param Registry                              $coreRegistry
     * @param MarketplaceHelper                     $marketplaceHelper
     * @param CustomerRepositoryInterface           $customerRepository
     * @param Email                                 $email
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        CollectionFactory $sellerGroupTypeCollection,
        DateTime $date,
        SellerCollection $sellerCollection,
        SellerProductCollection $sellerProductCollection,
        ProductAction $productAction,
        ProductCollectionFactory $productCollectionFactory,
        StoreManagerInterface $storeManager,
        IndexerProductPriceProcessor $productPriceIndexerProcessor,
        SellerGroupHelper $sellerGroupHelper,
        Registry $coreRegistry,
        MarketplaceHelper $marketplaceHelper,
        CustomerRepositoryInterface $customerRepository,
        Email $email
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_sellerGroupTypeCollection = $sellerGroupTypeCollection;
        $this->_date = $date;
        $this->_sellerCollection = $sellerCollection;
        $this->_sellerProductCollection = $sellerProductCollection;
        $this->_productAction = $productAction;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->_productPriceIndexerProcessor = $productPriceIndexerProcessor;
        $this->_sellerGroupHelper = $sellerGroupHelper;
        $this->_coreRegistry = $coreRegistry;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_customerRepository = $customerRepository;
        $this->_email = $email;
    }

    /**
     * Change Product Status on group expire
     *
     * @return void
     */
    public function execute()
    {
        $allStores = $this->_storeManager->getStores();
        $disableStatus = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_DISABLED;
        $enableStatus = \Magento\Catalog\Model\Product\Attribute\Source\Status::STATUS_ENABLED;

        $sellerGroupTypeData = $this->_sellerGroupTypeCollection->create()
            ->addFieldToFilter('expired_on', ['lt'=>$this->_date->gmtDate()])
            ->addFieldToFilter('expired_status', 0);
            
        $this->productReindexing($sellerGroupTypeData, $allStores, $disableStatus, $enableStatus);
        
        $sellerGroupTypeData = $this->_sellerGroupTypeCollection->create()
            ->addFieldToFilter('expired_on', ['gt'=>$this->_date->gmtDate()])
            ->addFieldToFilter('expired_status', 0);
        foreach ($sellerGroupTypeData as $sellerGroupType) {
            $sellerId = $sellerGroupType->getSellerId();
            if ($sellerGroupType->getCheckType()!=0) {
                $seller = $this->_sellerCollection->create()
                        ->addFieldToFilter('seller_id', $sellerGroupType->getSellerId())
                        ->getFirstItem();
                if ($seller->getId()) {
                    $this->saveSellerInfo($seller, 1);
                    /*Seller product disabled code*/
                    $sellersProducts = $this->_sellerProductCollection
                        ->addFieldToFilter('seller_id', $seller->getSellerId())
                        ->addFieldToFilter('status', 2);
                    $coditionArr = [];
                    $productIds = [];
                    foreach ($sellersProducts as $key => $sellersProduct) {
                        $id = $sellersProduct['mageproduct_id'];
                        $condition = "`mageproduct_id`=".$id;
                        array_push($coditionArr, $condition);
                        array_push($productIds, $id);
                    }
                    if (!empty($productIds)) {
                        $coditionData = implode(' OR ', $coditionArr);

                        $this->_sellerProductCollection->setProductData(
                            $coditionData,
                            ['status' => $enableStatus]
                        );

                        /*Catalog product disabled code*/
                        foreach ($allStores as $eachStoreId => $storeId) {
                            $this->_productAction->updateAttributes(
                                $productIds,
                                ['status' => $enableStatus],
                                $storeId
                            );
                        }
                        $this->_productAction->updateAttributes(
                            $productIds,
                            ['status' => $enableStatus],
                            0
                        );

                        $this->_productPriceIndexerProcessor->reindexList($productIds);
                    }
                }
            }

            /*Send Notify mail to seller*/

            $isNotify = $this->isNotifyMethod(
                $sellerGroupType->getCheckType(),
                $sellerGroupType['transaction_date'],
                $sellerGroupType['expired_on']
            );

            if ($isNotify) {
                $customerInfo = $this->_customerRepository->getById($sellerId);

                $adminStoremail = $this->_marketplaceHelper->getAdminEmailId();
                $defaultTransEmailId = $this->_marketplaceHelper->getDefaultTransEmailId();
                $adminEmail = $adminStoremail ? $adminStoremail : $defaultTransEmailId;
                $adminUsername = 'Admin';

                $senderInfo = [];
                $receiverInfo = [];

                $receiverInfo = [
                    'name' => $customerInfo->getFirstName(),
                    'email' => $customerInfo->getEmail(),
                ];
                $senderInfo = [
                    'name' => $adminUsername,
                    'email' => $adminEmail,
                ];
                $emailTemplateVariables = [];
                $emailTempVariables['seller_name'] = $customerInfo->getFirstName();
                $emailTempVariables['group_name'] = $sellerGroupType['group_name'];
                $emailTempVariables['fee_amount'] = $sellerGroupType['fee_amount'];
                $emailTempVariables['no_of_products'] = $sellerGroupType['no_of_products'];
                $emailTempVariables['time_periods'] = $sellerGroupType['time_periods']." days";
                if ($sellerGroupType['check_type'] == 1) {
                    $emailTempVariables['no_of_products'] = __('Unlimited');
                }
                $this->_email->sendGroupNotifyEmail(
                    $emailTempVariables,
                    $senderInfo,
                    $receiverInfo
                );
            }
        }
    }

    public function getGroupDataByGroupId($groupId)
    {
        return $this->_sellerGroupRepository->getById($groupId);
    }

    public function getSellerGroupData($sellerId)
    {
        $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($sellerId);
        if ($getSellerGroup->getSize()) {
            return $getSellerGroup;
        } else {
            return [];
        }
    }

    /**
     * isNotifyMethod Method
     *
     * @return bool
     */
    public function isNotifyMethod($checkType, $createdAt, $expireOn)
    {
        $expire = false;
        $today = $this->_date->gmtDate();
        if ($checkType != 0) {
            $notifyTime = $this->_sellerGroupHelper->getNotifyTime();
            $date = strtotime("-".$notifyTime." days", strtotime($expireOn));
            $notifyExpireDate = date("Y-m-d H:i:s", $date);
            if (strtotime($notifyExpireDate) <= strtotime($today)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Save Seller Info
     */
    public function saveSellerInfo($seller, $val)
    {
        $seller->setIsSeller($val)->save();
    }

    /**
     * Seller Product Delete
     */
    public function sellerProductDelete($sellersProduct)
    {
        $sellersProduct->delete();
    }

    /**
     * Mage Product Delete
     */
    public function mageProductDelete($mageProduct)
    {
        $mageProduct->delete();
    }

    /**
     * Save Seller Group Status
     */
    public function saveSellerGroupStatus($sellerGroupType, $val)
    {
        $sellerGroupType->setExpiredStatus($val)->save();
    }

    /**
     * Product Reindexing
     *
     * @return  void
     */
    
    public function productReindexing($sellerGroupTypeData, $allStores, $disableStatus, $enableStatus)
    {
        foreach ($sellerGroupTypeData as $sellerGroupType) {
            if ($sellerGroupType->getCheckType()!=0) {
                $sellerId = $sellerGroupType->getSellerId();
                $this->saveSellerGroupStatus($sellerGroupType, 1);
                $this->_sellerGroupHelper->expireSellerBoosterTypes($sellerId);
                $seller = $this->_sellerCollection->create()
                        ->addFieldToFilter('seller_id', $sellerId)
                        ->getFirstItem();
                $this->proReindexing($sellerGroupTypeData, $allStores, $disableStatus, $enableStatus);
                /*Send Expire mail to seller*/
                $customerInfo = $this->_customerRepository->getById($sellerId);

                $adminStoremail = $this->_marketplaceHelper->getAdminEmailId();
                $defaultTransEmailId = $this->_marketplaceHelper->getDefaultTransEmailId();
                $adminEmail = $adminStoremail ? $adminStoremail : $defaultTransEmailId;
                $adminUsername = 'Admin';

                $senderInfo = [];
                $receiverInfo = [];

                $receiverInfo = [
                    'name' => $customerInfo->getFirstName(),
                    'email' => $customerInfo->getEmail(),
                ];
                $senderInfo = [
                    'name' => $adminUsername,
                    'email' => $adminEmail,
                ];
                $groupName = $this->getGroupDataByGroupId(
                    $sellerGroupType['group_id']
                )->getGroupName();
                $emailTemplateVariables = [];
                $emailTempVariables['seller_name'] = $customerInfo->getFirstName();
                $emailTempVariables['group_name'] = $groupName;
                $emailTempVariables['fee_amount'] = $sellerGroupType['fee_amount'];
                $emailTempVariables['no_of_products'] = $sellerGroupType['no_of_products'];
                $emailTempVariables['time_periods'] = $sellerGroupType['time_periods']." days";
                if ($sellerGroupType['check_type'] == 1) {
                    $emailTempVariables['no_of_products'] = __('Unlimited');
                }
                $this->_email->sendGroupExpireEmail(
                    $emailTempVariables,
                    $senderInfo,
                    $receiverInfo
                );
            }
        }
    }

    /**
     * Seller Product Delete
     *
     * @return  null
     */

    public function productDelete($productIds, $sellersProducts)
    {
        foreach ($sellersProducts as $key => $sellersProduct) {
            $id = $sellersProduct['mageproduct_id'];
            array_push($productIds, $id);
            $this->sellerProductDelete($sellersProduct);
        }
    }

    /**
     * Product Reindexing
     */
    public function proReindexing($sellerGroupTypeData, $allStores, $disableStatus, $enableStatus)
    {
        if ($seller->getId()) {
            $this->saveSellerInfo($seller, 0);
            if ($this->_sellerGroupHelper->getProductStatusAfterMembershipExpire() == 2) {
                /*Seller product delete code*/
                $this->_coreRegistry->register('isSecureArea', 1);

                $sellersProducts = $this->_sellerProductCollection
                    ->addFieldToFilter('seller_id', $seller->getSellerId());
                $productIds = [];
                
                $this->productDelete($productIds, $sellersProducts);

                $mageProducts = $this->_productCollectionFactory->create()
                ->addFieldToFilter(
                    'entity_id',
                    ['in' => $productIds]
                );
                foreach ($mageProducts as $mageProduct) {
                    $this->mageProductDelete($mageProduct);
                }

                $this->_coreRegistry->unregister('isSecureArea');
            } else {
                /*Seller product disabled code*/
                $sellersProducts = $this->_sellerProductCollection
                    ->addFieldToFilter('seller_id', $seller->getSellerId());
                $coditionArr = [];
                $productIds = [];
                foreach ($sellersProducts as $key => $sellersProduct) {
                    $id = $sellersProduct['mageproduct_id'];
                    $condition = "`mageproduct_id`=".$id;
                    array_push($coditionArr, $condition);
                    array_push($productIds, $id);
                }
                if (!empty($productIds)) {
                    $coditionData = implode(' OR ', $coditionArr);

                    $this->_sellerProductCollection->setProductData(
                        $coditionData,
                        ['status' => $disableStatus]
                    );

                    /*Catalog product disabled code*/
                    foreach ($allStores as $eachStoreId => $storeId) {
                        $this->_productAction->updateAttributes(
                            $productIds,
                            ['status' => $disableStatus],
                            $storeId
                        );
                    }
                    $this->_productAction->updateAttributes(
                        $productIds,
                        ['status' => $disableStatus],
                        0
                    );

                    $this->_productPriceIndexerProcessor->reindexList($productIds);
                }
            }
        }
    }
}
