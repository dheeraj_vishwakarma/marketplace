<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Group;

use Webkul\MpSellerGroup\Api\Data\SellerGroupInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Controller\ResultFactory;

class Delete extends \Webkul\MpSellerGroup\Controller\Adminhtml\Group
{
    /**
     * seller group edit action
     *
     * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
     */
    public function execute()
    {
        $sellerGroupId = (int)$this->getRequest()->getParam('id');
        if ($sellerGroupId) {
            $sellerGroup = $this->_sellerGroupRepository->getById(
                $sellerGroupId
            );
            if ($sellerGroup->getEntityId()) {
                $groupName = $sellerGroup->getGroupName();
                $this->helper->deleteProductById($sellerGroup->getProductId());
                $sellerGroup->delete();
                $this->messageManager->addSuccess(
                    __(
                        'Group %1 has been deleted successfully.',
                        $groupName
                    )
                );
            }
        }
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('mpsellergroup/group/index/');
    }
}
