<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Group;

class Validate extends \Webkul\MpSellerGroup\Controller\Adminhtml\Group
{
    /**
     * Seller Group validation
     *
     * @param \Magento\Framework\DataObject $response
     * @return SellerGroupInterface|null
     *
     * @throws \Magento\Framework\Validator\Exception|\Exception
     */
    protected function _validateGroup($response)
    {
        $sellergroup = null;
        $errors = [];

        try {
            /** @var SellerGroupInterface $sellergroup */
            $sellergroup = $this->sellerGroupDataFactory->create();

            $data = $this->getRequest()->getParams();

            $dataResult = $data['mpsellergroup_group'];
            $errors = [];
            $getGroupByCode = $this->_sellerGroupRepository->getGroup($dataResult['group_code']);
            $existingGroupId = '';
            if ($getGroupByCode->getId()) {
                $existingGroupId = $getGroupByCode->getId();
            }
            $entityId = '';
            if (isset($dataResult['entity_id'])) {
                $entityId = $dataResult['entity_id'];
            }
            if ($existingGroupId) {
                if ($existingGroupId != $entityId) {
                    $errors[] =  __('Group with %1 group code already exist.', $dataResult['group_code']);
                }
            }
            $this->chkvalidation($dataResult);
        } catch (\Magento\Framework\Validator\Exception $exception) {
            $exceptionMsg = $exception->getMessages(
                \Magento\Framework\Message\MessageInterface::TYPE_ERROR
            );
            /**
             * @var $error Error
             */
            foreach ($exceptionMsg as $error) {
                $errors[] = $error->getText();
            }
        }

        if ($errors) {
            $messages = $response->hasMessages() ? $response->getMessages() : [];
            foreach ($errors as $error) {
                $messages[] = $error;
            }
            $response->setMessages($messages);
            $response->setError(1);
        }

        return $sellergroup;
    }
    
    /**
     * Check validation
     */

    private function chkvalidation($dataResult)
    {
        if (!isset($dataResult['group_image'][0]['name'])) {
            $errors[] =  __('Please upload group image.');
        }
        if (!isset($dataResult['group_code'])) {
            $errors[] =  __('Group code field can not be blank.');
        }
    }

    /**
     * AJAX customer validation action
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = $this->dataObject;
        ;
        $response->setError(0);

        $sellergroup = $this->_validateGroup($response);

        $resultJson = $this->resultJsonFactory->create();
        if ($response->getError()) {
            $response->setError(true);
            $response->setMessages($response->getMessages());
        }

        $resultJson->setData($response);
        return $resultJson;
    }
}
