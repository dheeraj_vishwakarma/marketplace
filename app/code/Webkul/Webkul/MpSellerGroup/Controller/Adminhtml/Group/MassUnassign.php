<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Group;

use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory as SellerGroupTypeCollection;

/**
 * Class Group MassUnassign.
 */
class MassUnassign extends \Magento\Backend\App\Action
{
    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var sellerGroupTypeRepository
     */
    private $sellerGroupTypeRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param SellerGroupTypeCollection $sellerGroupTypeCollection
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerGroupTypeCollection $sellerGroupTypeCollection
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->sellerGroupTypeCollection = $sellerGroupTypeCollection;
        parent::__construct($context);
    }

    /**
     * Execute action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        foreach ($collection as $item) {
            $sellerId = $item->getSellerId();
            $flag = $this->unassignSellerFromGroup($sellerId);
            if ($flag) {
                $this->messageManager->addSuccess(
                    __(
                        'A total of %1 seller(s) have been unassigned from group.',
                        $collection->getSize()
                    )
                );
            } else {
                $this->messageManager->addError(
                    __('Seller is not assign to any group.')
                );
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('marketplace/seller/index');
    }

    /**
     * unassignSellerFromGroup uassigns seller from all groups
     *
     * @return bool
     */
    private function unassignSellerFromGroup($sellerId)
    {
        $flag = false;
        $sellerGroupCollection = $this->sellerGroupTypeCollection->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('expired_status', 0)
            ->setOrder('created_at', 'desc');

        if ($sellerGroupCollection->getSize()) {
            foreach ($sellerGroupCollection as $value) {
                $flag = $this->sellerDelete($value);
            }
        }
        return $flag;
    }

    /**
     * Delete SellerGroup
     */

    private function sellerDelete($value)
    {
        return $this->sellerGroupTypeRepository->delete($value);
    }

    /**
     * Check for is allowed.
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Webkul_MpSellerGroup::group');
    }
}
