<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Adminhtml\Booster;

use Webkul\MpSellerGroup\Model\SellerGroupBooster;

class Save extends \Webkul\MpSellerGroup\Controller\Adminhtml\Booster
{
    /**
     * Save seller booster action.
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     *
     * @throws \Magento\Framework\Validator\Exception|\Exception
     */
    public function execute()
    {
        $returnToEdit = false;
        $originalRequestData = $this->getRequest()->getPostValue();
        $sellerBoosterId = isset($originalRequestData['mpsellergroup_booster']['entity_id'])
            ? $originalRequestData['mpsellergroup_booster']['entity_id']
            : null;
        if ($originalRequestData) {
            try {
                $sellerBoosterData = $originalRequestData['mpsellergroup_booster'];
                $sellerBoosterData['booster_image'] = $this->getSellerBoosterImageName($sellerBoosterData);
                $sellerBoosterData['booster_tmp_image'] = false;
                if (isset($originalRequestData['mpsellergroup_booster']['booster_image'][0]['tmp_name'])) {
                    $sellerBoosterData['booster_tmp_image'] = true;
                }
                $request = $this->getRequest();
                $isExistingSellerBooster = (bool) $sellerBoosterId;
                $sellerBooster = $this->sellerBoosterDataFactory->create();
                if ($isExistingSellerBooster) {
                    $sellerBoosterData['entity_id'] = $sellerBoosterId;
                }
                if (!$isExistingSellerBooster) {
                    $sellerBoosterData['created_at'] = $this->_date->gmtDate();
                }
                $sellerBoosterData['updated_at'] = $this->_date->gmtDate();
                $sellerBoosterData = $this->allowedfunctionalites($sellerBoosterData);

                $productId = $this->saveBoosterProduct($sellerBoosterData);

                $sellerBoosterData['product_id'] = $productId;

                $sellerBooster->setData($sellerBoosterData);

                // Save booster
                $sellerBoosterId = $this->saveBooster($sellerBooster, $isExistingSellerBooster);

                $this->_getSession()->unsSellerBoosterFormData();
                // Done Saving seller Booster, finish save action
                $this->messageManager->addSuccess(__('You saved the seller booster.'));
                $returnToEdit = (bool) $this->getRequest()->getParam('back', false);
            } catch (\Magento\Framework\Validator\Exception $exception) {
                $messages = $exception->getMessages();
                if (empty($messages)) {
                    $messages = $exception->getMessage();
                }
                $this->_addSessionErrorMessages($messages);
                $this->_getSession()->setSellerBoosterFormData($originalRequestData);
                $returnToEdit = true;
            } catch (\Exception $exception) {
                $this->messageManager->addException(
                    $exception,
                    __('Something went wrong while saving the booster. %1', $exception->getMessage())
                );
                $this->_getSession()->setSellerBoosterFormData($originalRequestData);
                $returnToEdit = true;
            }
        }
        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect = $this->setPath($returnToEdit, $sellerBoosterId, $resultRedirect);

        return $resultRedirect;
    }

    /**
     * Check allowed modules functionalities
     */

    private function allowedfunctionalites($sellerBoosterData)
    {
        if (isset($sellerBoosterData['allowed_modules_functionalities'])) {
            $allowedModule = $sellerBoosterData['allowed_modules_functionalities'];
            if ($allowedModule && $allowedModule != 'all') {
                $sellerBoosterData['allowed_modules_functionalities'] = implode(',', $allowedModule);
            } else {
                $sellerBoosterData['allowed_modules_functionalities'] = 'all';
            }
        } else {
            $sellerBoosterData['allowed_modules_functionalities'] = 'all';
        }
        return $sellerBoosterData;
    }
    /**
     * Save seller booster
     */
    protected function saveBooster($sellerBooster, $isExistingSellerBooster = null)
    {
        $sellerBoosterId = "";
        if ($isExistingSellerBooster) {
            $sellerBoosterId = $this->_sellerBoosterRepository->save($sellerBooster);
            $sellerBoosterId = $sellerBooster->getId();
        } else {
            $sellerBooster = $this->_sellerBoosterRepository->save($sellerBooster);
            $sellerBoosterId = $sellerBooster->getId();
        }
        return $sellerBoosterId;
    }

    /**
     * Set path
     */

    protected function setPath($returnToEdit, $sellerBoosterId, $resultRedirect)
    {
        if ($returnToEdit) {
            if (!empty($sellerBoosterId)) {
                $resultRedirect->setPath(
                    'mpsellergroup/booster/edit',
                    ['id' => $sellerBoosterId, '_current' => true]
                );
            } else {
                $resultRedirect->setPath(
                    'mpsellergroup/booster/new',
                    ['_current' => true]
                );
            }
        } else {
            $resultRedirect->setPath('mpsellergroup/booster/index');
        }
        return $resultRedirect;
    }

    /**
     * Get seller booster image name.
     *
     * @return string
     *
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getSellerBoosterImageName($sellerBoosterData)
    {
        if (isset($sellerBoosterData['booster_image'][0]['name'])) {
            if (isset($sellerBoosterData['booster_image'][0]['name'])) {
                return $sellerBoosterData['booster_image'] = $sellerBoosterData['booster_image'][0]['name'];
            } else {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __('Please upload booster image.')
                );
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __('Please upload booster image.')
            );
        }
    }

    /**
     * @param Array $data
     * @return int
     *
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function saveBoosterProduct($data)
    {
        $customDefineAttribute = $this->getUserDefineAttribute();
        $content = [];
        foreach ($customDefineAttribute as $key => $value) {
            $content['not_attributes'][] = $value['entity_attribute_id'];
        }
        if (!$customDefineAttribute) {
            $content['not_attributes'] = 0;
        }

        $content['attribute_set_name'] = 'Mp Seller Group';
        $content['attributes'] = 0;
        $content['groups'] = 0;
        $content['removeGroups'] = 0;

        $model = $this->set
            ->setEntityTypeId($this->getAttrSetId("Mp Seller Group"));
        $model->load($this->getAttrSetId("Mp Seller Group"));
        $model->organizeData($content);
        $model->save();
        $productId = '';
        try {
            $catalogProduct = $this->_productDataFactory->create();
            if (empty($data['product_id'])) {
                $productData['name'] = $data['booster_name'];
                $productData['visibility'] = 1;
                if ($this->getAttrSetId("Mp Seller Group")) {
                    $productData['attribute_set_id'] = $this->getAttrSetId("Mp Seller Group");
                }
                $productData['type_id'] = "virtual";
                $productData['price'] = $data['fee_amount'];
                $productData['tax_class_id'] = 0;
                $productData['sku'] = $this->randString(10);
                $catalogProduct->setData($productData);
                $websites = [];
                foreach ($this->_marketplaceHelper->getAllWebsites() as $website) {
                    $websites[] = $website->getId();
                }
                $catalogProduct->setWebsiteIds($websites);
                $catalogProduct->setStockData(
                    [
                        'use_config_manage_stock' => 0,
                        'is_in_stock' => 1,
                        'qty' => 9999,
                        'manage_stock' => 0,
                        'use_config_notify_stock_qty' => 0
                    ]
                );

                $product = $this->_productRepository->save($catalogProduct);

                $productId = $product->getId();

                $catalogProduct = $this->_productRepository->getById($productId);

                $baseTmpPath = 'mpsellergroup/boosterimage/';
                $target = $this->_mediaDirectory->getAbsolutePath($baseTmpPath);
                $imgPath = $target.$data['booster_image'];
                $catalogProduct->addImageToMediaGallery(
                    $imgPath,
                    ['image','small_image','thumbnail'],
                    false,
                    false
                );
                $catalogProduct->save();
                $this->reindexCommand->run(
                    new \Symfony\Component\Console\Input\StringInput('catalog_product_attribute'),
                    new \Symfony\Component\Console\Output\ConsoleOutput()
                );
            } else {
                $productData['entity_id'] = $data['product_id'];
                $productId = $data['product_id'];
                $catalogProduct = $this->_productRepository->getById(
                    $data['product_id'],
                    true,
                    0
                );
                $catalogProduct->setName($data['booster_name']);
                $catalogProduct->setPrice($data['fee_amount']);
                if ($data['booster_tmp_image']) {
                    $baseTmpPath = 'mpsellergroup/boosterimage/';
                    $target = $this->_mediaDirectory->getAbsolutePath($baseTmpPath);
                    $imgPath = $target.$data['booster_image'];
                    $catalogProduct->addImageToMediaGallery(
                        $imgPath,
                        ['image','small_image','thumbnail'],
                        false,
                        false
                    );
                }
                $catalogProduct->save();
                $this->reindexCommand->run(
                    new \Symfony\Component\Console\Input\StringInput('catalog_product_attribute'),
                    new \Symfony\Component\Console\Output\ConsoleOutput()
                );
            }
        } catch (\Exception $exception) {
            $this->messageManager->addException(
                $exception,
                $exception->getMessage()
            );
        }

        return $productId;
    }
    /**
     * @param int $length
     * @param string $charset = 'abcdefghijklmnopqrstuvwxyz0123456789'
     * @return string
     */
    public function randString(
        $length,
        $charset = 'abcdefghijklmnopqrstuvwxyz0123456789'
    ) {
        $str = 'booster-';
        $count = strlen($charset);
        while ($length--) {
            $str .= $charset[random_int(0, $count - 1)];
        }

        return $str;
    }
}
