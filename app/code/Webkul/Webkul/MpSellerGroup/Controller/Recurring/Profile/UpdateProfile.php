<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Recurring\Profile;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Webkul\MpSellerGroup\Api\RecurringProfileRepositoryInterface;

class UpdateProfile extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $mpHelper;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $helper;

    /**
     * @var RecurringProfileRepositoryInterface
     */
    protected $recurringProfileRepository;

    /**
     * @var \Magento\Customer\Model\Url
     */

    protected $loginUrl;

    /**
     * @param Context $context
     * @param Magento\Customer\Model\Session $customerSession
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param RecurringProfileRepositoryInterface $recurringProfileRepository
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Magento\Customer\Model\Url $loginUrl,
        RecurringProfileRepositoryInterface $recurringProfileRepository
    ) {
        $this->_customerSession = $customerSession;
        $this->mpHelper = $mpHelper;
        $this->helper = $helper;
        $this->loginUrl = $loginUrl;
        $this->recurringProfileRepository = $recurringProfileRepository;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->loginUrl->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /**
         * @var \Magento\Framework\Controller\Result\Redirect $resultRedirect
         */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $sellerData = $this->mpHelper->getSellerData();
        $params = $this->getRequest()->getParams();

        if (($sellerData->getSize()) && $this->helper->getStatus() && !empty($params['profile'])) {
            $profile = $this->recurringProfileRepository->getById($params['profile']);
            try {
                $isDataChanges = $this->recurringProfileRepository->fetchUpdate($profile->getId());
                if ($isDataChanges) {
                    $this->messageManager->addSuccess(
                        __("The profile has been updated.")
                    );
                } else {
                    $this->messageManager->addNotice(
                        __("The profile has no changes.")
                    );
                }
            } catch (\Exception $e) {
                $this->helper->logInfo(
                    "Controller_Recurring_Profile_UpdateState execute Exception : ".$e->getMessage()
                );
                $this->messageManager->addError(
                    __("Failed to update the profile.")
                );
            }
            if ($profile) {
                return $resultRedirect->setPath('*/*/view', ['profile' => $profile->getId()]);
            } else {
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }
}
