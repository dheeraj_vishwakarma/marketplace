<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Controller\Group;

use Magento\Framework\Controller\ResultFactory;
use Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface;
use Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\RecurringProfileRepositoryInterface;

/**
 * MpSellerGroup Group ReturnAction Controller.
 */
class ReturnAction extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Webkul\MpSellerGroup\Helper\Subscription
     */
    private $subscriptionHelper;

    /**
     * @var SellerGroupTypeInterface
     */
    protected $sellerGroupTypeInterface;

    /**
     * @var RecurringProfileInterface
     */
    protected $recurringProfileInterface;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    private $helper;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @var SellerGroupTypeRepositoryInterface
     */
    protected $sellerGroupTypeRepository;

    /**
     * @var RecurringProfileRepositoryInterface
     */
    protected $recurringProfileRepository;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper
     * @param SellerGroupTypeInterface $sellerGroupTypeInterface
     * @param RecurringProfileInterface $recurringProfileInterface
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param DateTime $date
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param RecurringProfileRepositoryInterface $recurringProfileRepository
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper,
        SellerGroupTypeInterface $sellerGroupTypeInterface,
        RecurringProfileInterface $recurringProfileInterface,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        DateTime $date,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        RecurringProfileRepositoryInterface $recurringProfileRepository
    ) {
        $this->subscriptionHelper = $subscriptionHelper;
        $this->sellerGroupTypeInterface = $sellerGroupTypeInterface;
        $this->recurringProfileInterface = $recurringProfileInterface;
        $this->helper = $helper;
        $this->_date = $date;
        $this->sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->recurringProfileRepository = $recurringProfileRepository;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /**
         * @var \Magento\Framework\Controller\Result\Redirect $resultRedirect
         */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $params = $this->getRequest()->getParams();
        $this->subscriptionHelper->logInfo("Controller_Group_returnAction execute params : ".json_encode($params));

        try {
            if (!empty($params['token'])) {
                $result = $this->subscriptionHelper->executeAnAgreement($params['token']);
                if ($result && !empty($result['id'])) {
                    $groupData = $this->helper->getGroupDataByGroupId($params['group_id']);
                    $data = [
                        'state' => strtolower($result['state']),
                        'seller_id' => $params['seller_id'],
                        'method_code' => $result['payer']['payment_method'],
                        'reference_id' => $result['id'],
                        'subscriber_name' =>
                        $result['payer']['payer_info']['first_name'] . " 
                        " . $result['payer']['payer_info']['last_name'],
                        'start_datetime' =>
                        $this->helper->convertResponseDateTime($result['start_date']),
                        'internal_reference_id' => $result['id'],
                        'schedule_description' => $groupData->getScheduleDescription(),
                        'suspension_threshold' => $result['plan']['merchant_preferences']['max_fail_attempts'],
                        'bill_failed_later' =>
                        ($result['plan']['merchant_preferences']['auto_bill_amount'] == "YES") ? 1 : 0,
                        'period_unit' => $result['plan']['payment_definitions'][0]['frequency'],
                        'period_frequency' => $result['plan']['payment_definitions'][0]['frequency_interval'],
                        'period_max_cycles' => $result['plan']['payment_definitions'][0]['cycles'],
                        'billing_amount' => $result['plan']['payment_definitions'][0]['amount']['value'],
                        'currency_code' => $result['plan']['currency_code'],
                        'init_amount' => $result['plan']['merchant_preferences']['setup_fee']['value'],
                        'init_may_fail' => $groupData->getAllowInitialFeeFailure(),
                        'additional_info' => json_encode($result),
                        'created_at' => $this->_date->gmtDate(),
                        'updated_at' => $this->_date->gmtDate(),
                    ];

                    $profileData = $this->recurringProfileInterface;
                    $profileData->setData($data);
                    $profileId = $this->recurringProfileRepository->save($profileData)->getId();
                    /**Fix: As not able to add recurring membership */
                    $last_date =
                    (
                        array_key_exists(
                            'last_payment_date',
                            $result['agreement_details']
                        )
                    )?$result['agreement_details']['last_payment_date']:'';
                    $next_date =
                    (
                        array_key_exists(
                            'next_billing_date',
                            $result['agreement_details']
                        )
                    )?$result['agreement_details']['next_billing_date']:'';
                    /**Fix: As not able to add recurring membership */
                    $data = [
                        'order_id'          => 0,
                        'seller_id'         => $params['seller_id'],
                        'group_id'          => $groupData->getId(),
                        'group_type'        => $groupData->getGroupType(),
                        'no_of_products'    => $groupData->getNoOfProducts(),
                        'time_periods'      => $groupData->getTimePeriods(),
                        'fee_amount'        => $result['plan']['payment_definitions'][0]['amount']['value'],
                        'allowed_modules_functionalities' => $groupData->getAllowedModulesFunctionalities(),
                        'check_type'        => $groupData->getCheckType(),
                        'transaction_id' => 'N\A',
                        'transaction_status' => 'pending',
                        'expired_status'    => 0,
                        'expired_on'        =>
                        $this->helper->convertResponseDateTime(
                            $result['agreement_details']['final_payment_date']
                        ),
                        'transaction_date' => $this->_date->gmtDate(),
                        'recurring_profile_id' => $profileId,
                        'last_payment_date' => $last_date,
                        'next_due_date' => $next_date,
                        'created_at' => $this->_date->gmtDate(),
                        'updated_at' => $this->_date->gmtDate(),
                    ];
                    if (empty($data['last_payment_date'])) {
                        unset($data['last_payment_date']);
                    }
                    if (empty($data['next_due_date'])) {
                        unset($data['next_due_date']);
                    }
                    $sellerGroup = $this->sellerGroupTypeInterface;
                    $sellerGroup->setData($data);
                    $this->sellerGroupTypeRepository->save($sellerGroup);

                    $this->messageManager->addSuccess(
                        __(
                            "Your recurring payment profiles:<br>Payment profile # %1: \"%2\".",
                            $result['id'],
                            $groupData->getGroupName()
                        )
                    );
                } else {
                    $this->messageManager->addError(
                        __("Something Went Wrong !!!")
                    );
                }
            } else {
                $this->messageManager->addError(
                    __("Payment Token not found")
                );
            }
        } catch (\Exception $e) {
            $this->subscriptionHelper->logInfo("Controller_Group_returnAction execute : ".$e->getMessage());
            $this->messageManager->addError(
                __("Something Went Wrong !!!")
            );
        }
        return $resultRedirect->setPath('mpsellergroup/group/manage');
    }
}
