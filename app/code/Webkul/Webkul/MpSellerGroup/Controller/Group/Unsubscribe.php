<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Controller\Group;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory as SellerGroupTypeCollection;

class Unsubscribe extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $mpHelper;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $helper;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $sellerGroupTypeRepository;

    /**
     * @var SellerGroupTypeCollection
     */
    protected $sellerGroupTypeCollection;

    /**
     * @var \Magento\Customer\Model\Url
     */

    protected $loginUrl;

    /**
     * @param Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param \Magento\Customer\Model\Url $loginUrl
     * @param SellerGroupTypeCollection $sellerGroupTypeCollection
     */
    public function __construct(
        Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Magento\Customer\Model\Url $loginUrl,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerGroupTypeCollection $sellerGroupTypeCollection
    ) {
        $this->_customerSession = $customerSession;
        $this->mpHelper = $mpHelper;
        $this->helper = $helper;
        $this->loginUrl = $loginUrl;
        $this->sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->sellerGroupTypeCollection = $sellerGroupTypeCollection;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->loginUrl->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     *
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /**
         * @var \Magento\Framework\Controller\Result\Redirect $resultRedirect
         */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $sellerData = $this->mpHelper->getSellerData();
        $params = $this->getRequest()->getParams();

        if (($sellerData->getSize())
             && $this->helper->getStatus()
             && !empty($params['id'])
             && !empty($params['sellerId'])
            ) {
            try {
                $flag = $this->unassignSellerFromGroup($params['sellerId']);
                if ($flag) {
                    $this->messageManager->addSuccess(
                        __('You have successfully cancelled your membership.')
                    );
                } else {
                    $this->messageManager->addError(
                        __('You are not subscribed to any membership.')
                    );
                }
            } catch (\Exception $e) {
                $this->helper->logInfo(
                    "Controller_Group_Unsubscribe execute Exception : ".$e->getMessage()
                );
                $this->messageManager->addError(
                    __("Failed to cancel the membership.")
                );
            }
            return $resultRedirect->setPath('*/*/manage');
        } else {
            return $this->resultRedirectFactory->create()->setPath(
                'marketplace/account/becomeseller',
                ['_secure' => $this->getRequest()->isSecure()]
            );
        }
    }

    /**
     * unassignSellerFromGroup uassigns seller from all groups
     *
     * @return bool
     */
    private function unassignSellerFromGroup($sellerId)
    {
        $flag = false;
        $sellerGroupCollection = $this->sellerGroupTypeCollection->create()
            ->addFieldToFilter('seller_id', $sellerId)
            ->addFieldToFilter('expired_status', 0)
            ->setOrder('created_at', 'desc');

        if ($sellerGroupCollection->getSize()) {
            foreach ($sellerGroupCollection as $value) {
                $flag = $this->sellerDelete($value);
            }
        }
        return $flag;
    }

    /**
     * Delete SellerGroup
     */

    private function sellerDelete($value)
    {
        return $this->sellerGroupTypeRepository->delete($value);
    }
}
