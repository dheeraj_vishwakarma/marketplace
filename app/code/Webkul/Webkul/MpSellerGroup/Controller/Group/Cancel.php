<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Controller\Group;

use Magento\Framework\Controller\ResultFactory;

/**
 * MpSellerGroup Group Cancel Controller.
 */
class Cancel extends \Magento\Customer\Controller\AbstractAccount
{
    /**
     * @var \Webkul\MpSellerGroup\Helper\Subscription
     */
    private $helper;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Webkul\MpSellerGroup\Helper\Subscription $helper
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Webkul\MpSellerGroup\Helper\Subscription $helper
    ) {
        $this->helper = $helper;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        /**
         * @var \Magento\Framework\Controller\Result\Redirect $resultRedirect
        */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        $params = $this->getRequest()->getParams();
        $this->helper->logInfo("Controller_Group_Cancel execute params : ".json_encode($params));

        $this->messageManager->addError(
            __("Payment has been cancelled")
        );
        return $resultRedirect->setPath('mpsellergroup/group/manage');
    }
}
