<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Controller\Group;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Framework\App\RequestInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;

class GetFeeAmount extends Action
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_customerSession;

    /**
     * @var PageFactory
     */
    protected $_resultPageFactory;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var \Magento\Customer\Model\Url
     */
    protected $loginUrl;

    /**
     * @var \Magento\Framework\Json\Helper\Data
     */
    protected $jsonData;
    /**
     * @param Context $context
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param Magento\Customer\Model\Session $customerSession
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Customer\Model\Url $loginUrl,
        \Magento\Framework\Json\Helper\Data $jsonData,
        PageFactory $resultPageFactory
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_customerSession = $customerSession;
        $this->_resultPageFactory = $resultPageFactory;
        $this->loginUrl = $loginUrl;
        $this->jsonData = $jsonData;
        parent::__construct($context);
    }

    /**
     * Check customer authentication
     *
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface
     */
    public function dispatch(RequestInterface $request)
    {
        $loginUrl = $this->loginUrl->getLoginUrl();

        if (!$this->_customerSession->authenticate($loginUrl)) {
            $this->_actionFlag->set('', self::FLAG_NO_DISPATCH, true);
        }
        return parent::dispatch($request);
    }

    /**
     * Get Group Records
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $groupId=$this->getRequest()->getParam('group_id');
        $groupData = $this->_sellerGroupRepository->getById($groupId);
        try {
            $this->getResponse()->representJson(
                $this->jsonData->jsonEncode(
                    $groupData->getData()
                )
            );
        } catch (\Exception $e) {
            $this->getResponse()->representJson(
                $this->jsonData->jsonEncode('')
            );
        }
    }
}
