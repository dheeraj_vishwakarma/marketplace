<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Controller\Group;

use Magento\Framework\Controller\ResultFactory;
use Magento\Paypal\Model\Info;
use Magento\Framework\Exception\NotFoundException;
use Magento\Sales\Model\Order\Payment;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;

/**
 * MpSellerGroup Group Paymentnotify Controller.
 */
class Paymentnotify extends \Magento\Framework\App\Action\Action implements
    \Magento\Framework\App\CsrfAwareActionInterface
{
    /**
     * Default log filename
     *
     * @var string
     */
    const DEFAULT_LOG_FILE = 'mpSellerGroup.log';

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    private $helper;

    /**
     * @var Webkul\MpSellerGroup\Helper\Subscription
     */
    private $subscriptionHelper;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile\CollectionFactory
     */
    private $recurringProfileCollection;

    /**
     * @var \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory
     */
    private $transactionCollection;

    /**
     * @var \Magento\Paypal\Model\Config
     */
    private $config;

    /**
     * IPN request data
     * @var array
     */
    protected $request = [];

    /**
     * Collected debug information
     *
     * @var array
     */
    protected $debugData = [];

    /*
     * Recurring profile instance
     *
     * @var \Webkul\MpSellerGroup\Model\RecurringProfile
     */
    protected $recurringProfile = null;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile\CollectionFactory $recurringProfileCollection
     * @param \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory $transactionCollection
     * @param \Magento\Paypal\Model\Config $config
     * @param DateTime $date
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Webkul\MpSellerGroup\Helper\Subscription $subscriptionHelper,
        \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile\CollectionFactory $recurringProfileCollection,
        \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory $transactionCollection,
        \Magento\Paypal\Model\Config $config,
        DateTime $date
    ) {
        $this->helper = $helper;
        $this->subscriptionHelper = $subscriptionHelper;
        $this->recurringProfileCollection = $recurringProfileCollection;
        $this->transactionCollection = $transactionCollection;
        $this->config = $config;
        $this->_date = $date;
        parent::__construct($context);
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(
        RequestInterface $request
    ): ?InvalidRequestException {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }

    public function execute()
    {
        $this->helper->logInfo("Controller_Group_Paymentnotify ENTER");
        if (!$this->getRequest()->isPost()) {
            return;
        }

        try {
            $params = $this->getRequest()->getParams();
            $this->helper->logInfo("Controller_Group_Paymentnotify execute params : ".json_encode($params));

            $this->processIpnRequest($params);
        } catch (\Exception $e) {
            $this->helper->logInfo("Controller_Group_Paymentnotify execute : ".$e->getMessage());
        }
    }

    /**
     * Get ipn data, send verification to PayPal, run corresponding handler
     *
     * @param array $request
     * @throws Exception
     */
    public function processIpnRequest($request = [])
    {
        $this->request   = $request;
        $this->debugData = ['ipn' => $request];
        ksort($this->debugData['ipn']);

        try {
            $this->getRecurringProfile();
            if (!empty($this->request['txn_type']) && 'recurring_payment_failed' == $this->request['txn_type']) {
                $this->saveRecurringPaymentFailed($this->request);
            } elseif (!empty($this->request['txn_type'])
                && 'recurring_payment_suspended_due_to_max_failed_payment' == $this->request['txn_type']
            ) {
                $this->recurringPaymentSuspendedDueToMaxFailedPayment($this->request);
            } elseif (!empty(
                $this->request['txn_type']
            ) && 'recurring_payment_suspended' == $this->request['txn_type']
                ) {
                $this->recurringPaymentSuspended($this->request);
            } elseif (!empty($this->request['txn_type']) && 'recurring_payment_skipped' == $this->request['txn_type']) {
                $this->recurringPaymentSkipped($request);
            } elseif (!empty($this->request['txn_type'])
                && 'recurring_payment' == $this->request['txn_type']
                && $this->request['payment_status'] == "Pending"
            ) {
                $this->saveRecurringPayment($this->request);
            } elseif (!empty($this->request['txn_type']) && 'recurring_payment_expired' == $this->request['txn_type']) {
                $this->saveRecurringPaymentExpired($this->request);
            } elseif (!empty($this->request['txn_type']) &&
                'recurring_payment_profile_cancel' == $this->request['txn_type']
                ) {
                    $this->saveRecurringPaymentProfileCancel($this->request);
            }
            if (!empty($this->request['txn_type'])
                && in_array(
                    $this->request['txn_type'],
                    ['recurring_payment','recurring_payment_profile_created']
                )
            ) {
                $this->helper->logInfo("Controller_Group_Paymentnotify processIpnRequest : Entered");
                $this->getRecurringProfile();
                $this->processRecurringProfile();
            }
        } catch (\Exception $e) {
            $this->debugData['exception'] = $e->getMessage();
            $this->helper->logInfo("Controller_Group_Paymentnotify processIpnRequest : ".$e->getMessage());
        }
        $this->debug();
    }

    /**
     * Load recurring profile
     *
     * @return \Webkul\MpSellerGroup\Model\RecurringProfile
     */
    protected function getRecurringProfile()
    {
        try {
            if (empty($this->recurringProfile)) {
                // get proper recurring profile
                $collection = $this->recurringProfileCollection->create()
                    ->addFieldToFilter(
                        "reference_id",
                        ["eq" => $this->request['recurring_payment_id']]
                    );
                foreach ($collection as $profile) {
                    $this->recurringProfile = $this->helper->getRecurringProfileData($profile->getId());
                    $this->helper->logInfo(
                        "Controller_Group_Paymentnotify getRecurringProfile : ".
                        json_encode($this->recurringProfile->getData())
                    );
                }
            }
        } catch (\Exception $e) {
            $this->helper->logInfo("Controller_Group_Paymentnotify getRecurringProfile : ".$e->getMessage());
        }
        return $this->recurringProfile;
    }

    /**
     * Process notification from recurring profile payments
     */
    protected function processRecurringProfile()
    {
        $this->recurringProfile = null;
        $this->getRecurringProfile();

        try {
            // handle payment_status
            $payment_request=
            isset($this->request['payment_status'])
            ?
            $this->request['payment_status']:
            $this->request['initial_payment_status'];
            $paymentStatus = $this->filterPaymentStatus($payment_request);
            $this->helper->logInfo("Controller_Group_Paymentnotify processRecurringProfile : ".$paymentStatus);
            switch ($paymentStatus) {
                // paid
                case Info::PAYMENTSTATUS_COMPLETED:
                    $this->registerRecurringProfilePaymentCapture();
                    break;

                default:
                    throw new \Exception__("Cannot handle payment status '{%1}'.", $paymentStatus);
            }
        } catch (\Exception $e) {
            $this->helper->logInfo("Controller_Group_Paymentnotify processRecurringProfile : ".$e->getMessage());
            throw $e;
        }
    }

    /**
     * Register recurring payment notification, create and process order
     */
    protected function registerRecurringProfilePaymentCapture()
    {
        try {
            $profileId = $this->recurringProfile->getId();
            $collection = $this->transactionCollection->create()
                ->addFieldToFilter(
                    'recurring_profile_id',
                    ['eq' => $profileId]
                );
            $this->helper->logInfo(
                "Controller_Group_Paymentnotify registerRecurringProfilePaymentCapture : "
                .json_encode($collection->getData())
            );
            foreach ($collection as $row) {
                if ($this->request['next_payment_date'] == "N/A") {
                    $row->setNextDueDate("");
                } else {
                    $nextDueDate = $this->helper->convertResponseDateTime($this->request['next_payment_date']);
                    $row->setNextDueDate($nextDueDate);
                    $row->setExpiredOn($nextDueDate);
                }
                $row->setTransactionStatus("active");
                $txnId = isset(
                    $this->request['txn_id']
                )?
                    $this->request['txn_id']:$this->request['initial_payment_txn_id'];
                $row->setTransactionId($txnId);
                $row->setTransactionEmail($this->request['payer_email']);
                $row->setIpnTrackId($this->request['ipn_track_id']);
                $row->setUpdatedAt($this->_date->gmtDate());
                $this->saveTransaction($row);
            }
        } catch (\Exception $e) {
            $this->helper->logInfo(
                "Controller_Group_Paymentnotify registerRecurringProfilePaymentCapture : "
                .$e->getMessage()
            );
            throw $e;
        }
    }

    /**
     * Filter payment status from NVP into paypal/info format
     *
     * @param string $ipnPaymentStatus
     * @return string
     */
    protected function filterPaymentStatus($ipnPaymentStatus)
    {
        switch ($ipnPaymentStatus) {
            case 'Created':
                // break is intentionally omitted
            case 'Completed':
                return Info::PAYMENTSTATUS_COMPLETED;
            case 'Denied':
                return Info::PAYMENTSTATUS_DENIED;
            case 'Expired':
                return Info::PAYMENTSTATUS_EXPIRED;
            case 'Failed':
                return Info::PAYMENTSTATUS_FAILED;
            case 'Pending':
                return Info::PAYMENTSTATUS_PENDING;
            case 'Refunded':
                return Info::PAYMENTSTATUS_REFUNDED;
            case 'Reversed':
                return Info::PAYMENTSTATUS_REVERSED;
            case 'Canceled_Reversal':
                return Info::PAYMENTSTATUS_UNREVERSED;
            case 'Processed':
                return Info::PAYMENTSTATUS_PROCESSED;
            case 'Voided':
                return Info::PAYMENTSTATUS_VOIDED;
        }
        return '';
    }

    /**
     * Log debug data to file
     *
     * @param mixed $debugData
     */
    protected function debug()
    {
        if ($this->config && $this->config->getValue("debug")) {
            $this->helper->logInfo("Controller_Group_Paymentnotify debug : ". json_encode($this->debugData));
        }
    }

    public function getPaypalUrl()
    {
        return 'https://www.'.$this->subscriptionHelper->getSandboxStatus().'paypal.com/cgi-bin/webscr';
    }

    public function saveTransaction($row)
    {
        $row->save();
    }

    /**
     * Save Recurring Profile
     */
    public function saveRecurringPaymentFailed($request)
    {
        if (!empty($this->recurringProfile)) {
            $profileId = $this->recurringProfile->getId();
            $collection = $this->transactionCollection->create()
                ->addFieldToFilter(
                    'recurring_profile_id',
                    ['eq' => $profileId]
                );
            foreach ($collection as $row) {
                $count = $row->getFailedTransactions();
                $row->setFailedTransactions($count + 1);
                $row->setTransactionEmail($request['payer_email']);
                $row->setIpnTrackId($request['ipn_track_id']);
                $row->setUpdatedAt($this->_date->gmtDate());
                $this->saveTransaction($row);
            }
        }
    }

    /**
     * Save recurring_payment_suspended_due_to_max_failed_payment
     */

    public function recurringPaymentSuspendedDueToMaxFailedPayment(
        $request
    ) {
     
        $this->request = $request;
        if (!empty($this->recurringProfile)) {
            if ($this->recurringProfile->getBillFailedLater() == 0) {
                $this->recurringProfile->setState("suspended");
                $this->recurringProfile->setUpdatedAt($this->_date->gmtDate());
                $this->recurringProfile->save();
            } else {
                $profileId = $this->recurringProfile->getId();
                $collection = $this->transactionCollection->create()
                    ->addFieldToFilter(
                        'recurring_profile_id',
                        ['eq' => $profileId]
                    );
                foreach ($collection as $row) {
                    if ($this->request['next_payment_date'] == "N/A") {
                        $row->setNextDueDate("");
                    } else {
                        $nextDueDate =
                        $this->helper->convertResponseDateTime(
                            $this->request['next_payment_date']
                        );
                        $row->setNextDueDate($nextDueDate);
                        $row->setExpiredOn($nextDueDate);
                    }
                    $row->setTransactionStatus("pending");
                    $row->setTransactionEmail($this->request['payer_email']);
                    $row->setIpnTrackId($this->request['ipn_track_id']);
                    $row->setUpdatedAt($this->_date->gmtDate());
                    $this->saveTransaction($row);
                }
            }
        }
    }

     /**
      * Save recurring_payment_suspended
      */

    public function recurringPaymentSuspended($request)
    {
        $this->request = $request;
        if (!empty($this->recurringProfile)) {
            $this->recurringProfile->setState("suspended");
            $this->recurringProfile->setUpdatedAt($this->_date->gmtDate());
            $this->recurringProfile->save();

            $profileId = $this->recurringProfile->getId();
            $collection = $this->transactionCollection->create()
                ->addFieldToFilter(
                    'recurring_profile_id',
                    ['eq' => $profileId]
                );
            foreach ($collection as $row) {
                if ($this->request['next_payment_date'] == "N/A") {
                    $row->setNextDueDate("");
                } else {
                    $nextDueDate = $this->helper->convertResponseDateTime($this->request['next_payment_date']);
                    $row->setNextDueDate($nextDueDate);
                    $row->setExpiredOn($nextDueDate);
                }
                $row->setTransactionStatus("suspended");
                $row->setTransactionEmail($this->request['payer_email']);
                $row->setIpnTrackId($this->request['ipn_track_id']);
                $row->setUpdatedAt($this->_date->gmtDate());
                $this->saveTransaction($row);
            }
        }
    }

    /**
     * Save recurring_payment_skipped
     */
    public function recurringPaymentSkipped($request)
    {
        $this->request = $request;
        if (!empty($this->recurringProfile)) {
            $profileId = $this->recurringProfile->getId();
            $collection = $this->transactionCollection->create()
                ->addFieldToFilter(
                    'recurring_profile_id',
                    ['eq' => $profileId]
                );
            foreach ($collection as $row) {
                if ($this->request['next_payment_date'] == "N/A") {
                    $row->setNextDueDate("");
                } else {
                    $nextDueDate = $this->helper->convertResponseDateTime($this->request['next_payment_date']);
                    $row->setNextDueDate($nextDueDate);
                    $row->setExpiredOn($nextDueDate);
                }
                $row->setTransactionStatus("pending");
                $row->setTransactionEmail($this->request['payer_email']);
                $row->setIpnTrackId($this->request['ipn_track_id']);
                $row->setUpdatedAt($this->_date->gmtDate());
                $this->saveTransaction($row);
            }
        }
    }

    /**
     * Save Recurring Payment
     */
    public function saveRecurringPayment($request)
    {
        $this->request = $request;
        if (!empty($this->recurringProfile)) {
            $profileId = $this->recurringProfile->getId();
            $collection = $this->transactionCollection->create()
                ->addFieldToFilter(
                    'recurring_profile_id',
                    ['eq' => $profileId]
                );
            foreach ($collection as $row) {
                if ($this->request['next_payment_date'] == "N/A") {
                    $row->setNextDueDate("");
                } else {
                    $nextDueDate = $this->helper->convertResponseDateTime($this->request['next_payment_date']);
                    $row->setNextDueDate($nextDueDate);
                    $row->setExpiredOn($nextDueDate);
                }
                $row->setTransactionStatus("pending");
                $row->setTransactionEmail($this->request['payer_email']);
                $row->setIpnTrackId($this->request['ipn_track_id']);
                $row->setUpdatedAt($this->_date->gmtDate());
                $this->saveTransaction($row);
            }
        }
    }

    /**
     * Save RecurringPaymentExpired
     */
    public function saveRecurringPaymentExpired($request)
    {
        $this->request = $request;
        if (!empty($this->recurringProfile)) {
            $this->recurringProfile->setState("expired");
            $this->recurringProfile->setUpdatedAt($this->_date->gmtDate());
            $this->recurringProfile->save();

            $collection = $this->transactionCollection->create()
                ->addFieldToFilter(
                    'recurring_profile_id',
                    ['eq' => $profileId]
                );
            foreach ($collection as $row) {
                if ($this->request['next_payment_date'] == "N/A") {
                    $row->setNextDueDate("");
                } else {
                    $nextDueDate = $this->helper->convertResponseDateTime($this->request['next_payment_date']);
                    $row->setNextDueDate($nextDueDate);
                    $row->setExpiredOn($nextDueDate);
                }
                $row->setExpiredStatus(1);
                $row->setTransactionStatus("expired");
                $row->setTransactionEmail($this->request['payer_email']);
                $row->setIpnTrackId($this->request['ipn_track_id']);
                $row->setUpdatedAt($this->_date->gmtDate());
                $this->saveTransaction($row);
            }
        }
    }

    /**
     * Save RecurringPaymentProfileCancel
     */

    public function saveRecurringPaymentProfileCancel($request)
    {
        $this->request = $request;
        if (!empty($this->recurringProfile)) {
            $profileId = $this->recurringProfile->getId();
            $this->recurringProfile->setState("cancelled");
            $this->recurringProfile->setUpdatedAt($this->_date->gmtDate());
            $this->recurringProfile->save();

            $collection = $this->transactionCollection->create()
                ->addFieldToFilter(
                    'recurring_profile_id',
                    ['eq' => $profileId]
                );
            foreach ($collection as $row) {
                if ($this->request['next_payment_date'] == "N/A") {
                    $row->setNextDueDate("");
                } else {
                    $nextDueDate = $this->helper->convertResponseDateTime($this->request['next_payment_date']);
                    $row->setNextDueDate($nextDueDate);
                    $row->setExpiredOn($nextDueDate);
                }
                $row->setExpiredStatus(1);
                $row->setTransactionStatus("cancelled");
                $row->setTransactionEmail($this->request['payer_email']);
                $row->setIpnTrackId($this->request['ipn_track_id']);
                $row->setUpdatedAt($this->_date->gmtDate());
                $this->saveTransaction($row);
            }
        }
    }
}
