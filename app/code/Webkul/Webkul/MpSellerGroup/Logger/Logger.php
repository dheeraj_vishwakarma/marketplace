<?php
/**
 * Webkul Software.
 *
 * @category Marketplace
 *
 * @package  Webkul_MpSellerGroup
 * @author   Webkul
 * @license  https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Logger;

class Logger extends \Monolog\Logger
{
}
