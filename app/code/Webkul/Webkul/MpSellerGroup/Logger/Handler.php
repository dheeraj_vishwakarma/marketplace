<?php
/**
 * Webkul Software.
 *
 * @category Marketplace
 *
 * @package  Webkul_MpSellerGroup
 * @author   Webkul
 * @license  https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Logger;

class Handler extends \Magento\Framework\Logger\Handler\Base
{
    /**
     * Logging level.
     *
     * @var int
     */
    protected $loggerType = Logger::INFO;

    /**
     * File name.
     *
     * @var string
     */
    protected $fileName = '/var/log/mpSellerGroup.log';
}
