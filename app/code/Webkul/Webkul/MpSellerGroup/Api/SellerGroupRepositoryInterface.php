<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api;

/**
 * @api
 */
interface SellerGroupRepositoryInterface
{
    /**
     * Create Seller Group.
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface $group
     * @param bool                                                $saveOptions
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\SellerGroupInterface $group);

    /**
     * Get info about Seller Group by groupCode.
     *
     * @param string $sku
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getGroup($groupCode);

    /**
     * Get info about seller group by group product id.
     *
     * @param int $productID
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getGroupByProductId($productID);

    /**
     * Get info about seller group by group id.
     *
     * @param int $groupId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($groupId);

    /**
     * Delete group.
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface $group
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\SellerGroupInterface $group);

    /**
     * @param int $groupId
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($groupId);

    /**
     * Get Seller Group list.
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupSearchResultsInterface
     */
    public function getList();

    /**
     * Get Active Seller Group list.
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupSearchResultsInterface
     */
    public function getActiveList();
}
