<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api\Data;

/**
 * MpSellerGroup SellerGroupBooster interface.
 *
 * @api
 */
interface SellerGroupBoosterInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ENTITY_ID = 'entity_id';

    const BOOSTER_NAME = 'booster_name';

    const BOOSTER_CODE = 'booster_code';

    const FEE_AMOUNT = 'fee_amount';

    const BOOSTER_IMAGE = 'booster_image';
    
    const BOOSTER_COLOR = 'booster_color';

    const STATUS = 'status';

    const CREATED_AT = 'created_at';

    const UPDATED_AT = 'updated_at';

    const SORT_ORDER = 'sort_order';

    /**#@-*/

    /**
     * Get ID.
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setId($id);

    /**
     * Get Booster Name.
     *
     * @return int|null
     */
    public function getBoosterName();

    /**
     * Set Booster Name.
     *
     * @param int $groupName
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setBoosterName($boosterName);

    /**
     * Get Booster Code.
     *
     * @return int|null
     */
    public function getBoosterCode();

    /**
     * Set Booster Code.
     *
     * @param int $boosterCode
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setBoosterCode($boosterCode);

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount();

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setFeeAmount($feeAmount);

    /**
     * Get Status.
     *
     * @return int|null
     */
    public function getStatus();

    /**
     * Set Status.
     *
     * @param int $status
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setStatus($status);

    /**
     * Product created date.
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set Booster created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt);

    /**
     * Product updated date.
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set Booster updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt);

    /**
     * Booster Image.
     *
     * @return string|null
     */
    public function getBoosterImage();

    /**
     * Set Booster image.
     *
     * @param string $groupImage
     *
     * @return $this
     */
    public function setBoosterImage($boosterImage);

    /**
     * Booster Color.
     *
     * @return string|null
     */
    public function getBoosterColor();

    /**
     * Set Booster color.
     *
     * @param string $groupColor
     *
     * @return $this
     */
    public function setBoosterColor($boosterColor);

    /**
     * Booster Sort Order.
     *
     * @return string|null
     */
    public function getSortOrder();

    /**
     * Set Booster sort order.
     *
     * @param string $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder);
}
