<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api;

/**
 * @api
 */
interface SellerBoosterTypeRepositoryInterface
{
    /**
     * Create Seller Booster for seller.
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface $sellerBoosterType
     * @param bool                                                $saveOptions
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface $sellerBoosterType);

    /**
     * Get info about seller booster by booster id.
     *
     * @param int $boosterId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBoosterType($boosterId);

    /**
     * Get info about seller booster by booster id.
     *
     * @param int $boosterTypeId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($boosterTypeId);

    /**
     * Get info about seller booster by seller id.
     *
     * @param int $sellerId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getBySellerId($sellerId);

    /**
     * Get count of seller booster by seller id.
     *
     * @param int $sellerId
     *
     * @return string
     */
    public function getBySellerCount($sellerId);

    /**
     * Get info about seller booster by order id.
     *
     * @param int $orderId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getByOrderId($orderId);

    /**
     * Delete seller booster record
     *
     * @param \Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface $booster
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\SellerBoosterTypeInterface $booster);

    /**
     * @param int $boosterTypeId
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($boosterTypeId);
}
