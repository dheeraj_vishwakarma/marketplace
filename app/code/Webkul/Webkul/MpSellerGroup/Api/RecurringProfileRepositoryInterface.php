<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api;

/**
 * @api
 */
interface RecurringProfileRepositoryInterface
{
    /**
     * Create Seller Group Recurring Profile.
     *
     * @param \Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface $sellerGroup
     * @param bool $saveOptions
     *
     * @return \Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface
     *
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\StateException
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface $sellerGroup);

    /**
     * Get info about recurring profile by profile id.
     *
     * @param int $profileId
     *
     * @return \Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getById($profileId);

    /**
     * Delete seller group recurring profile record
     *
     * @param \Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface $group
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\StateException
     */
    public function delete(\Webkul\MpSellerGroup\Api\Data\RecurringProfileInterface $group);

    /**
     * @param int $groupId
     *
     * @return bool Will returned True if deleted
     *
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\StateException
     */
    public function deleteById($groupTypeId);
}
