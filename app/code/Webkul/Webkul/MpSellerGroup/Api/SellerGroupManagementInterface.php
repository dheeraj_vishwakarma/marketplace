<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Api;

/**
 * @api
 */
interface SellerGroupManagementInterface
{
    /**
     * Retrieve related seller group based on given group status.
     *
     * @param string $groupCode
     *
     * @return Webkul\MpSellerGroup\Api\Data\SellerGroupInterface[]
     */
    public function getGroupCount($status = null);
}
