<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Helper;

use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as SellerProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupType\CollectionFactory as SellerGroupTypeCollectionFactory;
use Webkul\MpSellerGroup\Api\RecurringProfileRepositoryInterface;
use Magento\Paypal\Model\Info;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface;

/**
 * MpSellerGroup data helper.
 */
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerProductCollectionFactory
     */
    protected $_sellerProductCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\TimezoneInterface
     */
    private $timezone;

    /**
     * Logging instance.
     *
     * @var \Webkul\MpSellerGroup\Logger\Logger
     */
    private $logger;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $mpHelper;

    /**
     * @var RecurringProfileRepositoryInterface
     */
    protected $recurringProfileRepository;

    /**
     * @var SellerGroupTypeCollectionFactory
     */
    protected $groupTypeCollection;

    /**
     * @var \Magento\Framework\Pricing\PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $registry;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var SellerBoosterTypeRepository
     */
    protected $sellerBoosterTypeRepository;

    /**
     * @param \Magento\Framework\App\Helper\Context $context
     * @param \Magento\Customer\Model\Session $customerSession
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param SellerProductCollectionFactory $sellerProductCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface
     * @param \Webkul\MpSellerGroup\Logger\Logger $logger
     * @param \Webkul\Marketplace\Helper\Data $mpHelper
     * @param RecurringProfileRepositoryInterface $recurringProfileRepository
     * @param SellerGroupTypeCollectionFactory $groupTypeCollection
     * @param \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository
     */
    public function __construct(
        \Magento\Framework\App\Helper\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        SellerProductCollectionFactory $sellerProductCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $timezoneInterface,
        \Webkul\MpSellerGroup\Logger\Logger $logger,
        \Webkul\Marketplace\Helper\Data $mpHelper,
        RecurringProfileRepositoryInterface $recurringProfileRepository,
        SellerGroupTypeCollectionFactory $groupTypeCollection,
        \Magento\Framework\Pricing\PriceCurrencyInterface $priceCurrency,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\App\Http\Context $httpContext,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_customerSession = $customerSession;
        $this->_date = $date;
        $this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_storeManager = $storeManager;
        $this->timezone = $timezoneInterface;
        $this->logger = $logger;
        $this->mpHelper = $mpHelper;
        $this->recurringProfileRepository = $recurringProfileRepository;
        $this->groupTypeCollection = $groupTypeCollection;
        $this->priceCurrency = $priceCurrency;
        $this->productRepository = $productRepository;
        $this->registry = $registry;
        $this->httpContext = $httpContext;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->sellerBoosterTypeRepository = $sellerBoosterTypeRepository;
        parent::__construct($context);
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/status',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getAllowedPaymentMethods()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/allowedpaymentmethods',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getCheckType()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/checktype',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getDefaultGroupStatus()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/defaultAllowed',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getDefaultProductAllowed()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/defaultproductallowed',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getDefaultAllowedFeatures()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/defaultAllowedFeatures',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getDefaultTimeAllowed()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/defaulttimeallowed',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getProductStatusAfterMembershipExpire()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/product_after_membership_expire',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getNotifyTime()
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/general_settings/notifyTime',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function getDisplayBanner($type = 'pricing')
    {
        $configPath = 'mpsellergroup/'.$type.'/displaybanner';
        return $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return boolean
     */
    public function getBannerImage($type = "pricing")
    {
        $configPath = 'mpsellergroup/'.$type.'/banner';
        $bannerImage = $this->_storeManager->getStore()->getBaseUrl(
            \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
        ).'mpsellergroup/banner/'.$this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        // @codingStandardsIgnoreStart
        $file_headers = @get_headers($filename);
        // @codingStandardsIgnoreEnd

        if ($file_headers[0] == 'HTTP/1.0 404 Not Found') {
            return false;
        } elseif ($file_headers[0]=='HTTP/1.0 302 Found' && $file_headers[7]=='HTTP/1.0 404 Not Found') {
            return false;
        } else {
            return $bannerImage;
        }
        return false;
    }

    /**
     * @return string
     */

    public function getBannerLabel($type = 'pricing')
    {
        $configPath = 'mpsellergroup/'.$type.'/label';
        return  $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */

    public function getBannerContent($type = 'pricing')
    {
        $configPath = 'mpsellergroup/'.$type.'/bannercontent';
        return  $this->scopeConfig->getValue(
            $configPath,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return array
     */

    public function getSellerGroupData($sellerId)
    {
        $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($sellerId);
        if ($getSellerGroup) {
            return $getSellerGroup;
        } else {
            return [];
        }
    }

    public function getGroupDataByGroupId($groupId)
    {
        return $this->_sellerGroupRepository->getById($groupId);
    }

    /**
     * getPermission Returns seller permission details.
     *
     * @return Array Permission details
     */
    public function getPermission($duplicateFlag = false)
    {
        try {
            $sellerId = $this->getSellerId();
            $getSellerTypeGroup = $this->getSellerGroupData($sellerId);
            $getSellerTypeGroupCount = $this->_sellerGroupTypeRepository->getBySellerCount($sellerId);
            $expire = false;
            $today = $this->_date->gmtDate();
            $products = $this->_sellerProductCollectionFactory->create()
            ->addFieldToFilter(
                'seller_id',
                $sellerId
            );
            if (!$getSellerTypeGroupCount) {
                $getDefaultGroupStatus = $this->getDefaultGroupStatus();
                if ($getDefaultGroupStatus) {
                    $allowqty = $this->getDefaultProductAllowed();
                    $groupName = 'unassigned';
                    if ($allowqty <= ($products->getSize())) {
                        return $arr = [
                            'status' => true,
                            'type' => $groupName,
                            'qty' => $allowqty,
                            'payment' => '',
                            'group_type' => 0,
                            'expire' => false
                        ];
                    } elseif ($duplicateFlag && ($allowqty - $products->getSize()) < 2) {
                        return $arr = [
                            'status' => true,
                            'type' => $groupName,
                            'qty' => $allowqty,
                            'payment' => '',
                            'group_type' => 0,
                            'expire' => true
                        ];
                    }
                }
            } else {
                $groupName = 'None';
                $groupId = $getSellerTypeGroup->getGroupId();
                $groupType = $getSellerTypeGroup->getGroupType();
                $allowqty = $getSellerTypeGroup->getNoOfProducts();
                $remainingProducts = $getSellerTypeGroup->getRemainingProducts();
                $checkType = $getSellerTypeGroup->getCheckType();
                $createdAt = $getSellerTypeGroup->getTransactionDate();
                $groupData = $this->getGroupDataByGroupId(
                    $getSellerTypeGroup->getGroupId()
                );
                if ($groupData) {
                    $groupName = $groupData['group_name'];
                }
                $payment = $getSellerTypeGroup->getTransactionStatus();

                $marketplaceProductIds = [];
                foreach ($products as $product) {
                    $marketplaceProductIds = $this->productArr($product);
                }
                $collection = $this->_productCollectionFactory->create()
                ->addFieldToFilter(
                    'created_at',
                    ['datetime' => true, 'from' => $createdAt]
                )->addFieldToFilter(
                    'visibility',
                    4
                )->addFieldToFilter(
                    'entity_id',
                    ['in' => $marketplaceProductIds]
                );
                $arr = $this->checkPayment(
                    $payment,
                    $checkType,
                    $groupName,
                    $groupType,
                    $allowqty,
                    $remainingProducts,
                    $collection,
                    $getSellerTypeGroup,
                    $today,
                    $duplicateFlag
                );
                return $arr;
            }
        } catch (\Exception $e) {
            $this->logInfo("Helper_Data getPermission : ".$e->getMessage());
        }
        return $arr = ['status' => false];
    }
    public function convertPrice($amount = 0, $store = null, $currency = null)
    {
        $currency = $this->_storeManager->getStore()->getCurrentCurrency()->getCode();
        if ($store == null) {
            $store = $this->_storeManager->getStore()->getStoreId();
        }
        $rate = $this->priceCurrency->convertAndFormat(
            $amount,
            $includeContainer = true,
            $precision = 2,
            $store,
            $currency
        );
        return $rate;
    }

    /**
     * Make product arr
     */

    private function productArr($product)
    {
        if ($product->getMageproductId()) {
            $marketplaceProductIds[] = $product->getMageproductId();
        }
        return $marketplaceProductIds;
    }

    /**
     * Check payment
     */
    private function checkPayment(
        $payment,
        $checkType,
        $groupName,
        $groupType,
        $allowqty,
        $remainingProducts,
        $collection,
        $getSellerTypeGroup,
        $today,
        $duplicateFlag
    ) {
        if ($payment == '') {
            return $arr = [
                'status' => true,
                'type' => $groupName,
                'payment' => 'wait',
                'group_type' => $groupType,
                'expire' => false
            ];
        } elseif ($payment == 'pending') {
            return $arr = [
                'status' => true,
                'type' => $groupName,
                'payment' => $payment,
                'group_type' => $groupType,
                'expire' => false
            ];
        } elseif ($checkType == 0) {
            if ($allowqty <= $collection->getSize() && $remainingProducts < $allowqty) {
                return $arr = [
                    'status' => true,
                    'type' => $groupName,
                    'qty' => $allowqty,
                    'payment' => $payment,
                    'group_type' => $groupType,
                    'expire' => false
                ];
            }
            if ($duplicateFlag && ($remainingProducts < $allowqty) && (($allowqty - $remainingProducts) < 2)) {
                return $arr = [
                    'status' => true,
                    'type' => $groupName,
                    'qty' => $allowqty,
                    'payment' => $payment,
                    'group_type' => $groupType,
                    'expire' => true
                ];
            }
        } elseif ($checkType == 1) {
            if (strtotime($getSellerTypeGroup->getExpiredOn()) < strtotime($today)) {
                return $arr = [
                    'status' => true,
                    'type' => $groupName,
                    'payment' => $payment,
                    'group_type' => $groupType,
                    'expire' => true
                ];
            }
        } elseif ($checkType == 2) {
            if (strtotime($getSellerTypeGroup->getExpiredOn()) > strtotime($today)
                && ($remainingProducts < $allowqty)
            ) {
                $arr = [
                    'status' => false,
                    'type' => $groupName,
                    'qty' => $allowqty,
                    'payment' => $payment,
                    'group_type' => $groupType,
                    'expire' => false
                ];
                if ($duplicateFlag && (($allowqty - $remainingProducts) < 2)) {
                    $arr['status'] = true;
                    $arr['expire'] = true;
                }
                return $arr;
            } else {
                return $arr = [
                    'status' => true,
                    'type' => $groupName,
                    'qty' => $allowqty,
                    'payment' => $payment,
                    'group_type' => $groupType,
                    'expire' => true
                ];
            }
        }
    }

    /**
     * Get Current Date
     *
     * @return string
     */
    public function getCurrentDate()
    {
        return date('Y-m-d', strtotime(' +1 day'));
    }

    public function getCurrentTime($isDate = false)
    {
        // Date for a specific date/time:
        $date = new \DateTime();

        // Convert timezone
        $tz = new \DateTimeZone($this->getCurrentTimeZone());
        $date->setTimeZone($tz);

        // Output date after
        if ($isDate) {
            return $date->format('Y-m-d H:i:s');
        } else {
            return $date->format('H:i:s');
        }
    }

    /**
     * @return string
     */
    public function getCurrentTimeZone()
    {
        $tz = $this->timezone->getConfigTimezone();
        date_default_timezone_set($tz);
        return $tz;
    }

    /**
     * @return int
     */

    public function getSellerId()
    {
        try {
            return $this->mpHelper->getCustomerId();
        } catch (\Exception $e) {
            $this->logInfo("Helper_Data getSellerId : ".$e->getMessage());
        }
    }

    /**
     * @return null
     */

    public function logInfo($data)
    {
        $this->logger->info($data);
    }

    /**
     * @return string
     */

    public function convertResponseDateTime($date)
    {
        $convertdate = (new \DateTime())->setTimestamp(strtotime($date));
        return $convertdate->format(\Magento\Framework\Stdlib\DateTime::DATETIME_PHP_FORMAT);
    }

    /**
     * @param  int          $profileId
     * @return object
     */
    public function getRecurringProfileData($profileId)
    {
        return $this->recurringProfileRepository->getById($profileId);
    }

    /**
     * @param  int          $profileId
     * @return object
     */
    public function getGroupDataByProfile($profileId)
    {
        try {
            $collection = $this->groupTypeCollection->create()
                ->addFieldToFilter("recurring_profile_id", $profileId);
            if ($collection->getSize()) {
                foreach ($collection as $data) {
                    return $this->getGroupDataByGroupId($data->getGroupId());
                }
            }
        } catch (\Exception $e) {
            $this->logInfo("Helper_Data getGroupDataByProfile : ".$e->getMessage());
        }
    }

    /**
     * @param  integer $amount   [description]
     * @param  string  $currency [description]
     * @return string
     */
    public function formatPrice($amount = 0, $currency = null)
    {
        return $this->priceCurrency->format(
            $amount,
            $includeContainer = true,
            $precision = 2,
            $scope = null,
            $currency
        );
    }

    /**
     * @param  string  $dateTime
     * @param  boolean $flag
     * @return date
     */

    public function formatDateByDateTime($dateTime, $flag = true)
    {
        try {
            if ($flag) {
                $timeStamp = $this->convertDateTimeToConfigTimeZone($dateTime);
            } else {
                $timeStamp = strtotime($dateTime);
            }
            return date("M j, Y g:i:s a", $timeStamp);
        } catch (\Exception $e) {
            $this->logInfo("Helper_Data formatDateByDateTime : ".$e->getMessage());
        }
    }

    /**
     * convertDateTimeToConfigTimeZone convert Datetime from one zone to another
     * @param string $dateTime which we want to convert
     * @param string $toTz timezone in which we want to convert
     */
    public function convertDateTimeToConfigTimeZone($dateTime = "")
    {
        try {
            // Date for a specific date/time:
            $date = new \DateTime($dateTime);

            // Convert timezone
            $tz = new \DateTimeZone($this->timezone->getConfigTimezone());
            $date->setTimeZone($tz);

            // Output date after
            return strtotime($date->format('d-m-Y,h:i:s a'));
        } catch (\Exception $e) {
            $this->logInfo("Helper_Data convertDateTimeToConfigTimeZone : ".$e->getMessage());
        }
    }

    /**
     * @param  int   $id
     * @return null
     */

    public function deleteProductById($id)
    {
        try {
            $this->registry->register('isSecureArea', true);
            // using product id
            $product = $this->productRepository->getById($id);
            $this->productRepository->delete($product);
        } catch (\Exception $e) {
            $this->logInfo("Helper_Data deleteProduct : ".$e->getMessage());
        }
    }

    /**
     * @param  $moduleName
     * @return boolean
     */
    public function isModuleInstalled($moduleName)
    {
        if ($this->_moduleManager->isEnabled($moduleName)) {
            return true;
        }
        return false;
    }

    /**
     * @param  int     $fieldId
     * @return string
     */
    public function getPricingConfigValue($fieldId)
    {
        return $this->scopeConfig->getValue(
            'mpsellergroup/pricing/'.$fieldId,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * make array of payment status
     */
    public function arrayPaymentStatus()
    {
        $payment_arr = [];
        $payment_arr['Created'] = "";
        $payment_arr['Completed'] = Info::PAYMENTSTATUS_COMPLETED;
        $payment_arr['Denied'] = Info::PAYMENTSTATUS_DENIED;
        $payment_arr['Expired'] = Info::PAYMENTSTATUS_EXPIRED;
        $payment_arr['Failed'] = Info::PAYMENTSTATUS_FAILED;
        $payment_arr['Pending'] = Info::PAYMENTSTATUS_PENDING;
        $payment_arr['Refunded'] = Info::PAYMENTSTATUS_REFUNDED;
        $payment_arr['Reversed'] = Info::PAYMENTSTATUS_REVERSED;
        $payment_arr['Canceled_Reversal'] = Info::PAYMENTSTATUS_UNREVERSED;
        $payment_arr['Processed'] = Info::PAYMENTSTATUS_PROCESSED;
        $payment_arr['Voided'] = Info::PAYMENTSTATUS_VOIDED;
        return $payment_arr;
    }

    public function getCustomerId()
    {
        return $this->httpContext->getValue('customer_id');
    }

    /**
     * Get configuration value from path
     *
     * @param string $path
     * @param string $scope
     * @return string|null
     */
    public function getConfigurationValue($path, $scope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE)
    {
        return $this->scopeConfig->getValue(
            $path,
            $scope
        );
    }

    /**
     * Get seller Booster type data by booster type id
     *
     * @param int $boosterTypeId
     * @return object
     */
    public function getSellerBoosterTypeData($boosterTypeId)
    {
        return $this->sellerBoosterTypeRepository->getById($boosterTypeId);
    }

    /**
     * Get Booster data by booster id
     *
     * @param int $boosterId
     * @return object
     */
    public function getBoosterDataByBoosterId($boosterId)
    {
        return $this->sellerBoosterRepository->getById($boosterId);
    }

    /**
     * Expire all paid boosters of seller
     *
     * @param integer $sellerId
     * @return void
     */
    public function expireSellerBoosterTypes($sellerId)
    {
        $sellerBoosterTypes = $this->sellerBoosterTypeRepository->getCollectionBySellerId($sellerId, true, 'paid');
        foreach ($sellerBoosterTypes as $boosterType) {
            $boosterType->setExpiredStatus(1)->save();
        }
    }
}
