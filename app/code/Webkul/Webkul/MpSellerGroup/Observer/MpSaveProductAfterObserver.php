<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as SellerProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Model\ProductFactory as ProductFactory;
use Magento\Framework\UrlInterface;
use Webkul\MpSellerGroup\Helper\Email;

class MpSaveProductAfterObserver implements ObserverInterface
{
    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $_helperData;

    /**
     * @var \Webkul\Marketplace\Helper\Data
     */
    protected $_marketplaceHelperData;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerProductCollectionFactory
     */
    protected $_sellerProductCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_requestInterface;

    /**
     * @var Webkul\MpSellerGroup\Helper\Email
     */
    protected $email;

    /**
     * @param \Webkul\MpSellerGroup\Helper\Data $helperData
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelperData
     * @param \Magento\Customer\Model\Session $customerSession
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param SellerProductCollectionFactory $sellerProductCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductFactory $productFactory
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param UrlInterface $urlBuilder
     * @param \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $salesListCollection
     * @param Email $email
     */
    public function __construct(
        \Webkul\MpSellerGroup\Helper\Data $helperData,
        \Webkul\Marketplace\Helper\Data $marketplaceHelperData,
        \Magento\Customer\Model\Session $customerSession,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        SellerProductCollectionFactory $sellerProductCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        ProductFactory $productFactory,
        \Magento\Framework\App\RequestInterface $requestInterface,
        UrlInterface $urlBuilder,
        \Webkul\Marketplace\Model\ResourceModel\Saleslist\CollectionFactory $salesListCollection,
        Email $email
    ) {
        $this->_requestInterface = $requestInterface;
        $this->_urlBuilder = $urlBuilder;
        $this->_helperData = $helperData;
        $this->_marketplaceHelperData = $marketplaceHelperData;
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_customerSession = $customerSession;
        $this->_date = $date;
        $this->_productFactory = $productFactory;
        $this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->salesListCollection = $salesListCollection;
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function getSellerGroupData($sellerId)
    {
        $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($sellerId);
        if ($getSellerGroup) {
            return $getSellerGroup;
        } else {
            return [];
        }
    }

    /**
     * Marketplace Product save after observer
     * that checks the remaining products and expired time of partner group.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_helperData->getStatus() && !$this->_requestInterface->getParam('id')) {
            $sellerId = $this->_helperData->getSellerId();
            $today = $this->_date->gmtDate();
            $wholeData = $observer->getData();
            if (!empty($wholeData[0]['id'])) {
                $productId = $wholeData[0]['id'];
                $isSellerProduct = $this->_sellerProductCollectionFactory->create()
                ->addFieldToFilter(
                    'seller_id',
                    $sellerId
                )->addFieldToFilter(
                    'mageproduct_id',
                    $productId
                );

                $salesList = $this->salesListCollection->create()
                    ->addFieldToFilter(
                        'mageproduct_id',
                        ['eq' => $productId]
                    )->addFieldToSelect('mageproduct_id');
                $productCollection = $this->_sellerProductCollectionFactory->create()
                    ->addFieldToFilter(
                        'mageproduct_id',
                        ['eq' => $productId]
                    )->addFieldToFilter(
                        'status',
                        ['eq' => 1]
                    );

                if ($salesList->getSize()==0
                    && $productCollection->getSize()>0
                    && !$this->_marketplaceHelperData->getIsProductApproval()
                ) {
                    $this->checkValidation($sellerId);
                }

                $content = '';
                if ($isSellerProduct->getSize()) {
                    $product = $this->_productFactory->create()->load($productId);
                    $allowqty = 0;
                    $getSellerGroup = $this->getSellerGroupData($sellerId);

                    $products = $this->_sellerProductCollectionFactory->create()
                    ->addFieldToFilter(
                        'seller_id',
                        $sellerId
                    );

                    $adminEmail = $this->_marketplaceHelperData->getAdminEmailId();
                    $headers = 'From:Administrator'."\r\n".
                            'Reply-To: '.$adminEmail."\r\n".
                            'X-Mailer: PHP/'.phpversion();
                    if (!$getSellerGroup) {
                        $totalproduct = $products->getSize();
                        $allowqty = $this->_helperData->getDefaultProductAllowed();
                        $productleft = $allowqty - $totalproduct;
                        $senderInfo = [];
                        $receiverInfo = [];
                        $fName = $this->_customerSession->getCustomer()->getData('firstname');
                        $lName = $this->_customerSession->getCustomer()->getData('lastname');
                        $fullName = $fName." ".$lName;
                        $receiverInfo = [
                            'name' => $fullName,
                            'email' => $this->_customerSession->getCustomer()->getEmail()
                        ];
                        $senderInfo = [
                            'name' => "Admin",
                            'email' => $adminEmail,
                        ];
                        $emailTempVariables = [];
                        $emailTempVariables['group_name'] = $getSellerGroup->getGroupName();
                        $emailTempVariables['fee_amount'] = $getSellerGroup->getFeeAmount();
                        $emailTempVariables['no_of_products'] = $getSellerGroup->getNoOfProducts();
                        $emailTempVariables['time_periods'] = $getSellerGroup->getTimePeriods();

                        $this->email->sendGroupNotifyEmail($emailTempVariables, $senderInfo, $receiverInfo);
                    } else {
                        $allowqty = $getSellerGroup->getNoOfProducts();
                        $expirydate = $getSellerGroup->getExpiredOn();
                        $checkType = $getSellerGroup->getCheckType();
                        $createdAt = $getSellerGroup->getTransactionDate();

                        $marketplaceProductIds = [];
                        foreach ($products as $product) {
                            $marketplaceProductIds[] = $product->getMageproductId();
                        }

                        $collection = $this->_productCollectionFactory->create()
                        ->addFieldToFilter(
                            'created_at',
                            ['datetime' => true, 'from' => $createdAt]
                        )->addFieldToFilter(
                            'entity_id',
                            ['in' => $marketplaceProductIds]
                        );

                        $totalproduct = $collection->getSize();
                        $productleft = $allowqty - $totalproduct;
                        $expireDate = strtotime($expirydate);
                        $current = strtotime($today);
                        $difference = ($expireDate - $current) / 86400;
                        $subject = '';
                        $senderInfo = [];
                        $receiverInfo = [];
                        $fName = $this->_customerSession->getCustomer()->getData('firstname');
                        $lName = $this->_customerSession->getCustomer()->getData('lastname');
                        $fullName = $fName." ".$lName;
                        $receiverInfo = [
                            'name' => $fullName,
                            'email' => $this->_customerSession->getCustomer()->getEmail()
                        ];
                        $senderInfo = [
                            'name' => "Admin",
                            'email' => $adminEmail,
                        ];
                        $emailTempVariables = [];
                        $emailTempVariables['seller_name'] = "Seller";

                        $this->email->sendGroupExpireEmail(
                            $emailTempVariables,
                            $senderInfo,
                            $receiverInfo
                        );
                    }
                }
            }
        }
    }

    /**
     * [checkValidation used to set updated value of remaining products,
     * according to condition]
     *
     * @return void
     */
    private function checkValidation($sellerId)
    {
        try {
            $getSellerGroup = $this->getSellerGroupData($sellerId);

            $today = $this->_date->gmtDate();

            if (!empty($getSellerGroup)) {
                $allowqty = $getSellerGroup->getNoOfProducts();
                $checkType = $getSellerGroup->getCheckType();
                if ($checkType == 2) {
                    if (strtotime($getSellerGroup->getExpiredOn()) > strtotime($today)
                        && $getSellerGroup->getRemainingProducts() < $allowqty
                    ) {
                        $getSellerGroup->setRemainingProducts(
                            $getSellerGroup->getRemainingProducts()+1
                        );
                        $getSellerGroup->setUpdatedAt($today);
                        $getSellerGroup->save();
                    } elseif (strtotime($getSellerGroup->getExpiredOn()) <= strtotime($today)) {
                        if ($getSellerGroup->getRemainingProducts()>=$allowqty) {
                            $getSellerGroup->setExpiredStatus(1)->save();
                            $this->_helperData->expireSellerBoosterTypes($sellerId);
                        }
                    }
                } elseif ($checkType == 0) {
                    if ($getSellerGroup->getRemainingProducts() < $allowqty) {
                        $getSellerGroup->setRemainingProducts(
                            $getSellerGroup->getRemainingProducts()+1
                        );
                        $getSellerGroup->setUpdatedAt($today);
                        $getSellerGroup->save();

                        if ($getSellerGroup->getRemainingProducts()>=$allowqty) {
                            $getSellerGroup->setExpiredStatus(1)->save();
                            $this->_helperData->expireSellerBoosterTypes($sellerId);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
