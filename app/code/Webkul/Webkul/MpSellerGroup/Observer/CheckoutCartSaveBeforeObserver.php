<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\Message\ManagerInterface;
use Webkul\MpSellerGroup\Helper\Data as HelperData;

/**
 * Webkul MpSellerGroup CheckoutCartSaveBeforeObserver Observer.
 */
class CheckoutCartSaveBeforeObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var HelperData
     */
    protected $_helper;

    /**
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param CheckoutSession $checkoutSession
     * @param ManagerInterface $messageManager
     * @param HelperData $helper
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        CheckoutSession $checkoutSession,
        ManagerInterface $messageManager,
        HelperData $helper
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_checkoutSession = $checkoutSession;
        $this->_messageManager = $messageManager;
        $this->_helper = $helper;
    }

    /**
     * Checkout cart product save before event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_helper->getStatus()) {
                $items =  $this->_checkoutSession->getQuote()->getAllVisibleItems();
                foreach ($items as $item) {
                    $productId = $item->getProductId();
                    $flag = false;
                    $group = $this->_sellerGroupRepository->getGroupByProductId($productId);
                    $booster = $this->sellerBoosterRepository->getBoosterByProductId($productId);
                    if (($group['product_id'] && $group['product_id'] == $productId)
                    || ($booster['product_id'] && $booster['product_id'] == $productId)
                    ) {
                        $flag = true;
                    }
                    if ($flag && $item->getQty() > 1) {
                        $item->setQty(1);
                        $this->saveQuote($item);
                    }
                }
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }

    /**
     * Save Quote Item
     */
    public function saveQuote($item)
    {
        $item->save();
    }
}
