<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\HTTP\PhpEnvironment\Response;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\Framework\App\Request\Http as RequestHttp;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelperData;

/**
 * Webkul MpSellerGroup AfterAddProductToCartObserver Observer.
 */
class AfterAddProductToCartObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var RequestInterface
     */
    protected $_requestInterface;

    /**
     * @var CheckoutSession
     */
    protected $_checkoutSession;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var RequestHttp
     */
    protected $_requestHttp;

    /**
     * @var SellerGroupHelperData
     */
    protected $_sellerGroupHelperData;

    /**
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param RequestInterface $requestInterface
     * @param CheckoutSession $checkoutSession
     * @param Response $response
     * @param ManagerInterface $messageManager
     * @param UrlInterface $urlBuilder
     * @param OrderRepositoryInterface $orderRepository
     * @param RequestHttp $requestHttp
     * @param SellerGroupHelperData $sellerGroupHelperData
     * @param \Magento\Framework\App\Response\RedirectInterfaceFactory $redirect
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        RequestInterface $requestInterface,
        CheckoutSession $checkoutSession,
        Response $response,
        ManagerInterface $messageManager,
        UrlInterface $urlBuilder,
        OrderRepositoryInterface $orderRepository,
        RequestHttp $requestHttp,
        SellerGroupHelperData $sellerGroupHelperData,
        \Magento\Framework\App\Response\RedirectInterfaceFactory $redirect
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_requestInterface = $requestInterface;
        $this->_checkoutSession = $checkoutSession;
        $this->_response = $response;
        $this->_messageManager = $messageManager;
        $this->_urlBuilder = $urlBuilder;
        $this->_orderRepository = $orderRepository;
        $this->_requestHttp = $requestHttp;
        $this->_sellerGroupHelperData = $sellerGroupHelperData;
        $this->redirect = $redirect;
    }

    /**
     * Sale quote add item event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_sellerGroupHelperData->getStatus()) {
                $params = $observer->getRequest()->getParams();
                $product = $observer->getProduct();
                $productId = $product->getId();
                $flag = false;
                $group = $this->_sellerGroupRepository->getGroupByProductId($productId);
                $booster = $this->sellerBoosterRepository->getBoosterByProductId($productId);
                if (($group['product_id'] && $group['product_id'] == $productId)
                || ($booster['product_id'] && $booster['product_id'] == $productId)
                ) {
                    $flag = true;
                }
                if ($flag) {
                    $url = $this->_urlBuilder->getUrl('checkout');
                    $observer->getResponse()->setRedirect($url);
                    $observer->getResponse()->sendResponse();

                    $controller = $observer->getControllerAction();
                    $redirect = $this->redirect->create();
                    $redirect->redirect($controller->getResponse(), 'checkout/index/index');
                }
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }
}
