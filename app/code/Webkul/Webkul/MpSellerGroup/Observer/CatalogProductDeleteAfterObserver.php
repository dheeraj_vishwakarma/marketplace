<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupBoosterRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;

/**
 * Webkul MpSellerGroup CatalogProductDeleteAfterObserver Observer.
 */
class CatalogProductDeleteAfterObserver implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupBoosterRepositoryInterface
     */
    protected $sellerBoosterRepository;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupBoosterRepositoryInterface $sellerBoosterRepository
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupBoosterRepositoryInterface $sellerBoosterRepository,
        ManagerInterface $messageManager
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->sellerBoosterRepository = $sellerBoosterRepository;
        $this->_messageManager = $messageManager;
    }

    /**
     * Product delete after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $productId = $observer->getProduct()->getId();
            $sellerGroupData = $this->_sellerGroupRepository->getGroupByProductId(
                $productId
            );
            $sellerGroupData->delete();
            $sellerBoosterData = $this->sellerBoosterRepository->getBoosterByProductId(
                $productId
            );
            $sellerBoosterData->delete();
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }
}
