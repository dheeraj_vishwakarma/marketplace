<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelperData;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\Data\SellerGroupTypeInterface;
use Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpSellerGroup\Helper\Email;

/**
 * Webkul MpSellerGroup OrderCancelAfter Observer.
 */
class OrderCancelAfter implements ObserverInterface
{
    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var OrderRepositoryInterface
     */
    protected $_orderRepository;

    /**
     * @var SellerGroupHelperData
     */
    protected $_sellerGroupHelperData;

    /**
     * @var SellerGroupTypeRepositoryInterface
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerBoosterTypeRepositoryInterface
     */
    protected $sellerBoosterTypeRepository;

    /**
     * @var SellerGroupTypeInterface
     */
    protected $_sellerGroupTypeInterface;

    /**
     * @var DateTime
     */
    protected $_date;

    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var Email
     */
    protected $_email;

    /**
     * @param SellerGroupRepositoryInterface        $sellerGroupRepository
     * @param ManagerInterface                      $messageManager
     * @param OrderRepositoryInterface              $orderRepository
     * @param SellerGroupHelperData                 $sellerGroupHelperData
     * @param SellerGroupTypeRepositoryInterface    $sellerGroupTypeRepository
     * @param SellerGroupTypeInterface              $sellerGroupTypeInterface
     * @param DateTime                              $date
     * @param MarketplaceHelper                     $marketplaceHelper
     * @param Email                                 $email
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        ManagerInterface $messageManager,
        OrderRepositoryInterface $orderRepository,
        SellerGroupHelperData $sellerGroupHelperData,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerGroupTypeInterface $sellerGroupTypeInterface,
        SellerBoosterTypeRepositoryInterface $sellerBoosterTypeRepository,
        DateTime $date,
        MarketplaceHelper $marketplaceHelper,
        Email $email
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_messageManager = $messageManager;
        $this->_orderRepository = $orderRepository;
        $this->_sellerGroupHelperData = $sellerGroupHelperData;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_sellerGroupTypeInterface = $sellerGroupTypeInterface;
        $this->sellerBoosterTypeRepository = $sellerBoosterTypeRepository;
        $this->_date = $date;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_email = $email;
    }

    /**
     * Sale order invoice save after event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            if ($this->_sellerGroupHelperData->getStatus()) {
                $order = $observer->getEvent()->getOrder();
                $sellerGroupTypeData = $this->_sellerGroupTypeRepository->getByOrderId($order->getId());
                $sellerGroupTypeData->delete();
                $sellerBoosterTypeData = $this->sellerBoosterTypeRepository->getByOrderId($order->getId());
                $sellerBoosterTypeData->delete();
            }
        } catch (\Exception $e) {
            $this->_messageManager->addError($e->getMessage());
        }
    }
}
