<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as SellerProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Framework\UrlInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

class MpSaveProductBeforeObserver implements ObserverInterface
{
    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $_helperData;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerProductCollectionFactory
     */
    protected $_sellerProductCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $_requestInterface;

    /**
     * @var Configurable
     */
    protected $_configurable;

    /**
     * @param \Webkul\MpSellerGroup\Helper\Data $helperData
     * @param \Magento\Customer\Model\Session $customerSession
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param SellerProductCollectionFactory $sellerProductCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param \Magento\Framework\App\RequestInterface $requestInterface
     * @param UrlInterface $urlBuilder
     * @param Configurable $configurable
     */
    public function __construct(
        \Webkul\MpSellerGroup\Helper\Data $helperData,
        \Magento\Customer\Model\Session $customerSession,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        SellerProductCollectionFactory $sellerProductCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        \Magento\Framework\App\RequestInterface $requestInterface,
        UrlInterface $urlBuilder,
        Configurable $configurable
    ) {
        $this->_requestInterface = $requestInterface;
        $this->_urlBuilder = $urlBuilder;
        $this->_helperData = $helperData;
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_customerSession = $customerSession;
        $this->_date = $date;
        $this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_configurable = $configurable;
    }

    /**
     * @param  $groupId
     * @return object
     */
    public function getGroupDataByGroupId($groupId)
    {
        return $this->_sellerGroupRepository->getById($groupId);
    }

    /**
     * product create validate event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $duplicateFlag = false;
        $wholedata = $observer->getEvent()->getData();
        if (!empty($wholedata[0]) && !empty($wholedata[0]['back']) && $wholedata[0]['back']=="duplicate") {
            $duplicateFlag = true;
        }

        if ($this->_helperData->getStatus()) {
            if (!$this->_requestInterface->getParam('id')) {
                $flag = $this->_helperData->getPermission($duplicateFlag);
                if ($flag['status']) {
                    if (!empty($flag['expire']) && $flag['expire']) {
                        $defaultMsg = __(
                            "you can not add more products, your pack is expired. Please pay fee to add more products"
                        );
                        if ($duplicateFlag) {
                            $defaultMsg = __(
                                "you can not perform this action, only one product available. " .
                                "Please pay fee to add more products"
                            );
                        }
                        throw new \Magento\Framework\Exception\LocalizedException($defaultMsg);
                    } else {
                        $defaultMsg = __(
                            "you can not perform this action, Please pay fee to add more products"
                        );
                        throw new \Magento\Framework\Exception\LocalizedException($defaultMsg);
                    }
                }
            } else {
                $flag = $this->_helperData->getPermission(false);
                $productId = $this->_requestInterface->getParam('id');
                $visibility = 0;
                $collection = $this->_productCollectionFactory->create()
                ->addFieldToFilter(
                    'entity_id',
                    $productId
                )->addFieldToSelect(
                    '*'
                );
                foreach ($collection as $key => $value) {
                    $visibility = $value['visibility'];
                }
                if ($flag['status'] && $visibility==1) {
                    if (!empty($this->_configurable->getParentIdsByChild($productId))) {
                        if (!empty($flag['expire']) && $flag['expire']) {
                            $defaultMsg = __(
                                "You can not add more products, 
                                your pack is expired. Please pay fee to add more products"
                            );
                            throw new \Magento\Framework\Exception\LocalizedException($defaultMsg);
                        } else {
                            $defaultMsg = __(
                                "you can not perform this action, Please pay fee to add more products"
                            );
                            throw new \Magento\Framework\Exception\LocalizedException($defaultMsg);
                        }
                    }
                } elseif ($duplicateFlag
                    && $flag['status']
                    && !empty($flag['expire'])
                    && $flag['expire']
                ) {
                    $defaultMsg = __(
                        "you can not add more products, your pack is expired. Please pay fee to add more products"
                    );
                    throw new \Magento\Framework\Exception\LocalizedException($defaultMsg);
                }
            }
        }
    }
}
