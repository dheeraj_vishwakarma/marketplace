<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Observer;

use Magento\Framework\Event\ObserverInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;

class AllowedPaymentMethods implements ObserverInterface
{
    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    protected $_helper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $_checkoutSession;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @param \Webkul\MpSellerGroup\Helper\Data         $helper
     * @param \Magento\Checkout\Model\Session           $checkoutSession
     * @param \Magento\Framework\App\ResponseInterface  $response
     * @param SellerGroupRepositoryInterface            $sellerGroupRepository
     */
    public function __construct(
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Framework\App\ResponseInterface $response,
        SellerGroupRepositoryInterface $sellerGroupRepository
    ) {
        $this->_helper = $helper;
        $this->_checkoutSession = $checkoutSession;
        $this->_response = $response;
        $this->_sellerGroupRepository = $sellerGroupRepository;
    }
    /**
     * Payment method is active.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if ($this->_helper->getStatus()) {
            $paymentMethods = $this->_helper->getAllowedPaymentMethods();
            $paymentMethodArray = explode(',', $paymentMethods);
            $event = $observer->getEvent();
            $method = $event->getMethodInstance();
            $isGroupProduct = false;
            $cartData = $this->_checkoutSession->getQuote()->getAllVisibleItems();
            foreach ($cartData as $item) {
                $productId = $item->getProduct()->getId();
                $sellerGroupData = $this->_sellerGroupRepository->getGroupByProductId(
                    $productId
                );
                if ($sellerGroupData->getId()) {
                    $isGroupProduct = true;
                }
            }
            if (!empty($paymentMethods) && $isGroupProduct) {
                if (!in_array($method->getCode(), $paymentMethodArray)) {
                    $result = $observer->getEvent()->getResult();
                    $result->setData('is_available', false);
                }
            }
        }
    }
}
