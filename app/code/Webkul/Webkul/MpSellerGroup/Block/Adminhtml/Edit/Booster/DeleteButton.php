<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Block\Adminhtml\Edit\Booster;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Webkul\MpSellerGroup\Block\Adminhtml\Edit\GenericButton;

/**
 * Class DeleteButton booster delete button
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $sellerBoosterId = $this->getSellerBoosterId();
        $data = [];
        if ($sellerBoosterId) {
            $data = [
                'label' => __('Delete Booster'),
                'class' => 'delete',
                'id' => 'sellerbooster-edit-delete-button',
                'data_attribute' => [
                    'url' => $this->getDeleteUrl(),
                ],
                'on_click' => '',
                'sort_order' => 20,
            ];
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', ['id' => $this->getSellerBoosterId()]);
    }
}
