<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Block\Adminhtml\Edit\Group;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;
use Webkul\MpSellerGroup\Block\Adminhtml\Edit\GenericButton;

/**
 * Class DeleteButton group delete button
 */
class DeleteButton extends GenericButton implements ButtonProviderInterface
{
    /**
     * @return array
     */
    public function getButtonData()
    {
        $sellerGroupId = $this->getSellerGroupId();
        $data = [];
        if ($sellerGroupId) {
            $data = [
                'label' => __('Delete Group'),
                'class' => 'delete',
                'id' => 'sellergroup-edit-delete-button',
                'data_attribute' => [
                    'url' => $this->getDeleteUrl(),
                ],
                'on_click' => '',
                'sort_order' => 20,
            ];
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getDeleteUrl()
    {
        return $this->getUrl('*/*/delete', ['id' => $this->getSellerGroupId()]);
    }
}
