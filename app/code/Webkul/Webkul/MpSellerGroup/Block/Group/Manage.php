<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Block\Group;

use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\Marketplace\Model\ResourceModel\Product\CollectionFactory as SellerProductCollectionFactory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as ProductCollectionFactory;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Webkul\Marketplace\Model\ControllersRepository;

class Manage extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Magento\Catalog\Block\Product\ListProduct
     */
    protected $listProduct;

    /**
     * @var Session
     */
    protected $_customerSession;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerProductCollectionFactory
     */
    protected $_sellerProductCollectionFactory;

    /**
     * @var ProductCollectionFactory
     */
    protected $_productCollectionFactory;

    /**
     * @var \Magento\Framework\Stdlib\DateTime\DateTime
     */
    protected $_date;

    /**
     * @var ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var ControllersRepository
     */
    protected $_controllersRepository;

    /**
     * @var \Webkul\MpSellerGroup\Helper\Data
     */
    public $helper;
    public $catalogHelper;
   
    /**
     * @param \Magento\Catalog\Block\Product\Context $context
     * @param \Magento\Framework\ObjectManagerInterface $objectManager
     * @param \Magento\Customer\Model\Session $customerSession
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     * @param SellerProductCollectionFactory $sellerProductCollectionFactory
     * @param ProductCollectionFactory $productCollectionFactory
     * @param ProductRepositoryInterface $productRepositoryInterface
     * @param ControllersRepository $controllersRepository
     * @param \Webkul\MpSellerGroup\Helper\Data $helper
     * @param \Magento\Catalog\Helper\Output $catalogHelper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Catalog\Block\Product\ListProduct $listProduct
     * @param array $data
     */
    public function __construct(
        \Magento\Catalog\Block\Product\Context $context,
        \Magento\Framework\ObjectManagerInterface $objectManager,
        \Magento\Customer\Model\Session $customerSession,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        SellerProductCollectionFactory $sellerProductCollectionFactory,
        ProductCollectionFactory $productCollectionFactory,
        ProductRepositoryInterface $productRepositoryInterface,
        ControllersRepository $controllersRepository,
        \Webkul\MpSellerGroup\Helper\Data $helper,
        \Magento\Catalog\Helper\Output $catalogHelper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Block\Product\ListProduct $listProduct,
        array $data = []
    ) {
        $this->_objectManager = $objectManager;
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->_customerSession = $customerSession;
        $this->_date = $date;
        $this->_sellerProductCollectionFactory = $sellerProductCollectionFactory;
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->_productRepository = $productRepositoryInterface;
        $this->_controllersRepository = $controllersRepository;
        $this->helper = $helper;
        $this->catalogHelper = $catalogHelper;
        $this->messageManager = $messageManager;
        $this->listProduct = $listProduct;
        parent::__construct(
            $context,
            $data
        );
    }

    protected function _construct()
    {
        parent::_construct();
        $this->pageConfig->getTitle()->set(__('Manage Seller Membership'));
    }

    public function helperData()
    {
        return $this->helper;
    }

    public function catalogHelper()
    {
        return $this->catalogHelper;
    }

    public function getActiveGroupLists()
    {
        return $this->_sellerGroupRepository->getActiveList();
    }

    public function getGroupLists()
    {
        return $this->_sellerGroupRepository->getList();
    }

    public function getCustomerId()
    {
        return $this->_customerSession->getCustomerId();
    }

    public function getRequiredData()
    {
        $data = [];
        $data['seller_id'] = $this->helper->getSellerId();
        $data['seller_email'] = $this->_customerSession->getCustomer()->getEmail();
        $data['firstname'] = $this->_customerSession->getCustomer()->getFirstname();
        $data['lastname'] = $this->_customerSession->getCustomer()->getLastname();

        return $data;
    }

    public function getGroupDataByGroupId($groupId)
    {
        return $this->_sellerGroupRepository->getById($groupId);
    }

    public function getSellerGroupData()
    {
        $sellerId = $this->helper->getSellerId();
        $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($sellerId);
        if ($getSellerGroup) {
            return $getSellerGroup;
        } else {
            return [];
        }
    }

    /**
     * isRenewvalRequired Return flag for seller's group active or not.
     *
     * @return Boolean $flag Group active or not flag
     */
    public function isRenewvalRequired()
    {
        
        $sellerId = $this->helper->getSellerId();
        $getSellerGroup = $this->getSellerGroupData();
        $flag = false;
        $today = $this->_date->gmtDate();
        if ($getSellerGroup && $getSellerGroup->getExpiredStatus()==0) {
            if ($getSellerGroup->getTransactionStatus() == 'pending') {
                return true;
            }
            if ($getSellerGroup->getCheckType() == 1 || $getSellerGroup->getCheckType() == 2) {
                if ($getSellerGroup->getCheckType() == 2
                    && $getSellerGroup->getRemainingProducts()>=$getSellerGroup->getNoOfProducts()
                ) {
                    return false;
                }
                if (strtotime($getSellerGroup->getExpiredOn()) > strtotime($today)) {
                    $flag = true;
                }
            } else {
                $allowqty = $getSellerGroup->getNoOfProducts();
                $createdAt = $getSellerGroup->getTransactionDate();
                $products = $this->_sellerProductCollectionFactory->create()
                ->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
                $marketplaceProductIds = [];
                foreach ($products as $product) {
                    $marketplaceProductIds[] = $product->getMageproductId();
                }
                $collection = $this->_productCollectionFactory->create()
                ->addFieldToFilter(
                    'created_at',
                    ['datetime' => true, 'from' => $createdAt]
                )->addFieldToFilter(
                    'entity_id',
                    ['in' => $marketplaceProductIds]
                );
                if ($allowqty > $collection->getSize()) {
                    $flag = true;
                }
            }
        }

        return $flag;
    }

    public function getProductById($productId)
    {
        return $this->_productRepository->getById($productId);
    }

    public function getAllowedDefaultControllers()
    {
        $allowedModuleArr=[];
        $controllersList = $this->_controllersRepository->getList();
        foreach ($controllersList as $key => $value) {
            array_push($allowedModuleArr, $value['label']);
        }
        return $allowedModuleArr;
    }

    public function getAllowedControllersBySetData($allowedModule)
    {
        $allowedModuleArr=[];
        if ($allowedModule && $allowedModule!='all') {
            $allowedModuleControllers = explode(',', $allowedModule);
            foreach ($allowedModuleControllers as $key => $value) {
                $controllersList = $this->_controllersRepository->getByPath($value);
                foreach ($controllersList as $key => $value) {
                    if ($this->helper->isModuleInstalled($value['module_name'])) {
                        array_push($allowedModuleArr, $value['label']);
                    }
                }
            }
        } else {
            $controllersList = $this->_controllersRepository->getList();
            foreach ($controllersList as $key => $value) {
                if ($this->helper->isModuleInstalled($value['module_name'])) {
                    array_push($allowedModuleArr, $value['label']);
                }
            }
        }
        return $allowedModuleArr;
    }

    public function getImage($product, $image)
    {
        return $this->listProduct->getImage($product, $image);
    }

    public function getAddToCartPostParams($product)
    {
        return $this->listProduct->getAddToCartPostParams($product);
    }
}
