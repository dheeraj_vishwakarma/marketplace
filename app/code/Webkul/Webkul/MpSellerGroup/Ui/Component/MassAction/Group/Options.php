<?php
/**
 * Webkul Software.
 *
 * @category Marketplace
 * @package  Webkul_MpSellerGroup
 * @author   Webkul
 * @license  https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Ui\Component\MassAction\Group;

use Magento\Framework\UrlInterface;
use Zend\Stdlib\JsonSerializable;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;

/**
 * Class Options massAction opptions
 */
class Options implements JsonSerializable
{
    /**
     * @var array
     */
    protected $_options;

    /**
     * @var SellerGroupRepositoryInterface
     */
    protected $_sellerGroupRepository;

    /**
     * Additional options params
     *
     * @var array
     */
    protected $_data;

    /**
     * @var UrlInterface
     */
    protected $_urlBuilder;

    /**
     * Base URL for subactions
     *
     * @var string
     */
    protected $_urlPath;

    /**
     * Param name for subactions
     *
     * @var string
     */
    protected $_paramName;

    /**
     * Additional params for subactions
     *
     * @var array
     */
    protected $_additionalData = [];

    /**
     * Constructor
     *
     * @param SellerGroupRepositoryInterface $sellerGroupRepository
     * @param UrlInterface                   $urlBuilder
     * @param array                          $data
     */
    public function __construct(
        SellerGroupRepositoryInterface $sellerGroupRepository,
        UrlInterface $urlBuilder,
        array $data = []
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_data = $data;
        $this->_urlBuilder = $urlBuilder;
    }

    /**
     * Get action options
     *
     * @return array
     */
    public function jsonSerialize()
    {
        if ($this->_options === null) {
            $options = $this->_sellerGroupRepository->getActiveList();
            $this->prepareData();
            if (!empty($options)) {
                foreach ($options as $optionCode) {
                    $this->_options[$optionCode['group_code']] = [
                        'type' => 'seller_group_' . $optionCode['group_code'],
                        'label' => $optionCode['group_name'],
                    ];
                    if ($this->_urlPath && $this->_paramName) {
                        $this->_options[$optionCode['group_code']]['url'] = $this->_urlBuilder->getUrl(
                            $this->_urlPath,
                            [$this->_paramName => $optionCode['group_code']]
                        );
                    }
                    $this->_options[$optionCode['group_code']] = array_merge_recursive(
                        $this->_options[$optionCode['group_code']],
                        $this->_additionalData
                    );
                }
                if (!empty($this->_options)) {
                    $this->_options = array_values($this->_options);
                }
            }
        }
        return $this->_options;
    }

    /**
     * Prepare addition data for subactions
     *
     * @return void
     */
    protected function prepareData()
    {
        foreach ($this->_data as $key => $value) {
            switch ($key) {
                case 'urlPath':
                    $this->_urlPath = $value;
                    break;
                case 'paramName':
                    $this->_paramName = $value;
                    break;
                default:
                    $this->_additionalData[$key] = $value;
                    break;
            }
        }
    }
}
