<?php
/**
 * Webkul Software.
 *
 * @category Marketplace
 * @package  Webkul_MpSellerGroup
 * @author   Webkul
 * @license  https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Webkul\MpSellerGroup\Api\SellerGroupRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;

class AssignedGroup extends \Magento\Ui\Component\Listing\Columns\Column
{
    /**
     * @var SellerGroupRepository
     */
    protected $_sellerGroupRepository;

    /**
     * @var SellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @param ContextInterface                   $context
     * @param UiComponentFactory                 $uiComponentFactory
     * @param SellerGroupRepositoryInterface     $sellerGroupRepository
     * @param SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository
     * @param array                              $components
     * @param array                              $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        SellerGroupRepositoryInterface $sellerGroupRepository,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        array $components = [],
        array $data = []
    ) {
        $this->_sellerGroupRepository = $sellerGroupRepository;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source.
     *
     * @param array $dataSource
     *
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($item['seller_id']);
                $groupId = $getSellerGroup->getGroupId();
                $getGroup = $this->_sellerGroupRepository->getById($groupId);
                if ($getGroup) {
                    $item[$fieldName] = $getGroup['group_name'];
                } else {
                    $item[$fieldName] = __('None');
                }
            }
        }

        return $dataSource;
    }
}
