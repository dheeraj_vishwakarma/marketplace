<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\ViewModel;

use Webkul\Marketplace\Helper\Data as MpHelper;
use Webkul\MpSellerGroup\Helper\Data as MpgroupHelper;
use Magento\Framework\Json\Helper\Data as JsonHelper;

/**
 * MpSellerGroup View Model
 */
class MpSellerGroup implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    /**
     * @var JsonHelper
     */
    protected $jsonHelper;

    /**
     * @var MpHelper
     */
    protected $mpHelper;

    /**
     * @var MpgroupHelper
     */
    protected $mpgroupHelper;

    /**
     * @param CustomOrderHelper $customOrderHelper
     * @param GiftMessageHelper $giftMessageHelper
     * @param JsonHelper $jsonHelper
     */
    public function __construct(
        MpHelper $mpHelper,
        MpgroupHelper $mpgroupHelper,
        JsonHelper $jsonHelper
    ) {
        $this->mpHelper = $mpHelper;
        $this->mpgroupHelper = $mpgroupHelper;
        $this->jsonHelper = $jsonHelper;
    }

    /**
     * Get Marketplace Data Helper
     *
     * @return object \Webkul\Marketplace\Helper\Data
     */
    public function getMpHelper()
    {
        return $this->mpHelper;
    }

    /**
     * Get MpSellerGroup Data Helper
     *
     * @return object \Webkul\MpSellerGroup\Helper\Data
     */
    public function getMpgroupHelper()
    {
        return $this->mpgroupHelper;
    }

    /**
     * Get Json Helper
     *
     * @return object \Magento\Framework\Json\Helper\Data
     */
    public function getJsonHelper()
    {
        return $this->jsonHelper;
    }
}
