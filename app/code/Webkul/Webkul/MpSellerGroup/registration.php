<?php
/**
 * Webkul Software
 *
 * @category Marketplace
 * @package  Webkul_MpSellerGroup
 * @author   Webkul
 * @license  https://store.webkul.com/license.html
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Webkul_MpSellerGroup',
    __DIR__
);
