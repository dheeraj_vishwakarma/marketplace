<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Model\AbstractModel;
use Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * SellerGroup Booster Model.
 *
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster _getResource()
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster getResource()
 */
class SellerGroupBooster extends AbstractModel implements SellerGroupBoosterInterface, IdentityInterface
{
    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**#@+
     * Feedback's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Marketplace SellerGroup cache tag.
     */
    const CACHE_TAG = 'marketplace_sellergroup_booster';

    /**
     * @var string
     */
    protected $_cacheTag = 'marketplace_sellergroup_booster';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'marketplace_sellergroup_booster';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster::class);
    }

    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteSellerGroupBooster();
        }

        return parent::load($id, $field);
    }

    /**
     * Load No-Route SellerGroupBooster.
     *
     * @return \Webkul\MpSellerGroup\Model\SellerGroupBooster
     */
    public function noRouteSellerGroupBooster()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Prepare group's statuses.
     * Available event mpsellergroup_sellergroup_booster_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Booster Name.
     *
     * @return int|null
     */
    public function getBoosterName()
    {
        return $this->_getData(self::BOOSTER_NAME);
    }

    /**
     * Set Booster Name.
     *
     * @param int $boosterName
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setBoosterName($boosterName)
    {
        return $this->setData(self::BOOSTER_NAME, $boosterName);
    }

    /**
     * Get Booster Code.
     *
     * @return int|null
     */
    public function getBoosterCode()
    {
        return $this->_getData(self::BOOSTER_CODE);
    }

    /**
     * Set Booster Code.
     *
     * @param int $boosterCode
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setBoosterCode($boosterCode)
    {
        return $this->_getData(self::BOOSTER_CODE, $boosterCode);
    }

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount()
    {
        return $this->_getData(self::FEE_AMOUNT);
    }

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupBoosterInterface
     */
    public function setFeeAmount($feeAmount)
    {
        return $this->_getData(self::FEE_AMOUNT, $feeAmount);
    }

    /**
     * Get Group status.
     *
     * @return int
     */
    public function getStatus()
    {
        $status = $this->_getData(self::STATUS);

        return $status !== null ? $status : self::STATUS_ENABLED;
    }

    /**
     * Get Group status.
     *
     * @return int
     */
    public function setStatus($status)
    {
        $status = $this->_getData(self::STATUS);

        return $status !== null ? $status : self::STATUS_ENABLED;
    }

    /**
     * Get Group Booster creation date.
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set Group Booster created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get Group Booster update date.
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set Group Booster updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get Group Booster Image.
     *
     * @return string
     */
    public function getBoosterImage()
    {
        return $this->_getData(self::BOOSTER_IMAGE);
    }

    /**
     * Set Group Booster Image.
     *
     * @param string $boosterImage
     *
     * @return $this
     */
    public function setBoosterImage($boosterImage)
    {
        return $this->setData(self::BOOSTER_IMAGE, $boosterImage);
    }

    /**
     * Get Group Booster Color.
     *
     * @return string
     */
    public function getBoosterColor()
    {
        return $this->_getData(self::BOOSTER_COLOR);
    }

    /**
     * Set Group Booster Color.
     *
     * @param string $boosterColor
     *
     * @return $this
     */
    public function setBoosterColor($boosterColor)
    {
        return $this->setData(self::BOOSTER_COLOR, $boosterColor);
    }

    /**
     * Group Booster Sort Order.
     *
     * @return string|null
     */
    public function getSortOrder()
    {
        return $this->_getData(self::SORT_ORDER);
    }

    /**
     * Set group booster Sort Order.
     *
     * @param string $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }
}
