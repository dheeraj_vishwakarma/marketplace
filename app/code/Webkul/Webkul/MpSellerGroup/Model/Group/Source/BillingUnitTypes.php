<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

/**
 * Used in seller group billing period unit type.
 */
namespace Webkul\MpSellerGroup\Model\Group\Source;

use Magento\Framework\Data\OptionSourceInterface;

class BillingUnitTypes implements OptionSourceInterface
{
    /**
     * Options getter.
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::DAY,
                'label' => __('DAY'),
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::WEEK,
                'label' => __('WEEK'),
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::MONTH,
                'label' => __('MONTH'),
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\SellerGroup::YEAR,
                'label' => __('YEAR'),
            ]
        ];
    }
}
