<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Marketplace
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\ResourceModel\Seller\Grid;

use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Search\AggregationInterface;
use Webkul\Marketplace\Model\ResourceModel\Seller\Collection as SellerCollection;

/**
 * Class Collection
 * Collection for displaying grid of marketplace seller
 */
class Collection extends \Webkul\Marketplace\Model\ResourceModel\Seller\Grid\Collection
{
    public function __construct(
        \Magento\Framework\Data\Collection\EntityFactoryInterface $entityFactoryInterface,
        \Psr\Log\LoggerInterface $loggerInterface,
        \Magento\Framework\Data\Collection\Db\FetchStrategyInterface $fetchStrategyInterface,
        \Magento\Framework\Event\ManagerInterface $eventManagerInterface,
        \Magento\Store\Model\StoreManagerInterface $storeManagerInterface,
        $mainTable,
        $eventPrefix,
        $eventObject,
        $resourceModel,
        $model = \Magento\Framework\View\Element\UiComponent\DataProvider\Document::class,
        $connection = null,
        \Magento\Framework\Model\ResourceModel\Db\AbstractDb $resource = null,
        \Webkul\MpSellerGroup\Model\SellerGroupTypeRepository $sellerGroupTypeRepository,
        \Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory $sellers
    ) {
        parent::__construct(
            $entityFactoryInterface,
            $loggerInterface,
            $fetchStrategyInterface,
            $eventManagerInterface,
            $storeManagerInterface,
            $mainTable,
            $eventPrefix,
            $eventObject,
            $resourceModel,
            $model,
            $connection,
            $resource
        );
        $this->sellers = $sellers;
        $this->sellerGroupTypeRepository = $sellerGroupTypeRepository;
    }

    protected function _renderFiltersBefore()
    {
        $groupTable = $this->getTable('marketplace_sellergroup_type');
        $groupTypeTable = $this->getTable('marketplace_sellergroup');
        $this->getSelect()->joinLeft(
            $groupTable.' as sgt',
            'main_table.seller_id = sgt.seller_id',
            [
                'group_id' => 'sgt.group_id'
                
            ]
        )
        ->joinLeft(
            $groupTypeTable.' as sg',
            'sgt.group_id = sg.entity_id',
            [
                'group_code' => 'sg.group_name'
            ]
        );

        parent::_renderFiltersBefore();
    }
}
