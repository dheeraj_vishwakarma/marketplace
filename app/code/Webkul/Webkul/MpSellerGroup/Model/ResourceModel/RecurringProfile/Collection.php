<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile;

use \Webkul\MpSellerGroup\Model\ResourceModel\AbstractCollection;

/**
 * Webkul MpSellerGroup ResourceModel RecurringProfile collection
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Webkul\MpSellerGroup\Model\RecurringProfile::class,
            \Webkul\MpSellerGroup\Model\ResourceModel\RecurringProfile::class
        );
        $this->_map['fields']['entity_id'] = 'main_table.profile_id';
    }
   
    /**
     * Add filter by store
     *
     * @param int|array|\Magento\Store\Model\Store $store
     * @param bool $withAdmin
     * @return $this
     */
    public function addStoreFilter($store, $withAdmin = true)
    {
        if (!$this->getFlag('store_filter_added')) {
            $this->performAddStoreFilter($store, $withAdmin);
        }
        return $this;
    }
}
