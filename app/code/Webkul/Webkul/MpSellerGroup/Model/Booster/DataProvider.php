<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Booster;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\Session\SessionManagerInterface;
use Webkul\MpSellerGroup\Model\ResourceModel\SellerGroupBooster\CollectionFactory as SellerBoosterCollectionFactory;

/**
 * Class DataProvider data provider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{
    /**
     * @var Collection
     */
    protected $collection;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var SessionManagerInterface
     */
    protected $session;

    /**
     * Constructor.
     *
     * @param string                        $name
     * @param string                        $primaryFieldName
     * @param string                        $requestFieldName
     * @param SellerBoosterCollectionFactory  $sellerBoosterCollectionFactory
     * @param array                         $meta
     * @param array                         $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        SellerBoosterCollectionFactory $sellerBoosterCollectionFactory,
        array $meta = [],
        array $data = []
    ) {
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->collection = $sellerBoosterCollectionFactory->create();
        $this->collection->addFieldToSelect('*');
    }

    /**
     * Get session object.
     *
     * @return SessionManagerInterface
     */
    protected function getSession()
    {
        if ($this->session === null) {
            $this->session = ObjectManager::getInstance()->get(
                \Magento\Framework\Session\SessionManagerInterface::class
            );
        }

        return $this->session;
    }

    /**
     * Get data.
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        /** @var Seller Group $group */
        foreach ($items as $booster) {
            $result['booster'] = $booster->getData();
            $this->loadedData[$booster->getId()] = $result;
        }

        $data = $this->getSession()->getSellerBoosterFormData();
        if (!empty($data)) {
            $boosterId = isset($data['mpsellergroup_booster']['entity_id'])
            ? $data['mpsellergroup_booster']['entity_id'] : null;
            $this->loadedData[$boosterId] = $data;
            $this->getSession()->unsSellerBoosterFormData();
        }

        return $this->loadedData;
    }
}
