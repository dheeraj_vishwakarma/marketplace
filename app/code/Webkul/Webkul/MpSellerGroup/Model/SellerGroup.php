<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model;

use Magento\Framework\Model\AbstractModel;
use Webkul\MpSellerGroup\Api\Data\SellerGroupInterface;
use Magento\Framework\DataObject\IdentityInterface;

/**
 * SellerGroup Model.
 *
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup _getResource()
 * @method \Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup getResource()
 */
class SellerGroup extends AbstractModel implements SellerGroupInterface, IdentityInterface
{
    /**
     * No route page id.
     */
    const NOROUTE_ENTITY_ID = 'no-route';

    /**#@+
     * Feedback's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Check types.
     */
    const ONLY_NUMBER_OF_PRODUCTS = 0;

    const ONLY_TIME = 1;

    const TIME_AND_NUMBER_OF_PRODUCTS = 2;

    /**
     * Check types.
     */
    const MEMBERSHIP_TYPE_ONE_TIME = 0;

    const MEMBERSHIP_TYPE_SUBSCRIPTION = 1;

    /**
     * Billing period unit types.
     */
    const DAY = 1;

    const WEEK = 2;

    const TWO_WEEKS = 3;

    const MONTH = 4;

    const YEAR = 5;

    /**
     * Check subscription types.
     */
    const SUBSCRIPTION_ENABLE = 1;

    const SUBSCRIPTION_DISABLE = 0;

    /**
     * Marketplace SellerGroup cache tag.
     */
    const CACHE_TAG = 'marketplace_sellergroup';

    /**
     * @var string
     */
    protected $_cacheTag = 'marketplace_sellergroup';

    /**
     * Prefix of model events names.
     *
     * @var string
     */
    protected $_eventPrefix = 'marketplace_sellergroup';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Webkul\MpSellerGroup\Model\ResourceModel\SellerGroup::class);
    }

    /**
     * Load object data.
     *
     * @param int|null $id
     * @param string   $field
     *
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteSellerGroup();
        }

        return parent::load($id, $field);
    }

    /**
     * Load No-Route SellerGroup.
     *
     * @return \Webkul\MpSellerGroup\Model\SellerGroup
     */
    public function noRouteSellerGroup()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }

    /**
     * Prepare group's statuses.
     * Available event mpsellergroup_sellergroup_get_available_statuses to customize statuses.
     *
     * @return array
     */
    public function getAvailableStatuses()
    {
        return [self::STATUS_ENABLED => __('Enabled'), self::STATUS_DISABLED => __('Disabled')];
    }

    /**
     * Get identities.
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG.'_'.$this->getId()];
    }

    /**
     * Get ID.
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID.
     *
     * @param int $id
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }

    /**
     * Get Group Name.
     *
     * @return int|null
     */
    public function getGroupName()
    {
        return $this->_getData(self::GROUP_NAME);
    }

    /**
     * Set Group Name.
     *
     * @param int $groupName
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setGroupName($groupName)
    {
        return $this->setData(self::GROUP_NAME, $groupName);
    }

    /**
     * Get Group Code.
     *
     * @return int|null
     */
    public function getGroupCode()
    {
        return $this->_getData(self::GROUP_CODE);
    }

    /**
     * Set Group Code.
     *
     * @param int $groupCode
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setGroupCode($groupCode)
    {
        return $this->_getData(self::GROUP_CODE, $groupCode);
    }

    /**
     * Get Number of products.
     *
     * @return int|null
     */
    public function getNoOfProducts()
    {
        return $this->_getData(self::NO_OF_PRODUCTS);
    }

    /**
     * Set Number of products.
     *
     * @param int $noOfProducts
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setNoOfProducts($noOfProducts)
    {
        return $this->_getData(self::NO_OF_PRODUCTS, $noOfProducts);
    }

    /**
     * Get Time Periods.
     *
     * @return int|null
     */
    public function getTimePeriods()
    {
        return $this->_getData(self::TIME_PERIODS);
    }

    /**
     * Set Time Periods.
     *
     * @param int $timePeriods
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setTimePeriods($timePeriods)
    {
        return $this->_getData(self::TIME_PERIODS, $timePeriods);
    }

    /**
     * Get Fee Amount.
     *
     * @return int|null
     */
    public function getFeeAmount()
    {
        return $this->_getData(self::FEE_AMOUNT);
    }

    /**
     * Set Fee Amount.
     *
     * @param int $feeAmount
     *
     * @return \Webkul\MpSellerGroup\Api\Data\SellerGroupInterface
     */
    public function setFeeAmount($feeAmount)
    {
        return $this->_getData(self::FEE_AMOUNT, $feeAmount);
    }

    /**
     * Get Group status.
     *
     * @return int
     */
    public function getStatus()
    {
        $status = $this->_getData(self::STATUS);

        return $status !== null ? $status : self::STATUS_ENABLED;
    }

    /**
     * Get Group status.
     *
     * @return int
     */
    public function setStatus($status)
    {
        $status = $this->_getData(self::STATUS);

        return $status !== null ? $status : self::STATUS_ENABLED;
    }

    /**
     * Get Group creation date.
     *
     * @return string
     */
    public function getCreatedAt()
    {
        return $this->_getData(self::CREATED_AT);
    }

    /**
     * Set Group created date.
     *
     * @param string $createdAt
     *
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    /**
     * Get Group update date.
     *
     * @return string
     */
    public function getUpdatedAt()
    {
        return $this->_getData(self::UPDATED_AT);
    }

    /**
     * Set Group updated date.
     *
     * @param string $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt($updatedAt)
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    /**
     * Get Group Image.
     *
     * @return string
     */
    public function getGroupImage()
    {
        return $this->_getData(self::GROUP_IMAGE);
    }

    /**
     * Set Group Image.
     *
     * @param string $groupImage
     *
     * @return $this
     */
    public function setGroupImage($groupImage)
    {
        return $this->setData(self::GROUP_IMAGE, $groupImage);
    }

    /**
     * Get Group Image.
     *
     * @return string
     */
    public function getGroupColor()
    {
        return $this->_getData(self::GROUP_COLOR);
    }

    /**
     * Set Group Image.
     *
     * @param string $groupColor
     *
     * @return $this
     */
    public function setGroupColor($groupColor)
    {
        return $this->setData(self::GROUP_COLOR, $groupColor);
    }

    /**
     * Get Group Type.
     *
     * @return string
     */
    public function getGroupType()
    {
        return $this->_getData(self::GROUP_TYPE);
    }

    /**
     * Set Group Type.
     *
     * @param string $groupType
     *
     * @return $this
     */
    public function setGroupType($groupType)
    {
        return $this->setData(self::GROUP_TYPE, $groupType);
    }

    /**
     * Group Check type.
     *
     * @return string|null
     */
    public function getCheckType()
    {
        return $this->_getData(self::CHECK_TYPE);
    }

    /**
     * Set group check type.
     *
     * @param string $checkType
     *
     * @return $this
     */
    public function setCheckType($checkType)
    {
        return $this->setData(self::CHECK_TYPE, $groupType);
    }

    /**
     * Group Sort Order.
     *
     * @return string|null
     */
    public function getSortOrder()
    {
        return $this->_getData(self::SORT_ORDER);
    }

    /**
     * Set group Sort Order.
     *
     * @param string $sortOrder
     *
     * @return $this
     */
    public function setSortOrder($sortOrder)
    {
        return $this->setData(self::SORT_ORDER, $sortOrder);
    }
}
