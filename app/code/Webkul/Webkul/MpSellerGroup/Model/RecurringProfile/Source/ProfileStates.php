<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\RecurringProfile\Source;

use Magento\Framework\Data\OptionSourceInterface;

/**
 * Class ProfileStates Profile States
 */
class ProfileStates implements OptionSourceInterface
{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            [
                'value' => \Webkul\MpSellerGroup\Model\RecurringProfile::STATE_PENDING,
                'label'=>__('Pending')
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\RecurringProfile::STATE_ACTIVE,
                'label'=>__('Active')
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\RecurringProfile::STATE_SUSPENDED,
                'label'=>__('Suspended')
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\RecurringProfile::STATE_CANCELLED,
                'label'=>__('Cancelled')
            ],
            [
                'value' => \Webkul\MpSellerGroup\Model\RecurringProfile::STATE_EXPIRED,
                'label'=>__('Expired')
            ]
        ];
    }
}
