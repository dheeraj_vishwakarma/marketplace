<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Model\Plugin\AttributeSet;

class Options
{
    /**
     * @param  Magento\Catalog\Ui\DataProvider\Produc\Form\Modifier\AttributeSet $subject
     * @param  object                                                            $result
     * @return array
     */
    public function afterGetOptions(
        \Magento\Catalog\Ui\DataProvider\Product\Form\Modifier\AttributeSet $subject,
        $result
    ) {
    
        $attributeset = [];
        foreach ($result as $set) {
            if ($set['label'] != 'Mp Seller Group') {
                array_push($attributeset, $set);
            }
        }
        return $attributeset;
    }
}
