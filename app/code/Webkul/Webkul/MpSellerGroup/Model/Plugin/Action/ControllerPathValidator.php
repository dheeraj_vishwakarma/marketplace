<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpSellerGroup\Model\Plugin\Action;

use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session as CustomerSession;
use Webkul\MpSellerGroup\Api\SellerGroupTypeRepositoryInterface;
use Webkul\MpSellerGroup\Api\SellerBoosterTypeRepositoryInterface;
use Webkul\Marketplace\Model\ControllersRepository;
use Webkul\Marketplace\Helper\Data as MarketplaceHelper;
use Webkul\MpSellerGroup\Helper\Data as SellerGroupHelper;
use Webkul\Marketplace\Model\ResourceModel\Seller\CollectionFactory;

/**
 * MpSellerGroup Action dispatch Plugin
 */
class ControllerPathValidator
{
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_url;

    /**
     * @var \Magento\Framework\App\ResponseInterface
     */
    protected $_response;

    /**
     * @var CustomerSession
     */
    protected $_customerSession;

    /**
     * @var sellerGroupTypeRepository
     */
    protected $_sellerGroupTypeRepository;

    /**
     * @var SellerBoosterTypeRepositoryInterface
     */
    protected $boosterTypeRepository;

    /**
     * @var ControllersRepository
     */
    protected $_controllersRepository;

    /**
     * @var MarketplaceHelper
     */
    protected $_marketplaceHelper;

    /**
     * @var SellerGroupHelper
     */
    protected $_sellerGroupHelper;

    /**
     * @var CollectionFactory
     */
    protected $_sellerCollection;

    /**
     * @var Webkul\Marketplace\Model\Product
     */
    protected $product;

    /**
     * @param Context                               $context
     * @param CustomerSession                       $customerSession
     * @param SellerGroupTypeRepositoryInterface    $sellerGroupTypeRepository
     * @param ControllersRepository                 $controllersRepository
     * @param MarketplaceHelper                     $marketplaceHelper
     * @param SellerGroupHelper                     $sellerGroupHelper
     * @param CollectionFactory                     $sellerCollection
     */
    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        SellerGroupTypeRepositoryInterface $sellerGroupTypeRepository,
        SellerBoosterTypeRepositoryInterface $boosterTypeRepository,
        ControllersRepository $controllersRepository,
        MarketplaceHelper $marketplaceHelper,
        SellerGroupHelper $sellerGroupHelper,
        CollectionFactory $sellerCollection,
        \Webkul\Marketplace\Model\ProductFactory $product
    ) {
        $this->_url = $context->getUrl();
        $this->_response = $context->getResponse();
        $this->_customerSession = $customerSession;
        $this->_sellerGroupTypeRepository = $sellerGroupTypeRepository;
        $this->boosterTypeRepository = $boosterTypeRepository;
        $this->_controllersRepository = $controllersRepository;
        $this->_marketplaceHelper = $marketplaceHelper;
        $this->_sellerGroupHelper = $sellerGroupHelper;
        $this->_sellerCollection = $sellerCollection;
        $this->product = $product;
    }

    /**
     * @param  $request
     * @param  $controllerPath
     * @return int
     */
    public function getSellerId($request, $controllerPath)
    {
        $sellerId = $this->_customerSession->getCustomerId();
        if ($sellerId) {
            return $sellerId;
        } elseif (!empty($request->getParam('shop'))) {
            $shopUrl = '';
            if ($controllerPath == 'marketplace/seller/profile') {
                $shopUrl = $this->_marketplaceHelper->getProfileUrl();
            }
            if ($controllerPath == 'marketplace/seller/collection') {
                $shopUrl = $this->_marketplaceHelper->getCollectionUrl();
            }
            if ($controllerPath == 'marketplace/seller/feedback') {
                $shopUrl = $this->_marketplaceHelper->getFeedbackUrl();
            }
            if ($controllerPath == 'marketplace/seller/location') {
                $shopUrl = $this->_marketplaceHelper->getLocationUrl();
            }
            if (!$shopUrl) {
                $shopUrl = $request->getParam('shop');
            }
            if ($shopUrl) {
                $data = $this->_sellerCollection->create()
                ->addFieldToFilter('shop_url', $shopUrl);
                foreach ($data as $seller) {
                    $sellerId = $seller->getSellerId();
                }
            }
        }
        return $sellerId;
    }

    /**
     * @param  $sellerId
     * @return array
     */

    public function getSellerGroupData($sellerId)
    {
        $getSellerGroup = $this->_sellerGroupTypeRepository->getBySellerId($sellerId);
        if (!empty($getSellerGroup->getData())) {
            return $getSellerGroup;
        } else {
            return [];
        }
    }

    /**
     * Get All Functionalities Data
     *
     * @param string $allowedModuleFunctonalities
     * @param object $sellerBoosterTypes
     * @return string
     */
    public function getAllFunctionalitiesData($allowedModuleFunctonalities, $sellerBoosterTypes)
    {
        $boosterFunctionalities = '';
        foreach ($sellerBoosterTypes as $boosterType) {
            $boosterFunctionalities = $boosterFunctionalities.",".$boosterType['allowed_modules_functionalities'];
        }
        $allowedModuleFunctonalities = $allowedModuleFunctonalities .$boosterFunctionalities;
        return $allowedModuleFunctonalities;
    }

    /**
     * Redirect to customer page if action is not allowed in the current group
     *
     * @param  $request
     * @return void
     */
    public function beforeDispatch(\Magento\Framework\App\Action\Action $dispatch, $request)
    {
        if ($this->_sellerGroupHelper->getStatus() && $this->_customerSession->isLoggedIn()) {
            $helper = $this->_marketplaceHelper;
            $controllerPath = str_replace('_', '/', $request->getFullActionName());
            $sellerId = $this->getSellerId($request, $controllerPath);
            $getSellerTypeGroup = $this->getSellerGroupData($sellerId);

            $url = $this->_url->getUrl('mpsellergroup/group/manage/');

            /*For Product Attribute Controllers */
            if (strpos($controllerPath, 'marketplace/product/attribute') !== false) {
                $controllerPath = 'marketplace/product_attribute/new';
            }
            /*For Product's Controllers */
            if (strpos($controllerPath, 'marketplace/product/') !== false) {
                $controllerPath = 'marketplace/product/add';
            }
            /*For Transaction's Controllers */
            if (strpos($controllerPath, 'marketplace/transaction') !== false) {
                $controllerPath = 'marketplace/transaction/history';
            }
            /*For Order's Controllers */
            if (strpos($controllerPath, 'marketplace/order') !== false) {
                $controllerPath = 'marketplace/order/history';
            }

            $allModuleArr = [];
            $controllersList = $this->_controllersRepository->getList();
            foreach ($controllersList as $key => $value) {
                array_push($allModuleArr, $value['controller_path']);
            }

            if (!empty($getSellerTypeGroup)) {
                $allowedModuleFunctonalities = $getSellerTypeGroup['allowed_modules_functionalities'];
                $sellerBoosterTypes = $this->boosterTypeRepository->getCollectionBySellerId($sellerId, true, 'paid');
                if (!empty($sellerBoosterTypes->getData())) {
                    $allowedModuleFunctonalities = $this->getAllFunctionalitiesData(
                        $allowedModuleFunctonalities,
                        $sellerBoosterTypes
                    );
                }

                $allowedModuleArr = $helper->getAllowedControllersBySetData(
                    $allowedModuleFunctonalities
                );
                if (in_array($controllerPath, $allModuleArr, true)) {
                    $transactionStatus = $getSellerTypeGroup['transaction_status'];
                    if ($transactionStatus == 'pending' || $transactionStatus == '') {
                        $this->_response->setRedirect($url);
                        $this->_response->sendResponse();
                    } elseif (!in_array($controllerPath, $allowedModuleArr, true)) {
                        $this->_response->setRedirect($url);
                        $this->_response->sendResponse();
                    }
                }
            } elseif (!$this->_sellerGroupTypeRepository->getBySellerCount($sellerId)) {
                $products = $this->product->create()->getCollection()->addFieldToFilter(
                    'seller_id',
                    $sellerId
                );
                $getDefaultGroupStatus = $this->_sellerGroupHelper->getDefaultGroupStatus();
                if ($getDefaultGroupStatus) {
                    $allowqty = $this->_sellerGroupHelper->getDefaultProductAllowed();
                    if (count($products)>=$allowqty) {
                        $allowFunctionalities = explode(
                            ',',
                            $this->_sellerGroupHelper->getDefaultAllowedFeatures()
                        );
                        if (in_array($controllerPath, $allModuleArr, true)) {
                                $this->_response->setRedirect($url);
                                $this->_response->sendResponse();
                        }
                    }
                } else {
                    if (in_array($controllerPath, $allModuleArr, true)) {
                        $this->_response->setRedirect($url);
                        $this->_response->sendResponse();
                    }
                }
            } else {
                if (in_array($controllerPath, $allModuleArr, true)) {
                    $this->_response->setRedirect($url);
                    $this->_response->sendResponse();
                }
            }
        }
    }
}
