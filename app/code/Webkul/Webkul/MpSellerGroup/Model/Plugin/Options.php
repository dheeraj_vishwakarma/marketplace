<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpSellerGroup
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpSellerGroup\Model\Plugin;

class Options
{
    /**
     * @param  Magento\Catalog\Model\Product\AttributeSet\Options $subject
     * @param  object                                             $result
     * @return array
     */
    public function afterToOptionArray(\Magento\Catalog\Model\Product\AttributeSet\Options $subject, $result)
    {
        $attributeset = [];
        foreach ($result as $set) {
            if ($set['label'] != 'Mp Seller Group') {
                array_push($attributeset, $set);
            }
        }
        return $attributeset;
    }
}
