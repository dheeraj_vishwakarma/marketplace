<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
namespace Webkul\MpEasyPost\Block;

/**
 * Webkul MpEasyPost Block Data
 */
class EasyPostData extends \Magento\Framework\View\Element\Template
{
    /**
     * @var \Webkul\MpEasyPost\Helper\Data
     */
    protected $currentHelper;

    /**
     * Initialize dependencies
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Webkul\MpEasyPost\Helper\Data $currentHelper
     * @param \Webkul\Marketplace\Helper\Data $marketplaceHelperData
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Webkul\MpEasyPost\Helper\Data $currentHelper,
        \Webkul\Marketplace\Helper\Data $marketplaceHelperData,
        array $data = []
    ) {
        $this->marketplaceHelperData = $marketplaceHelperData;
        $this->currentHelper = $currentHelper;
        parent::__construct($context, $data);
    }
    /**
     * Get current module helper.
     *
     * @return \Webkul\MpEasyPost\Helper\Data
     */
    public function getHelper()
    {
        return $this->currentHelper;
    }
    /**
     * Get current module helper.
     *
     * @return \Webkul\MpEasyPost\Helper\Data
     */
    public function getMarketplaceHelper()
    {
        return $this->marketplaceHelperData;
    }
}
