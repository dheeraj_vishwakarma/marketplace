<?php
/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_MpEasyPost
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

namespace Webkul\MpEasyPost\Observer;

use Magento\Framework\Event\Manager;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Session\SessionManager;

/**
 * Webkul MpEasyPost Create Shipment Observer
 */
class CreateShipmentObserver implements ObserverInterface
{
    /**
     * @var \Webkul\MpEasyPost\Model\Carrier
     */
    private $mpEasyPostCarrier;

    /**
     * @var \Webkul\MpEasyPost\Logger\Logger
     */
    private $logger;

    /**
     * Initialize dependencies
     *
     * @param \Webkul\MpEasyPost\Model\Carrier $mpEasyPostCarrier
     * @param \Webkul\MpEasyPost\Logger\Logger $logger
     */
    public function __construct(
        \Webkul\MpEasyPost\Model\Carrier $mpEasyPostCarrier,
        \Webkul\MpEasyPost\Logger\Logger $logger
    ) {
        $this->logger = $logger;
        $this->mpEasyPostCarrier = $mpEasyPostCarrier;
    }

    /**
     * When shipment generates from seller panel
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        try {
            $orderId = $observer->getOrderId();
            $this->mpEasyPostCarrier->_doShipmentRequest($orderId);
        } catch (\Exception $e) {
            $this->logger->info('MpEasyPost CreateShipmentObserver'.$e->getMessage());
        }
    }
}
