/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
require([
    "jquery"
    ], function($wk_jq){
    $wk_jq(document).ready(function() {

        var newHTML = $wk_jq('.xtremo-category-sidebar-main .navigation').nextAll();
        $wk_jq('.xtremo-category-sidebar-main .navigation').nextAll().remove();
        $wk_jq('.xtremo-category-sidebar-main .navigation').find('.nav-menu-left').append(newHTML);

        var navMenuLeftHeight = $wk_jq('body').find('.xtremo-category-sidebar-main').find('.nav-menu-left').outerHeight();
        var mainContainerHeight = navMenuLeftHeight;

        if(screen.width>767){
            if(navMenuLeftHeight <= 340){
                mainContainerHeight = 340;
            }
            $wk_jq('body').find('.xtremo-category-sidebar-main').height(navMenuLeftHeight);
            $wk_jq('body').find('.xtremo-category-sidebar-main').parent().height(mainContainerHeight);
        }
        if($wk_jq('body').find('.xtremo-category-sidebar-main')
            .find('.nav-menu-right')
            .find('ul').length){
            $wk_jq('body').find('.xtremo-category-sidebar-main').find('.nav-menu-right').find('ul').each(function(){
                if($wk_jq(this).outerHeight() > navMenuLeftHeight){
                    $wk_jq(this).css({"max-height":navMenuLeftHeight+'px',"overflow-y":"auto"});
                }

                if(navMenuLeftHeight < 350){
                    $wk_jq(this).find('.wk-cat-image').css({"max-height":navMenuLeftHeight-50+'px'});
                    $wk_jq(this).find('.wk-cat-image').find('.img-cat-0').css({"max-height":navMenuLeftHeight-50+'px'});
                }
            });
        }

        $wk_jq('.xtremo-category-sidebar-main .nav-menu-left li.level0').on("mouseover",function(){

            $wk_jq(this).siblings().each(function(){
                $wk_jq(this).children('a').css("color","");
                if($wk_jq(this).hasClass("active-level")){
                    $wk_jq(this).removeClass("active-level");
                }
            });
            $wk_jq(this).children('a').css("color","#fc8927");

            var levelClass = '';
            var allClasses = $wk_jq(this).attr('class').split(' ');
            for(var i=0; i < allClasses.length; i++){
                if (allClasses[i].indexOf("-") >= 0){
                    var newStr = allClasses[i].split('-');
                    if(newStr[0] == "nav"){
                        levelClass = allClasses[i];
                    }
                }
            }
            if($wk_jq(this).hasClass("level0")){
                levelClass += '.level0';
            }

            if(!$wk_jq(this).hasClass("active-level")){
                $wk_jq(this).addClass("active-level");
            }

            $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').find('ul').hide();
            $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').find('ul.'+levelClass).show();

            var navLeftWidth = $wk_jq(this).parents('.nav-menu-left').outerWidth();
            var navLeftHeight = $wk_jq(this).parents('.nav-menu-left').height();

            if($wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').find('ul.'+levelClass).length){
                if(screen.width > 800){
                    $wk_jq(this).parents('.xtremo-category-sidebar-main').width(800);
                }else{
                    var tempWidth = (screen.width - 130);
                    $wk_jq(this).parents('.xtremo-category-sidebar-main').width(tempWidth);
                }
                // $wk_jq(this).parents('.xtremo-category-sidebar-main').width(800);
                $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').height(navLeftHeight);
            }else{
                $wk_jq(this).parents('.xtremo-category-sidebar-main').width(navLeftWidth);
                $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').height('auto');
            }
        });

        $wk_jq('.xtremo-category-sidebar-main').on("mouseleave",function(){
            if($wk_jq(this).find('ul.ui-menu').find('li.active-level').length){
                $wk_jq(this).find('ul.ui-menu').find('li.active-level').children('a').css("color","");
                $wk_jq(this).find('ul.ui-menu').find('li.active-level').removeClass('active-level');
                $wk_jq(this).find('.nav-menu-right').find('ul').hide();

                $wk_jq(this).width($wk_jq(this).find('.nav-menu-left').outerWidth());
            }
        });
    });
});
