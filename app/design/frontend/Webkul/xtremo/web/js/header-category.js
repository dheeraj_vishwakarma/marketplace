/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
require([
    "jquery"
], function($wk_jq){

    $wk_jq(document).ready(function() {
        var dropdownHeight = $wk_jq('.xtremo.header .navigation ul.dropdown').height();

        $wk_jq('.xtremo.header .nav-menu-left li.level0').on("mouseenter",function(){
            $wk_jq(this).siblings().each(function(){
                $wk_jq(this).children('a').css("color","");
                if($wk_jq(this).hasClass("active-level")){
                    $wk_jq(this).removeClass("active-level");
                }
            });
            $wk_jq(this).children('a').css("color","#fc8927");

            var levelClass = '';
            var allClasses = $wk_jq(this).attr('class').split(' ');
            for(var i=0; i < allClasses.length; i++){
                if (allClasses[i].indexOf("-") >= 0){
                    var newStr = allClasses[i].split('-');
                    if(newStr[0] == "nav"){
                        levelClass = allClasses[i];
                    }
                }
            }
            if($wk_jq(this).hasClass("level0")){
                levelClass += '.level0';
            }

            if(!$wk_jq(this).hasClass("active-level")){
                $wk_jq(this).addClass("active-level");
            }

            $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').find('ul').hide();
            $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').find('ul.'+levelClass).show();

            var navLeftWidth = $wk_jq(this).parents('.nav-menu-left').outerWidth(true);

            if($wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').find('ul.'+levelClass).length){
                if(screen.width > 900){
                    $wk_jq(this).parents('ul.dropdown').width(900);
                    // $wk_jq(this).parents('ul.dropdown').width(navLeftWidth+$wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').find('ul.'+levelClass).outerWidth());
                }else{
                    var tempWidth = (screen.width - 50);
                    $wk_jq(this).parents('ul.dropdown').width(tempWidth);
                }
                $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').height(dropdownHeight);
            }else{
                if(screen.width < navLeftWidth){
                    navLeftWidth = navLeftWidth - 60;
                }
                console.log("new="+navLeftWidth);
                $wk_jq(this).parents('ul.dropdown').width(navLeftWidth);
                $wk_jq(this).parents('.nav-menu-left').siblings('.nav-menu-right').height('auto');
            }
        });
        $wk_jq('.xtremo.header .wk-cms-modal').on("click",function(){
            if(screen.width < 768){
                $wk_jq(this).siblings('.wk-xtremo-phone-cms').toggleClass('active');
                if(!$wk_jq('body').find('div.wk-loader').length){
                    $wk_jq('body').append($wk_jq('<div>').addClass('wk-loader'));
                }
            }
        });

        $wk_jq(document).on("click",function(e){
            if(screen.width < 768){
                if(!$wk_jq(e.target).hasClass('wk-cms-modal')
                    && !$wk_jq(e.target).hasClass('wk-xtremo-phone-cms')
                    && !$wk_jq(e.target).parents('.wk-xtremo-phone-cms').length
                ){
                    if($wk_jq('body').find('.wk-xtremo-phone-cms').hasClass('active')){
                        $wk_jq('body').find('.wk-xtremo-phone-cms').removeClass('active');

                        if($wk_jq('body').find('div.wk-loader').length){
                            $wk_jq('body').find('div.wk-loader').remove();
                        }
                    }
                }
            }
            // applyCheckbox();
        });

        if($wk_jq('.panel.header').find('.switcher-toplink').length){
            setTimeout(function(){
                var linksHtml = $wk_jq('.panel.header').find('.switcher-toplink').find('ul.header.links').clone();
                $wk_jq('.page-wrapper').children('.nav-sections').find('div[aria-controls="store.links"]').next('.section-item-content').html("");
                $wk_jq('.page-wrapper').children('.nav-sections').find('div[aria-controls="store.links"]').next('.section-item-content').append($wk_jq(linksHtml));
            }, 5000);
        }
        $wk_jq('body').find('input[type="radio"]').each(function(){

            if(!$wk_jq(this).next().is('label')){
                if(!$wk_jq(this).attr('id')){
                    $wk_jq(this).attr('id', $wk_jq(this).attr('name')+"-"+$wk_jq(this).val());
                }
                $wk_jq(this).after($wk_jq('<label>').attr('for',$wk_jq(this).attr('id')));
            }
        });
        // applyCheckbox();
    });

    function applyCheckbox()
    {
        $wk_jq('body').find('input[type="checkbox"]').each(function(){
            if(!$wk_jq(this).hasClass('xtremo-checkbox')){
                $wk_jq(this).addClass('xtremo-checkbox');
                if(!$wk_jq(this).next().is('label')){
                    $wk_jq(this).after($wk_jq('<label>').attr('for',$wk_jq(this).attr('id')));
                }else{
                    $wk_jq(this).next().attr('for',$wk_jq(this).attr('id'));
                }
            }
        });
    }
});
