/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_Xtremo
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */
var config = {
    map: {
        '*': {
            customlayer : 'Magento_LayeredNavigation/js/layerfilter'
        }
    }
};
