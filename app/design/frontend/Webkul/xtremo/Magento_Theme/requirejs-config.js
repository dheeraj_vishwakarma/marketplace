/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */

var config = {
    paths: {
            'owlcarousel': "Magento_Theme/js/owl.carousel"
    },
    shim: {
        'owlcarousel': {
            deps: ['jquery']
        }
    }
};
