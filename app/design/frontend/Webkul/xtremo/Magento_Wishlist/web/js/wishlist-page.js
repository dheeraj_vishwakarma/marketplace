/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
require([
    "jquery"
], function($wk_jq){
    if(screen.width < 640){
        var actionsHtml = $wk_jq('body').find('.form-wishlist-items').find('.wk-mp-page-title').find('.action.tocart').clone();
        $wk_jq('body').find('.form-wishlist-items').find('.wk-mp-page-title').find('.action.tocart').remove();
        $wk_jq('body').find('.form-wishlist-items').append(
            $wk_jq('<div>').addClass('add-all-to-cart-wrapper')
                .append($wk_jq(actionsHtml).addClass('action-cart-clone'))
        );

        $wk_jq('body').find('.form-wishlist-items').find('.box-tocart').each(function(){
            var actionsEditHtml = $wk_jq(this).find('.action.edit').clone();
            $wk_jq(this).find('.action.edit').remove();
            $wk_jq(this).find('.action.tocart.primary').after($wk_jq(actionsEditHtml).addClass('action-edit-clone'));
        });

        $wk_jq('body').find('.form-wishlist-items').find('.actions-toolbar').find('.primary').find('.update.action').children().text("update");
        $wk_jq('body').find('.form-wishlist-items').find('.actions-toolbar').find('.primary').find('.share.action').children().text("share");
    }
});
