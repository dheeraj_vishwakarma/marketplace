/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Mpxtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
require([
    "jquery"
], function($wk_jq){
    applyNewCheckbox();
    $wk_jq(document).ready(function() {
        $wk_jq(document).on("click",function(e){
            applyNewCheckbox();
        });
        applyNewCheckbox();
    });

    function applyNewCheckbox() {
        var counter = 0;
        $wk_jq('body').find('.wk-mp-design input[type="checkbox"]').each(function(){
            var checkboxName = "";
            if(!$wk_jq(this).attr('id')){
                if(!$wk_jq(this).attr('name')){
                    if($wk_jq(this).attr('class')){
                        checkboxName = $wk_jq(this).attr('class') + '-' + counter;
                        counter++;
                    }
                }else{
                    if($wk_jq(this).val()){
                        checkboxName = $wk_jq(this).attr('name')+"-"+$wk_jq(this).val();
                    }else{
                        checkboxName = $wk_jq(this).attr('name') + '-' + counter;
                    }
                }
                $wk_jq(this).attr('id', checkboxName);
            }else{
                checkboxName = $wk_jq(this).attr('id');
            }
            if(!$wk_jq(this).next().is('label')){
                $wk_jq(this).after($wk_jq('<label>').attr('for',checkboxName));
            }else{
                $wk_jq(this).next().attr('for',checkboxName);
            }
        });
        $wk_jq('body').find('.wk-mp-design input[type="radio"]').each(function(){

            if(!$wk_jq(this).next().is('label')){
                if(!$wk_jq(this).attr('id')){
                    $wk_jq(this).attr('id', $wk_jq(this).attr('name'));
                }
                $wk_jq(this).after($wk_jq('<label>').attr('for',$wk_jq(this).attr('id')));
            }
        });
    }
});
