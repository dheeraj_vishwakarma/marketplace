/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
require([
    "jquery"
], function($){
    if ($(window).width() < 768) {
        setTimeout(function(){
            var actionsHtml = $('body').find('.box-tocart').find('.actions').clone();
            $('body').find('.column.main').append($('<div>').addClass('actions action-clone fix').append($(actionsHtml)));
            $('body').find('.actions.action-clone.fix').show();
        }, 1000);
    }
    
    if ($(window).width() > 768) {
        $('body').find('.actions.action-clone.fix').hide();
    }

    $('body ').on("mouseout", ".magnify-lens", function(){
        $(this).addClass("magnify-hidden");
        $("body .magnifier-preview").addClass("magnify-hidden");
    });

    var iScrollPos = 0;
    $(document).ready(function() {

        $('body').on("click",".actions.action-clone .action.primary",function(e){
            e.preventDefault();
            if($(this).hasClass('tocart')){
                $('body').find('.box-tocart').find('.actions').find('.action.primary.tocart').trigger('click');
            }
            if($(this).hasClass('buynow-button')){
                $('body').find('.box-tocart').find('.actions').find('.action.primary.buynow-button').trigger('click');
            }

            $('html, body').animate({
               scrollTop: $("body .product-add-form").offset().top
            }, 2000);

        });

    });

    $(window).on("scroll",function(e){
        if(screen.width < 768 ){
            if($('body').find('.column.main').find('.actions.action-clone').length){
               var hT = $('body .product.info.detailed').offset().top,
                   hH = $('body .product.info.detailed').outerHeight(),
                   wH = $(window).height(),
                   wS = $(this).scrollTop(),
                   actionH = $('body').find('.column.main').find('.actions.action-clone').outerHeight();

                if (wS > iScrollPos && wS > (hT+hH+actionH-wH)) {
                    if($('body').find('.column.main').find('.actions.action-clone.fix').length){
                        $('body').find('.column.main').find('.actions.action-clone.fix').removeClass('fix');
                        $('body').find('.column.main').find('.actions.action-clone').addClass('static');
                    }
                }else{
                    var nT = $('body').find('.column.main').find('.actions.action-clone').offset().top,
                        nH = $('body').find('.column.main').find('.actions.action-clone').outerHeight();

                    if(wS <= Math.floor(nT+nH-wH) && $('body').find('.column.main').find('.actions.action-clone.static').length){
                        $('body').find('.column.main').find('.actions.action-clone.static').removeClass('static');
                        $('body').find('.column.main').find('.actions.action-clone').addClass('fix');
                    }
                }
                iScrollPos = wS;
            }
        }
    });
});
