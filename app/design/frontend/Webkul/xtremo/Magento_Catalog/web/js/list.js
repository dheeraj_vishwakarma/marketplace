/**
 * Webkul Software.
 *
 * @category  Webkul
 * @package   Webkul_Xtremo
 * @author    Webkul
 * @copyright Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license   https://store.webkul.com/license.html
 */
/*jshint browser:true jquery:true*/
define([
    "jquery",
    "jquery/ui"
], function($){
    "use strict";

    $.widget('mage.compareList', {
        _create: function() {

            var elem = this.element,
                products = $('thead td', elem);

            if (products.length > this.options.productsInRow) {
                var headings = $('<table/>')
                    .addClass('comparison headings data table')
                    .insertBefore(elem.closest('.container'));

                elem.addClass('scroll');

                $('th', elem).each(function(){
                    var th = $(this),
                        thCopy = th.clone();

                    th.animate({
                        top: '+=0'
                    }, 50, function(){
                        var height = th.height();

                        thCopy.css('height', height)
                            .appendTo(headings)
                            .wrap('<tr />');
                    });
                });
            }

            $(this.options.windowPrintSelector).on('click', function(e) {
                e.preventDefault();

                var divToPrint = $('.column.main');
                var divToPrintLogo = $('.logo');

                var newWin = window.open('','Print-Window');
                newWin.document.open();
                newWin.document.write(
                    '<html><body onload="window.print()">'+$(divToPrintLogo).html()+$(divToPrint).html()+'</body></html>'
                );
                newWin.document.close();
                setTimeout(function(){newWin.close();},10);

                // window.print();
            });
        }
    });

    return $.mage.compareList;
});
