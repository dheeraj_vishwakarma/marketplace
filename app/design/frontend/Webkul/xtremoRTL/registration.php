<?php
/**
 * Webkul Software.
 *
 * @category   Webkul
 * @package    Webkul_Xtremo
 * @author     Webkul Software Private Limited
 * @copyright  Copyright (c) Webkul Software Private Limited (https://webkul.com)
 * @license    https://store.webkul.com/license.html
 */

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::THEME,
    'frontend/Webkul/xtremoRTL',
    __DIR__
);
